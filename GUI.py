import tkinter as tk
from tkinter import ttk, messagebox
import os
import subprocess
from datetime import datetime
import json
import signal

# Initialize a global variable to store the current job directory
current_job_dir = None
config_file = os.path.expanduser("~/.apd_gui_config.json")  # Configuration file path

# Flag to track if the scrollbar is being used
scrollbar_being_used = False

def load_config():
    """Load configuration from the config file."""
    if os.path.exists(config_file):
        try:
            with open(config_file, 'r') as f:
                return json.load(f)
        except Exception as e:
            messagebox.showerror("Error", f"Failed to load config file: {e}")
    return {}

def save_all_settings():
    """Save all settings (basic and advanced) to the config file."""
    config = {
        "basic": {
            "mp_id": mp_id_entry.get().strip(),
            "mapi_key": mapi_key_entry.get().strip(),
            "temp_range": temp_entry.get().strip(),
            "remote_addr": remote_addr_entry.get().strip(),
            "defects": defects_entry.get().strip(),
            "skip_0K_props": basic_skip_0k_props.get(),
            "skip_phonons": basic_skip_phonons.get(),
            "phonon_mode": basic_phonon_mode.get()
        },
        "advanced": {
            "dft_energy_tol": adv_dft_energy_tol.get().strip(),
            "xc_func": adv_xc_func.get().strip(),
            "cell_for_phonons": adv_cell_for_phonons.get().strip(),
            "num_atoms_DFT_MD": adv_num_atoms_dft_md.get().strip(),
            "num_defect_configs": adv_num_defect_configs.get().strip(),
            "num_pots": adv_num_pots.get().strip(),
            "phonon_fine_kpoint_path": adv_phonon_fine_kpoint_path.get().strip(),
            "pot_types": adv_pot_types.get().strip(),
            "species_pot": adv_species_pot.get().strip()
        }
    }
    try:
        with open(config_file, 'w') as f:
            json.dump(config, f, indent=4)
    except Exception as e:
        messagebox.showerror("Error", f"Failed to save config file: {e}")

def load_all_settings():
    """Load all settings from the config file and populate the GUI fields."""
    config = load_config()
    basic = config.get("basic", {})
    advanced = config.get("advanced", {})

    # Populate Basic Settings
    mp_id_entry.delete(0, tk.END)
    mp_id_entry.insert(0, basic.get("mp_id", ""))

    mapi_key_entry.delete(0, tk.END)
    mapi_key_entry.insert(0, basic.get("mapi_key", ""))

    temp_entry.delete(0, tk.END)
    temp_entry.insert(0, basic.get("temp_range", ""))

    remote_addr_entry.delete(0, tk.END)
    remote_addr_entry.insert(0, basic.get("remote_addr", ""))

    defects_entry.delete(0, tk.END)
    defects_entry.insert(0, basic.get("defects", ""))

    basic_skip_0k_props.set(basic.get("skip_0K_props", "False"))
    basic_skip_phonons.set(basic.get("skip_phonons", "False"))
    basic_phonon_mode.set(basic.get("phonon_mode", "AUTO"))

    # Populate Advanced Settings
    adv_dft_energy_tol.delete(0, tk.END)
    adv_dft_energy_tol.insert(0, advanced.get("dft_energy_tol", ""))

    adv_xc_func.delete(0, tk.END)
    adv_xc_func.insert(0, advanced.get("xc_func", "pbe"))

    adv_cell_for_phonons.delete(0, tk.END)
    adv_cell_for_phonons.insert(0, advanced.get("cell_for_phonons", ""))

    adv_num_atoms_dft_md.delete(0, tk.END)
    adv_num_atoms_dft_md.insert(0, advanced.get("num_atoms_DFT_MD", ""))

    adv_num_defect_configs.delete(0, tk.END)
    adv_num_defect_configs.insert(0, advanced.get("num_defect_configs", ""))

    adv_num_pots.delete(0, tk.END)
    adv_num_pots.insert(0, advanced.get("num_pots", ""))

    adv_phonon_fine_kpoint_path.delete(0, tk.END)
    adv_phonon_fine_kpoint_path.insert(0, advanced.get("phonon_fine_kpoint_path", ""))

    adv_pot_types.delete(0, tk.END)
    adv_pot_types.insert(0, advanced.get("pot_types", ""))

    adv_species_pot.delete(0, tk.END)
    adv_species_pot.insert(0, advanced.get("species_pot", ""))

def generate_job_directory(mp_id, defects, temp_range, base_dir="/home/d1111019/jobs/"):
    """Generate a job directory name without a timestamp."""
    defects_sanitized = defects.replace(" ", "_")
    job_dir_name = f"{mp_id}_{defects_sanitized}_temp{temp_range}"
    job_dir = os.path.join(base_dir, job_dir_name)
    os.makedirs(job_dir, exist_ok=True)
    return job_dir, job_dir_name

def write_settings_file(job_dir, basic_settings, advanced_settings):
    """Write settings to the settings.APD file in the job directory."""
    settings_file_path = os.path.join(job_dir, "settings.APD")
    try:
        with open(settings_file_path, 'w') as f:
            f.write("[APD]\n")
            for key, value in basic_settings.items():
                if value:
                    f.write(f"{key} = {value}\n")
            for key, value in advanced_settings.items():
                if value:
                    f.write(f"{key} = {value}\n")
        return settings_file_path
    except Exception as e:
        messagebox.showerror("Error", f"Error writing settings file: {e}")
        return None

def is_apd_running(job_dir):
    """Check if the APDbackground process is running based on the PID file."""
    pid_path = os.path.join(job_dir, "run.pid")
    if not os.path.exists(pid_path):
        return False
    try:
        with open(pid_path, 'r') as f:
            pid_str = f.read().strip()
            if not pid_str:
                return False
            pid = int(pid_str)
    except Exception:
        return False
    try:
        os.kill(pid, 0)  # Check if process is running
    except ProcessLookupError:
        return False
    except Exception:
        return False
    return True

def start_apd(job_dir):
    """Start the APDbackground process and save its PID."""
    global current_job_dir  # Declare the global variable
    try:
        current_job_dir = job_dir  # Set the current job directory
        nohup_path = os.path.join(current_job_dir, "nohup.out")
        pid_path = os.path.join(current_job_dir, "run.pid")

        # Run startAPD using subprocess and save the PID
        command = "~/codes/automated-potential-development/APDbackground"
        with open(nohup_path, "a") as nohup_file:
            process = subprocess.Popen(
                command,
                cwd=job_dir,
                shell=True,
                stdout=nohup_file,
                stderr=subprocess.STDOUT,
                preexec_fn=os.setsid  # To ensure the process can be killed properly
            )
            # Write the PID to run.pid
            with open(pid_path, 'w') as pid_file:
                pid_file.write(str(process.pid))

        result_label.config(text=f"APD started in {job_dir} with PID {process.pid}")
        update_output_display()  # Start updating output from nohup.out
    except Exception as e:
        messagebox.showerror("Error", f"Failed to start APD: {e}")

def stop_apd():
    """Stop the APDbackground process using the PID from run.pid."""
    try:
        if current_job_dir is None:
            messagebox.showwarning("Warning", "No APD job is currently running.")
            return

        pid_path = os.path.join(current_job_dir, "run.pid")
        if not os.path.exists(pid_path):
            messagebox.showwarning("Warning", "run.pid file not found. Cannot stop APD.")
            return

        with open(pid_path, 'r') as pid_file:
            pid_str = pid_file.read().strip()
            if not pid_str:
                messagebox.showwarning("Warning", "run.pid is empty. Cannot stop APD.")
                return
            pid = int(pid_str)

        # Terminate the process group
        os.killpg(os.getpgid(pid), signal.SIGTERM)

        result_label.config(text=f"APD process {pid} stopped")

        # Remove run.pid after stopping
        os.remove(pid_path)

        # Reset the Start APD button
        create_button.config(text="Create Job and Start APD", bg="lightgray", state="normal")
    except ProcessLookupError:
        messagebox.showwarning("Warning", "APD process not found. It might have already stopped.")
    except Exception as e:
        messagebox.showerror("Error", f"Failed to stop APD: {e}")

def on_scrollbar_press(event):
    """Handle scrollbar press event."""
    global scrollbar_being_used
    scrollbar_being_used = True

def on_scrollbar_release(event):
    """Handle scrollbar release event."""
    global scrollbar_being_used
    scrollbar_being_used = False

def update_output_display():
    """Continuously update the output display from nohup.out."""
    try:
        if current_job_dir is None:
            output_text.config(state=tk.NORMAL)
            output_text.delete(1.0, tk.END)
            output_text.insert(tk.END, "No job directory set.")
            output_text.config(state=tk.DISABLED)
        else:
            nohup_path = os.path.join(current_job_dir, "nohup.out")
            if not os.path.exists(nohup_path):
                output_text.config(state=tk.NORMAL)
                output_text.delete(1.0, tk.END)
                output_text.insert(tk.END, "nohup.out not found.")
                output_text.config(state=tk.DISABLED)
            else:
                with open(nohup_path, "r") as f:
                    output_content = f.read()

                if not scrollbar_being_used:
                    output_text.config(state=tk.NORMAL)
                    output_text.delete(1.0, tk.END)
                    output_text.insert(tk.END, output_content)
                    output_text.see(tk.END)  # Scroll to the end
                    output_text.config(state=tk.DISABLED)
    except Exception as e:
        messagebox.showerror("Error", f"Error reading nohup.out: {e}")
    finally:
        # Schedule the function to run again after 2000 milliseconds (2 seconds)
        root.after(2000, update_output_display)

def check_existing_job():
    """Check if an existing job is running based on loaded settings."""
    global current_job_dir
    config = load_config()
    basic = config.get("basic", {})
    mp_id = basic.get("mp_id", "").strip()
    defects = basic.get("defects", "").strip()
    temp_range = basic.get("temp_range", "").strip()

    if not mp_id or not temp_range:
        print('Cannot determine job_dir without key settings')
        # Cannot determine job_dir without key settings
        return

    if not defects:
        pass

    # Generate job_dir_name without timestamp
    defects_sanitized = defects.replace(" ", "_")
    job_dir_name = f"{mp_id}_{defects_sanitized}_temp{temp_range}"
    job_dir = os.path.join("/home/d1111019/jobs/", job_dir_name)

    if os.path.exists(job_dir):
        if is_apd_running(job_dir):
            # APD is running, update button and set current_job_dir
            create_button.config(text="APD Running", bg="green", state="disabled")
            current_job_dir = job_dir
        else:
            # APD is not running, start it and update button
            start_apd(job_dir)
            create_button.config(text="APD Running", bg="green", state="disabled")
    else:
        # Job directory does not exist, ensure button is enabled
        create_button.config(text="Create Job and Start APD", bg="lightgray", state="normal")

def create_job():
    """Create a new job directory or use an existing one, then start APD."""
    mp_id = mp_id_entry.get().strip()
    mapi_key = mapi_key_entry.get().strip()
    defects = defects_entry.get().strip()
    temp_range = temp_entry.get().strip()
    remote_addr = remote_addr_entry.get().strip()

    # Check if required fields are filled
    if not mp_id or not mapi_key or not temp_range or not remote_addr:
        messagebox.showwarning("Input Error", "Please fill in all required fields: MP ID, MAPI_KEY, Temperature Range, and Remote Address.")
        return

    # Parse the remote address (ensure it is in the correct format)
    try:
        remote_user, remote_rest = remote_addr.split('@')
        remote_host, remote_dir = remote_rest.split(':', 1)
    except ValueError:
        messagebox.showwarning("Input Error", "Please enter a valid remote address in the format 'username@cluster:/path/to/jobdir'.")
        return

    try:
        # Generate job_dir_name without timestamp
        defects_sanitized = defects.replace(" ", "_")
        job_dir_name = f"{mp_id}_{defects_sanitized}_temp{temp_range}"
        job_dir = os.path.join("/home/d1111019/jobs/", job_dir_name)

        if not os.path.exists(job_dir):
            os.makedirs(job_dir, exist_ok=True)
            result_label.config(text=f"Job directory created: {job_dir}")
        else:
            result_label.config(text=f"Using existing job directory: {job_dir}")

        # Save all settings to config file
        save_all_settings()

        # Write settings.APD
        advanced_settings = {
            "dft_energy_tol": adv_dft_energy_tol.get(),
            "xc_func": adv_xc_func.get(),
            "cell_for_phonons": adv_cell_for_phonons.get(),
            "num_atoms_DFT_MD": adv_num_atoms_dft_md.get(),
            "num_defect_configs": adv_num_defect_configs.get(),
            "num_pots": adv_num_pots.get(),
            "phonon_fine_kpoint_path": adv_phonon_fine_kpoint_path.get(),
            "pot_types": adv_pot_types.get(),
            "species_pot": adv_species_pot.get()
        }

        basic_settings = {
            "mp_id": mp_id,
            "mapi_key": mapi_key,
            "temp_range": temp_range,
            "defects": defects,
            "skip_0K_props": basic_skip_0k_props.get(),
            "skip_phonons": basic_skip_phonons.get(),
            "phonon_mode": basic_phonon_mode.get()
        }

        settings_file_path = write_settings_file(job_dir, basic_settings, advanced_settings)
        if settings_file_path:
            result_label.config(text=f"Settings file created: {settings_file_path}")

        # Write the remoteComputerSettings file
        remote_file_path = os.path.join(job_dir, "remoteComputerSettings")
        with open(remote_file_path, 'w') as f:
            f.write(f"REMOTE_USER = {remote_user}\n")
            f.write(f"REMOTE_ADDRS = {remote_host}\n")
            f.write(f"REMOTE_DIR = {remote_dir}/{job_dir_name}\n")

        # Start APD
        start_apd(job_dir)

        # Update 'Create Job and Start APD' button to indicate APD is running
        create_button.config(text="APD Running", bg="green", state="disabled")

    except Exception as e:
        messagebox.showerror("Error", f"An error occurred: {e}")

def refresh_settings():
    """Reset all settings fields to their default values."""
    # Reset Basic Settings
    mp_id_entry.delete(0, tk.END)
    mp_id_entry.insert(0, "")

    mapi_key_entry.delete(0, tk.END)
    mapi_key_entry.insert(0, "")

    temp_entry.delete(0, tk.END)
    temp_entry.insert(0, "")

    remote_addr_entry.delete(0, tk.END)
    remote_addr_entry.insert(0, "")

    defects_entry.delete(0, tk.END)
    defects_entry.insert(0, "")

    basic_skip_0k_props.set("False")
    basic_skip_phonons.set("False")
    basic_phonon_mode.set("AUTO")

    # Reset Advanced Settings
    adv_dft_energy_tol.delete(0, tk.END)
    adv_dft_energy_tol.insert(0, "")

    adv_xc_func.delete(0, tk.END)
    adv_xc_func.insert(0, "pbe")

    adv_cell_for_phonons.delete(0, tk.END)
    adv_cell_for_phonons.insert(0, "")

    adv_num_atoms_dft_md.delete(0, tk.END)
    adv_num_atoms_dft_md.insert(0, "")

    adv_num_defect_configs.delete(0, tk.END)
    adv_num_defect_configs.insert(0, "")

    adv_num_pots.delete(0, tk.END)
    adv_num_pots.insert(0, "")

    adv_phonon_fine_kpoint_path.delete(0, tk.END)
    adv_phonon_fine_kpoint_path.insert(0, "")

    adv_pot_types.delete(0, tk.END)
    adv_pot_types.insert(0, "")

    adv_species_pot.delete(0, tk.END)
    adv_species_pot.insert(0, "")

    # Save the reset settings to config file
    save_all_settings()

    # Update the 'Create Job and Start APD' button to be enabled
    create_button.config(text="Create Job and Start APD", bg="lightgray", state="normal")

# Modified add_scrollable_frame function to center content in the canvas

def add_scrollable_frame(notebook, title):
    frame = ttk.Frame(notebook)
    canvas = tk.Canvas(frame)
    scrollbar = ttk.Scrollbar(frame, orient="vertical", command=canvas.yview)
    scrollable_frame = ttk.Frame(canvas)

    scrollable_frame.bind(
        "<Configure>",
        lambda e: canvas.configure(
            scrollregion=canvas.bbox("all")
        )
    )

    # Create the window within the canvas, using "center" anchor to center the frame
    canvas.create_window((0, 0), window=scrollable_frame, anchor="n", width=780)  # Adjust width if needed

    # Configure the canvas scrollbar and pack elements
    canvas.configure(yscrollcommand=scrollbar.set)
    canvas.pack(side="left", fill="both", expand=True)
    scrollbar.pack(side="right", fill="y")

    notebook.add(frame, text=title)
    return scrollable_frame


# Create the main window
root = tk.Tk()
root.title("APD Workflow")
root.geometry("800x700")  # Increased size for better visibility

# Create a tabbed notebook
notebook = ttk.Notebook(root)
notebook.pack(expand=1, fill="both")

# Create Basic Options tab with a scrollable frame
basic_frame = add_scrollable_frame(notebook, "Basic Options")

# Create a frame inside basic_frame to use grid
input_frame = ttk.Frame(basic_frame)
input_frame.pack(expand=True, anchor="center", pady=20)  # Added pady for vertical centering

# Configure the grid to prevent stretching
input_frame.columnconfigure(0, weight=0)
input_frame.columnconfigure(1, weight=0)

# Define padding
label_padding = {'padx': 10, 'pady': 5}
entry_padding = {'padx': 10, 'pady': 5}

# MP ID
tk.Label(input_frame, text="MP ID *:", font=("Arial", 12)).grid(row=0, column=0, sticky="e", **label_padding)
mp_id_entry = tk.Entry(input_frame, width=30)
mp_id_entry.grid(row=0, column=1, **entry_padding)

# MAPI_KEY
tk.Label(input_frame, text="MAPI_KEY *:", font=("Arial", 12)).grid(row=1, column=0, sticky="e", **label_padding)
mapi_key_entry = tk.Entry(input_frame, width=30)
mapi_key_entry.insert(0, "")  # Initially empty; will be populated by load_all_settings()
mapi_key_entry.grid(row=1, column=1, **entry_padding)  # Use grid instead of pack

# Temperature Range
tk.Label(input_frame, text="Temperature Range (e.g., 1000 or 1000,1200) *:", font=("Arial", 12)).grid(row=2, column=0, sticky="e", **label_padding)
temp_entry = tk.Entry(input_frame, width=30)
temp_entry.grid(row=2, column=1, **entry_padding)

# Remote Address
tk.Label(input_frame, text="Remote Address (username@cluster:/path/to/jobdir):", font=("Arial", 12)).grid(row=3, column=0, sticky="e", **label_padding)
remote_addr_entry = tk.Entry(input_frame, width=30)
remote_addr_entry.insert(0, "")  # Initially empty; will be populated by load_all_settings()
remote_addr_entry.grid(row=3, column=1, **entry_padding)

# Defects
tk.Label(input_frame, text="Defects:", font=("Arial", 12)).grid(row=4, column=0, sticky="e", **label_padding)
defects_entry = tk.Entry(input_frame, width=30)
defects_entry.grid(row=4, column=1, **entry_padding)

# Skip 0K Properties
tk.Label(input_frame, text="Skip 0K Properties:", font=("Arial", 12)).grid(row=5, column=0, sticky="e", **label_padding)
basic_skip_0k_props = tk.StringVar(value="False")
ttk.Combobox(input_frame, textvariable=basic_skip_0k_props, values=["True", "False"], state="readonly", width=28).grid(row=5, column=1, **entry_padding)

# Skip Phonons
tk.Label(input_frame, text="Skip Phonons:", font=("Arial", 12)).grid(row=6, column=0, sticky="e", **label_padding)
basic_skip_phonons = tk.StringVar(value="False")
ttk.Combobox(input_frame, textvariable=basic_skip_phonons, values=["True", "False"], state="readonly", width=28).grid(row=6, column=1, **entry_padding)

# Phonon Mode
tk.Label(input_frame, text="Phonon Mode:", font=("Arial", 12)).grid(row=7, column=0, sticky="e", **label_padding)
basic_phonon_mode = tk.StringVar(value="AUTO")
ttk.Combobox(input_frame, textvariable=basic_phonon_mode, values=["finite_disp", "DFPT", "AUTO"], state="readonly", width=28).grid(row=7, column=1, **entry_padding)

# Create Advanced Options tab with a scrollable frame
advanced_frame = add_scrollable_frame(notebook, "Advanced Options")

# Create a frame inside advanced_frame to use grid
advanced_input_frame = ttk.Frame(advanced_frame)
advanced_input_frame.pack(expand=True, anchor="center", pady=20)  # Added pady for vertical centering

# Configure the grid to prevent stretching
advanced_input_frame.columnconfigure(0, weight=0)
advanced_input_frame.columnconfigure(1, weight=0)

# Define padding
adv_label_padding = {'padx': 10, 'pady': 5}
adv_entry_padding = {'padx': 10, 'pady': 5}

# DFT Energy Tolerance
tk.Label(advanced_input_frame, text="DFT Energy Tolerance (meV/atom):", font=("Arial", 12)).grid(row=0, column=0, sticky="e", **adv_label_padding)
adv_dft_energy_tol = tk.Entry(advanced_input_frame, width=30)
adv_dft_energy_tol.grid(row=0, column=1, **adv_entry_padding)

# XC Functional
tk.Label(advanced_input_frame, text="XC Functional (default 'pbe'):", font=("Arial", 12)).grid(row=1, column=0, sticky="e", **adv_label_padding)
adv_xc_func = tk.Entry(advanced_input_frame, width=30)
adv_xc_func.insert(0, "pbe")  # Default value
adv_xc_func.grid(row=1, column=1, **adv_entry_padding)

# Cell for Phonons
tk.Label(advanced_input_frame, text="Cell for Phonons (e.g., 2 2 2):", font=("Arial", 12)).grid(row=2, column=0, sticky="e", **adv_label_padding)
adv_cell_for_phonons = tk.Entry(advanced_input_frame, width=30)
adv_cell_for_phonons.grid(row=2, column=1, **adv_entry_padding)

# Number of Atoms for DFT MD
tk.Label(advanced_input_frame, text="Number of Atoms for DFT MD:", font=("Arial", 12)).grid(row=3, column=0, sticky="e", **adv_label_padding)
adv_num_atoms_dft_md = tk.Entry(advanced_input_frame, width=30)
adv_num_atoms_dft_md.grid(row=3, column=1, **adv_entry_padding)

# Number of Defect Configurations
tk.Label(advanced_input_frame, text="Number of Defect Configurations:", font=("Arial", 12)).grid(row=4, column=0, sticky="e", **adv_label_padding)
adv_num_defect_configs = tk.Entry(advanced_input_frame, width=30)
adv_num_defect_configs.grid(row=4, column=1, **adv_entry_padding)

# Number of Potentials
tk.Label(advanced_input_frame, text="Number of Potentials:", font=("Arial", 12)).grid(row=5, column=0, sticky="e", **adv_label_padding)
adv_num_pots = tk.Entry(advanced_input_frame, width=30)
adv_num_pots.grid(row=5, column=1, **adv_entry_padding)

# Phonon Fine K-point Path
tk.Label(advanced_input_frame, text="Phonon Fine K-point Path:", font=("Arial", 12)).grid(row=6, column=0, sticky="e", **adv_label_padding)
adv_phonon_fine_kpoint_path = tk.Entry(advanced_input_frame, width=30)
adv_phonon_fine_kpoint_path.grid(row=6, column=1, **adv_entry_padding)

# Potential Types
tk.Label(advanced_input_frame, text="Potential Types:", font=("Arial", 12)).grid(row=7, column=0, sticky="e", **adv_label_padding)
adv_pot_types = tk.Entry(advanced_input_frame, width=30)
adv_pot_types.grid(row=7, column=1, **adv_entry_padding)

# Species Potential
tk.Label(advanced_input_frame, text="Species Potential:", font=("Arial", 12)).grid(row=8, column=0, sticky="e", **adv_label_padding)
adv_species_pot = tk.Entry(advanced_input_frame, width=30)
adv_species_pot.grid(row=8, column=1, **adv_entry_padding)

# **Load all settings AFTER creating the widgets**
load_all_settings()

# Create Output tab with its own scrollbar
output_frame = ttk.Frame(notebook)
notebook.add(output_frame, text="Output")

# Create a vertical scrollbar for the Text widget
output_scrollbar = ttk.Scrollbar(output_frame, orient='vertical')
output_scrollbar.pack(side='right', fill='y')

# Create the Text widget and link it to the scrollbar
output_text = tk.Text(output_frame, wrap=tk.WORD, state=tk.DISABLED, yscrollcommand=output_scrollbar.set)
output_text.pack(side='left', fill='both', expand=True, padx=10, pady=10)

# Configure the scrollbar to control the Text widget
output_scrollbar.config(command=output_text.yview)

# Bind the mouse events to the scrollbar to track interaction
output_scrollbar.bind("<ButtonPress-1>", on_scrollbar_press)
output_scrollbar.bind("<ButtonRelease-1>", on_scrollbar_release)

# Create a frame for buttons to center them
button_frame = ttk.Frame(root)
button_frame.pack(pady=10)

# Create button to generate the job directory
create_button = tk.Button(button_frame, text="Create Job and Start APD", command=create_job, width=25)
create_button.pack(pady=5)

# Create Stop APD button
stop_button = tk.Button(button_frame, text="Stop APD", command=stop_apd, width=25)
stop_button.pack(pady=5)

# Create Refresh Settings button
refresh_button = tk.Button(button_frame, text="Refresh Settings", command=refresh_settings, width=25)
refresh_button.pack(pady=5)

# Create an Exit button
exit_button = tk.Button(button_frame, text="Exit", command=root.destroy, width=25)
exit_button.pack(pady=5)

# Label to display status
result_label = tk.Label(root, text="Ready", font=("Arial", 10))
result_label.pack(pady=10)

# After setting up the GUI, check for existing jobs
check_existing_job()

# Start the Tkinter main loop
root.mainloop()

