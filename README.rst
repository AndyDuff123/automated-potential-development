.. image:: https://img.shields.io/badge/License-BSD%203--Clause-blue.svg
      :target: https://opensource.org/licenses/BSD-3-Clause

.. image:: https://img.shields.io/badge/maintenance%20status-actively%20developed-brightgreen
      :target: https://gitlab.com/AndyDuff123/automated-potential-development

Automated potential development (APD) workflow
==============================================

A Python-based workflow for guiding and automating the entire pipeline of developing interatomic 
potentials— from generating density functional theory (DFT) data, performing molecular dynamics 
(MD) simulations, to active learning and property calculations.

Status
------
The workflow is currently in alpha. While it is being continuously improved, we encourage users to try it and report any issues.

Usage
-----

For a detailed guide on usage, including installation, refer to the `User guide <https://andyduff123.gitlab.io/automated-potential-development>`_.

Contributing
------------

To report bugs, suggest enhancements, or contribute to the code, please open an issue. 
Contributions are welcome; please adhere to code standards and run 'run_unit_tests.sh' before pushing changes 
to ensure functionality. For more details, consult the 
`User guide <https://andyduff123.gitlab.io/automated-potential-development>`_.

Citation
--------

If you use APD for publication, please cite:

Duff et al, Comp. Phys. Comm. 293, pg 108896 (2023)
