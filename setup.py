import setuptools

with open("README.rst", "r") as fh:
    long_description = fh.read()
with open("environment.yml", "r") as fh:
    environment = [line.strip() for line in fh]

setuptools.setup(
    name="automated-potential-development",
    version="0.0.1",
    author="Andrew Duff",
    author_email="andrew.duff@stfc.ac.uk",
    description="A workflow to automate development of bottom-up material science interatomic potentials",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD 3-Clause License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=environment,
)
