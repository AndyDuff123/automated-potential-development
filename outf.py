# Automated Potential Development (APD) software
#
# Copyright (C) 2020-2023, Dr Andrew Ian Duff,
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import os

import var


def printt(verbLev, string):
    if var.verbosity >= verbLev:
        print(string)


def toAction(string):
    if var.verbosity >= 1:
        print("  *** " + string + " ***")
    var.toAction.write("- " + string + "\n")


def title(string):
    if var.verbosity >= 1:
        top = "/" + "-" * (len(string) + 2) + "\\"
        bottom = "\\" + "-" * (len(string) + 2) + "/"
        print(top)
        print("| " + string + " |")
        print(bottom)


def underline(string):
    if var.verbosity >= 1:
        underline = "-" * len(string)
        print("\n" + string)
        print(underline + "\n")


def printCitation(extPkg):
    # Outputs citation of external package 'extPkg' to 'citations.list' file (along with citation) provided that package is not already in the file.
    # Also adds the citation in latex format to citations.latex

    # Check if citation has already been included
    alreadyIncluded = False
    if os.path.exists(var.dirpath + "/citations.list"):
        citationsListFile = open(var.dirpath + "/citations.list", "r")
        while True:
            lineReadin = citationsListFile.readline()
            if not lineReadin:
                break
            content_list = lineReadin.split()
            if extPkg == content_list[0]:
                alreadyIncluded = True
        citationsListFile.close()

    if alreadyIncluded == False:
        citationsListFile = open(var.dirpath + "/citations.list", "a")
        citationsLatexFile = open(var.dirpath + "/citations.latex", "a")
        citations = {
            "MEAMfit": "A. I. Duff, et. al., Comp. Phys. Comm. 196, 439 (2015)",
            "MLIP2": "A. V. Shapeev, Mul. Mod. Sim. 14 (3) (2016) 1153–1173",
            "lammps": "A. P. Thompson, et. al., Comp. Phys. Comm. 271, 108171 (2022)",
            "PyMatGen": "S. Ong, et. al., Comp. Mat. Sci. 68, 314-319 (2013)",
            "atomsk": "P. Hirel, Comp. Phys. Comm. 197, 212-219 (2015)",
            "PymbarDetectEq": "J. D. Chodera, J. Chem. Theory Comp. 12, 1799-1805 (2016)",
            "VASP": "G. Kresse and J. Hafner, Phys. Rev. B 47, 558 (1993); ibid. 49, 14 251 (1994)\n G. Kresse and J. Furthm???ller, Comput. Mat. Sci. 6, 15 (1996)\nG. Kresse and J. Furthm???ller, Phys. Rev. B 54, 11 169 (1996)\nG. Kresse and D. Joubert, Phys. Rev. 59, 1758 (1999)",
            "CASTEP": "S. J. Clark, et. al., Zeit. fuer Krist. 220, 567-570 (2005)",
        }
        citationsLatex = {
            "MEAMfit": "@article{DUFF2015439, title = {MEAMfit: A reference-free modified embedded atom method (RF-MEAM) energy and force-fitting code}, journal = {Computer Physics Communications}, volume = {196}, pages = {439-445}, year = {2015}, issn = {0010-4655}, doi = {https://doi.org/10.1016/j.cpc.2015.05.016}, url = {https://www.sciencedirect.com/science/article/pii/S0010465515001964}, author = {Andrew Ian Duff and M.W. Finnis and Philippe Maugis and Barend J. Thijsse and Marcel H.F. Sluiter} }",
            "MLIP2": "@article{shapeev2016moment, title={Moment tensor potentials: A class of systematically improvable interatomic potentials}, author={Shapeev, Alexander V}, journal={Multiscale Modeling \& Simulation}, volume={14}, number={3}, pages={1153--1173}, year={2016}, publisher={SIAM}}",
            "lammps": "@article{thompson2022lammps, title={LAMMPS-a flexible simulation tool for particle-based materials modeling at the atomic, meso, and continuum scales}, author={Thompson, Aidan P and Aktulga, H Metin and Berger, Richard and Bolintineanu, Dan S and Brown, W Michael and Crozier, Paul S and in't Veld, Pieter J and Kohlmeyer, Axel and Moore, Stan G and Nguyen, Trung Dac and others}, journal={Computer Physics Communications}, volume={271}, pages={108171}, year={2022}, publisher={Elsevier}}",
            "PyMatGen": "@article{ong2013python,title={Python Materials Genomics (pymatgen): A robust, open-source python library for materials analysis}, author={Ong, Shyue Ping and Richards, William Davidson and Jain, Anubhav and Hautier, Geoffroy and Kocher, Michael and Cholia, Shreyas and Gunter, Dan and Chevrier, Vincent L and Persson, Kristin A and Ceder, Gerbrand}, journal={Computational Materials Science}, volume={68}, pages={314--319}, year={2013}, publisher={Elsevier}}",
            "atomsk": "@article{hirel2015atomsk, title={Atomsk: A tool for manipulating and converting atomic data files}, author={Hirel, Pierre}, journal={Computer Physics Communications}, volume={197}, pages={212--219}, year={2015}, publisher={Elsevier}}",
            "PymbarDetectEq": "@article{chodera2016simple, title={A simple method for automated equilibration detection in molecular simulations}, author={Chodera, John D}, journal={Journal of chemical theory and computation}, volume={12}, number={4}, pages={1799--1805}, year={2016}, publisher={ACS Publications}}",
            "VASP": '@article{kresse1993ab, title={Ab initio molecular dynamics for liquid metals}, author={Kresse, Georg and Hafner, J{"u}rgen}, journal={Physical review B}, volume={47}, number={1}, pages={558}, year={1993}, publisher={APS}} \n @article{kresse1996efficiency, title={Efficiency of ab-initio total energy calculations for metals and semiconductors using a plane-wave basis set}, author={Kresse, Georg and Furthm{"u}ller, J{"u}rgen}, journal={Computational materials science}, volume={6}, number={1}, pages={15--50}, year={1996}, publisher={Elsevier} \n @article{kresse1996efficient, title={Efficient iterative schemes for ab initio total-energy calculations using a plane-wave basis set}, author={Kresse, Georg and Furthm{"u}ller, J{"u}rgen}, journal={Physical review B}, volume={54}, number={16}, pages={11169}, year={1996}, publisher={APS}} \n @article{kresse1999ultrasoft, title={From ultrasoft pseudopotentials to the projector augmented-wave method}, author={Kresse, Georg and Joubert, Daniel}, journal={Physical review b}, volume={59}, number={3}, pages={1758}, year={1999}, publisher={APS}}',
            "CASTEP": '@article{clark2005first, title={First principles methods using CASTEP}, author={Clark, Stewart J and Segall, Matthew D and Pickard, Chris J and Hasnip, Phil J and Probert, Matt IJ and Refson, Keith and Payne, Mike C}, journal={Zeitschrift f{"u}r kristallographie-crystalline materials}, volume={220}, number={5-6}, pages={567--570}, year={2005}, publisher={Oldenbourg Wissenschaftsverlag}}',
        }
        citationsListFile.write(extPkg + " : " + citations[extPkg] + "\n")
        citationsLatexFile.write(citationsLatex[extPkg] + "\n")
        citationsListFile.close()
        citationsLatexFile.close()
