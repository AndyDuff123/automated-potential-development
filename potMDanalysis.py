# Copyright (C) 2020-2023, Dr Andrew Ian Duff,
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import glob
import json
import os
import re
import shutil

# Modules:
from pathlib import Path
from string import ascii_uppercase

import ase
import ase.io
import h5py
import numpy as np

import filesys
import outf
import subprocess
import var  # variable container

#from MDANSE.Framework.Jobs.IJob import IJob
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

import DFTanalysis, potMDsetup


def init_matplotlib():
    global plt
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt


def writeGeomToOutputdir(xc, mat, pot):
    lammpsDumpFile = open(var.potMDnonMDgeomOptpath + "/dump2.meam", "r")
    atomsRelaxed = ase.io.lammpsrun.read_lammps_dump_text(lammpsDumpFile)
    # atomsRelaxed=atomsRelaxed[0]
    relaxedCell = atomsRelaxed.cell  # for some reason unlike DFTanalysis.writeGeom... 'get_cell' doesn't work...
    outputGeomFile = open(var.APDoutputpath + "/geometry_" + xc + "_" + mat, "a")
    outputGeomFile.write(pot + ": ")
    outputGeomFile.write("  Cell vectors: " + str(relaxedCell) + "\n")
    outputGeomFile.write("  Cell lengths: " + str(relaxedCell.lengths()) + "\n")
    outputGeomFile.write("  Cell angles: " + str(relaxedCell.angles()) + "\n")
    outputGeomFile.write("  Cell volume: " + str(relaxedCell.volume) + "\n")
    outputGeomFile.close()


def checkPotMDoutput(directories, potType, checkForRdfFile):

    # Checks presence of LAMMPS output files in each directory in 'directories', filling the three lists below according to their status. Also return any directory that still contains unfinished jobs.

    var.incompleteJobs = []
    var.notStartedJobs = []
    var.failedJobs = []  # I need to add code to display these jobs and exit with a 'fail'

    # outf.printt(2,'  Directories before removing those without all '+var.potMDoutput+' files present: '+str(directories))
    directoriesToRemove = ([]) # Remove those directories that do not have a full set of vasprun/castep.castep files in
            # all of their subdirectories
    potMDmissing = False
    for direc in directories:
        subdirs = [
            name
            for name in os.listdir(var.dirpath + "/" + direc + "/")
            if os.path.isdir(os.path.join(var.dirpath, direc, name)) and name != "out"
        ]
        if not subdirs:
            subdirs = ["/"]  # if no subdirs present just check in direc/. For that we need to set subdirs=['/']
        for subdir in subdirs:
            potMDoutputFilePath = Path(var.dirpath + "/" + direc + "/" + subdir + "/" + var.potMDoutput)
            # Check for jobs that haven't started:
            if not potMDoutputFilePath.is_file():
                outf.toAction(var.potMDoutput + " not present in " + direc + "/" + subdir + "/")
                var.notStartedJobs.append(direc + "/" + subdir + "/")
                potMDmissing = True
            else:
                potMDoutputFile = open(potMDoutputFilePath, "r")
                jobCompletedLineRead = False
                error = False
                while True:
                    lineReadin = potMDoutputFile.readline()
                    if not lineReadin:
                        break
                    # Check for jobs that haven't finished:
                    if "Loop time of" in lineReadin:
                        jobCompletedLineRead = True
                    if "ERROR" in lineReadin or "MLIP: Breaking threshold exceeded" in lineReadin:
                        outf.toAction(var.potMDoutput + " has error in " + direc + "/" + subdir + "/")
                        var.failedJobs.append(direc + "/" + subdir + "/")
                        error = True
                        # potMDmissing = True
                        break

                if jobCompletedLineRead == True and checkForRdfFile == True:
                    # Also need to check that the partial.rdf file has been written (this is a post-processing LAMMPS step)
                    potMDrdfOutputFilePath = Path(var.dirpath + "/" + direc + "/" + subdir + "/" + var.potMDrdfOutput)
                    if not potMDrdfOutputFilePath.is_file():
                        outf.toAction(var.potMDrdfOutput + " not completed in " + direc + "/" + subdir + "/")
                        var.incompleteJobs.append(direc + "/" + subdir + "/")
                        potMDmissing = True
                    else:
                        with open(potMDrdfOutputFilePath, "r") as f:
                            fileLength = len(f.readlines())
                        if fileLength < 504:
                            outf.toAction( f"{var.potMDrdfOutput} not completed in {direc}/{subdir}/ (file length < "
                                "504 lines)")
                            var.incompleteJobs.append(direc + "/" + subdir + "/")
                            potMDmissing = True

                if jobCompletedLineRead == False and error == False:
                    outf.toAction(var.potMDoutput + " not completed in " + direc + "/" + subdir + "/")
                    var.incompleteJobs.append(direc + "/" + subdir + "/")
                    potMDmissing = True

        if potMDmissing:
            directoriesToRemove.append(direc)
        else:
            outf.printt(1, "  All " + var.potMDoutput + " present in: " + direc)

    for direc in directoriesToRemove:
        directories.remove(direc)
    # outf.printt(2,'  Directories after removing those without all '+var.potMDoutput+' files present: '+str(directories))

    # If M/EAM, proceed with relabelling even if only a single temp#/ directory is complete (complete= finished 
    # successfully or failed). If we then find configurations appropriate for relabelling in this/these dirs, we then 
    # proceed to relabel/reoptimize etc using only these. For MTP however we wait for _all_ temp# dirs to complete. The 
    # difference is because with M/EAM (unlike MTP) we don't have a way to identify similar configurations, and so 
    # waiting for all temp#/ to complete would not be profitable as we would end up relabelling too many configs.
    if "EAM" in potType and not directories:
        outf.printt(2, "  No directories with complete set of " + var.potMDoutput + " files in subdirectories")
        var.stopAPD = True
    elif potType == "MTP" and potMDmissing:
        outf.printt(2, "  Some directories without a complete set of " + var.potMDoutput + " files in subdirectories")
        var.stopAPD = True

    return directories


def checkMaxVolOutput(directory):

    # Checks presence of output file for max vol job in directory

    var.incompleteJobs = []
    var.notStartedJobs = []
    var.failedJobs = []

    outf.printt(2, "    Checking directory: " + directory + " for outputs of max vol analysis" )
    maxVolOutput1FilePath = Path(var.dirpath + "/" + directory + "/" + var.maxVolOutput1)
    maxVolOutput1FilePresent = maxVolOutput1FilePath.is_file()
    outf.printt(2, f"    {var.maxVolOutput1} file present? : {maxVolOutput1FilePresent}")
    # Check for jobs that haven't started:
    if not maxVolOutput1FilePresent:
        outf.toAction(var.maxVolOutput1 + " not present in " + directory + "/")
        var.notStartedJobs.append(directory + "/")
        var.stopAPD = True
    else:
        maxVolOutput2FilePath = Path(var.dirpath + "/" + directory + "/" + var.maxVolOutput2)
        maxVolOutput2FilePresent = maxVolOutput2FilePath.is_file()
        outf.printt(2, f"    {var.maxVolOutput2} file present? : {maxVolOutput2FilePresent}")
        if maxVolOutput2FilePresent:
            maxVolOutput2File = open(maxVolOutput2FilePath, "r")
            linesReadin = maxVolOutput2File.readlines()
            if linesReadin[-1] != "\n" and linesReadin[-1] != "END_CFG":
                outf.toAction(var.maxVolOutput2 + " has error in " + directory + "/")
                var.failedJobs.append(directory + "/")
                var.stopAPD = True
            else:
                outf.printt(2, "    " + var.maxVolOutput2 + " file seems complete; max vol seems successful")
        else:
            outf.toAction(var.maxVolOutput2 + " not present in " + directory + "/")
            var.incompleteJobs.append(directory + "/")
            var.stopAPD = True

    return


def retainLastConfigs():
    # If LAMMPS has failed we assume the reason is within the last 100 configs of the failure. This is because we use atomsk to convert dump.meam into multiple
    # .cfg files, en-route to setting up DFT 'correction' jobs, and if the potMD job is v. long it could mean creating 10,000s files.

    # First cut the log.lammps file
    logfile = open("log.lammps", "r")
    newlogfile = open("log.lammps_new", "w")
    for line in logfile:
        if line.startswith("Step Temp"):
            break
    for i, line in enumerate(logfile):
        if line.startswith("ERROR:"):
            break
    numconfigs = i
    logfile.seek(0)
    for line in logfile:
        newlogfile.write(line)
        if line.startswith("Step Temp"):
            break
    for i, line in enumerate(logfile):
        # do not write lines from logfile to logfile_new unless we are at or past the numConfigs-100 th config
        if i >= numconfigs - 100:
            newlogfile.write(line)
    newlogfile.write("ERROR:")
    logfile.close()
    newlogfile.close()
    outf.printt(2, "    Cutting log.lammps file (saving original as log.lammps.bkp) to assist file manipulation")
    os.system("cp log.lammps log.lammps.bkp")
    os.system("mv log.lammps_new log.lammps")

    # Now cut the dump.meam file
    dumpfile = open("dump.meam", "r")
    newdumpfile = open("dump.meam_new", "w")
    numTimesteps = 0
    for line in dumpfile:
        if line.startswith("ITEM: TIMESTEP"):
            numTimesteps = numTimesteps + 1
        if numTimesteps > numconfigs - 100:
            newdumpfile.write(line)
    dumpfile.close()
    newdumpfile.close()
    outf.printt(2, "    Cutting dump.meam file (saving original as dump.meam.bkp) to assist file manipulation")
    os.system("cp dump.meam dump.meam_bkp")
    os.system("mv dump.meam_new dump.meam")


def computeThermExpn(QbyC, xc, mat, pot, temp, configsForRelabelling):

    # Read thermal expansion data from log.lammps file. Returns average Lx, Ly, Lz, followed by a boolean to indicate 
    # if there was an error reading in lammps output. If this is called with QbyC, then extra columns in log.lammps 
    # are expected, corresponding to the energies of other potentials along the MD trajectory of the current potential. 
    # These are then assessed for similarity, and configs with too-different energies are flagged and added to.

    from pymbar.timeseries import detectEquilibration

    init_matplotlib()

    potMDoutputFile = open(var.potMDoutput, "r")
    step = []
    Lx = []
    Ly = []
    Lz = []
    vol = []

    eEAM = []
    for pot_num in range(0, var.num_pots):
        eEAM.append([])

    lmpErr = False
    for line in potMDoutputFile:
        if (line.startswith("ERROR:") or line.startswith("MLIP: ERROR:")):
            lmpErr = True
            break
        elif re.match(r"\s*Step\s+Temp\s+Press",line):
            break
    if lmpErr:
        # LAMMPS error occurred before main MD loop started
        nMDsteps = 0
    else:
        for i, line in enumerate(potMDoutputFile):
            if line.startswith("Loop time"):
                break
            elif (line.startswith("ERROR:") or line.startswith("MLIP: ERROR:") or 
                    line.startswith("MLIP: Breaking threshold exceeded")):
                lmpErr = True
                break
            else:
                p = re.compile(r"[-+]?\d*\.\d+ +|\d+ |-?[\d.]+(?:e-?\d+)?")  # Compile a pattern to capture float values
                numbersFromLine = [float(j) for j in p.findall(line)]  # Convert strings to float
                step.append(numbersFromLine[0])
                Lx.append(numbersFromLine[3])
                Ly.append(numbersFromLine[4])
                Lz.append(numbersFromLine[5])
                vol.append(numbersFromLine[6])
                if QbyC:
                    for pot_num in range(0, var.num_pots):
                        eEAM[pot_num].append(numbersFromLine[9 + pot_num])
                else:
                    eEAM[0].append(numbersFromLine[8])
        nMDsteps = i
    
    outf.printt(2, "    nMDsteps= " + str(nMDsteps))

    if lmpErr:
        outf.printt(1, "    Error in LAMMPS run detected")
        # Find the bad configs at the end. We used to use these directly to relabel DFT, however if we want to use QbyC over the whole MD then we can use these as the upper limit to cut out the really badly behaved part (which messes up our attempts below to determine the preequilibration)
        outf.printt(2, "    dump.meam exists: " + str(os.path.isfile("dump.meam")))
        if os.path.isfile("dump.meam"):
            # Find the configurations where things started to go wrong
            outf.printt(1, "    -- Failed LAMMPS configs --")

            # Determine largest force per config. We will use this as a check to see if a 'bad config' is too bad to be re-labelled with DFT
            outf.printt(2, "    Determining largest force per config.")
            largestForce = []
            atomSigOofBperConfig = ([])  # List of booleans for each config. True if any one atom cooridinates are 
                    # more than one supercell displaced from all the other atoms (in direct coord terms, >2 or < -1). 
                    # This can detect anomolous configs NOT to be recomputed by DFT. For now we have just put this into
                    # if there is a LAMMPS error, as it takes time to compute this along with largestForce. But may be 
                    # neccessary to move out of the if lmpErr conditional (e.g. an atom might 'fly off' but simulation 
                    # still 'seems ok')
            config = -1
            readForces = False
            dumpfile = open("dump.meam", "r")
            while True:
                lineReadin = dumpfile.readline()
                if not lineReadin:  # if line is empty end of file is reached
                    largestForce.append(largestForceCompFound)
                    break
                if "ITEM: TIMESTEP" in lineReadin:
                    if config >= 0:
                        largestForce.append(largestForceCompFound)
                        atomSigOofBperConfig.append(atomSigOofB)
                    config = config + 1
                    readForces = False
                    atomSigOofB = False
                if readForces == True:
                    # Read through forces and record largest magnitude component
                    numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                    numbersFromLine = [float(x) for x in numbersFromLine]
                    largestForceCompFound = max([largestForceCompFound, abs(numbersFromLine[5]),
                            abs(numbersFromLine[6]), abs(numbersFromLine[7])])
                    if (numbersFromLine[2] < -1 or numbersFromLine[2] > 2 or numbersFromLine[3] < -1
                        or numbersFromLine[3] > 2 or numbersFromLine[4] < -1 or numbersFromLine[4] > 2):
                        atomSigOofB = True
                if "ITEM: ATOMS" in lineReadin:
                    readForces = True
                    largestForceCompFound = 0

            outf.printt(2, "    Identifying configurations where observables began to become unphysical...")
            badCfgs, sd_largestForce, avrg_largestForce, vBadCfg = identifyBadConfigs(largestForce)
            outf.printt(1, f"    LAMMPS configs beyond: {badCfgs} to be ignored in determination of prequilibration "
                    "and Q by C analysis")  # We used to use the badCfgs from here also to relabel DFT, but now we use 
                    # Q by C for that (sometimes LAMMPS configs at tail end of a simulation with an ERROR are highly 
                    # unphysical)
        else:
            outf.toAction(f"Lammps output has error, and dump.meam file is not present "
                    "so not possible to recompute configurations using DFT. Please ensure atom positions are dumped "
                    "in lammps input file, STOPPING")
            var.stopAPD = True
            badCfgs = []
            return 0, 0, 0, 0, 0, 0, 0, 0, 0
    else:
        badCfgs = []

    # Detect prequilibration (prequilibration detection takes too long on all 100000 configs. Cut length of Lx, Ly, Lz for prequilibration purposes)
    outf.printt(1, "    Computing preequilibration")
    eEAMsmaller = []
    for pot_num in range(0, var.num_pots):
        eEAMsmaller.append([])
    if len(Lx) > 10000:
        LxSmaller = Lx[0:10000]
        LySmaller = Ly[0:10000]
        LzSmaller = Lz[0:10000]
        eEAMsmaller[0] = eEAM[0][0:10000]
    else:
        LxSmaller = Lx
        LySmaller = Ly
        LzSmaller = Lz
        eEAMsmaller[0] = eEAM[0]
    if lmpErr and min(badCfgs) < len(LxSmaller):
        outf.printt(
            2,
            "      ...setting maximum considered configuration to first config where observables start to diverge due to failed LAMMPS run, I.e. config nmr:"
            + str(min(badCfgs) - 1),
        )
        LxSmaller = LxSmaller[
            0 : min(badCfgs) - 1
        ]  # if we don't cut out these bad configs at the end (especially those just after these initial bad configs) then prequilibration determination will be messed up
    [Lx_preequil, g, Neff_max] = detectEquilibration(np.array(LxSmaller))
    outf.printt(2, "      ...Lx prequil detection done: no. equil. steps: " + str(Lx_preequil))
    [eEAM1_preequil, g, Neff_max] = detectEquilibration(np.array(eEAMsmaller[0]))
    outf.printt(2, "      equilibration for eEAM1 = " + str(eEAM1_preequil))
    # Check that these quantities are converged within the 10,000 config range considered
    converged = True
    if (
        eEAM1_preequil == 0 and lmpErr
    ):  # prequilibration=0 can happen if detectEquilibration can't find equilibrium length (e.g. if profile does not have a clear converging behaviour) - in this case try a lower temperature, since configuration may be significantly off-equilibrium. The lmpErr check is added to try to reduce the possibility that the prequilibration=0 is 'legitimate' (E.g. as might happen if temperature is v. low).
        converged = False
    if eEAM1_preequil > 20000 or eEAM1_preequil > 0.5 * len(eEAM[0]):
        outf.printt(1, "    WARNING: eEAM[0] not converged (or likely not converged); skipping MD run and moving to "
                "lower temperature")
        outf.printt(2, f"    ( eEAM1_preequil={eEAM1_preequil}, len(eEAM[0])={len(eEAM[0])}, criterion: eEAM1_preeuil "
                "> 20000 or 0.5*len(eEAM[0]) )")
        converged = False
    if converged == False:
        return converged, 0, 0, 0, 0, 0, 0, 0, 0
    n_preequilSteps = eEAM1_preequil

    # Find which configurations potentials disagree with in energy prediction (Q by C)
    outf.printt(2, "    QbyC=" + str(QbyC))
    badCfgs_QbyC_perTemp = []
    sigma_badCfgs_QbyC_perTemp = []
    if QbyC:
        outf.printt(1, "    -- Checking configs using query by committee --")
        outf.printt(
            2,
            "    Check which configs disagree in energy predictions using the different potentials to decide which configs to relabel using DFT",
        )
        QbyCfile = open("QbyC.dat", "w")
        QbyCfile_good = open("QbyCgood.dat", "w")
        # QbyCfile.write('config # | eEAM1 | eEAM2 | eEAM3 | eEAMsd | thrs1 | thrs2 \n')
        fileHeader = "config # | "
        for pot_num in range(0, var.num_pots):
            fileHeader = fileHeader + "eEAM" + str(pot_num + 1) + " | "
        fileHeader = fileHeader + "eEAMsd | thrs1 | thrs2 \n"
        QbyCfile.write(fileHeader)
        # QbyCfile_good.write('config # | eEAM1 | eEAM2 | eEAM3 | eEAMsd | thrs1 | thrs2 \n')
        QbyCfile_good.write(fileHeader)
        eEAMsd = []
        if badCfgs:
            outf.printt(1, "    Only considering up to " + str(vBadCfg) + " (where LAMMPS run failed)")
            nMDmax = vBadCfg
        else:
            outf.printt(
                2, "    LAMMPS did not fail therefore considering all configs (I.e. up to " + str(nMDsteps) + ")"
            )
            nMDmax = nMDsteps
        outf.printt(2, "    Finding standard deviation of energies over the potentials")
        for i in range(n_preequilSteps + 1, nMDmax):
            for pot_num in range(0, var.num_pots):
                eEAM[pot_num][i] = eEAM[pot_num][i] - eEAM[pot_num][n_preequilSteps]
            sd = np.std([eEAM[pot_num][i] for pot_num in range(0, var.num_pots)])
            eEAMsd.append(sd)
        avrgSd = np.mean(eEAMsd)
        outf.printt(
            2,
            "    Average value of s.d. of energy predictions of potentials including (configs where potentials may strongly disagree): "
            + str(avrgSd),
        )
        # Sort through configs and record those with s.d > var.QbyC_LowerThreshold * avrgSd
        # Update, trying a new measure: a rolling update of average over s.d. of energies of each potential: avrgSdRolling
        configsGood = []  # store configs and energies of 'good configs'
        eEAMgood = []
        for pot_num in range(0, var.num_pots):
            eEAMgood.append([])
        configsBad = []  # store configs and energies of 'good configs'
        eEAMbad = []
        for pot_num in range(0, var.num_pots):
            eEAMbad.append([])
        nVbadConfigs = 0
        outf.printt(
            2,
            "    Instead use a rolling value of the s.d. to determine which configurations should be relabelled using DFT",
        )
        previousBadConfigs = 0
        for i in range(n_preequilSteps + 1, nMDmax):
            sd = np.std(
                [eEAM[pot_num][i] for pot_num in range(0, var.num_pots)]
            )  # change to 0,var.num_pots afterwards, also below
            formatString = "{:>12} {:>12} {:>12} {:>12}"
            for pot_num in range(0, var.num_pots):
                formatString = formatString + " {:>12}"
            eEAMlist = [eEAM[pot_num][i] for pot_num in range(0, var.num_pots)]
            QbyCfile.write(
                formatString.format(
                    i, *eEAMlist, sd, var.QbyC_LowerThreshold * avrgSd, var.QbyC_UpperThreshold * avrgSd
                )
            )  # *eEAMlist 'splats' the list into a sequence of parameters separated by commas

            QbyCfile.write("\n")
            if i == n_preequilSteps + 1:
                avrgSdRolling = sd  # this is actually the numerator of the average - to avoid recomputing the sum over Sd's each time
                QbyCfile_good.write(
                    formatString.format(
                        i,
                        *eEAMlist,
                        sd,
                        var.QbyC_LowerThreshold * avrgSdRolling / (i - n_preequilSteps),
                        var.QbyC_UpperThreshold * avrgSdRolling / (i - n_preequilSteps)
                    )
                )
                QbyCfile_good.write("\n")
            else:
                if i <= n_preequilSteps + 10 or sd < var.QbyC_LowerThreshold * avrgSdRolling / (
                    i - n_preequilSteps
                ):  # first condition ensures we have a stable average s.d. to use for checking bad configs. Otherwise e.g. the third or fourth s.d. can trigger the 'bad config' condition
                    avrgSdRolling = avrgSdRolling + sd
                    QbyCfile_good.write(
                        formatString.format(
                            i,
                            *eEAMlist,
                            sd,
                            var.QbyC_LowerThreshold * avrgSdRolling / (i - n_preequilSteps),
                            var.QbyC_UpperThreshold * avrgSdRolling / (i - n_preequilSteps)
                        )
                    )
                    QbyCfile_good.write("\n")
                    # print('updating avrgSdRolling to: ',avrgSdRolling,'. The actual rolling avrg is =',avrgSdRolling/(i-n_preequilSteps+1))
                    configsGood.append(i)
                    for pot_num in range(0, var.num_pots):
                        eEAMgood[pot_num].append(eEAM[pot_num][i])
                    previousBadConfigs = 0
                else:
                    if sd < var.QbyC_UpperThreshold * avrgSdRolling / (i - n_preequilSteps):
                        if previousBadConfigs <= 11:
                            outf.printt(
                                1,
                                "    s.d. = "
                                + str(sd)
                                + " for config "
                                + str(i)
                                + " > "
                                + str(var.QbyC_LowerThreshold)
                                + "*rolling average of sd_energies = "
                                + str(var.QbyC_LowerThreshold * avrgSdRolling / (i - n_preequilSteps)),
                            )
                        if previousBadConfigs <= 10:
                            atomOofB = False
                            if lmpErr:
                                if atomSigOofBperConfig[i]:
                                    atomOofB = True
                            if not atomOofB:
                                outf.printt(
                                    1,
                                    "    < "
                                    + str(var.QbyC_UpperThreshold)
                                    + "*rolling average of sd_energies = "
                                    + str(var.QbyC_UpperThreshold * avrgSdRolling / (i - n_preequilSteps))
                                    + " : adding to list of configs to be relabelled using DFT",
                                )
                                badCfgs_QbyC_perTemp.append(i)
                                sigma_badCfgs_QbyC_perTemp.append(sd)
                            else:
                                outf.printt(
                                    1,
                                    "    < "
                                    + str(var.QbyC_UpperThreshold)
                                    + "*rolling average of sd_energies = "
                                    + str(var.QbyC_UpperThreshold * avrgSdRolling / (i - n_preequilSteps))
                                    + ", however at least one position component significantly O of B - not relabelling",
                                )
                        else:
                            if previousBadConfigs == 11:
                                outf.printt(
                                    1,
                                    "    < "
                                    + str(var.QbyC_UpperThreshold)
                                    + "*rolling average of sd_energies = "
                                    + str(var.QbyC_UpperThreshold * avrgSdRolling / (i - n_preequilSteps))
                                    + " : candidate to relabel - declining ("
                                    + str(previousBadConfigs)
                                    + " configs already in this block...)",
                                )
                    else:
                        outf.printt(
                            1,
                            "    s.d. = "
                            + str(sd)
                            + " for config "
                            + str(i)
                            + " > "
                            + str(var.QbyC_LowerThreshold)
                            + "*rolling average of sd_energies = "
                            + str(var.QbyC_LowerThreshold * avrgSdRolling / (i - n_preequilSteps)),
                        )
                        if sd < var.QbyC_UpperThreshold2 * avrgSdRolling / (i - n_preequilSteps):
                            outf.printt(
                                1,
                                "    > "
                                + str(var.QbyC_UpperThreshold)
                                + "*rolling average of sd_energies = "
                                + str(var.QbyC_UpperThreshold * avrgSdRolling / (i - n_preequilSteps))
                                + " : disagreement too large - not to be relabelled using DFT",
                            )
                            nVbadConfigs = nVbadConfigs + 1
                        else:
                            outf.printt(
                                1,
                                "    > "
                                + str(var.QbyC_UpperThreshold2)
                                + "*rolling average of sd_energies = "
                                + str(var.QbyC_UpperThreshold2 * avrgSdRolling / (i - n_preequilSteps))
                                + " : catastrophic disagreement - MD trajectory unsafe; exiting",
                            )
                            break
                    previousBadConfigs = previousBadConfigs + 1
                    # regardless of whether we want to relabel, include in plot as 'bad points':
                    configsBad.append(i)
                    for pot_num in range(0, var.num_pots):
                        eEAMbad[pot_num].append(eEAM[pot_num][i])
            if len(badCfgs_QbyC_perTemp) == 5:
                outf.printt(1, "    Acquired 5 configurations for relabelling - stopping search for more")
                break
            if nVbadConfigs == 100:
                outf.printt(
                    1,
                    "    100 configurations for which energies are too dissimilar for relabelling identified in a row; likely something wrong with the MD - stopping search bad configs",
                )
                break
        QbyCfile.close()
        QbyCfile_good.close()
        print("compare the QbyC files")

        outf.printt(2, "    Configs to recompute using DFT (determined by Q by C): " + str(badCfgs_QbyC_perTemp))

        plt.title("Molecular dynamics on " + mat + " (MP-id) using " + pot + " (fit to " + xc + " DFT data)")
        plt.xlabel("Configuration (post equilibration)")
        plt.ylabel("$\Delta$Energy (eV/atom)")
        colors = ["red", "blue", "green", "magenta", "cyan"]
        for pot_num in range(0, var.num_pots):
            plt.plot(configsGood, eEAMgood[pot_num], color=colors[pot_num], linewidth=0.5)
        xlimBefore = plt.xlim()
        ylimBefore = plt.ylim()
        yrange = ylimBefore[1] - ylimBefore[0]
        plt.xlim(xlimBefore)
        plt.ylim(ylimBefore[0] - yrange / 2, ylimBefore[1] + yrange / 2)
        for pot_num in range(0, var.num_pots):
            plt.plot(configsBad, eEAMbad[pot_num], color=colors[pot_num], linestyle="dashed", linewidth=0.5)
        legendList = []
        for pot_num in range(0, var.num_pots):
            legendList.append("MEAM" + str(pot_num + 1))
        plt.legend(legendList)
        plt.savefig("energiesVsConfig.png")
        plt.clf()

    potMDoutputFile.close()

    LxAvrg = 0
    LyAvrg = 0
    LzAvrg = 0
    volAvrg = 0
    if lmpErr == False:
        # Compute thermal expansion
        LxAvrg = float(sum(Lx[n_preequilSteps:]) / len(Lx[n_preequilSteps:]))
        LyAvrg = float(sum(Ly[n_preequilSteps:]) / len(Ly[n_preequilSteps:]))
        LzAvrg = float(sum(Lz[n_preequilSteps:]) / len(Lz[n_preequilSteps:]))
        volAvrg = float(sum(vol[n_preequilSteps:]) / len(vol[n_preequilSteps:]))
        outf.printt(1, "    Ensemble average of Lx, Ly, Lz, vol over equilibrated configurations completed")

        # Compute rdf. Here we plot DFT points where available on top of potential calculated curves. Curves first...
        init_matplotlib()
        # We are overlaying the DFT rdf plot, but rdf data corresponds to the potential-ordering of species. Get the latter
        # from the potential filename. Then when we read in the rdf data we know the species, and can re-order accordingly
        # when writing to the rdf array (and thence to the plot via plt).
        if 'MTP' in pot:
            potFile = glob.glob("Trained.mtp_*")
            potFile = potFile[0] # Should be just one file like 'Trained.mtp_LiOZrNbLa_MTP1'...
            compositionPotentialString, speciesPotentialString, speciesPotential = potMDsetup.compositionFromPotFile(
                    potFile, 'MTP')
        else:
            # I think for M/EAM the potential file is reordered to the DFT ordering, but I am not certain... If that is
            # the case we just want to set speciesPotential = var.speciesInitialStrings in that case...
            speciesPotential = var.speciesInitialStrings
        print('DFT order:',var.speciesInitialStrings)
        print('potential order:',speciesPotential)
        # Determine if DFT data is to be plotted alongside potential data for rdf
        plot_DFT = ( not var.defects and var.config_gen != "DFT_NPT_EXISTING" and var.config_gen != "DFT_NVT_EXISTING" and
                os.path.isdir(defectPath) )
        print('Plotting DFT? : ',plot_DFT)
        # Now DFT (we only attempt to add DFT data in the case where we aren't considering defects. For the latter we
        # would need to extend code to keep track of which defect config each lammps job is running, and plot alongside
        # only the DFT data for that defect)
        defectPath = os.path.join(var.DFTconfigsDFTNPTpath, f"temp{temp}", "bulk")
        if plot_DFT:
            # For now do the compute rdf for the potential only in the case we do it for DFT too. These lines can be brought
            # out this conditional (i.e. plot rdf for potential but not DFT) but there is currently a bug due to the compute
            # rdf LAMMPS command not having enough pairs, e.g. for 5 atom types
            computeRdf(speciesPotential, plot_DFT) # We need to pass plot_DFT here as well because if we are plotting DFT later
                    # we need to adjust the species indicies of the potential data to match the DFT data
            if var.stopAPD == True:  # this occurs if partial.rdf file has not yet been written
                return 0, 0, 0, 0, 0, 0, 0, 0, 0

            traj, nSteps, errors, contDir = DFTanalysis.readTraj(defectPath)
            nPreEqrSteps, nPostEqrSteps = DFTanalysis.computeThermExpn(traj, defectPath, writeThermExpn = False)
            DFTanalysis.computeRdf(traj, nPreEqrSteps, nPostEqrSteps, defectPath, plt)
            # DFTanalysis.computeRdf(temp, var.DFTconfigsDFTNPTdir + "/temp" + str(temp),
            #         var.DFTconfigsDFTNPTpath + "/temp" + str(temp), plt) # old syntax (keep for now for reference)
        plt.savefig("rdf_" + str(temp) + ".png", dpi=300)
        if os.path.exists(var.APDoutputpath) == False: # Directory may not yet exist if we have skipped the initial
            filesys.createWorkdir(var.APDoutputdir) # DFTnonMD stages
        plt.savefig(var.APDoutputpath + "/" + "rdf_" + xc + "_" + mat + "_" + pot + "_" + str(temp) + ".png", dpi=300)
        plt.clf()

        # if 'MTP' in pot and not configsForRelabelling:
           # Intensive, and so only done if no configs set for relabelling across all temps for this potential. Given
           # the relabelling logic for M/EAM is all tied up in this subroutine, we can't determine this ahead of time
           # yet for M/EAM (will require decoupling the QbC and observable calculations in this subroutine) we only
           # output this for MTPs
           # computeDynStrucFact(len(Lx[n_preequilSteps:]), n_preequilSteps, speciesPotential) # Compute dynamic 
                   # structure factor

    return (converged, LxAvrg, LyAvrg, LzAvrg, volAvrg, lmpErr, badCfgs, badCfgs_QbyC_perTemp, 
            sigma_badCfgs_QbyC_perTemp )


#def computeDynStrucFact(nSteps, nPreEqrSteps, speciesPotential):
#
#   # Dyn struc fact
#
#   print('nSteps, nPreEqrSteps, speciesPotential = ',nSteps, nPreEqrSteps, speciesPotential)
#   # Access the global elements dictionary from initialize.py
#   elements = var.elements
#   
#   # Automatically generate atom_aliases based on speciesPotential
#   atom_aliases = {}
#   for i, species in enumerate(speciesPotential, start=1):
#       element = elements[species]
#       alias_key = f'mass={element.atomic_mass}'
#       atom_aliases[alias_key] = {str(i): species}
#   
#   # Convert atom_aliases dictionary to the required string format
#   atom_aliases_str = json.dumps(atom_aliases)
#   print('atom_aliases_str=',atom_aliases_str)
#
#   parameters1 = {'config_file': 'POSCAR.lmp', # LAMMPS configuration (structure) file
#           'atom_aliases': atom_aliases_str, #'{"mass=6.941": {"1": "Li"}, "mass=15.999": {"2": "O"}, "mass=91.224": {"3": "Zr"}, "mass=92.906": {"4": "Nb"}, "mass=138.905": {"5": "La"}}', 
#           'fold': False, # Fold coordinates in to box
#           'lammps_units': 'metal', # LAMMPS unit system
#           'n_steps': '0', # Number of time steps (0 for automatic detection)
#           'output_files': ('mdanse_trajectory', 32, 'gzip', 'INFO'), # MDANSE trajectory (filename, format)
#           'time_step': '0.002', # Time step (lammps units, depends on unit system)
#           'trajectory_file': 'dump2.meam', # LAMMPS trajectory file
#           'trajectory_format': 'custom'} # LAMMPS trajectory format
#   print('parameters1=',parameters1)
#   parameters2 = {'atom_selection': '{"all": true}', # atom_selection
#           'frames': [nPreEqrSteps, nSteps, 1], # frames  UPDATE: used to have step 20. But I think for calculating dynamic structure factor one needs ALL configs (I.e. inc. correlated snapshots), because that is the point, the function is calculating the correlation. Might need to limit nSteps instead to reduce number.
#           'grouping_level': 'atom', # grouping_level
#           'instrument_resolution': ('gaussian', {'mu': 0.0, 'sigma': 0.65}), # instrument_resolution
#           'output_files': ('result_disf_script', ['MDAFormat']), #, 'no logs'),  # output_files
#           'projection': ('NullProjector', []), # project coordinates
#           'q_vectors': ('SphericalLatticeQVectors', {'seed': 0, 'shells': [0.0, 5.0, 0.25], 'n_vectors': 150, 'width': 0.5}),  # q_vectors
#           #'q_vectors': ('approximatedispersionqvectors', {'q_start': [0, 0, 0], 'q_end': [0, 2, 0], 'q_step': 0.1}),
#           'running_mode': ('single-core',), # running_mode
#           'trajectory': 'OUTPUT_TRAJECTORY.mdt', # trajectory
#           'weights': 'b_incoherent2'} # weights   
#   # first, we will convert the trajectory from the lammps format to an HDF5 file that MDANSE uses
#   converter = IJob.create('LAMMPS')
#   converter.run(parameters1, status=True)
#   # Next, we run the analysis.
#   dynamicincoherentstructurefactor = IJob.create('DynamicIncoherentStructureFactor')
#   dynamicincoherentstructurefactor.run(parameters2, status=True)
#   # We also load the main dataset of the results from the file using h5py.
#   result_file = h5py.File('result_disf_script.mda')
#   try:
#       data = result_file['s(q,f)_total'][:]
#       energy_axis = result_file['omega'][:]
#       q_axis = result_file['q'][:]
#               # Let's plot s(q,f)_total for a specific q value
#       q_index = 0  # Replace with the index of the q value you are interested in
#       plt.plot(energy_axis, data[q_index, :])
#       plt.xlabel('Energy Transfer (meV)')
#       plt.ylabel('s(q,f)_total')
#       plt.title(f'S(q,f)_total for q = {q_axis[q_index]}')
#       plt.savefig('plot_output.png')
#       plt.clf()
#       print(f"Keys in-file: {result_file.keys()}")
#   except:
#       print(f"Could not find s(q,f)_total in the output")
#       print(f"Instead, there were: {result_file.keys()}")
#       outf.printt(0, 'ERROR: Error in computeDynStrucFact, STOPPING')
#       quit()
#   #else:
#   #    print(f"Output array size: {data.shape}")
#   #    print(f"Omega array size: {energy_axis.shape}")
#   #    print(f"Q array size: {q_axis.shape}")
#   #    print(f"All datasets in the file are: {result_file.keys()}")
#   plt.clf()


def computeRdf(speciesPotential, plot_DFT):

    # Compute rdf. Began implementing plot_DFT to enable computeRdf for the case of just the potential, i.e. no DFT data plotting
    # this affects the indices for species.

    outf.printt(2, "    Determining rdfs")

    # read in rdf from partial.rdf file
    partialRdffile = open(var.potMDrdfOutput, "r")
    lineReadin = partialRdffile.readline()
    lineReadin = partialRdffile.readline()
    lineReadin = partialRdffile.readline()
    lineReadin = partialRdffile.readline()
    if (
        not lineReadin
    ):  # we do the check on fourth line because it may be that first three lines are written initially but then subsequent lines are all written out together after post-processing. In other words, if the fourth line is present the file is probably complete...
        outf.printt(
            0,
            "    ERROR: "
            + var.potMDrdfOutput
            + " file not yet written (first line/s of file not present) - exiting stage",
        )
        var.stopAPD = True
        return

    numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
    nRdf = int(numbersFromLine[1])
    rvalues = []
    #print('len(speciesPotential)=',len(speciesPotential))
    rdf = np.zeros(shape=(len(speciesPotential), len(speciesPotential), nRdf))
    iLine = 0
    while True:
        lineReadin = partialRdffile.readline()
        if not lineReadin:
            break
        for ispc1, spc1 in enumerate(speciesPotential):
            for ispc2, spc2 in enumerate(speciesPotential[ispc1:]):
                #print('ispc1,ispc2=',ispc1,ispc2)
                if plot_DFT: # In this case change we need to plot the potential derived rdf against the species as-ordered in the DFT
                    jspc1 = var.speciesInitialStrings.index(spc1)
                    jspc2 = var.speciesInitialStrings.index(spc2)
                else: # No need to change ordering of species in this case
                    jspc1 = ispc1
                    jspc2 = ispc2
                if jspc1 > jspc2:
                    jspc1, jspc2 = jspc2, jspc1
                #print('jspc1,jspc2=',jspc1,jspc2)
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                #print('numbersFromLine=',numbersFromLine)
                #print('numers from line index=',2 * (ispc1 * len(speciesPotential) + ispc2 + ispc1) + 2,', len(numbersFromLine)=',len(numbersFromLine))
                rdf[jspc1][jspc2][iLine] = numbersFromLine[
                    2 * (ispc1 * len(speciesPotential) + ispc2 + ispc1) + 2
                ]
        rvalues.append(float(numbersFromLine[1]))
        #print('iLine=',iLine)
        iLine = iLine + 1

    # plot
    plt.gca().set_prop_cycle(None)
    if plot_DFT:
        for ispc1, spc1 in enumerate(var.speciesInitialStrings):
            for ispc2, spc2 in enumerate(var.speciesInitialStrings[ispc1:]):
                plt.plot(
                    rvalues, rdf[ispc1][ispc2 + ispc1][0:nRdf], linestyle="solid"
                )  # if we do in same order as for DFT there should be no need to label
    else:
        for ispc1, spc1 in enumerate(speciesPotential):
            for ispc2, spc2 in enumerate(speciesPotential[ispc1:]):
                plt.plot(
                    rvalues, rdf[ispc1][ispc2 + ispc1][0:nRdf], linestyle="solid"
                )


def identifyBadConfigs(largestForce):

    from pymbar.timeseries import detectEquilibration

    badCfgs = []
    totEnergy = []
    logfile = open("log.lammps", "r")
    for line in logfile:
        if line.startswith("Step Temp"):
            break
    for line in logfile:
        if line.startswith("ERROR:"):
            break
        else:
            p = re.compile(r"[-+]?\d*\.\d+ +|\d+ |-?[\d.]+(?:e-?\d+)?")  # Compile a pattern to capture float values
            numbers = line.split()
            numbersFromLine = [float(i) for i in numbers]
            totEnergy.append(numbersFromLine[7])
    logfile.close()

    # To find where totEnergy starts to depart from normal, reverse it and detect prequilibration period (also reverse largestForce here - for convenience we manipulate these arrays in their reversed form from
    # now on
    #
    # should perhaps use forces here as well as they are more stable (trends less visible)? (see later)
    totEnergy.reverse()
    largestForce.reverse()
    numpyTotEnergy = np.array(totEnergy)
    outf.printt(
        2, "    Detecting number of configurations at end of MD where total energy misbehaves..."
    )  # provides an upper limit on the configurations that we will use for subsequent analysis
    if len(totEnergy) > 10000:
        totEnergySmaller = totEnergy[0:10000]
    else:
        totEnergySmaller = totEnergy
    [prequil, g, Neff_max] = detectEquilibration(
        np.array(totEnergySmaller)
    )  # equilibrated period is: totEnergy_equlibrated = totEnergy[prequil:]
    outf.printCitation("PymbarDetectEq")
    outf.printt(
        2,
        "    Total energy significantly misbehaving in last "
        + str(prequil)
        + " configurations (total "
        + str(len(totEnergy))
        + " configurations)",
    )
    outf.printt(
        2,
        "    Configurations just before this have totEnergy (reverse order)= "
        + str(totEnergy[prequil : prequil + 5])
        + "...",
    )  # Previously we would consider these configurations for recomputing with DFT, but now we just use these configurations as an upper limit for our subsequent analysis

    outf.printt(
        2, "    Checking that these values do not have too large forces - if so trace back to earlier configs..."
    )
    # We don't want to select any configurations from the 'misbehaving' region, but instead just before this. This is because atoms will already have started sampling the
    # configurations responsible for this misbehaviour prior to it showing up in the total energy. Furthermore we don't want to refit configs with v. large energy
    # as this will affect the quality of the fit elsewhere, and we don't need to reproduce the energies of these configs.

    # Ideally the relevant region would be immediately after the prequilibration done above, however 'detectEquilibration' does not successfully remove all
    # configurations with large energies. This is probably because the functional form of the divergence at the end of a failed LAMMPS run can be unusual,
    # in that there can be a spike followed by subsequent reduction as thermostat kicks in, rather than an exponential increase (or decrease) in values.
    # Hence this extra check

    # First find the 'normal' sd and avrg of totEnergy and largestForce, so we have a reference for deciding to remove configurations
    # Note that while in principle we should be able to do e.g. sd_totEnergy=np.std(np.array(totEnergy[prequil:])), this will include some of the really bad configs that the
    # 'detectEquilibration' libary did not detect, and badly skew the s.d. Therefore we take the 'first half' of the configs for computing s.d.s.
    # (This approach could probably entirely replace the 'detectEquilibration' approach which we lead with...)
    #
    # Update: energies can still have a drift due to incomplete equilbiration. Just use forces.
    halfway = int(len(totEnergy) / 2)
    # outf.printt(2,'    ...First determine benchmark sd and average of total energy across the (mostly) well-behaving configs:')
    # sd_totEnergy=np.std(np.array(totEnergy[halfway:]))
    # avrg_totEnergy=np.mean(np.array(totEnergy[halfway:]))
    # outf.printt(2,'    sd_totEnergy='+str(sd_totEnergy))
    # outf.printt(2,'    avrg_totEnergy='+str(avrg_totEnergy))
    # outf.printt(2,'    Benchmark sd and average of largest force components across the (mostly) well-behaving configs:')
    # print('largestForce[halfway:]=',largestForce[halfway:])
    # quit()
    sd_largestForce = np.std(np.array(largestForce[halfway:]))
    avrg_largestForce = np.mean(np.array(largestForce[halfway:]))
    outf.printt(2, "    sd_largestForce=" + str(sd_largestForce))
    outf.printt(2, "    avrg_largestForce=" + str(avrg_largestForce))

    # outf.printt(2,'    Removing configurations from our selection with energies outside 5*sd_totEnergy of avrg_totEnergy or largest force components outside 5*sd_largestForce of avrg_largestForce')
    # I am not sure which of the following criteria to use on the forces yet to weed out unreasonable configs. Keep them both for now but may need to adjust as I try on other materials
    outf.printt(
        2,
        "    Removing configurations from our selection with largest force components that do not satisfy either: < 1000*sd_largestForce or < 1000 eV/Angstrom ",
    )
    totEnergy.reverse()  # reverse it back
    largestForce.reverse()  # reverse it back
    vBadCfg = len(totEnergy) - prequil
    config = vBadCfg - 1
    configsToRecompute = 0
    outf.printt(2, "    (largestForce[vBadCfg-20:vBadCfg]=" + str(largestForce[vBadCfg - 20 : vBadCfg]) + ")")
    while configsToRecompute < 5:
        # outf.printt(2,'    Checking totEnergy = '+str(totEnergy[config])+' > '+str(avrg_totEnergy-5*sd_totEnergy)+' and < '+str(avrg_totEnergy+5*sd_totEnergy)+'... and largestForce = '+str(largestForce[config])+' > '+str(avrg_largestForce-5*sd_largestForce)+' and < '+str(avrg_largestForce+5*sd_largestForce))
        # if totEnergy[config]>avrg_totEnergy-5*sd_totEnergy and totEnergy[config]<avrg_totEnergy+5*sd_totEnergy and largestForce[config]>avrg_largestForce-5*sd_largestForce and largestForce[config]<avrg_largestForce+5*sd_largestForce:
        outf.printt(
            2,
            "    Checking if largestForce = "
            + str(largestForce[config])
            + " < "
            + str(avrg_largestForce + 1000 * sd_largestForce)
            + " or < 1000",
        )
        if largestForce[config] < avrg_largestForce + 1000 * sd_largestForce or largestForce[config] < 1000:
            outf.printt(2, "      ...keeping")
            badCfgs.append(config)
            configsToRecompute = configsToRecompute + 1
        else:
            outf.printt(2, "      ...removing")
        config = config - 1
        if config == 0:
            outf.printt(0, "    ERROR: Unable to find any suitable configurations from our analysis of end of MD run")
            quit()

    return badCfgs, sd_largestForce, avrg_largestForce, vBadCfg


def concatBadMTPconfigs(temperatures):

    """
    Concatenate all preselected.cfg files from the temp#/ dirs
    """

    preselectedAllFile = open(os.path.join(var.potMDsimulpath, "preselected.cfg"), "w")
    for i, temp in enumerate(temperatures):
        tempDirec = f"temp{temp}"
        preselected_path = os.path.join(var.potMDsimulpath, tempDirec, "out", "preselected.cfg")
        if os.path.exists(preselected_path):
            lines = open(preselected_path, "r").readlines()
            preselectedAllFile.writelines(lines)
    preselectedAllFile.close()

# Defunct: used to run this 'in-script' but can be v. slow, so now submit as job instead using setupMaxVolJob
#
# def applyMaxVol():
# 
#    """
#    Apply MaxVol criteria to determine which of the atomic configs in var.potMDsimulpath/preselected.cfg should be
#    relabelled
#    """
# 
#    # Copy over training set (I.e. that used to train the current iteration of potential)
#    configCfgpath = os.path.join(var.potTestpath, 'config.cfg')
#    shutil.copy2(configCfgpath, "./")
# 
#    # Identify the potential file; will be something like 'Trained.mtp_LiOZrNbLa_MTP1'
#    files = os.listdir(var.potMDsimulpath)
#    matching_files = [file for file in files if file.startswith('Trained.mtp_')]
# 
#    # Set up and run command to apply Max Vol algorithm to select configs sufficiently different to those in config.cfg
#    MLIPcommand = f'mlp select-add {matching_files[0]} config.cfg preselected.cfg new_selected.cfg' # how to point to mlp?
#    outf.printt(2, f"  About to run {MLIPcommand} in path: {var.potMDsimulpath}")
#    subprocess.run(MLIPcommand, shell=True, check=True, cwd=var.potMDsimulpath)
# 
#    return 


def setupMaxVolJob():

    """
    Set up job to apply MaxVol criteria to determine which of the atomic configs in var.potMDsimulpath/
    preselected.cfg should be relabelled
    """

    # Copy over training set (I.e. that used to train the current iteration of potential)
    configCfgpath = os.path.join(var.potTestpath, 'config.cfg')
    shutil.copy2(configCfgpath, "./")

    # Identify the potential file; will be something like 'Trained.mtp_LiOZrNbLa_MTP1'
    files = os.listdir(var.potMDsimulpath)
    matching_files = [file for file in files if file.startswith('Trained.mtp_')]

    if not matching_files:
        raise Exception("No matching 'Trained.mtp_' files found.")

    if os.path.isfile(var.dirpath + "/script_MLIP_max_vol"):
        outf.printt( 2, "  script_MLIP_max_vol job submission script present in run directory. Copying to stage "
            "sub-directories and renaming as script" )
        shutil.copy2(var.dirpath + "/script_MLIP_max_vol", "./script")

        # Read the script, replace the placeholder with the actual file name, and write it back
        with open("./script", 'r') as file:
            script_content = file.read()
        script_content = script_content.replace('Trained.mtp_', matching_files[0])
        with open("./script", 'w') as file:
            file.write(script_content)

    return


def EvFunc(x, A, B, C, D):
    y = A + B * (C - x) ** 2 + D * (C - x) ** 3
    return y


def postProcessEv(xc, mat, pot):

    # Read energies from directories for each V and ...

    init_matplotlib()

    subdirs = [
        name for name in os.listdir(var.potMDnonMDevpath + "/") if os.path.isdir(var.potMDnonMDevpath + "/" + name)
    ]
    V = []
    E = []
    for subdir in subdirs:
        V.append(float(subdir.split("N")[1]))
        logfile = open(var.potMDnonMDevpath + "/" + subdir + "/log.lammps")
        while True:
            lineReadin = logfile.readline()
            if not lineReadin:  # if line is empty end of file is reached
                break
            if "Loop time" in lineReadin:
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadinPrev)
                E.append(float(numbersFromLine[7]))
                break
            lineReadinPrev = lineReadin

    figEv, ax1 = plt.subplots()
    xcFuncForGraph = xc.lower().replace("b8", "B8").replace("pbe", "PBE")
    ax1.set_title(xcFuncForGraph + " and " + pot + " energy-volume for " + mat + " (MP-id)")

    # First DFT part
    Emin = DFTanalysis.plotEv(ax1, True)
    print("DFT Emin=", Emin)

    # Now potential part
    parameters, covariance = curve_fit(EvFunc, V, E)  # Fit curve to data
    print("parameters = ",parameters,"; 2nd deriv @ E0 = 2 B = ",2*parameters[1])
    print("potential Emin=", parameters[0])
    # ax1.plot(V, E - parameters[0] + Emin, "o", label=pot, color="Blue") # We used to rescale potential curve so it
            # matches the DFT at the minimum
    ax1.plot(V, E, "o", label=pot, color="Blue")
    ax1.legend(loc="upper center")
    VhigherDens = np.arange(V[0], V[-1] + (V[1] - V[0]), 0.01)
    # fit_E = EvFunc(VhigherDens, Emin, parameters[1], parameters[2], parameters[3])  # note Emin replacing 
            # parameters[0], since we want to rescale energy of potential plot
    fit_E = EvFunc(VhigherDens, *parameters)
    ax1.plot(VhigherDens, fit_E, "-", color="Blue")
    ax1.set_xlabel(r"Volume/atom ($\mathrm{\AA}^3$)")

    # Finish off plot
    ax1.yaxis.offsetText.set_visible(False)
    figEv.canvas.draw()  # this is necessary before we can get the offset
    offset = ax1.yaxis.get_major_formatter().get_offset()
    ax1.yaxis.set_label_text("Energy " + offset + " (eV)")
    figEv.savefig(
        var.potMDnonMDevpath + "/EvsV.png", dpi=300, bbox_inches="tight"
    )  # 'tight' is necessary to avoid ylabel cutting off
    figEv.savefig(
        var.APDoutputpath + "/" + "EvsV_" + xc + "_" + mat + "_" + pot + ".png", dpi=300, bbox_inches="tight"
    )
    plt.clf()


def postProcessElastic(xc, mat, pot):
    if var.dft_engine == "CASTEP":
        lines = open(var.potMDnonMDelasticpath + "/log.lammps", "r").readlines()
        elasticLines = []
        for line in lines:
            match = re.findall(r"^Elastic Constant.*", line)
            if match:
                elasticLines.append(match[0])
        print("elasticLines=", elasticLines)
        elasticConstFromLine = []
        for elasticLine in elasticLines:
            print("searching elasticLine=", elasticLine)
            numbers = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", elasticLine)
            elasticConstFromLine.append(float(numbers[1]))
        print("elasticConstFromLine=", elasticConstFromLine)

        # Now reorder into elasticTensor form for writing to file
        elasticTensor = np.zeros(shape=(6, 6))
        elasticTensor[0][0] = elasticConstFromLine[0]
        elasticTensor[1][1] = elasticConstFromLine[1]
        elasticTensor[2][2] = elasticConstFromLine[2]
        elasticTensor[0][1] = elasticConstFromLine[3]
        elasticTensor[0][2] = elasticConstFromLine[4]
        elasticTensor[1][2] = elasticConstFromLine[5]
        elasticTensor[3][3] = elasticConstFromLine[6]
        elasticTensor[4][4] = elasticConstFromLine[7]
        elasticTensor[5][5] = elasticConstFromLine[8]
        elasticTensor[0][3] = elasticConstFromLine[9]
        elasticTensor[0][4] = elasticConstFromLine[10]
        elasticTensor[0][5] = elasticConstFromLine[11]
        elasticTensor[1][3] = elasticConstFromLine[12]
        elasticTensor[1][4] = elasticConstFromLine[13]
        elasticTensor[1][5] = elasticConstFromLine[14]
        elasticTensor[2][3] = elasticConstFromLine[15]
        elasticTensor[2][4] = elasticConstFromLine[16]
        elasticTensor[2][5] = elasticConstFromLine[17]
        elasticTensor[3][4] = elasticConstFromLine[18]
        elasticTensor[3][5] = elasticConstFromLine[19]
        elasticTensor[4][5] = elasticConstFromLine[20]
        # some missing off diagonals (matrix should be symmetrical):
        elasticTensor[1][0] = elasticConstFromLine[3]
        elasticTensor[2][0] = elasticConstFromLine[4]
        elasticTensor[2][1] = elasticConstFromLine[5]
        print("elasticTensor=", elasticTensor)
    else:
        print("update postProcessElastic for vasp")
        quit()

    # Write elastic tensor out to file
    elasticFile = open(var.APDoutputpath + "/elasticConstants_" + xc + "_" + mat, "a")
    elasticFile.write(str(pot) + ":\n")
    for i in range(6):
        rowToWrite = []
        for j in range(6):
            rowToWrite.append(str("{:.2f}".format(elasticTensor[i][j])))
        elasticFile.write("{: >10} {: >10} {: >10} {: >10} {: >10} {: >10}".format(*rowToWrite))
        elasticFile.write("\n")
    elasticFile.close()
    # Write derived properties from elastic tensor components, e.g. bulk modulus, shear modulus, etc, as well as some specific tensor components
    elasticFile = open(var.APDoutputpath + "/elasticDerived_" + xc + "_" + mat, "a")
    C11 = elasticTensor[0][0]
    C12 = elasticTensor[0][1]
    C44 = elasticTensor[3][3]
    B = (C11 + 2 * C12) / 3
    G1 = C44
    G2 = (C11 - C12) / 2
    rowToWrite = [
        str(pot) + ":",
        str("{:.2f}".format(C11)),
        str("{:.2f}".format(C12)),
        str("{:.2f}".format(C44)),
        str("{:.2f}".format(B)),
        str("{:.2f}".format(G1)),
        str("{:.2f}".format(G2)),
    ]
    elasticFile.write("{: >10} {: >10} {: >10} {: >10} {: >10} {: >10} {: >10}".format(*rowToWrite))
    elasticFile.write("\n")
    elasticFile.close()


def postAnalyzePotMD(xcFunc_potSim):

    # Post-analysis on thermal expansion output if available

    init_matplotlib()

    outf.printt(
        1,
        "Averaging thermal expansions in: "
        + var.potMDdir
        + "/{xc}/{mat}/{pot}/8_simulation/stageOutput over {pot} and recording in: "
        + var.potMDdir
        + "/{xc}/{mat}/thermalExpansion_pot for each xc and mat:",
    )
    for mat in var.materials:
        for pot_type in var.pot_types:  # consider EAM and RF-MEAM separately
            noNewJobsCompleted = True
            thermalExpansion_potFiles = (
                []
            )  # Store filenames of thermalExpansion_pot files that exist so that we can accumulate these in a single file later
            thermalExpansionRel_potFiles = []
            xc_potFiles = []  # ...Store the xc of these filenames alongside
            pot_final_per_xc = [None] * len(var.xcFunc)
            for xcNum, xc in enumerate(var.xcFunc):  # consider each xc separately
                outf.printt(2, "  mat=" + mat + ", pot_type=" + pot_type + ", xc=" + xc)

                # May have potentials with different numbers of reoptimizations. E.g. EAM1, EAM2, EAM2B, EAM2C, EAM3, EAM3B. We only want EAM1, EAM2B and EAM3B
                # Store these in pot_final[#] where # is the potential number, e.g. 1 in EAM1
                # (We do this even if the thermal expansion file averaged over the potentials is present, since we need pot_final_per_xc for copying 'final' potentials later.)
                pot_final = [None] * var.num_pots
                outf.printt(
                    2,
                    "  Searching for sublists containing "
                    + xc
                    + " within the list xcFunc_potSim="
                    + str(xcFunc_potSim),
                )
                for xc2, pot in xcFunc_potSim:
                    idx = re.search(r"\d", pot)  # index of first digit in string 'pot'
                    pot_type_tmp = pot[
                        0 : idx.start()
                    ]  # e.g. MEAM2B -> MEAM. To check against pot_type to see if should be considered.
                    if (
                        xc == xc2 and pot_type == pot_type_tmp
                    ):  #''.join([i for i in pot if not i.isdigit()]): # the second part here removes numbers from pot so that e.g. EAM1 becomes EAM
                        outf.printt(2, "  ... found xc=" + xc2 + " pot=" + pot)
                        if pot[-1].isnumeric():
                            pot_final[int(pot[-1]) - 1] = pot  # e.g if pot=EAM3, pot_final[2]='EAM3'
                            outf.printt(2, "  storing pot_final[" + str(int(pot[-1]) - 1) + "]=" + pot)
                        for c in ascii_uppercase:
                            if pot[-1] == c:
                                pot_final[int(pot[-2]) - 1] = pot  # e.g. if pot=EAM2C, pot_final[1]='EAM2C'
                                outf.printt(
                                    2,
                                    "  pot[-1]="
                                    + pot[-1]
                                    + ": storing pot_final["
                                    + str(int(pot[-2]) - 1)
                                    + "]="
                                    + pot,
                                )
                pot_final_per_xc[
                    xcNum
                ] = pot_final  # need to store this 'per xc' so we can copy the potentials to the output dir later on for the best performing xc
                outf.printt(2, "  Potentials to consider:" + str(pot_final))

                # Check if thermal expansion file/s is already present
                if os.path.isfile(var.potMDpath + "/" + xc + "/" + mat + "/thermalExpansion_" + pot_type) == True:
                    outf.printt(
                        1,
                        "  "
                        + var.potMDdir
                        + "/"
                        + xc
                        + "/"
                        + mat
                        + "/thermalExpansion_"
                        + pot_type
                        + " already exists. (Please remove it if you wish to recalculate it.)",
                    )
                    thermalExpansion_potFiles.append(
                        var.potMDpath + "/" + xc + "/" + mat + "/thermalExpansion_" + pot_type
                    )
                    thermalExpansionRel_potFiles.append(
                        var.potMDpath + "/" + xc + "/" + mat + "/thermalExpansionRel_" + pot_type
                    )
                    xc_potFiles.append(xc)
                else:
                    # Check these potentials have completed their MD
                    allCompleted = True
                    # e.g. directory: APDworkdir/potMD/pbe/mp-22862/EAM1/8_simulation/stageOutput
                    for pot in pot_final:
                        if pot == None:
                            allCompleted = False
                        else:
                            outf.printt(
                                2,
                                "  Checking for file: "
                                + str(var.potMDdir)
                                + "/"
                                + str(xc)
                                + "/"
                                + str(mat)
                                + "/"
                                + str(pot)
                                + "/8_simulation/stageOutput...",
                            )
                            if (
                                not os.path.isfile(
                                    var.potMDpath + "/" + xc + "/" + mat + "/" + pot + "/8_simulation/stageOutput"
                                )
                                == True
                            ):
                                if (
                                    os.path.isfile(
                                        var.potMDpath + "/" + xc + "/" + mat + "/" + pot + "/8_simulation/stageFailed"
                                    )
                                    == True
                                ):
                                    outf.printt(
                                        2,
                                        "  ...not present (this stage failed and staged single point energy calculations)",
                                    )
                                else:
                                    outf.printt(2, "  ...not present")
                                allCompleted = False
                    outf.printt(2, "  Simulations for all potentials completed?: " + str(allCompleted))

                    if allCompleted == True:
                        noNewJobsCompleted = False
                        outf.printt(
                            1,
                            "  Writing file: " + var.potMDdir + "/" + xc + "/" + mat + "/thermalExpansion_" + pot_type,
                        )
                        outf.printt(
                            1,
                            "  Writing file: "
                            + var.potMDdir
                            + "/"
                            + xc
                            + "/"
                            + mat
                            + "/thermalExpansionRel_"
                            + pot_type,
                        )  # relative to T=0K lattice parameter or density
                        thermExpnAvrg = open(
                            var.potMDpath + "/" + xc + "/" + mat + "/thermalExpansion_" + pot_type, "w"
                        )
                        thermExpnRelAvrg = open(
                            var.potMDpath + "/" + xc + "/" + mat + "/thermalExpansionRel_" + pot_type, "w"
                        )
                        if var.thermal_expansion_units == "gcm-3":
                            thermExpnAvrg.write(
                                "temp (K) | calculated mean density (gcm-3) | s.d. on mean calc dens (gcm-3)\n"
                            )
                            thermExpnRelAvrg.write(
                                "temp (K) | calculated mean density relative to 0 K (gcm-3) | s.d. on mean calc dens (gcm-3)\n"
                            )
                        elif var.thermal_expansion_units == "Ang":
                            thermExpnAvrg.write(
                                "temp (K) | calculated mean lattice para (Ang) | s.d. on mean lattice para (Ang)\n"
                            )
                            thermExpnRelAvrg.write(
                                "temp (K) | calculated mean lattice para relative to 0 K (Ang) | s.d. on mean lattice para (Ang)\n"
                            )
                        else:
                            print("unsupported thermal expansion units, stopping")
                            quit()

                        #  Following is deprecated. Originally I thought to take relative thermal expansion w.r.t
                        #  0K. But the experimental data is taken w.r.t 273 K so I do that instead.
                        #  # Read in T=0 K optimized geometries to calculate the relative values of thermal expansion as well
                        #  if var.thermal_expansion_units == "Ang":
                        #     geom0Kpot=[]
                        #     for pot in pot_final:
                        #        geom0KpotFile=open(var.APDoutputpath+'/geometry_'+xc+'_'+mat,'r')
                        #        found=False
                        #        while True:
                        #           lineReadin=geom0KpotFile.readline()
                        #           if re.match(pot+':', lineReadin): # this ensures that pot is at start of line (so e.g. searching for 'EAM1' will not result in a positive match if lineReadin contains 'MEAM1' at the start of the line)
                        #              lineReadin=geom0KpotFile.readline()
                        #              p = re.compile(r"[-+]?\d*\.\d+|\d+")  # Compile a pattern to capture float values
                        #              numbersFromLine = [float(i) for i in p.findall(lineReadin)]  # Convert strings to float
                        #              geom0Kpot.append(float(numbersFromLine[0]))
                        #              found=True
                        #           if not lineReadin:
                        #              break
                        #        if found==False:
                        #           print('ERROR: unable to find relaxed geometry information in '+var.APDoutputpath+'/geometry_'+xc+'_'+mat+' for potential: ',pot,' STOPPING')
                        #           quit()
                        #     print('geom0Kpot=',geom0Kpot)
                        #  else:
                        #     print('please update code for relative therm expn of liquids')
                        #     quit()

                        # print('determining geom273Kpot values')
                        if var.thermal_expansion_units == "Ang":
                            geom273Kpot = []
                            for ipot, pot in enumerate(pot_final):
                                # print('pot=',pot)
                                tempReadin = []
                                lengthReadin = []  # arrays defined over number of potentials
                                thermExpn = open(
                                    var.potMDpath + "/" + xc + "/" + mat + "/" + pot + "/8_simulation/stageOutput", "r"
                                )
                                # print('opened file:',var.potMDpath+'/'+xc+'/'+mat+'/'+pot+'/8_simulation/stageOutput')
                                lineReadin = thermExpn.readline()
                                lineReadin = thermExpn.readline()
                                while True:
                                    lineReadin = thermExpn.readline()
                                    if not lineReadin:
                                        break
                                    lineSplit = lineReadin.split()
                                    tempReadin.append(float(lineSplit[0]))
                                    lengthReadin.append(float(lineSplit[5]))
                                # We now interpolate to find the 273 K value. Note that this will need generalizing
                                # for case where 273 K is not included within simulated values (we will need to add
                                # in the 0 K value to allow interpolation)
                                tempReadin = np.array(tempReadin)
                                lengthReadin = np.array(lengthReadin)
                                f_linear = interp1d(tempReadin, lengthReadin)
                                geom273Kpot.append(float(f_linear(273)))

                            print("geom273Kpot=", geom273Kpot)
                        else:
                            print("please update code for relative therm expn of liquids")
                            quit()

                        tempIdx = 0
                        finished = False
                        while True:  # loop over temperatures
                            calcDens = [None] * len(
                                pot_final
                            )  # this is the calculated density (for liquids) or lattice parameter (for solids)
                            calcDensRel = [None] * len(
                                pot_final
                            )  # this is the calculated density (for liquids) or lattice parameter (for solids)
                            for ipot, pot in enumerate(pot_final):
                                thermExpn = open(
                                    var.potMDpath + "/" + xc + "/" + mat + "/" + pot + "/8_simulation/stageOutput", "r"
                                )
                                lineReadin = thermExpn.readline()
                                lineReadin = thermExpn.readline()
                                lineNum = 0
                                while lineNum <= tempIdx:
                                    lineReadin = thermExpn.readline()
                                    if not lineReadin:
                                        finished = True
                                    lineNum = lineNum + 1
                                if finished:
                                    break
                                lineSplit = lineReadin.split()
                                temp = int(lineSplit[0])
                                calcDens[ipot] = float(lineSplit[5])
                                calcDensRel[ipot] = calcDens[ipot] - geom273Kpot[ipot]
                            if finished:
                                break
                            avrgCalcDens = np.average(calcDens[:])
                            avrgCalcDensRel = np.average(calcDensRel[:])
                            sdOfMeanDens = np.std(calcDens[:]) / np.sqrt(np.size(calcDens[:]))
                            sdOfMeanDensRel = np.std(calcDensRel[:]) / np.sqrt(np.size(calcDensRel[:]))
                            print(
                                "  Average density/alatt for temp ",
                                temp,
                                "K = ",
                                avrgCalcDens,
                                ", sd on meam = ",
                                sdOfMeanDens,
                            )
                            print(
                                "  Average density/alatt - 0K value for temp ",
                                temp,
                                "K = ",
                                avrgCalcDensRel,
                                ", sd on meam = ",
                                sdOfMeanDensRel,
                            )
                            tempIdx = tempIdx + 1
                            thermExpnAvrg.write(str(temp) + " " + str(avrgCalcDens) + " " + str(sdOfMeanDens) + "\n")
                            thermExpnRelAvrg.write(
                                str(temp) + " " + str(avrgCalcDensRel) + " " + str(sdOfMeanDensRel) + "\n"
                            )
                        thermExpnAvrg.close()
                        thermExpnRelAvrg.close()
                        thermalExpansion_potFiles.append(
                            var.potMDpath + "/" + xc + "/" + mat + "/thermalExpansion_" + pot_type
                        )
                        thermalExpansionRel_potFiles.append(
                            var.potMDpath + "/" + xc + "/" + mat + "/thermalExpansionRel_" + pot_type
                        )
                        xc_potFiles.append(xc)

            if noNewJobsCompleted == True:
                outf.printt(
                    1,
                    "  No new jobs in " + var.potMDdir + "/{xc}/" + mat + "/" + pot_type + "/8_simulation/ to process",
                )

            if not thermalExpansion_potFiles:
                outf.printt(
                    1, "  No completed jobs in " + var.potMDdir + "/{xc}/" + mat + "/" + pot_type + "/8_simulation/"
                )
            else:
                # Combined thermalExpansion_{pot} files into one file for that xc and mat, in the job directory. Also plot it with pyplot
                # Do first for the absolute value thermal expansions, then for the relative values.

                # ---- Absolute values: ----
                plt.title("Thermal expansion of " + mat + " (MP-id) from potential MD")
                plt.xlabel("Temperature (K)")
                plt.ylabel(r"Lattice parameter ($\mathrm{\AA}$)")
                thermExpnAvrgCombined = open(var.potMDpath + "/thermalExpansion_" + mat + "_" + pot_type, "w")
                rmsError = [0] * len(
                    thermalExpansion_potFiles
                )  # determine the rms error of computed vs exp therm expn for each xc-functional
                for index, thermalExpansion_potFile in enumerate(thermalExpansion_potFiles):
                    thermExpnAvrgCombined.write('"' + xc_potFiles[index] + '"\n')
                    thermExpnAvrg = open(thermalExpansion_potFile, "r")
                    line = thermExpnAvrg.readline()
                    temps = []
                    expThermExpns = []
                    predThermExpn = []
                    predThermExpnErr = []
                    while True:
                        line = thermExpnAvrg.readline()
                        if not line:
                            break
                        else:
                            p = re.compile(r"[-+]?\d*\.\d+|\d+")  # Compile a pattern to capture float values
                            numbersFromLine = [float(i) for i in p.findall(line)]  # Convert strings to float
                            thermExpnAvrgCombined.write(
                                str(numbersFromLine[0])
                                + " "
                                + str(numbersFromLine[1])
                                + " "
                                + str(numbersFromLine[2])
                                + "\n"
                            )
                            temps.append(numbersFromLine[0])
                            predThermExpn.append(
                                numbersFromLine[1]
                            )  # will have to adjust for other potential types...
                            predThermExpnErr.append(
                                numbersFromLine[2]
                            )  # will have to adjust for other potential types...
                    temps = np.array(temps)
                    predThermExpn = np.array(predThermExpn)
                    predThermExpnErr = np.array(predThermExpnErr)
                    plt.plot(temps, predThermExpn, color="blue", alpha=0.5, label="_nolegend_")
                    plt.fill_between(
                        temps, predThermExpn - predThermExpnErr, predThermExpn + predThermExpnErr, label=pot_type
                    )

                # Add experimental (user provided) data:
                for iSet, thermal_expansion_set in enumerate(var.thermal_expansion):
                    if var.thermal_expansion_names != None:
                        thermExpnAvrgCombined.write('\n\n"' + var.thermal_expansion_names[iSet] + '"\n')
                        expLabel = var.thermal_expansion_names[iSet]
                    else:
                        thermExpnAvrgCombined.write('\n"Experimental dataset: ' + str(iSet) + '"\n')
                        expLabel = "Experimental dataset: " + str(iSet)
                    tempExp = []
                    thermalExpnExp = []
                    if iSet == 0:
                        color = "red"
                    elif iSet == 1:
                        color = "green"
                    elif iSet == 2:
                        color = "black"
                    elif iSet == 3:
                        color = "orange"
                    else:
                        print("ERROR: augment colors for plotting")
                        quit()
                    for tempThermExpnPair in thermal_expansion_set:
                        thermExpnAvrgCombined.write(
                            str(tempThermExpnPair[0]) + " " + str(tempThermExpnPair[1]) + " 0 \n"
                        )
                        tempExp.append(tempThermExpnPair[0])
                        thermalExpnExp.append(tempThermExpnPair[1])
                    plt.plot(
                        tempExp, thermalExpnExp, color=color, linestyle=":", marker="o", label=expLabel
                    )  # linewidth=1.0)

                plt.legend()

                # Save figure
                if os.path.exists(var.APDoutputpath) == False:
                    filesys.createWorkdir(var.APDoutputdir)
                # os.system('rm '+var.APDoutputpath+'/*') # Make sure we clean the directory first... (since we will also be saving potentials as well, and this is important esp if we are putting potentials from a new xc there)
                plt.savefig(var.APDoutputpath + "/" + "thermalExpansion.png", dpi=300)
                plt.clf()

                # ---- Relative values: ----
                plt.title("Relative thermal expansion of " + mat + " (MP-id) from potential MD")
                plt.xlabel("Temperature ($^{\circ}$C)")  # degrees C
                # plt.xlabel('Temperature (K)')
                plt.ylabel(r"Delta Lattice parameter ($\mathrm{\AA}$)")
                thermExpnRelAvrgCombined = open(var.potMDpath + "/thermalExpansionRel_" + mat + "_" + pot_type, "w")
                rmsError = [0] * len(
                    thermalExpansionRel_potFiles
                )  # determine the rms error of computed vs exp therm expn for each xc-functional
                for index, thermalExpansionRel_potFile in enumerate(thermalExpansionRel_potFiles):
                    thermExpnRelAvrgCombined.write('"' + xc_potFiles[index] + '"\n')
                    thermExpnRelAvrg = open(thermalExpansionRel_potFile, "r")
                    line = thermExpnRelAvrg.readline()
                    temps = []
                    expThermExpns = []
                    predThermExpn = []
                    predThermExpnErr = []
                    while True:
                        line = thermExpnRelAvrg.readline()
                        if not line:
                            break
                        else:
                            p = re.compile(r"[-+]?\d*\.\d+|\d+")  # Compile a pattern to capture float values
                            numbersFromLine = [float(i) for i in p.findall(line)]  # Convert strings to float
                            thermExpnRelAvrgCombined.write(
                                str(numbersFromLine[0])
                                + " "
                                + str(numbersFromLine[1])
                                + " "
                                + str(numbersFromLine[2])
                                + "\n"
                            )
                            temps.append(numbersFromLine[0] - 273)  # degrees C
                            predThermExpn.append(
                                numbersFromLine[1]
                            )  # will have to adjust for other potential types...
                            predThermExpnErr.append(
                                numbersFromLine[2]
                            )  # will have to adjust for other potential types...
                    temps = np.array(temps)
                    predThermExpn = np.array(predThermExpn)
                    predThermExpnErr = np.array(predThermExpnErr)
                    plt.plot(temps, predThermExpn, color="blue", alpha=0.5, label="_nolegend_")
                    plt.fill_between(
                        temps, predThermExpn - predThermExpnErr, predThermExpn + predThermExpnErr, label=pot_type
                    )

                # Add experimental (user provided) data:
                for iSet, thermal_expansion_set in enumerate(var.thermal_expansion):
                    if var.thermal_expansion_names != None:
                        thermExpnRelAvrgCombined.write('\n\n"' + var.thermal_expansion_names[iSet] + '"\n')
                        expLabel = var.thermal_expansion_names[iSet]
                    else:
                        thermExpnRelAvrgCombined.write('\n"Experimental dataset: ' + str(iSet) + '"\n')
                        expLabel = "Experimental dataset: " + str(iSet)
                    tempExp = []
                    thermalExpnExp = []
                    if iSet == 0:
                        color = "red"
                        marker = "o"
                    elif iSet == 1:
                        color = "green"
                        marker = "^"
                    elif iSet == 2:
                        color = "black"
                        marker = "s"
                    elif iSet == 3:
                        color = "orange"
                        marker = "d"
                    else:
                        print("ERROR: augment colors for plotting")
                        quit()
                    for tempThermExpnPair in thermal_expansion_set:
                        print("iSet=", iSet, ", var.thermal_expansion_273K=", var.thermal_expansion_273K)
                        thermExpnRelAvrgCombined.write(
                            str(tempThermExpnPair[0])
                            + " "
                            + str(tempThermExpnPair[1] - var.thermal_expansion_273K[iSet])
                            + " 0 \n"
                        )
                        tempExp.append(tempThermExpnPair[0] - 273)  # degrees C
                        thermalExpnExp.append(tempThermExpnPair[1] - var.thermal_expansion_273K[iSet])
                    plt.plot(tempExp, thermalExpnExp, color=color, linestyle=":", marker=marker, label=expLabel)

                plt.legend(loc="upper left")  # (legend)
                xlimBefore = plt.xlim()
                plt.xlim(0, xlimBefore[1])  # used: plt.xlim(0,1125) to tune the plot for the paper
                plt.ylim(
                    0, 0.06
                )  # plt.ylim(auto=True) < -- should have something like this but for now use the fix shown here to get plot right

                # Save figure
                plt.savefig(var.APDoutputpath + "/" + "thermalExpansionRel.png", dpi=300)
                plt.clf()

                # Save potentials (NOT FINISHED)
                # ---
                for (
                    mat
                ) in (
                    var.materials
                ):  # we draw from potMD dirs rather than pot dirs because ordering of species in former is consistent with MP queried input files. Once we support multiple materials we may be copying back potential files corresponding to different combinations of the full range of species.
                    for pot_type in var.pot_types:  # consider EAM and RF-MEAM separately
                        for iXc, xc in enumerate(var.xcFunc):  # consider each xc separately
                            for pot in pot_final_per_xc[iXc]:  # i.e.: pot[0:2]
                                outf.printt(
                                    1,
                                    "  Copying potentials located at "
                                    + var.potMDdir
                                    + "/"
                                    + xc
                                    + "/"
                                    + mat
                                    + "/"
                                    + pot
                                    + "/8_simulation/",
                                )
                                for potFile in glob.glob(
                                    os.path.join(
                                        var.potMDpath
                                        + "/"
                                        + xc
                                        + "/"
                                        + mat
                                        + "/"
                                        + pot
                                        + "/8_simulation/*.eam.alloy_*"
                                    )
                                ):
                                    # Find the composition part (e.g. NaCl)
                                    filename = os.path.basename(potFile)
                                    composition = re.sub(r"\.(.*?)$", "", filename)  # e.g. NaCl
                                    newPotFile = composition + ".eam.alloy_" + xc + "_" + pot
                                    outf.printt(
                                        2, "  Copying " + potFile + " to " + var.APDoutputdir + "/" + newPotFile
                                    )
                                    shutil.copy2(potFile, var.APDoutputpath + "/" + newPotFile)
                # ---

                #  outf.printt(1,'  Copying potentials located at '+var.potdir+'/'+xc_potFiles[idx]+'/{'+str(pot_final_per_xc[idx])+'}/6_potOpt to '+var.APDoutputdir)
                #  for pot in pot_final_per_xc[idx]:
                #     # Copy potentials (use wildcards rather than species from Pymatgen's 'symbol_set' because ordering of chem symbols in MEAMfit output depends on order in POTCAR
                #     #print('pot=',pot,', glob.glob(os.path.join('+var.potpath+'/'+xc_potFiles[idx]+'/'+pot+'/6_potOpt/*.eam.alloy_1)= '+str(glob.glob(os.path.join(var.potdir+'/'+xc_potFiles[idx]+'/'+pot+'/6_potOpt/*.eam.alloy_1'))))
                #     for potFile in glob.glob(os.path.join(var.potpath+'/'+xc_potFiles[idx]+'/'+pot+'/6_potOpt/*.eam.alloy_1')):
                #        # Find the composition part (e.g. NaCl)
                #        filename=os.path.basename(potFile)
                #        #print('filename=',filename)
                #        composition=re.sub(r'\.(.*?)$', '', filename) # e.g. NaCl
                #        newPotFile=composition+'.eam.alloy_'+xc_potFiles[idx]+'_'+pot
                #        outf.printt(2,'  Copying '+potFile+' to '+var.APDoutputdir+'/'+newPotFile)
                #        shutil.copy2(potFile,var.APDoutputpath+'/'+newPotFile)
                #
                #        # os.system('mv *.eam.alloy_1 *.eam.alloy_'+xc_potFiles[idx]+'_'+pot)
