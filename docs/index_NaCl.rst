..
.. NaCl worked example, to add later
.. ---------------------------------
..
.. Here we present a more indepth example which also explains a little more of the under-pinning algorithms. This example shows how to fit a potential to describe NaCl molten salt. For more detailed explanation see :ref:`Workflow` and :ref:`settings tags`. First ensure you have completed all the items in :ref:`Setup`. Now set up a new directory and create a ``settings.APD`` file containing:
..
.. .. code-block::
..        :raw:`[APD]`
..        mp_id = mp-22862
..        MAPI_KEY = # 
..        temp_range = 1000,2000
..        auto_select_temp = False
..        pot_types = EAM
..
.. You will need to obtain a ``MAPI_KEY`` from Materials Project and insert it at ``#``. Here we are presupposing that we already know the temperature range of interest (1000-2000 K) and so are focusing the potential fitting efforts exclusively in this range (``auto_select_temp = False`` to avoid auto-scanning other temperatures). In the present case this temperature range corresponds to liquid NaCl. Out of the two currently implemented potential types, EAM and RF-MEAM, we choose EAM, as we do not expect strong angular forces. 
..
.. Run ``python path/APD.py`` where ``path`` is the path of your APD python executable. This will run 'stage 1', which sets up the directory structure and DFT input files based on the contents of the ``settings.APD`` file. It also sets up 'stage 2' DFT jobs to determine DFT MD parameters (for Langevin dynamics).  Please run the DFT simulations yourself, using your own submission scripts or using/adapting scripts from ``jobScriptExamples``. If you are using SLURM scripts and APD detects ``#SBATCH -n num``, where ``num`` is the number of cores, then APD will automatically add an appropriate NPAR value to the INCAR file. If not you will need to add this parameter yourself.
..
.. Once DFT jobs have completed rerun APD to analyze the results of the DFT MD simulations and determine values of ``POTIM`` and ``LANGEVIN_GAMMA``. APD will prompt you to examine the results if necessary and then run the code again. If you want to check these values you can plot the computed values of *{temperature}* against the Langevin parameters by running ``APD/tools/gnu-plotting/plot_2_MDparas.gp`` in gnuplot in the stage 2 directory (``APDworkdir/DFT/pbe/mp-22862/2_MDparas/``). Check that the APD-determined Langevin parameters yield a *{temperature}* close to 300 K.  
..   
.. Run APD again to set up new 'stage 3' jobs to perform NPT simulations. Run these jobs. Once the jobs are completed APD can be rerun. If there are insufficient atomic configurations from the DFT runs you will be notified and asked to run further jobs (c.f. :ref:`Stage 3: NPT jobs` for details on continuation jobs). Once there are sufficient configurations for each temperature APD will analyze the results and determine the averaged lattice parameters in each case.  
..
.. Run APD again to set up 'stage 4', which will consist of NVT jobs for each temperature at each of the lattice parameters determined in stage 3. Run these jobs. Once the NVT jobs are completed, rerun APD, and you will be prompted regarding adding a tag ``sep_coulomb_pot=True`` to your settings.APD file. Do this and rerun APD. Next you will need to copy some files from the MEAMfit ``src/`` directory to your job directory - APD will prompt you.  Please do this, re-run APD again to set up 'stage 5' potential fitting directories, and then run the new jobs.
..
.. Once the potential optimizations are completed, run APD again. It should finish with the line ``APD finished: no jobs need to run by user``. If there are no higher-level observables specified in the settings file, APD will finish here. However, if observables are supplied APD will attempt to find the best xc-functional to reproduce these observables. To demonstrate this include in the settings file the lines:
..
.. .. code-block::
..
..        # Experimental properties
..        thermal_expansion = [[1149,1.516],[1162,1.503],[1202,1.486],[1208,1.483],[1404,1.388],[1459,1.367],[1502,1.347],[1590,1.305],[1552,1.323],[1573,1.312],[1670,1.267]]
..        thermal_expansion_units = gcm-3
..
.. and rerun APD. LAMMPS runs will be set up. Run these and rerun APD once they are completed.
.. 
.. APD will analyze the LAMMPS run. If there was a problem with any of the runs it will propose new DFT jobs in an attempt to correct the problem. In this case please proceed by running APD and submitting jobs as you have so far, paying attention to the actions required by APD in the ``toAction.APD`` file. Instead, if all LAMMPS simulations completed successfully, APD will calculate the thermal expansion and compare against experiment. In this case you can plot the thermal expansion and observe the discrepency with the experimental data. You can use ``tools/gnu-plotting/plot_8_thermalExpansion_per_xc.gp`` for this purpose, running this script on the ``APDworkdir/potMD/pbe/mp-22862/thermalExpansion_EAM`` datafile. Note that this datafile will only be generated when *all three* potentials have been successfully optimized.
..
.. To obtain a better agreement let us fit some new potentials using different xc-functionals. Add 
.. 
.. ``xc_func = pbe,revpbe,optpbe,optb88,optb86b``
.. 
.. to the settings file and rerun APD.  Note that since these xc-functionals (except pbe) correspond to different van der Waals correction schemes, it will be necessary to add the ``vdw_kernel.bindat`` file to the job directory. Repeat the steps as outlined previously until you have completed all stages for all xc-functionals, or until you are happy you have found one that provides good agreement with experiment. Note that for the new xc-functionals, stage 2 'completes' without requiring DFT jobs to determine Langevin parameters. Instead these are taken from the analysis performed for pbe. After stage 2, rerun APD to progress to stage 3. You can use ``tools/gnu-plotting/plot_8_thermalExpansion.gp`` to visualize the agreement of all xc-functionals simultaneously with experiment, and should find output similar to that included below. You should find that optpbe provides the best agreement with experiment.
..
.. .. image:: thermalExpansion.png

