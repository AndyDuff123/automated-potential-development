# run in: automated-potential-development/
git rm -rf public/
mkdir public
cd docs/
make clean
make html
cd ..
cp -r docs/buildDocs/html/* public/
git add public/
git commit -m "updated docs" public/
git push
