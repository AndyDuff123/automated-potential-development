#
# Copyright (C) 2020-2023, Dr Andrew Ian Duff,
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import os
import outf

# Modules:
import var  # variable container


def pathToDir(path):
    # This converts path to directory. It is currently not used. Can consider this instead of having to define all the paths and all the directories.
    # (Alternatively could just store directories and than use APDpath+directory for the paths.)

    directory = path[path.index(var.APDworkdir) :]
    pathReduced = ""
    return pathReduced


def createWorkdir(direc):
    # Creates directory 'direc' in APDworkdir/

    # fullDirec=var.APDworkdir+'/'+direc
    outf.printt(2, "  Creating: " + str(direc))
    path = var.dirpath + "/" + direc
    os.makedirs(path)#, exist_ok = True)


def recordTemps(userTempLower, userTempUpper, autoTemp1, autoTemp2, autoTemp3, corrTimeVsTemp):
    # Record temperatures to be used for production DFT run to 4_configsDFTNPTpath/stageOutput. If autoTemp1 = 0 omit autoTemp1-3 (happens if
    # 'auto_select_temp = False' in settings.). Also record the correlation time if it not 'None' (if it is None it is because 'NVT' in config_gen,
    # so that we haven't removed correlated snapshots from the NPT run. We will do so instead in the NVT run and report in the stageOutput file there.)

    directoriesFile = open(var.DFTconfigsDFTNPTpath + "/stageOutput", "w")
    directoriesFile.write("Temperatures for stage 4_configs:\n")
    if userTempUpper == 0:
        directoriesFile.write(str(userTempLower) + "\n")
    else:
        directoriesFile.write(str(userTempLower + int(1 / 3 * (userTempUpper - userTempLower))) + "\n")
        directoriesFile.write(str(userTempLower + int(2 / 3 * (userTempUpper - userTempLower))) + "\n")
        directoriesFile.write(str(userTempUpper) + "\n")
    if autoTemp1 > 0:
        directoriesFile.write(str(autoTemp1) + "\n")
        directoriesFile.write(str(autoTemp2) + "\n")
        directoriesFile.write(str(autoTemp3) + "\n")
    if corrTimeVsTemp != None:
        directoriesFile.write("Correlation time per temp: " + str(corrTimeVsTemp) + "\n")
    directoriesFile.close()


def create_backup_name(file, path):

    # Function to create a new backup name

    base_name = "failed"
    counter = 1
    new_name = base_name + str(counter) + "_" + file
    while os.path.exists(os.path.join(path, new_name)):
        counter += 1
        new_name = base_name + str(counter) + "_" + file

    return new_name
