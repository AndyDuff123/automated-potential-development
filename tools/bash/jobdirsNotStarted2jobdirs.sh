mapfile -t jobdirs_notStarted_array < jobdirs_notStarted
$APDdir/tools/bash/jobDirs.sh > queueOutput
echo "Comparing jobs in queue with those in file: jobdirs_notStarted"
for jobdir_notStarted in ${jobdirs_notStarted_array[@]}; do 
   jobdir_notStarted_orig=$jobdir_notStarted
   jobdir_notStarted=`echo $jobdir_notStarted | tr -s '\/'` # remove excess /'s to make comparison with queue
   if [ `grep -c $jobdir_notStarted$ queueOutput` == 0 ]; then 
      echo $jobdir_notStarted" from jobdirs_notStarted not present in queue - adding to jobdirs file"
      echo $jobdir_notStarted >> jobdirs; 
      sed -i "\%$jobdir_notStarted_orig%d" jobdirs_notStarted; # '_orig' to ensure original formatting (i.e. '//')
   fi; 
done
echo
echo "Jobs in jobdirs_notStarted already in queue:"
cat jobdirs_notStarted
echo
if [ -f jobdirs ]; then
   echo "Total list of jobs now to be submitted (contents of jobdirs file):"
   echo
   cat jobdirs
else
   echo "No jobs to submit (no jobdirs file)"
fi
