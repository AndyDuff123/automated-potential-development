#!/bin/bash
#
# Given POSCAR-001, etc files, creates directories and copies through POSCAR-001 to
# disp-001/POSCAR, etc, as well as KPOINTS, INCAR, POTCAR and input
#
# Andrew Duff

ls castep_cij__*.cell > cell_files
sed -i 's/castep\_cij\_\_//g' cell_files
sed -i 's/\.cell//g' cell_files

while read DIR; do 

   cp cell$DIR/castep.castep ./castep_cij__${DIR}.castep

done < cell_files
rm cell_files

