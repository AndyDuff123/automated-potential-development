#!/bin/bash

# Get the list of running jobs and their submission directories
bjobs -noheader -o "jobid stat exec_cwd" | while read -r jobid status exec_cwd; do
    if [ "$status" == "RUN" ] && [ -n "$exec_cwd" ]; then
        echo "$exec_cwd"
    fi
done

