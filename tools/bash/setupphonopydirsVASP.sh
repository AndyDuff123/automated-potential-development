#!/bin/bash
#
# Given POSCAR-001, etc files, creates directories and copies through POSCAR-001 to
# disp-001/POSCAR, etc, as well as KPOINTS, INCAR, POTCAR and input
#
# Andrew Duff
set -e  # exit if any command fails

ls POSCAR-* > POSCARfiles || { echo "ls command failed"; exit 1; }
sed -i 's/POSCAR//g' POSCARfiles || { echo "sed command failed"; exit 1; }
n_indep_disp=$(wc -l < POSCARfiles) || { echo "wc command failed"; exit 1; }
echo $n_indep_disp

while read DIR; do
    mkdir disp$DIR || { echo "mkdir failed for dir: disp$DIR"; exit 1; }
    mv POSCAR$DIR disp$DIR/POSCAR || { echo "mv command failed"; exit 1; }
    cp {POTCAR,KPOINTS,INCAR} disp$DIR/ || { echo "cp command failed"; exit 1; }
    if [ -f script ]; then
        cp script disp$DIR/ # Absence of submission script does not cause a fail...
    fi
done < POSCARfiles
rm POSCARfiles || { echo "rm command failed"; exit 1; }
