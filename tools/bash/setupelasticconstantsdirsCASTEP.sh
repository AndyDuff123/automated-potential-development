#!/bin/bash
#
# Given POSCAR-001, etc files, creates directories and copies through POSCAR-001 to
# disp-001/POSCAR, etc, as well as KPOINTS, INCAR, POTCAR and input
#
# Andrew Duff

ls castep_cij__*.cell > cell_files
sed -i 's/castep\_cij\_\_//g' cell_files
sed -i 's/\.cell//g' cell_files

while read DIR; do 

   mkdir cell$DIR
   # First remove the additional 'FIX_ALL_CELL true' added by generate_strain.py (we already have one of these in the
   # form 'FIX_ALL_CELL: TRUE' due to APD and having another causes an error when CASTEP is run).
   sed -i '/FIX_ALL_CELL true/d' castep_cij__${DIR}.cell
   cp castep_cij__${DIR}.cell cell$DIR/castep.cell
   cp castep_cij__${DIR}.param cell$DIR/castep.param
   cp script cell$DIR/

   # Check if any .recpot files exist and copy them if they do
   for recpot_file in *.recpot; do
      [ -e "$recpot_file" ] && cp "$recpot_file" cell$DIR/
   done

done < cell_files
rm cell_files

