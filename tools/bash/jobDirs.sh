#!/bin/bash

## Old method: v. slow when there are lots of jobs
#echo "Starting old method..."
# Get the total number of jobs for progress tracking
#total_jobs=$(squeue -u scarf781 | wc -l)
#total_jobs=$((total_jobs - 1))  # Subtracting the header line
#current_count=0
#for (( c=2; c<=$((total_jobs+1)); c++ )) do
#   job_id=$(squeue -u scarf781 | awk "NR==$c"' {print $1}')
#   job_dir=$(scontrol show job "$job_id" | grep -o -P 'WorkDir=\K.*')
#   echo "$job_id:" >> "$old_output"
#   echo "$job_dir" >> "$old_output"
#   ((current_count++))
#   echo -ne "Old method progress: $((current_count * 100 / total_jobs))% \r"
#done
#echo -ne "\n"

# New method: much simpler
squeue -u $(whoami) --format="%A,%Z" | tail -n +2 | awk -F, '{print $1":"; print $2}'
