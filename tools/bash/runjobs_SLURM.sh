#!/bin/bash
#
# Runs jobs in a set of directories supplied by the script by the file: jobdirs
#
# Andrew Duff, 2014, Imperial College London

if [ -f jobdirs ]; then

   path=`pwd`
   
   echo
   echo "Jobs ready to run in:"
   cat jobdirs
   echo "(current directory:"
   pwd
   echo ")"
   echo
   echo "Run jobs? (y=yes)"
   read continue
   if [ $continue == 'y' ]; then
     while read JOBDIR; do
       JOBDIR=$path/$JOBDIR
       cd $JOBDIR
       echo "Running job in "$JOBDIR
       sbatch script #sbatch script
     done < $path/jobdirs
   fi

else

   echo
   echo "No jobs to submit (no jobdirs file)"

fi
