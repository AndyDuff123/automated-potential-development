#!/bin/bash
#
# Given POSCAR-001, etc files, creates directories and copies through POSCAR-001 to
# disp-001/POSCAR, etc, as well as KPOINTS, INCAR, POTCAR and input
#
# Andrew Duff
set -e  # exit if any command fails

ls supercell-*.cell > cell_files || { echo "ls command failed"; exit 1; }
n_indep_disp=$(wc -l < cell_files) || { echo "wc command failed"; exit 1; }
echo $n_indep_disp
sed -i 's/supercell//g' cell_files || { echo "sed command 1 failed"; exit 1; }
sed -i 's/\.cell//g' cell_files || { echo "sed command 2 failed"; exit 1; }

while read DIR; do
    mkdir disp$DIR || { echo "mkdir failed for dir: disp$DIR"; exit 1; }
    mv supercell${DIR}.cell disp$DIR/castep.cell || { echo "mv command failed"; exit 1; }
    cp castep.param disp$DIR/ || { echo "cp command failed"; exit 1; }
    if [ -f script ]; then
        cp script disp$DIR/ # Absence of submission script does not cause a fail...
    fi
done < cell_files
rm cell_files || { echo "rm command failed"; exit 1; }
