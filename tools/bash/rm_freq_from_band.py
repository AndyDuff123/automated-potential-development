import shutil

def backup_and_modify_file(file_path):
    # Backup the original file
    backup_path = file_path + '.bak'
    shutil.copyfile(file_path, backup_path)

    # Read the original file
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # Process the lines and remove the specified ones
    new_lines = []
    for i in range(len(lines)):
        if "frequency:" in lines[i]:
            frequency = float(lines[i].split(':')[1].strip())
            if frequency > 15:
                # Skip this line and the previous one
                if i > 0 and lines[i-1].strip().startswith('- #'):
                    new_lines.pop()  # Remove the previous line
                continue
        new_lines.append(lines[i])

    # Write the modified content back to the original file
    with open(file_path, 'w') as file:
        file.writelines(new_lines)

# Use the function
file_path = 'band.yaml'
backup_and_modify_file(file_path)

