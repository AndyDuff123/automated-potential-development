import numpy as np
import matplotlib.pyplot as plt
import dynasor
from ase import Atoms
from ase.build import bulk
from dynasor import compute_dynamic_structure_factors, Trajectory
from seekpath import get_path
import ase.io
from dynasor.qpoints import get_supercell_qpoints_along_path
import argparse

# Set up argument parser
parser = argparse.ArgumentParser(description="Run dynamic structure factor analysis")
parser.add_argument('--frame_start', type=int, default=1, help='Frame start for the trajectory')
parser.add_argument('--frame_stop', type=int, default=40000, help='Frame stop for the trajectory')
parser.add_argument('--frame_step', type=int, default=1, help='How many snapshots to step through when forming frames from the trajectory')
parser.add_argument('--dt', type=int, default=40, help='Time step in fs, must equal that from LAMMPS simulation')
parser.add_argument('--window_size', type=int, default=500, help='Maximum lag in time-steps considered')
parser.add_argument('--window_step', type=int, default=1, help='How far window moves each time. Smaller = better converged but slower')
parser.add_argument('--vmax', type=float, default=2, help='Maximum value for color scale')
parser.add_argument('--emax', type=float, default=6, help='Upper energy limit in color map plot')
parser.add_argument('--smax', type=float, default=10, help='Upper S(q,w) limit in cross section plot. Give value 0 for automatic')
args = parser.parse_args()

# Load trajectory
print('loading trajectory...')
trajectory_filename = 'dump2.meam'
traj = Trajectory(trajectory_filename,
                  trajectory_format='lammps_internal', frame_start=args.frame_start, frame_stop=args.frame_stop, frame_step=args.frame_step)
print('loaded trajectory.')

# Load atoms and path information
lammpsInput = open("POSCAR.lmp", "r")
atoms = ase.io.read(lammpsInput, format='lammps-data', style='atomic')
path_info = get_path((
    atoms.cell,
    atoms.get_scaled_positions(),
    atoms.numbers))

# Generate q-points and compute dynamic structure factors
point_coordinates = path_info['point_coords']
path = path_info['path']
q_segments = get_supercell_qpoints_along_path(
    path, point_coordinates, atoms.cell, traj.cell)
q_points = np.vstack(q_segments)

# In the following we hard wire the q point related quantities to just look at the X-M part of the path:
point_coordinates =  {'M': [0.5, 0.5, 0.0], 'X': [0.0, 0.5, 0.0]}
path = [('X', 'M')]
q_segments =  [np.array([[0.        , 0.34193949, 0.        ],
       [0.17096975, 0.34193949, 0.        ],
       [0.34193949, 0.34193949, 0.        ]])]
q_points = np.vstack(q_segments)
#the result of the above line should look like: q_points =  [[0.         0.34193949 0.        ]
# [0.17096975 0.34193949 0.        ]
# [0.34193949 0.34193949 0.        ]
# [0.34193949 0.34193949 0.        ]]
print('q-point related quantities:')
print('   point_coordinates = ',point_coordinates,', path = ',path,', q_segments = ',q_segments,', q_points = ',q_points)
print('computing dynamic structure factor...')
sample = compute_dynamic_structure_factors(
    traj, q_points, dt=args.dt, window_size=args.window_size,
    window_step=args.window_step, calculate_currents=False)
print('computed dynamic structure factor.')
sample.write_to_npz('my_sample.npz')
# sample = read_sample_from_npz('my_sample.npz')

# Gather data for plotting
q_distances = []
q_labels = dict()
qr = 0.0
for it, q_segment in enumerate(q_segments):
    q_distances.append(qr)
    q_labels[qr] = path[it][0]
    for qi, qj in zip(q_segment[1:], q_segment[:-1]):
        qr += np.linalg.norm(qi - qj)
        q_distances.append(qr)

q_labels[qr] = path[-1][1]
q_distances = np.array(q_distances)
print('q_distances: ',q_distances)

# Plot
conversion_factor = 658.2119  # found in dynasor/correlation_functions.py
fig = plt.figure(figsize=(5.2, 2.8), dpi=140)
ax = fig.add_subplot(111)
ax.pcolormesh(q_distances, conversion_factor * sample.omega,
              sample.Sqw_coh.T, cmap='Blues', vmin=0, vmax=args.vmax)

xticks = []
xticklabels = []
for q_dist, q_label in q_labels.items():
    ax.axvline(q_dist, c='0.5', alpha=0.5, ls='--')
    xticks.append(q_dist)
    xticklabels.append(q_label.replace('GAMMA', r'$\Gamma$'))

ax.set_xticks(xticks)
ax.set_xticklabels(xticklabels)
ax.set_xlim([0, q_distances.max()])
ax.set_ylim([0, args.emax])

fig.tight_layout()
fig.savefig('output_figure.png', format='png')


# Plot structure factor S(q, ω) against energy (ω) for selected q values
#q_values_to_plot = [0.3426113, 0.51391695, 0.6852226]  # X, midpoint between X and M, M
q_values_to_plot = [0.0000000, 0.17096975, 0.34193949]  # as above, but these are for the case where we calculate fewer q points

# Extract the indices of the q_distances closest to the desired q values
indices_to_plot = [np.argmin(np.abs(q_distances - q_value)) for q_value in q_values_to_plot]

fig2, ax2 = plt.subplots(figsize=(5.2, 2.8), dpi=140)

for index in indices_to_plot:
    q_value = q_distances[index]
    ax2.plot(conversion_factor * sample.omega, sample.Sqw_coh[index, :], label=f'q={q_value:.4f}')

ax2.set_xlabel('Energy (meV)')
ax2.set_ylabel('S(q, ω)')
ax2.set_title('Structure Factor S(q, ω) vs Energy for Selected q-values')
ax2.legend()
ax2.set_xlim([0, 6])  # Limit the x-axis to 6 meV
#ax2.set_xlim([0, conversion_factor * sample.omega.max()])
#ax2.set_ylim([0, np.max(sample.Sqw_coh[indices_to_plot]) * 1.1])  # Extend y-lim a bit for better visualization
if args.smax != 0:
    ax2.set_ylim([0, args.smax])

# Recently added this to allow me to average the Sqw cross sections from multiple independent runs:
# Write the extracted S(q, ω) data to a file
output_filename = 'Sqw_crosssection.dat'
with open(output_filename, 'w') as f:
    f.write("# Energy (meV)   S(q, ω) for selected q-values\n")
    for i, index in enumerate(indices_to_plot):
        q_value = q_distances[index]
        f.write(f"# q = {q_value:.4f}\n")
        for omega, Sqw in zip(conversion_factor * sample.omega, sample.Sqw_coh[index, :]):
            f.write(f"{omega:.6f} {Sqw:.6f}\n")
        f.write("\n")  # Add a blank line between different q-values


fig2.tight_layout()
fig2.savefig('structure_factor_vs_energy.png', format='png')
