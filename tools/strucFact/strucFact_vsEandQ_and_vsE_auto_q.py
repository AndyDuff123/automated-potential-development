import numpy as np
import matplotlib.pyplot as plt
import dynasor
from ase import Atoms
from ase.build import bulk
from dynasor import compute_dynamic_structure_factors, Trajectory
from seekpath import get_path
import ase.io
from dynasor.qpoints import get_supercell_qpoints_along_path
import argparse
import os

# Set up argument parser
parser = argparse.ArgumentParser(description="Run dynamic structure factor analysis")
parser.add_argument('--frame_start', type=int, default=1, help='Frame start for the trajectory')
parser.add_argument('--frame_stop', type=int, default=40000, help='Frame stop for the trajectory')
parser.add_argument('--frame_step', type=int, default=1, help='How many snapshots to step through when forming frames from the trajectory')
parser.add_argument('--dt', type=int, default=40, help='Time step in fs, must equal that from LAMMPS simulation')
parser.add_argument('--window_size', type=int, default=500, help='Maximum lag in time-steps considered')
parser.add_argument('--window_step', type=int, default=50, help='How far window moves each time. Smaller = better converged but slower')
parser.add_argument('--vmax', type=float, default=2, help='Maximum value for color scale')
parser.add_argument('--emax', type=float, default=6, help='Upper energy limit in color map plot')
parser.add_argument('--smax', type=float, default=10, help='Upper S(q,w) limit in cross section plot. Give value 0 for automatic')
parser.add_argument('--path', type=str, help="The path in the format [('X', 'M'), ('M', 'GAMMA')]")
parser.add_argument('--q_segments', type=str, help="The q_segments in the format [np.array([[0., 0., 0.], [0.5, 0.5, 0.]])]")
parser.add_argument('--q_values_to_plot', type=str, help="Comma-separated q values to plot, e.g., '0.00000000, 0.17110074, 0.34220148'")
args = parser.parse_args()

# Load trajectory
print('Loading trajectory...')
trajectory_filename = 'dump2.meam'
traj = Trajectory(trajectory_filename,
                  trajectory_format='lammps_internal', frame_start=args.frame_start, frame_stop=args.frame_stop, frame_step=args.frame_step)
print('Loaded trajectory.')

# Load atoms and path information
lammpsInput = open("POSCAR.lmp", "r")
atoms = ase.io.read(lammpsInput, format='lammps-data', style='atomic')
path_info = get_path((atoms.cell, atoms.get_scaled_positions(), atoms.numbers))

# Generate q-points for the first run
point_coordinates = path_info['point_coords']
full_path = path_info['path']
q_segments = get_supercell_qpoints_along_path(full_path, point_coordinates, atoms.cell, traj.cell)
full_q_points = np.vstack(q_segments)

# Save the full q-points path for review
if not args.path or not args.q_segments:
    print("First run: generating full q-points path. Please review the output below and specify your desired path and q_segments in the next run.")
    print('Full path:', full_path)
    print('Full q_segments:', q_segments)
    np.savez('full_q_points.npz', q_segments=q_segments, path=full_path)
    print("Full q-points data saved to 'full_q_points.npz'. Please specify your desired path and q_segments in the next run.")
    exit()

# Parse specified path and q_segments from command-line arguments
try:
    path = eval(args.path)  # Convert string input to a Python list
    q_segments = eval(args.q_segments)  # Convert string input to a Python list
    if not isinstance(path, list) or not all(isinstance(seg, tuple) for seg in path):
        raise ValueError("Invalid path format. It should be a list of tuples, e.g., [('X', 'M')].")
    if not isinstance(q_segments, list) or not all(isinstance(seg, np.ndarray) for seg in q_segments):
        raise ValueError("Invalid q_segments format. It should be a list of numpy arrays.")
except Exception as e:
    print(f"Error parsing path or q_segments: {e}")
    exit()

# Use the specified path and q_segments
q_points = np.vstack(q_segments)
print('Using specified path and q_segments:')
print('   path =', path)
print('   q_segments =', q_segments)
print('   q_points =', q_points)

# Compute dynamic structure factors
print('Computing dynamic structure factor...')
sample = compute_dynamic_structure_factors(
    traj, q_points, dt=args.dt, window_size=args.window_size,
    window_step=args.window_step, calculate_currents=False)
print('Computed dynamic structure factor.')
sample.write_to_npz('my_sample.npz')

# Gather data for plotting
q_distances = []
q_labels = dict()
qr = 0.0
for it, q_segment in enumerate(q_segments):
    q_distances.append(qr)
    q_labels[qr] = path[it][0]
    for qi, qj in zip(q_segment[1:], q_segment[:-1]):
        qr += np.linalg.norm(qi - qj)
        q_distances.append(qr)
q_labels[qr] = path[-1][1]
q_distances = np.array(q_distances)
print('q_distances:', q_distances)

# Plot the full dynamic structure factor map
conversion_factor = 658.2119  # Conversion factor found in dynasor/correlation_functions.py
fig = plt.figure(figsize=(5.2, 2.8), dpi=140)
ax = fig.add_subplot(111)
ax.pcolormesh(q_distances, conversion_factor * sample.omega,
              sample.Sqw_coh.T, cmap='Blues', vmin=0, vmax=args.vmax)

xticks = []
xticklabels = []
for q_dist, q_label in q_labels.items():
    ax.axvline(q_dist, c='0.5', alpha=0.5, ls='--')
    xticks.append(q_dist)
    xticklabels.append(q_label.replace('GAMMA', r'$\Gamma$'))

ax.set_xticks(xticks)
ax.set_xticklabels(xticklabels)
ax.set_xlim([0, q_distances.max()])
ax.set_ylim([0, args.emax])

fig.tight_layout()
fig.savefig('output_figure.png', format='png')

# Plot structure factor S(q, ω) against energy (ω) for selected q values
# Extract the indices of the q_distances closest to the desired q values
#q_values_to_plot = [0.00000000, 0.17110074, 0.34220148]  # Adjust this as necessary or make it another argument
# Convert q_values_to_plot argument to a list of floats
if args.q_values_to_plot:
    q_values_to_plot = [float(q.strip()) for q in args.q_values_to_plot.split(',')]
else:
    print("Error: q_values_to_plot must be specified")
    exit()
indices_to_plot = [np.argmin(np.abs(q_distances - q_value)) for q_value in q_values_to_plot]

fig2, ax2 = plt.subplots(figsize=(5.2, 2.8), dpi=140)
for index in indices_to_plot:
    q_value = q_distances[index]
    ax2.plot(conversion_factor * sample.omega, sample.Sqw_coh[index, :], label=f'q={q_value:.4f}')

ax2.set_xlabel('Energy (meV)')
ax2.set_ylabel('S(q, ω)')
ax2.set_title('Structure Factor S(q, ω) vs Energy for Selected q-values')
ax2.legend()
ax2.set_xlim([0, 6])  # Limit the x-axis to 6 meV
if args.smax != 0:
    ax2.set_ylim([0, args.smax])

# Write the extracted S(q, ω) data to a file
output_filename = 'Sqw_crosssection.dat'
with open(output_filename, 'w') as f:
    f.write("# Energy (meV)   S(q, ω) for selected q-values\n")
    for i, index in enumerate(indices_to_plot):
        q_value = q_distances[index]
        f.write(f"# q = {q_value:.4f}\n")
        for omega, Sqw in zip(conversion_factor * sample.omega, sample.Sqw_coh[index, :]):
            f.write(f"{omega:.6f} {Sqw:.6f}\n")
        f.write("\n")

fig2.tight_layout()
fig2.savefig('structure_factor_vs_energy.png', format='png')

