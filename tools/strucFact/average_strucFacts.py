import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# Check if the correct number of command line arguments are provided
if len(sys.argv) not in [4, 7]:
    print("Usage: python script_name.py <approx_pos_q0> <approx_pos_q1> <approx_pos_q2> [<fit_range_q0> <fit_range_q1> <fit_range_q2>]")
    sys.exit(1)

# Parse command line arguments for initial guesses of peak positions
# NOTE: Please change following positions by hand based on Sqw files of subdirs (e.g. it may be 0.1710 rather than 0.1711)
#       (3 instances)
# NOTE: Also be sure to reset the fit ranges below as well (make the number smaller if the gaussian is more localized; e.g.
#       if it is part of a larger peak)
try:
    peak_positions = {
        0.0: float(sys.argv[1]),     # Approximate position for q=0.0
        0.1712: float(sys.argv[2]),  # Approximate position for q=0.1711
        0.3424: float(sys.argv[3])   # Approximate position for q=0.3422
    }
except ValueError:
    print("Error: Please provide valid numerical values for peak positions.")
    sys.exit(1)

# Default fit ranges (±30 data points)
fit_ranges = {
    0.0: 30,
    0.1712: 30,
    0.3424: 30 # 15
}

# If 6 arguments are provided, override default ranges with user-specified ones
if len(sys.argv) == 7:
    try:
        fit_ranges[0.0] = int(sys.argv[4])
        fit_ranges[0.1712] = int(sys.argv[5])
        fit_ranges[0.3424] = int(sys.argv[6])
    except ValueError:
        print("Error: Please provide valid integer values for fitting ranges.")
        sys.exit(1)

# Gaussian function definition
def gaussian(x, a, x0, sigma):
    return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

# Initialize a dictionary to store all data by q-value
all_data_by_q = {}

# Get the list of subdirectories that are named as numbers
subdirectories = [d for d in os.listdir() if os.path.isdir(d) and d.isdigit()]
subdirectories.sort(key=int)  # Sort directories by numeric order

# Loop through each subdirectory and read the Sqw_crosssection.dat file
for dir_name in subdirectories:
    file_path = os.path.join(dir_name, 'Sqw_crosssection.dat')

    if os.path.exists(file_path):
        with open(file_path, 'r') as f:
            current_q = None
            data = []
            for line in f:
                if line.startswith("# q ="):
                    if current_q is not None and data:
                        # Add data to the corresponding q-value
                        if current_q not in all_data_by_q:
                            all_data_by_q[current_q] = []
                        all_data_by_q[current_q].append(np.array(data))
                    # Update the current q value and reset the data
                    current_q = float(line.split('=')[1].strip())
                    data = []
                elif line.strip() and not line.startswith("#"):
                    omega, Sqw = map(float, line.split())
                    data.append([omega, Sqw])
            if current_q is not None and data:
                # Add last block of data
                if current_q not in all_data_by_q:
                    all_data_by_q[current_q] = []
                all_data_by_q[current_q].append(np.array(data))

# Now average the data for each q-value
output_filename = 'averaged_Sqw_crosssection.dat'
with open(output_filename, 'w') as f:
    for q_value, datasets in all_data_by_q.items():
        # Assuming all datasets have the same omega values
        omega_values = datasets[0][:, 0]
        avg_Sqw = np.mean([data[:, 1] for data in datasets], axis=0)

        # Write the averaged data for each q-value
        f.write(f"# q = {q_value:.4f}\n")
        for omega, avg_S in zip(omega_values, avg_Sqw):
            f.write(f"{omega:.6f} {avg_S:.6f}\n")
        f.write("\n")  # Add a blank line between different q-values

print(f"Averaged S(q, ω) data has been written to {output_filename}")

# Plotting the averaged cross-sections for each q-value using points and fitting Gaussians
fig, ax = plt.subplots(figsize=(5.2, 2.8), dpi=140)
for q_value, datasets in all_data_by_q.items():
    omega_values = datasets[0][:, 0]
    avg_Sqw = np.mean([data[:, 1] for data in datasets], axis=0)
    ax.scatter(omega_values, avg_Sqw, label=f'q={q_value:.4f}', s=3)

    # Gaussian fitting for the peak nearest to the specified starting position
    if q_value in peak_positions:
        start_pos = peak_positions[q_value]
        # Find index closest to the initial guess
        start_idx = np.abs(omega_values - start_pos).argmin()

        # Use the specified fit range for this q value
        fit_range = slice(max(0, start_idx - fit_ranges[q_value]), min(len(omega_values), start_idx + fit_ranges[q_value]))

        # Fit the Gaussian curve
        popt, _ = curve_fit(gaussian, omega_values[fit_range], avg_Sqw[fit_range], p0=[max(avg_Sqw), start_pos, 0.1])
        fitted_gaussian = gaussian(omega_values[fit_range], *popt)
        ax.plot(omega_values[fit_range], fitted_gaussian, '--')

        # Output the position of the maximum
        max_position = popt[1]  # The peak position (x0)
        print(f"Max position of Gaussian fit for q={q_value:.4f} is at {max_position:.4f} meV.")

        # Write fitted Gaussian data to a file for each q-value
        gaussian_filename = f'gaussian_vs_energy_q{q_value:.4f}.dat'
        with open(gaussian_filename, 'w') as gf:
            for omega, fitted_S in zip(omega_values[fit_range], fitted_gaussian):
                gf.write(f"{omega:.6f} {fitted_S:.6f}\n")
        print(f"Fitted Gaussian data for q={q_value:.4f} written to {gaussian_filename}")

ax.set_xlabel('Energy (meV)')
ax.set_ylabel('Averaged S(q, ω)')
ax.set_title('Averaged Structure Factor S(q, ω) vs Energy with Gaussian Fits')
# Remove Gaussian fits from the legend by manually setting the handles
handles, labels = ax.get_legend_handles_labels()
handles = [h for h, l in zip(handles, labels) if 'Gaussian fit' not in l]
ax.legend(handles=handles, loc='best')

ax.set_xlim([0, 6])  # Adjust as needed
ax.set_ylim([0, 10])  # Adjust as needed

fig.tight_layout()
fig.savefig('averaged_structure_factor_points_vs_energy_with_gaussian.png', format='png')
print("Averaged cross-section plot with Gaussian fits has been saved as 'averaged_structure_factor_points_vs_energy_with_gaussian.png'")

# Plotting with adjusted x-axis and y-axis ranges using points and fitting Gaussians
fig, ax = plt.subplots(figsize=(5.2, 2.8), dpi=140)
for q_value, datasets in all_data_by_q.items():
    omega_values = datasets[0][:, 0]
    avg_Sqw = np.mean([data[:, 1] for data in datasets], axis=0)
    ax.scatter(omega_values, avg_Sqw, label=f'q={q_value:.4f}', s=3)

    # Gaussian fitting for the peak nearest to the specified starting position
    if q_value in peak_positions:
        start_pos = peak_positions[q_value]
        start_idx = np.abs(omega_values - start_pos).argmin()
        fit_range = slice(max(0, start_idx - fit_ranges[q_value]), min(len(omega_values), start_idx + fit_ranges[q_value]))
        popt, _ = curve_fit(gaussian, omega_values[fit_range], avg_Sqw[fit_range], p0=[max(avg_Sqw), start_pos, 0.1])
        fitted_gaussian = gaussian(omega_values[fit_range], *popt)
        ax.plot(omega_values[fit_range], fitted_gaussian, '--')

        # Output the position of the maximum
        max_position = popt[1]  # The peak position (x0)
        print(f"Max position of Gaussian fit for q={q_value:.4f} is at {max_position:.4f} meV.")

ax.set_xlabel('Energy (meV)')
ax.set_ylabel('S(q, ω)')
ax.set_xlim([3, 6])  # Adjust x-axis to be between 3 and 6 meV
ax.set_ylim([0, 6])  # Adjust y-axis to a maximum of 6

# Remove Gaussian fits from the legend by manually setting the handles
handles, labels = ax.get_legend_handles_labels()
handles = [h for h, l in zip(handles, labels) if 'Gaussian fit' not in l]
ax.legend(handles=handles, loc='best')

fig.tight_layout(pad=0)  # Remove any extra padding to eliminate space above the plot
fig.savefig('structure_factor_zoomed_points_vs_energy_with_gaussian.png', format='png')
print("Zoomed cross-section plot with Gaussian fits has been saved as 'structure_factor_zoomed_points_vs_energy_with_gaussian.png'")

# Final plot: Zoomed in, but with lines connecting points and no Gaussian fits
fig, ax = plt.subplots(figsize=(5.2, 2.8), dpi=140)

for q_value, datasets in all_data_by_q.items():
    omega_values = datasets[0][:, 0]
    avg_Sqw = np.mean([data[:, 1] for data in datasets], axis=0)

    # Plot lines connecting the points (no scatter)
    ax.plot(omega_values, avg_Sqw, label=f'q={q_value:.4f}', linestyle='-', marker='None')  # Line plot, no markers

ax.set_xlabel('Energy (meV)')
ax.set_ylabel('S(q, ω)')
ax.set_xlim([3, 6])  # Zoom in: x-axis between 3 and 6 meV
ax.set_ylim([0, 6])  # Set y-axis range to 0-6

# Add the legend
ax.legend(loc='best')

# Tight layout and save the plot
fig.tight_layout(pad=0)
fig.savefig('final_structure_factor_line_plot_zoomed.png', format='png')
print("Final zoomed cross-section plot with lines has been saved as 'final_structure_factor_line_plot_zoomed.png'")


