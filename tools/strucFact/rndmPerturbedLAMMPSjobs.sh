#!/bin/bash

# Usage: ./script.sh nmin nmax
# Example: ./script.sh 11 20

# Check if two arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 nmin nmax"
    exit 1
fi

# Get the range from the input arguments
nmin=$1
nmax=$2

# Loop over the range
for n in $(seq $nmin $nmax); do
    echo "Processing folder: ${n}"

    # Create directory and copy files
    mkdir ${n}
    cp ../POSCAR.lmp ${n}
    cp 1/{lammpsIn*,mlip3*,script*,state3*,Train*} ${n}

    # Change to the new directory
    cd ${n}

    # Set seed_nmr as n + 10
    seed_nmr=$((n + 10))

    # Run Python script with seed_nmr
    python ~/progs/APD/automated-potential-development/tools/randomPerturbLAMMPSstruc.py POSCAR.lmp POSCARperturbed.lmp --range 0.05 --seed ${seed_nmr}

    # Move the perturbed structure back to POSCAR.lmp
    mv POSCARperturbed.lmp POSCAR.lmp

    # Submit the job using sbatch
    sbatch script

    # Go back to the parent directory
    cd ..

done

echo "Finished processing range ${nmin} to ${nmax}."

