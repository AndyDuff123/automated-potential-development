import numpy as np
import matplotlib.pyplot as plt
import dynasor
from ase import Atoms
from ase.build import bulk
from dynasor import compute_dynamic_structure_factors, Trajectory
from dynasor.post_processing import compute_spherical_qpoint_average
from seekpath import get_path
import ase.io
from dynasor.qpoints import get_spherical_qpoints
import argparse
import os

def parse_q_bins_ranges(q_bins_ranges_str):
    """
    Parses a string of q range specifications into a list of tuples.

    Args:
        q_bins_ranges_str (str): A string like '0.2-0.5,0.5-0.8,0.8-1.1,1.1-1.4'

    Returns:
        List[Tuple[float, float]]: A list of (start, end) tuples.
    """
    q_bins = []
    for range_str in q_bins_ranges_str.split(','):
        try:
            start, end = map(float, range_str.split('-'))
            if start >= end:
                raise ValueError(f"Invalid range '{range_str}': start must be less than end.")
            q_bins.append((start, end))
        except ValueError as e:
            raise argparse.ArgumentTypeError(f"Invalid q bin range '{range_str}': {e}")
    return q_bins

# Set up argument parser
parser = argparse.ArgumentParser(description="Run dynamic structure factor analysis")
parser.add_argument('--frame_start', type=int, default=1, help='Frame start for the trajectory')
parser.add_argument('--frame_stop', type=int, default=40000, help='Frame stop for the trajectory')
parser.add_argument('--frame_step', type=int, default=1, help='How many snapshots to step through when forming frames from the trajectory')
parser.add_argument('--dt', type=int, default=40, help='Time step in fs, must equal that from LAMMPS simulation')
parser.add_argument('--window_size', type=int, default=500, help='Maximum lag in time-steps considered')
parser.add_argument('--window_step', type=int, default=50, help='How far window moves each time. Smaller = better converged but slower')
parser.add_argument('--vmax', type=float, default=10, help='Maximum value for color scale (not used in line plots)')
parser.add_argument('--emax', type=float, default=6, help='Upper energy limit in plots')
parser.add_argument('--smax', type=float, help='Upper S(q,w) limit in cross section plot. Do not specify for automatic')
parser.add_argument('--path', type=str, help="The path in the format [('X', 'M'), ('M', 'GAMMA')]")
parser.add_argument('--q_min', type=float, help="The minimum |q| value for analysis")
parser.add_argument('--q_max', type=float, help="The maximum |q| value for analysis")
parser.add_argument('--max_q_points', default=None, help="The maximum number of q points for analysis")
parser.add_argument('--q_bins', type=int, default=40, help="Number of bins to use when taking spherical average over q-space")
parser.add_argument('--q_bins_ranges', type=str, default=None,
                    help="Comma-separated list of q range bins, e.g., '0.2-0.5,0.5-0.8,0.8-1.1,1.1-1.4'")
args = parser.parse_args()

if args.max_q_points is not None:
    args.max_q_points = int(args.max_q_points)
print('max_q_points = ',args.max_q_points)

# Validate and parse q_bins_ranges if provided
if args.q_bins_ranges:
    try:
        q_bins_ranges = parse_q_bins_ranges(args.q_bins_ranges)
        print(f"Using q_bins_ranges: {q_bins_ranges}")
    except argparse.ArgumentTypeError as e:
        parser.error(str(e))
else:
    q_bins_ranges = []

# Load trajectory
print('Loading trajectory...')
trajectory_filename = 'dump2.meam'
traj = Trajectory(trajectory_filename,
                 trajectory_format='lammps_internal', frame_start=args.frame_start, frame_stop=args.frame_stop, frame_step=args.frame_step)
print('Loaded trajectory.')

# Use the specified path and q_segments
if args.max_q_points:
    q_points = get_spherical_qpoints(traj.cell, q_max=args.q_max, max_points=args.max_q_points)
else:
    q_points = get_spherical_qpoints(traj.cell, q_max=args.q_max)
print('   q_points =', q_points)

if args.q_min and args.q_min > 0:
    # Remove q_points with |q| less than q_min
    # Note, |q| = np.linalg.norm(q_points, axis=1)
    q_magnitudes = np.linalg.norm(q_points, axis=1)
    q_points = q_points[q_magnitudes >= args.q_min]
    print(f'Filtered q_points = {q_points} (removed all those with |q| < {args.q_min})')
q_magnitudes = np.linalg.norm(q_points, axis=1)

# Compute dynamic structure factors for all q_points
print('Computing dynamic structure factor for all q_points...')
sample_raw_all = compute_dynamic_structure_factors(
    traj, q_points, dt=args.dt, window_size=args.window_size,
    window_step=args.window_step, calculate_currents=False, calculate_incoherent=True)
print('Computed dynamic structure factor for all q_points.')
sample_raw_all.write_to_npz('my_sample_raw_all.npz')

# Average them over |q|
sample_averaged_all = compute_spherical_qpoint_average(sample_raw_all, q_bins=args.q_bins)

conversion_factor = 658.2119  # conversion from 1/fs to meV
sample_averaged_all.omega *= conversion_factor

# Plot color mesh for all q_points
fig, ax = plt.subplots(figsize=(3.4, 2.5), dpi=140)
ax.pcolormesh(sample_averaged_all.q_norms, sample_averaged_all.omega,
              sample_averaged_all.Sqw_incoh.T,
              cmap='Blues', vmin=0, vmax=args.vmax)
ax.text(0.05, 0.85, '$S(|\mathbf{q}|, \omega)$', transform=ax.transAxes,
        bbox={'color': 'white', 'alpha': 0.8, 'pad': 3})
ax.set_xlabel('$|\mathbf{q}|$ (1/Å)')
ax.set_ylabel('Frequency (meV)')
ax.set_ylim([0, args.emax])
fig.tight_layout()
fig.savefig('structure_factor_color_map.png', format='png')
print('Saved structure_factor_color_map.png')

# Save the color mesh data to a file (CSV format)
color_map_filename = 'structure_factor_color_map_data.csv'
# Combine q_norms, omega, and Sqw_incoh for saving
with open(color_map_filename, 'w') as f:
    # Write headers: |q| values
    f.write('Energy(meV)/|q|(1/Å),' + ','.join(map(str, sample_averaged_all.q_norms)) + '\n')
    # Write S(q,ω) values row by row for each energy level
    for i, omega_value in enumerate(sample_averaged_all.omega):
        f.write(f'{omega_value},' + ','.join(map(str, sample_averaged_all.Sqw_incoh[:, i])) + '\n')
print(f'Saved color mesh data to {color_map_filename}')

# Plot S(q,w) vs w for all q_points (Linear scale)
fig, ax = plt.subplots(figsize=(6, 4), dpi=150)
S_qw_summed = sample_averaged_all.Sqw_incoh.sum(axis=0)
ax.plot(sample_averaged_all.omega, S_qw_summed, label='All |q|')
ax.set_xlabel('Energy (meV)')
ax.set_ylabel('S(q, ω)')
ax.set_xlim([0, args.emax])
if args.smax:
   ax.set_ylim([0, args.smax])
ax.set_title('Dynamic Structure Factor for All |q|')
ax.legend()
fig.tight_layout()
fig.savefig('structure_factor_all_q.png', format='png')
print('Saved structure_factor_all_q.png')

# Save the data to a file (CSV format)
data_filename = 'structure_factor_all_q_data.csv'
np.savetxt(data_filename, np.column_stack([sample_averaged_all.omega, S_qw_summed]),
           delimiter=',', header='Energy(meV),S(q,ω)', comments='')
print(f'Saved data to {data_filename}')

# Plot S(q,w) vs w for all q_points (Logarithmic scale)
fig_log, ax_log = plt.subplots(figsize=(6, 4), dpi=150)
ax_log.plot(sample_averaged_all.omega, S_qw_summed, label='All |q|')
ax_log.set_yscale('log')  # Set the y-axis to a logarithmic scale
ax_log.set_xlabel('Energy (meV)')
ax_log.set_ylabel('S(q, ω) (log scale)')
ax_log.set_xlim([0, args.emax])
ax_log.set_title('Dynamic Structure Factor for All |q| (Logarithmic Scale)')
ax_log.legend()
fig_log.tight_layout()
fig_log.savefig('structure_factor_all_q_log.png', format='png')
print('Saved structure_factor_all_q_log.png')

# If q_bins_ranges are provided, generate additional plots and logarithmic plots
if q_bins_ranges:
    for idx, (q_start, q_end) in enumerate(q_bins_ranges, start=1):
        print(f'Processing q bin {idx}: {q_start} <= |q| < {q_end}')
        # Filter q_points within the current bin
        bin_mask = (q_magnitudes >= q_start) & (q_magnitudes < q_end)
        q_points_bin = q_points[bin_mask]
        q_magnitudes_bin = q_magnitudes[bin_mask]
        print(f'   Number of q_points in bin {idx}: {len(q_points_bin)}')

        if len(q_points_bin) == 0:
            print(f'   Warning: No q_points found in bin {idx} ({q_start}-{q_end}). Skipping plot.')
            continue

        # Sum over the precomputed structure factors for q-points in this bin
        S_qw_bin_summed = sample_raw_all.Sqw_incoh[bin_mask].sum(axis=0)

        # Plot S(q,w) vs w for the current bin (Linear scale)
        fig_bin, ax_bin = plt.subplots(figsize=(6, 4), dpi=150)
        ax_bin.plot(sample_averaged_all.omega, S_qw_bin_summed, label=f'Bin {idx}: {q_start}-{q_end} 1/Å')
        ax_bin.set_xlabel('Energy (meV)')
        ax_bin.set_ylabel('S(q, ω)')
        ax_bin.set_xlim([0, args.emax])
        if args.smax:
           ax_bin.set_ylim([0, args.smax])
        ax_bin.set_title(f'Dynamic Structure Factor for {q_start}-{q_end} 1/Å')
        ax_bin.legend()
        fig_bin.tight_layout()

        # Save the plot with modified filename format
        plot_filename = f'structure_factor_bin_{idx}_{q_start}-{q_end}.png'
        fig_bin.savefig(plot_filename, format='png')
        print(f'   Saved {plot_filename}')

        # Plot S(q,w) vs w for the current bin (Logarithmic scale)
        fig_bin_log, ax_bin_log = plt.subplots(figsize=(6, 4), dpi=150)
        ax_bin_log.plot(sample_averaged_all.omega, S_qw_bin_summed, label=f'Bin {idx}: {q_start}-{q_end} 1/Å')
        ax_bin_log.set_yscale('log')  # Set the y-axis to a logarithmic scale
        ax_bin_log.set_xlabel('Energy (meV)')
        ax_bin_log.set_ylabel('S(q, ω) (log scale)')
        ax_bin_log.set_xlim([0, args.emax])
        ax_bin_log.set_title(f'Dynamic Structure Factor for {q_start}-{q_end} 1/Å (Logarithmic Scale)')
        ax_bin_log.legend()
        fig_bin_log.tight_layout()

        # Save the log plot with modified filename format
        plot_log_filename = f'structure_factor_bin_{idx}_{q_start}-{q_end}_log.png'
        fig_bin_log.savefig(plot_log_filename, format='png')
        print(f'   Saved {plot_log_filename}')

        # Save the binned data to a CSV file
        bin_data_filename = f'structure_factor_bin_{idx}_{q_start}-{q_end}_data.csv'
        np.savetxt(bin_data_filename, np.column_stack([sample_averaged_all.omega, S_qw_bin_summed]),
                   delimiter=',', header='Energy(meV),S(q,ω)', comments='')
        print(f'   Saved binned data to {bin_data_filename}')

