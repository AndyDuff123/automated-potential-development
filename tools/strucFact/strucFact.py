import numpy as np
import matplotlib.pyplot as plt
import dynasor
from ase import Atoms
from ase.build import bulk
from dynasor import compute_dynamic_structure_factors, Trajectory
from seekpath import get_path
import ase.io
from dynasor.qpoints import get_supercell_qpoints_along_path
import argparse

# Set up argument parser
parser = argparse.ArgumentParser(description="Run dynamic structure factor analysis")
parser.add_argument('--frame_start', type=int, default=1, help='Frame start for the trajectory')
parser.add_argument('--frame_stop', type=int, default=40000, help='Frame stop for the trajectory')
parser.add_argument('--dt', type=int, default=40, help='Time step in fs, must equal that from LAMMPS simulation')
parser.add_argument('--window_size', type=int, default=500, help='Maximum lag in time-steps considered')
parser.add_argument('--window_step', type=int, default=1, help='How far window moves each time. Smaller = better converged but slower')
parser.add_argument('--vmax', type=float, default=1, help='Maximum value for color scale')
parser.add_argument('--ylim', type=float, default=6, help='Upper y-axis limit')
args = parser.parse_args()

# Load trajectory
trajectory_filename = 'dump2.meam'
traj = Trajectory(trajectory_filename,
                  trajectory_format='lammps_internal', frame_start=args.frame_start, frame_stop=args.frame_stop)

# Load atoms and path information
lammpsInput = open("POSCAR.lmp", "r")
atoms = ase.io.read(lammpsInput, format='lammps-data', style='atomic')
path_info = get_path((
    atoms.cell,
    atoms.get_scaled_positions(),
    atoms.numbers))

# Generate q-points and compute dynamic structure factors
point_coordinates = path_info['point_coords']
path = path_info['path']
q_segments = get_supercell_qpoints_along_path(
    path, point_coordinates, atoms.cell, traj.cell)
q_points = np.vstack(q_segments)
sample = compute_dynamic_structure_factors(
    traj, q_points, dt=args.dt, window_size=args.window_size,
    window_step=50, calculate_currents=False)
sample.write_to_npz('my_sample.npz')

# Gather data for plotting
q_distances = []
q_labels = dict()
qr = 0.0
for it, q_segment in enumerate(q_segments):
    q_distances.append(qr)
    q_labels[qr] = path[it][0]
    for qi, qj in zip(q_segment[1:], q_segment[:-1]):
        qr += np.linalg.norm(qi - qj)
        q_distances.append(qr)

q_labels[qr] = path[-1][1]
q_distances = np.array(q_distances)
print('q_distances: ',q_distances)

# Plot
conversion_factor = 658.2119  # found in dynasor/correlation_functions.py
fig = plt.figure(figsize=(5.2, 2.8), dpi=140)
ax = fig.add_subplot(111)
ax.pcolormesh(q_distances, conversion_factor * sample.omega,
              sample.Sqw_coh.T, cmap='Blues', vmin=0, vmax=args.vmax)

xticks = []
xticklabels = []
for q_dist, q_label in q_labels.items():
    ax.axvline(q_dist, c='0.5', alpha=0.5, ls='--')
    xticks.append(q_dist)
    xticklabels.append(q_label.replace('GAMMA', r'$\Gamma$'))

ax.set_xticks(xticks)
ax.set_xticklabels(xticklabels)
ax.set_xlim([0, q_distances.max()])
ax.set_ylim([0, args.ylim])

fig.tight_layout()
fig.savefig('output_figure.png', format='png')

