# Gnuplot script to plot {temperature} as a function of gamma for different timesteps. The .dat file is in APDworkdir/DFT/pbe/mp-#/2_MDparas
# Run in gnuplot with 'load path/to/file/plot_2_MDparas.gp'. IDX=0:20 to cover larger range of timesteps (even though only 5 considered by default). changed u 1:2 to u 2:3. think this is now right
# was:     p for [IDX=0:20] 'tempAvrgVsTimestepGamma.dat'  i IDX u 1:2 title columnheader(1), 300
set xlabel "Langevin gamma"
set ylabel "{temp}"
set logscale x
p for [IDX=0:20] 'tempAvrgVsTimestepGamma.dat' i IDX u 2:3 title columnheader(1), 'nominalTemp.dat' w l
