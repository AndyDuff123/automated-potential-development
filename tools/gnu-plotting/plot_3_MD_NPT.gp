# Run in gnuplot with 'load path/to/file/plot_3_MD_NPT'.
set xlabel "MD step"
set ylabel "Volume (Angstrom ^3)"
p 'thermalExpansion.dat' every ::8
