# Run from command-line with: gnuplot -persist -e "filename='thermalExpansion_mp-22862_EAM'" /path/to/automated-potential-development/tools/gnu-plotting/plot_8_thermalExpansion.gp
# (replace '/path/to' and the filename accordingly)
#
# To output a ps or pdf file prepend the following lines to this script:
# set terminal postscript color
# set output "thermalExpansion.ps"
#
# then execute gnuplot command, then ps2pdf thermalExpansion.ps
#
# Instead for a png file (as can be incorporated into .rst files for example):
set terminal png
set output "thermalExpansion.png"


set xlabel "Temperature (K)"
set ylabel "Density (g cm^-3)"
p for [IDX=0:20] filename i IDX u 1:2:3 w yerrorbars title columnheader(1)
