# Run on the thermal expansion output file in e.g.: APDworkdir/potMD/optb88/mp-22862/ to plot the thermal expansion for a particular
# xc functional averaged over the potentials.
# Run from command-line with: gnuplot -persist -e "filename='thermalExpansion_EAM'" /path/to/automated-potential-development/tools/gnu-plotting/plot_8_thermalExpansion_per_xc.gp
# (replace '/path/to' and the filename accordingly)
set xlabel "Temperature (K)"
set ylabel "Density (g cm^-3)"
p filename every ::1 u 1:2:3 w yerrorbars t "Calculated", filename every ::1 u 1:4 t "Experimental"
