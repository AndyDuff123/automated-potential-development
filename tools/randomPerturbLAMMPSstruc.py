import numpy as np
import argparse

# Set up argument parser
parser = argparse.ArgumentParser(description="Randomly perturb atomic coordinates in a LAMMPS structure file.")
parser.add_argument('input_file', type=str, help='Path to the LAMMPS input structure file (e.g., POSCAR.lmp)')
parser.add_argument('output_file', type=str, help='Path to the output file with perturbed coordinates.')
parser.add_argument('--range', type=float, default=0.1, help='Range for random perturbation (default: 0.1)')
parser.add_argument('--seed', type=int, default=None, help='Random seed for reproducibility (default: None)')

args = parser.parse_args()

# Set the random seed for reproducibility
np.random.seed(args.seed)

# Read the input file
with open(args.input_file, 'r') as f:
    lines = f.readlines()

# Find the "Atoms" section and start perturbing coordinates
atoms_section_found = False
perturbed_lines = []

for line in lines:
    if line.strip().startswith("Atoms"):
        atoms_section_found = True
        perturbed_lines.append(line)  # Add the "Atoms" line itself
        continue

    if atoms_section_found:
        if line.strip() == "":
            perturbed_lines.append(line)
        else:
            split_line = line.split()
            if len(split_line) >= 5:  # Ensure this line has atom coordinates
                atom_id = split_line[0]
                atom_type = split_line[1]
                x = float(split_line[2])
                y = float(split_line[3])
                z = float(split_line[4])
                
                # Apply random perturbation
                x_perturbed = x + np.random.uniform(-args.range, args.range)
                y_perturbed = y + np.random.uniform(-args.range, args.range)
                z_perturbed = z + np.random.uniform(-args.range, args.range)

                # Format the output to match original spacing and precision
                perturbed_lines.append(f"{atom_id: >4} {atom_type: >4} {x_perturbed: >13.9f}  {y_perturbed: >13.9f}  {z_perturbed: >13.9f}\n")
            else:
                perturbed_lines.append(line)  # If line doesn't match expected format, keep as is
    else:
        perturbed_lines.append(line)

# Write the perturbed structure to the output file
with open(args.output_file, 'w') as f:
    f.writelines(perturbed_lines)

print(f"Perturbed structure written to {args.output_file}.")

