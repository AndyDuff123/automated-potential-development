# Script to trigger generation of castep_keywords.json file. This is necessary to run on a system that has access to the CASTEP executable (e.g. a SCARF
# compute node)
#
# Copyright (C) 2020-2023, Dr Andrew Ian Duff, 
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import ase
import ase.calculators.castep

if __name__ == "__main__":

    print('Running ase.calculators.castep.Castep(). This should trigger generation of the castep_keywords.json file...')
    calc = ase.calculators.castep.Castep()
    print('...completed')

