#!/bin/bash

# Runs unit tests in: tutorial/{system}/unitTesting/ (over the set of systems listed below)
# Compares outputs of APD with those in: tutorial/{system}/correctOutput/
# (Job submission scripts are instead compared with those in scripts/-- copied through to tutorial/{system}/unitTesting --
# since these are computer-system specific.)
# Ensure you are in correct environment (e.g. source activate APD-env) and copy castep_keywords.json to the 
# tutorial/#/correctOutput/ directories

# Warning: In correctOutput/, DFT outputs may not always correspond to DFT inputs. E.g., I may change kp1x1x1 to kp2x2x2 in
# the VASP highElecPara test to improve stability; adjust the KPOINTS in the relevant directories accordingly; but then do not
# redo all the VASP calculations using kp2x2x2. To do so would be inefficient (I would have to do for all materials) and the purpose 
# of these unit tests is not to test the external codes, but to test APD outputs based on the inputs provided.

#declare -a systems=("BaZrO3_CASTEP" "LLZO_VASP" "LLZO_CASTEP")
declare -a systems=("LLZO_VASP" "LLZO_CASTEP")
#declare -a systems=("LLZO_CASTEP" "LLZO_VASP")

for system in "${systems[@]}"; do

  echo
  echo "Commencing tests on system: "$system
  cd $APDdir/tutorial/$system/unitTesting/ || { echo "Directory change failed, exiting."; exit 1; }

  # clean up
  echo "Cleaning up old test files..."
  rm *
  rm -rf APDworkdir 2> temp_error.txt
  # (Following check doesn't work. need to fix)
  if grep -q "cannot remove" temp_error.txt; then
    echo "Clean-up failed, exiting."
    exit 1
  fi
  rm -rf APDoutputdir 2> temp_error.txt
  if grep -q "cannot remove" temp_error.txt; then
    echo "Clean-up failed, exiting."
    exit 1
  fi
 
  echo "Copying script files..."
  cp $APDdir/scripts/script* ./
  cp $APDdir/scripts/run_MEAMfit.sh ./

  echo "Copying input files..."
  cp ../{settings.APD,therm*} ./
  
  # check MAPI key is present in settings.APD
  if ! grep -q "MAPI_KEY = [^[:space:]]" settings.APD; then
    echo "Error: MAPI_KEY is missing or empty in settings.APD"
    exit 1
  fi
 
  # Set some variables to determine which stages (and in what way) the output is to be checked
  DFTengine="${system##*_}"
  if grep -q 'dft_energy_tol' settings.APD; then
    dft_energy_tol_check=true
  else
    dft_energy_tol_check=false
  fi
  skip_phonons=$(grep 'skip_phonons =' settings.APD | awk -F'= ' '{print $2}')
  mp_id=$(grep 'mp_id =' settings.APD | cut -d ' ' -f 3)
  echo "DFT engine: "$DFTengine
  echo "Energy tolerance check?: "$dft_energy_tol_check
  echo "mp_id: "$mp_id
  echo "skip_phonons: "$skip_phonons

  echo
  echo "Starting test iterations..."
  for i in $(seq 0 6); do

    if [ "$i" -eq 0 ] && [ "$DFTengine" == "VASP" ]; then
      echo "Skipping iteration $i for VASP."
      continue
    fi

    if [ "$i" -eq 1 ] && [ "$dft_energy_tol_check" == "false" ]; then
      echo "Skipping iteration $i due to dft_energy_tol_check being false."
      continue
    fi

    # Check if there are no subdirs in the geomOpt == no grid-based geom opt
    if [ "$i" -eq 4 ]; then
      if [ -z "$(find "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/geomOpt/" -mindepth 1 -maxdepth 1 -type d)" ]; then
        echo "Skipping iteration $i as none-grid-based geometry optimization, so no need for energy calc at minimum."
        continue
      fi
    fi

    # No phonon calcs if skip_phonons=True
    if [ "$i" -eq 5 ] && [ "skip_phonons" == "true" ]; then
       echo "Skipping iteration $i as skip_phonons = true"
       continue
    fi

    echo "Running iteration $i..."
    
    echo "   Executing APD.py..."
    python $APDdir/APD.py -v 2 > APDoutput_$i 2>APDerror_$i
    if [ $? -ne 0 ]; then
      echo "   APD.py failed at iteration $i"
      exit 1
    fi
    
    ../../../check_directories_and_outputs.sh "$i" "$system" "$DFTengine" "$mp_id" "$dft_energy_tol_check"
    if [ $? -ne 0 ]; then
      echo "   check_directories_and_outputs.sh failed at iteration $i"
      exit 1
    fi
    
    echo "   Copying external code output from:"
    ../../../copy_external_code_output.sh "$i" "$system" "$DFTengine" "$mp_id"
    if [ $? -ne 0 ]; then
      echo "   copy_external_code_output.sh failed at iteration $i"
      exit 1
    fi
    
  done

done

echo "All tests passed."
exit 0  # Exit successfully if all checks passed
