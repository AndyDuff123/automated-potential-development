FROM continuumio/miniconda3

# Copy existing environment file
COPY environment.yml /tmp/environment.yml

# Create Conda environment from existing environment file
RUN conda env create -f /tmp/environment.yml

# Activate the environment (this is what `conda run -n your_env_name` would do)
SHELL ["conda", "run", "-n", "APD-env", "/bin/bash", "-c"]

