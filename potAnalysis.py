# Copyright (C) 2020-2023, Dr Andrew Ian Duff,
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import math
import os
import re

import numpy as np

# Modules:
import outf
import var  # variable container


def init_matplotlib():

    global plt
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt


def calcNumPotParas():
    outf.printt(2, "  Determining number of potential parameters:")

    if "MEAM" in var.pot_types:
        # parameter number for MEAM. If pot_types=[EAM,MEAM] we would need to take the paras for MEAM, since this determines the no. of datapoints we need from DFT
        outf.printt(2, "     Potential type: MEAM")
        nsp = var.structure.ntypesp
        lmax = 3
        outf.printt(
            2,
            "     "
            + str(int(nsp * lmax))
            + " meam_t paras, "
            + str(int(2 * var.NTERMS * nsp * lmax))
            + " density paras, "
            + str(int(2 * var.NTERMS * (math.factorial(2 + nsp - 1) / (2 * math.factorial(nsp - 1)))))
            + " pair potential paras, "
            + str(3 * nsp)
            + " embedding function paras",
        )
        nPotParas = int(
            nsp * lmax
            + 2 * var.NTERMS * nsp * lmax
            + 2 * var.NTERMS * (math.factorial(2 + nsp - 1) / (2 * math.factorial(nsp - 1)))
            + 3 * nsp
        )

    elif "EAM" in var.pot_types:
        outf.printt(2, "     Potential type: EAM")
        nsp = var.structure.ntypesp
        outf.printt(
            2,
            "    "
            + str(int(2 * var.NTERMS * nsp))
            + " density paras, "
            + str(int(2 * var.NTERMS * (math.factorial(2 + nsp - 1) / (2 * math.factorial(nsp - 1)))))
            + " pair potential paras, "
            + str(3 * nsp)
            + " embedding function paras",
        )
        nPotParas = int(
            2 * var.NTERMS * (nsp + math.factorial(2 + nsp - 1) / (2 * math.factorial(nsp - 1))) + 3 * nsp
        )  # terms: densities; pairpotentials; embedding functions
    # no. pair potential combinations for nsp types of species = (2+nsp-1)!/(2!*(nsp-1)!)
    #    else:
    #        outf.printt(0,'  ERROR: calcNumPotParas needs updating for MEAM potentials') # this also applies for 'Auto' which could include MEAM, so we need enough configs
    #        quit()

    outf.printt(2, "  nPotParas = " + str(nPotParas))

    return nPotParas


def getCostFunc(path, direc, optimizer):

    # Return cost function

    costFunc = None

    if optimizer == "MEAMfit":

        bestOptFuncsFile = open(path + "/bestoptfuncs")
        while True:
            line = bestOptFuncsFile.readline()
            if "1:" in line:
                numbersFromLine = re.findall(r"\d+\.\d+", line)
                costFunc = numbersFromLine[0]
                outf.printt(1, "  Cost function from optimization: " + str(costFunc))
                return True, costFunc # successfulOpt, costFunc

        # We don't do this anymore but if we want cost func from a single shot run instead (e.g. to evaluate on test
        # set) instead use:
        # MEAMfitFile = open(path + "/output.0", "r")
        # while True:
        #     line = MEAMfitFile.readline()
        #     if "Objective function=" in line:
        #         numbersFromLine = re.findall(r"\d+\.\d+", line)
        #         costFunc = numbersFromLine[0]
        #         outf.printt(1, "  Cost function from evaluation of potential on test set: " + str(costFunc))
        #         return costFunc

    else:

        # equivalent code for MLIP
        MLIPfile = open(path + "/output_p.txt", "r")
        previousLine = None
        completedOpt = False
        while True:
            line = MLIPfile.readline()
            if "BFGS ended" in line:
                completedOpt = True
                successfulOpt = True
                outf.printt(1, "  Optimization successful")
            elif "step limit reached" in line:
                completedOpt = True
                successfulOpt = False
                outf.printt(1, "  WARNING: optimization completed but iteration limit reached")
            if completedOpt:
                #print('previousLine=',previousLine) # debug
                numbersFromLine = re.findall(r"\d+\.\d+", previousLine) # e.g "BFGS iter 74: f=0.33005" -> [0.33005]
                #print('previousLine=',previousLine,', numbersFromLine=',numbersFromLine) # debug
                costFunc = float(numbersFromLine[0])
                outf.printt(1, "  Cost function from evaluation of potential on test set: " + str(costFunc))
                return successfulOpt, costFunc
            previousLine = line

    return False, None


def analyzeOpt(direc, path, pot):

    """
    Check if potential optimization job in direc/path is complete. (Checks in the latest cont/ dir.) Writes stage
    output file if so. If not writes file to the appropriate list (var.incompleteJobs or var.notStartedJobs)

    Note, pot contains eg 'EAM1B'. We can use this to determine which optimizer was used (MEAMfit or MLIP) and so how
    to parse the output.

    Returns:
        optCompleted (Boolean) - whether optimization is completed
    """

    contdir = ""
    while True:
        if os.path.isdir(os.path.join(path, contdir, "cont")):
            contdir = os.path.join(contdir, "cont")
        else:
            break

    if 'EAM' in pot: # also counts for MEAM...
        optimizer = "MEAMfit"
        completionString = " Optimization completed"
        optimizerOutput = "output.0"
    else:
        optimizer = "MLIP2"
        completionString = "TRAIN ERRORS"
        optimizerOutput = "output_p.txt"

    if os.path.isfile(os.path.join(path, contdir, optimizerOutput)) == True:
        outf.printCitation(optimizer)
        with open(os.path.join(path, contdir, optimizerOutput)) as f:
            if completionString in f.read():
                successfulOpt, costFunc = getCostFunc(os.path.join(path, contdir), direc, optimizer)
                if costFunc == None:
                    outf.printt(0, "  ERROR: Cost function not parsed, STOPPING")
                    quit()
                stageOutputFile = open(os.path.join(path, contdir, "stageOutput"), "w")
                stageOutputFile.write( f"Potential optimization complete in {direc}/{contdir}, cost function: "
                    f"{costFunc}\n" )
                stageOutputFile.close()
                optCompleted = True
            else:
                outf.printt(2, "  " + optimizerOutput + " file present but optimization not completed")
                outf.toAction( f"Potential optimization started but not completed in {var.potOptdir}/ Please allow "
                    "to finish, or check if it has ended prematurely" )
                var.incompleteJobs.append( os.path.join(direc, contdir) ) # changed from: direc + "/" + contdir
                optCompleted = False
    else:
        outf.printt(2, "  " + optimizerOutput + " file not present in " + os.path.join(direc, contdir))
        outf.toAction( f"Potential optimization not started in {var.potOptdir}/ If it has not yet been submitted "
            "please submit it" )
        var.notStartedJobs.append( os.path.join(direc, contdir) ) # changed from: direc + "/" + contdir
        print('for debugging: os.path.join(direc, contdir) added to var.notStartedJobs.')
        print(f'  = os.path.join({direc}, {contdir}) = {os.path.join(direc, contdir)}')
        optCompleted = False


def count_begin_cfg(file_path):
    count = 0
    with open(file_path, 'r') as file:
        for line in file:
            if "BEGIN_CFG" in line:
                count += 1
    return count


def analyzeTest(direc, path):

    """
    Check if potential test job in direc/path is complete. If not writes file to the appropriate list 
    (var.incompleteJobs or var.notStartedJobs)
    """

    # Old code:
    #if os.path.isfile(os.path.join(path, "temp.cfg")) == False:
    #    outf.printt(2, "  test job not completed")
    #    outf.toAction(f"  Potential test job not completed in {var.potOptdir}/")
    #    var.incompleteJobs.append( direc )

    # Recently updated to check that file sizes are all the same (though not the _prev file if we haven't started AL)
    # Update: not diff.cfg. This has an extra field in each CFG field, so should be larger by the number of configs

    ALbegun = os.path.isfile(os.path.join(path, "Trained_prev.mtp_"))
    
    files_to_check = ["fitted_EFS.cfg", "temp.cfg"]
    if ALbegun:
        files_to_check.append("fitted_EFS_prev.cfg")
        outf.printt(2, "  ... also checking for fitted_EFS_prev.cfg")

    all_files_present = all(os.path.isfile(os.path.join(path, f)) for f in files_to_check)
    if not all_files_present:
        outf.printt(2, f"  test job not completed: not all files present ({files_to_check})")
        outf.toAction(f"  Potential test job not completed in {var.potOptdir}/")
        var.incompleteJobs.append(direc)
        return

    file_lengths = [sum(1 for line in open(os.path.join(path, f))) for f in files_to_check]
    
    begin_cfg_count = count_begin_cfg(os.path.join(path, "temp.cfg"))

    if ALbegun:
        fitted_efs_length = file_lengths[0]  # length of fitted_EFS.cfg
        temp_length = file_lengths[1]  # length of temp.cfg
        fitted_efs_prev_length = file_lengths[2]  # length of fitted_EFS_prev.cfg
        expected_temp_length = fitted_efs_length + begin_cfg_count

        if not (fitted_efs_length == fitted_efs_prev_length and temp_length == expected_temp_length):
            outf.printt(2, "  test job not completed")
            outf.printt(2, f"  fitted_efs_length, fitted_efs_prev_length = {fitted_efs_length}, "
                    f"{fitted_efs_prev_length}; temp_length, expected_temp_length = {temp_length}, "
                    f"{expected_temp_length}")
            outf.toAction(f"  Potential test job not completed in {var.potOptdir}/")
            var.incompleteJobs.append(direc)
            return
    else:
        fitted_efs_length = file_lengths[0]  # length of fitted_EFS.cfg
        temp_length = file_lengths[1]  # length of temp.cfg
        expected_temp_length = fitted_efs_length + begin_cfg_count

        if temp_length != expected_temp_length:
            outf.printt(2, f"  temp_length, expected_temp_length = {temp_length}, {expected_temp_length}")
            outf.printt(2, "  test job not completed")
            outf.toAction(f"  Potential test job not completed in {var.potOptdir}/")
            var.incompleteJobs.append(direc)
            return

    # Job is complete if none of the above conditions are met
    outf.printt(2, "  test job completed")
    outf.toAction(f"  Potential test job completed in {var.potOptdir}/")


def getObservables(optimizer, potItn):

    """
    Return errors DFT on observables (forces, energies, stresses) and rms of fitted observables. Not for MEAMfit these
    will come from the single shot (7_) job while for MLIP are taken from the optimization job (6_). In addition, EFS
    of all configs are analyzed to generate graphs of fitted vs target properties. In this case for both fitting codes
    the data is from 7_.
    """

    init_matplotlib()

    # Extract RMSEs
    if optimizer == "MEAMfit":

        MEAMfitOutputFile = open(var.potTestpath + "/fitted_quantities.out")
        variancesFound = False
        rmsErrorsFound = False
        while True:
            lineReadin = MEAMfitOutputFile.readline()
            if not lineReadin:
                break  # if line is empty end of file is reached
            if "Variances of energies" in lineReadin:
                variancesFound = True
                lineReadin = MEAMfitOutputFile.readline()
                numbersFromLine = re.findall(
                    r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin
                )  # MEAMfit can produce output in scientific notation (exponentials) - this can capture that, before used: r"[-+]?\d*\.\d+|\d+"
                varianceDFTenergies = float(numbersFromLine[0])
                varianceDFTforces = float(numbersFromLine[1])
                varianceDFTstresses = float(numbersFromLine[2])
                outf.printt(1, f"  Variances of DFT energies, forces and stresses: {varianceDFTenergies}, "
                    "{varianceDFTforces}, {varianceDFTstresses}")
            if "rms error on energies" in lineReadin:
                rmsErrorsFound = True
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorEnergies = float(numbersFromLine[0])
                lineReadin = MEAMfitOutputFile.readline()
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorForces = float(numbersFromLine[0])
                lineReadin = MEAMfitOutputFile.readline()
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorStresses = float(numbersFromLine[0])
                outf.printt(1, f"  rms error on energies, forces and stresses: {rmsErrorEnergies}, {rmsErrorForces}, "
                    "{rmsErrorStresses}")
            if variancesFound and rmsErrorsFound:
                break

        if variancesFound == False:
            outf.printt(0, f"  ERROR: Variances of DFT observables not found in {var.potTestdir}/fitted_quantities.out, "
                "STOPPING")
            quit()
        if rmsErrorsFound == False:
            outf.printt(0, f"  ERROR: rms errors of observables not found in {var.potTestdir}/fitted_quantities.out, "
                "STOPPING")
            quit()

        rmsErrorEnergiesOverSdDFT = rmsErrorEnergies / np.sqrt(varianceDFTenergies)
        rmsErrorForcesOverSdDFT = rmsErrorForces / np.sqrt(varianceDFTforces)
        rmsErrorStressesOverSdDFT = rmsErrorStresses / np.sqrt(varianceDFTstresses)

    else:
        # for MLIP - here we will not have rmsErrorEnergiesOverSdDFT, but all other quantities we will

        MLIPoutputFile = open(var.potOptpath + "/output_p.txt")
        rmsErrorEnergies = None
        rmsErrorForces = None
        rmsErrorStresses = None
        rmsErrorEnergiesOverSdDFT = None
        rmsErrorForcesOverSdDFT = None
        rmsErrorStressesOverSdDFT = None
        while True:
            lineReadin = MLIPoutputFile.readline()
            if not lineReadin:
                break  # if line is empty end of file is reached
            if "Energy per atom:" in lineReadin:
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorEnergies = float(numbersFromLine[0])
                outf.printt(1, f"  RMSE of energies: {rmsErrorEnergies}")
            if "Forces:" in lineReadin:
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorForces = float(numbersFromLine[0])
                outf.printt(1, f"  RMSE of forces: {rmsErrorForces}")
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorForcesOverSdDFT = float(numbersFromLine[0])
                outf.printt(1, f"  RMSE of forces over RMS of DFT forces: {rmsErrorForcesOverSdDFT}")
            if "Stresses (in eV):" in lineReadin:
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorStresses = float(numbersFromLine[0])
                outf.printt(1, f"  RMSE of stresses: {rmsErrorStresses}")
                lineReadin = MLIPoutputFile.readline()
                lineReadin = MLIPoutputFile.readline()
                numbersFromLine = re.findall(r"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *[-+]?\ *[0-9]+)?", lineReadin)
                rmsErrorStressesOverSdDFT = float(numbersFromLine[0])
                outf.printt(1, f"  RMSE of stresses over RMS of DFT stresses: {rmsErrorStressesOverSdDFT}")

        if any(variable is None for variable in [rmsErrorEnergies, rmsErrorForces, rmsErrorStresses, 
            rmsErrorForcesOverSdDFT, rmsErrorStressesOverSdDFT]):    
            outf.printt(0, "  ERROR: some or all of: rmsErrorEnergies, rmsErrorForces, rmsErrorStresses, "
                "rmsErrorForcesOverSdDFT, rmsErrorStressesOverSdDFT not found " # MLIP: no rms/sd output for energies
                f"(c.f. 'None' in, respectively: {rmsErrorEnergies}, {rmsErrorForces}, {rmsErrorStresses}, "
                f"{rmsErrorForcesOverSdDFT}, {rmsErrorStressesOverSdDFT}) "
                f"in {var.potOptpath}/output_p.txt, STOPPING")
            quit()
        MLIPoutputFile.close()  # reopen file to get back to start

    # Extract energies, forces and stress componenents (DFT vs potential) from file/s
    forcesDFT = []
    forcesPot = []
    stressesDFT = []
    stressesPot = []
    energiesDFT = []
    energiesPot = []
    minForce = 0
    maxForce = 0
    if optimizer == "MEAMfit":

        readingForces = False
        while True:
            lineReadin = MEAMfitOutputFile.readline()
            if not lineReadin:
                break
            if readingForces == False:
                if "Forces" in lineReadin:
                    readingForces = True
            if readingForces == True:
                numbersFromLine = re.findall(r"[-+]?\d*\.\d+|\d+", lineReadin)
                numbersFromLineFloat = [float(x) for x in numbersFromLine]
                for number in numbersFromLineFloat:
                    if number < minForce:
                        minForce = number
                    if number > maxForce:
                        maxForce = number
                if len(numbersFromLine) == 0:
                    readingForces = False
                elif len(numbersFromLine) == 6:
                    forcesPot.append(numbersFromLineFloat[0])
                    forcesPot.append(numbersFromLineFloat[1])
                    forcesPot.append(numbersFromLineFloat[2])
                    forcesDFT.append(numbersFromLineFloat[3])
                    forcesDFT.append(numbersFromLineFloat[4])
                    forcesDFT.append(numbersFromLineFloat[5])
                else:
                    print("ERROR: expected 6 numbers from line in MEAMfit output readin, STOPPING")
                    quit()
        outf.printt(2, f"Most negative force component: {minForce}, most positive force component: {maxForce}")
        # rangeForces=max(-minForce,maxForce)
        #
        # AxisLimit=((max(-minForce,maxForce))%2.5) # Take largest modulus force component as limit for -ve/+ve x and y axes
        # AxisLimit=(AxisLimit%2.5) #, and round to nearest 2.5 for clarity

        forcesPot = np.array(forcesPot)
        forcesDFT = np.array(forcesDFT)
        print(f"forcesPot[0:20]={forcesPot[0:20]}, forcesDFT[0:20]={forcesDFT[0:20]}") # write out a selection to screen
            # to check

        # Extract stress componenents (DFT vs potential) from file and plot
        MEAMfitOutputFile.close()  # reopen file to get back to start
        MEAMfitOutputFile = open(var.potTestpath + "/fitted_quantities.out")
        readingStresses = False
        minStress = 0
        maxStress = 0
        while True:
            lineReadin = MEAMfitOutputFile.readline()
            if not lineReadin:
                break
            if readingStresses == False:
                if "Stress" in lineReadin:
                    readingStresses = True
                    print("reading stresses, line=", lineReadin)
            if readingStresses == True:
                numbersFromLine = re.findall(r"[-+]?\d*\.\d+|\d+", lineReadin)
                numbersFromLineFloat = [float(x) for x in numbersFromLine]
                for number in numbersFromLineFloat:
                    if number < minStress:
                        minStress = number
                    if number > maxStress:
                        maxStress = number
                print(f"numbersFromLine={numbersFromLine}, len(...)={len(numbersFromLine)}")
                if len(numbersFromLine) == 0:
                    readingStresses = False
                elif len(numbersFromLine) == 6:
                    stressesPot.append(numbersFromLineFloat[0])
                    stressesPot.append(numbersFromLineFloat[1])
                    stressesPot.append(numbersFromLineFloat[2])
                    stressesDFT.append(numbersFromLineFloat[3])
                    stressesDFT.append(numbersFromLineFloat[4])
                    stressesDFT.append(numbersFromLineFloat[5])
                else:
                    print("ERROR: expected 6 numbers from line in MEAMfit output readin, STOPPING")
                    quit()
        outf.printt(2, f"Most negative stress component: {minStress}, most positive stress component: {maxStress}")

        stressesPot = np.array(stressesPot)
        stressesDFT = np.array(stressesDFT)
        print(f"stressesPot[0:20]={stressesPot[0:20]}, stressesDFT[0:20]={stressesDFT[0:20]}") # write out a selection
            # to screen to check

        MEAMfitOutputFile.close()
        # please see: work4/scd/scarf781/jobs/correlated-electron-multiscale/fe2o3-10atom/lammps_phononTesting/velocityAutoCorr/npt/equilibrateLatticeParas/thermalExpansion.py   for the original script this was based on
    else:

        def process_config_file(file_path):
           energies, forces, stresses = [], [], []
        
           with open(file_path, 'r') as file:
               lines = file.readlines()
               config_block = []
               for line in lines:
                   if line.startswith("BEGIN_CFG"):
                       config_block = []
                   elif line.startswith("END_CFG"):
                       e, f, s, natoms = process_config_block(config_block)
                       energies.extend(e)
                       forces.extend(f)
                       stresses.extend(s)
                   else:
                       config_block.append(line)
        
           return energies, forces, stresses, natoms

        # Function to process a single configuration block
        def process_config_block(config_lines):
          
           energies=[]
           forces=[]
           stresses=[]
           
           reading_forces = False
           for i, line in enumerate(config_lines):
               if line.startswith(" AtomData:"):
                   reading_forces = True
                   natoms = 0
                   continue
               if reading_forces:
                   if line.strip() and not line.startswith(" Energy") and not line.startswith(" PlusStress"):
                       parts = line.split()
                       if len(parts) >= 8:
                           # Extract fx, fy, fz and append to forces
                           fx, fy, fz = float(parts[5]), float(parts[6]), float(parts[7])
                           forces.extend([fx, fy, fz])
                           natoms += 1
                   else:
                       reading_forces = False
        
               if line.startswith(" Energy"):
                   energy_line = config_lines[i + 1].strip()
                   energy = float(energy_line)
                   energies.append(energy)
                   continue
               if line.startswith(" PlusStress:"):
                   stress_line = config_lines[i + 1].strip()
                   parts = stress_line.split()
                   if len(parts) >= 6:
                       # Extract stress tensor components
                       stresses_read = [float(part) for part in parts[1:7]]
                       stresses.append(stresses_read)
                   else:
                       print('Too few stresses in cfg file, STOPPING')
                       quit()
           
           return energies, forces, stresses, natoms

        # extract EFS for mlip. First DFT EFSs:
        energiesDFT, forcesDFT, stressesDFT, natoms = process_config_file(var.potTestpath + "/config.cfg")
        energiesPot, forcesPot, stressesPot, natoms = process_config_file(var.potTestpath + "/fitted_EFS.cfg")
        print('stressesDFT[1:10]=',stressesDFT[1:10])
        print('stressesPot[1:10]=',stressesPot[1:10])
        if potItn != "":
            energiesPotPrev, forcesPotPrev, stressesPotPrev, natoms = process_config_file(var.potTestpath + 
                    "/fitted_EFS_prev.cfg")
            print('stressesPotPrev[1:10]=',stressesPotPrev[1:10])
        else:
            energiesPotPrev = forcesPotPrev = stressesPotPrev = None

    # Plot EFS
    plt.xlabel("Force component, potential (eV/$\mathrm{\AA}$)")
    plt.ylabel("Force component, DFT (eV/$\mathrm{\AA}$)")
    plt.scatter(forcesPot, forcesDFT)  # , color='red'
    locs, labels = plt.xticks()
    x = np.linspace(locs[0], locs[-1], 2)
    plt.plot(x, x, alpha=0)
    # This will extend the plot slightly to accommodate the line. to get the line extended beyond the plot space we do:
    xlimBefore = plt.xlim()
    ylimBefore = plt.ylim()
    x = np.linspace(locs[0] - 5, locs[-1] + 5, 2)
    plt.plot(x, x, color="black", linestyle="dashed")
    plt.xlim(xlimBefore)
    plt.ylim(ylimBefore)
    plt.savefig(
        var.potTestpath + "/forcesPotVsDFT.png", dpi=300
    )  # I just added the dpi flag recently, as the png output was too low res. the properties box in windows didn't report dpi however so I am just guessing a value here - might need to experiment
    plt.clf()
    # please see: work4/scd/scarf781/jobs/correlated-electron-multiscale/fe2o3-10atom/lammps_phononTesting/velocityAutoCorr/npt/equilibrateLatticeParas/thermalExpansion.py   for the original script this was based on
    plt.xlabel("Stress component, potential (eV/$\mathrm{\AA}$)")
    plt.ylabel("Stress component, DFT (eV/$\mathrm{\AA}$)")
    plt.scatter(stressesPot, stressesDFT)  # , color='red'
    locs, labels = plt.xticks()
    x = np.linspace(locs[0], locs[-1], 2)
    plt.plot(x, x, alpha=0)
    # This will extend the plot slightly to accommodate the line. to get the line extended beyond the plot space we do:
    xlimBefore = plt.xlim()
    ylimBefore = plt.ylim()
    x = np.linspace(locs[0] - 5, locs[-1] + 5, 2)
    plt.plot(x, x, color="black", linestyle="dashed")
    plt.xlim(xlimBefore)
    plt.ylim(ylimBefore)
    plt.savefig(
        var.potTestpath + "/stressesPotVsDFT.png", dpi=300
    )  # I just added the dpi flag recently, as the png output was too low res. the properties box in windows didn't report dpi however so I am just guessing a value here - might need to experiment
    plt.clf()

    if potItn != "":

        # Compare forcesPotPrev and forcesDFT and if they are different by some threshold return the config and
        # atom index. Note, forces are just lists of all forces, so either need to make them instead lists on configs
        # then on indices or else use natoms here to determined the config and atom index

        discrepancy_threshold = 0.1  # threshold for RMS error
        discrepancies = {}  # To store indices of configurations and atoms with discrepancies
        
        # Assuming forces are ordered as fx1, fy1, fz1, fx2, fy2, fz2, ..., for each atom in each configuration
        for config_start_idx in range(0, len(forcesDFT), natoms * 3):
            for atom_idx in range(natoms):
                dft_forces = forcesDFT[config_start_idx + atom_idx * 3: config_start_idx + (atom_idx + 1) * 3]
                prev_forces = forcesPotPrev[config_start_idx + atom_idx * 3: config_start_idx + (atom_idx + 1) * 3]
                
                # Calculate RMS error for the current atom
                rms_error = np.sqrt(np.mean(np.square(np.subtract(dft_forces, prev_forces))))
                
                # Check if the RMS error exceeds the threshold
                if rms_error > discrepancy_threshold:
                    # Store configuration and atom index if discrepancy is found
                    config_idx = config_start_idx // (natoms * 3)
                    if config_idx not in discrepancies:
                        discrepancies[config_idx] = []
                    discrepancies[config_idx].append(atom_idx)

        outf.printt(1, "  Config | atom indices, where forces evaluated using previous iteration potential and DFT")
        outf.printt(1, f"  disagree by more than {discrepancy_threshold}:")
        for config_idx, atom_indices in discrepancies.items():
            atom_indices_str = ", ".join(map(str, atom_indices))
            outf.printt(1, f"Config {config_idx} | Atom indices: {atom_indices_str}")

    return (rmsErrorEnergies, rmsErrorForces, rmsErrorStresses, rmsErrorEnergiesOverSdDFT, rmsErrorForcesOverSdDFT,
        rmsErrorStressesOverSdDFT)
