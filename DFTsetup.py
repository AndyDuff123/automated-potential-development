#
# Copyright (C) 2020-2023, Dr Andrew Ian Duff,
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import copy
import glob
import itertools
import json
import math
import os
import random
import re
import shutil
import types
from types import SimpleNamespace
from collections import Counter
from math import floor, log10
from pathlib import Path
from string import ascii_letters

# For CASTEP:
import ase
import ase.calculators.castep
import ase.io
import ase.io.castep
import ase.io.lammpsrun
import numpy as np
import pymatgen.io.ase as aio
from ase.build import (
    find_optimal_cell_shape,
    get_deviation_from_optimal_cell_shape,
    make_supercell,
)
from ase.spacegroup import get_spacegroup
from pymatgen.core import Structure
from pymatgen.io.vasp.inputs import Kpoints, Poscar
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
import DFTanalysis
import DFTsetup
import filesys
import initialize
import outf
import potSetup, potMDsetup
import subprocess

# Modules:
import var  # variable container

if not (getattr(var, 'skip_0K_props', False) or getattr(var, 'skip_phonons', False)):
   try:
      import seekpath
   except:
      print('seekpath not detected, but also not needed...')

def round_to_1(x):
    return round(x, -int(floor(log10(abs(x)))) + 1)


def setupDFTfromPrevStage():
    if var.stage > 1:
        # If we are in a stage involving DFT calculations initialize DFT inputs from a previous stage directory
        if var.stage == 2:
            DFTsetup.setupDFTinput(
                var.DFTinputdir, var.DFTinputpath
            )  # in the DFTnonMD sub itself we make a separate call to setupDFTinput once the high parameters have been determined
        elif var.stage == 3:
            DFTsetup.setupDFTinput(var.DFTinputdir, var.DFTinputpath)
        elif var.stage == 4: # Set up DFT MD parameters if necessary
            if var.config_gen != 'DFT_NPT_EXISTING' and var.config_gen != 'DFT_NVT_EXISTING': # Where using existing 
                    # DFT MD data, don't need to set DFT inputs
                DFTsetup.setupDFTinput(var.DFTMDparasdir + "/temp" + str(var.maxTempMDparasChecked),
                    var.DFTMDparaspath + "/temp" + str(var.maxTempMDparasChecked))
            elif var.dft_engine == "PWSCF": # unless we are using QE, in which case we _do_ need to ensure var.DFTprefix is defined
                    # and for QE case we can't read this in from settings.APD but have to read it from QE input. Need to read from
                    # 1_ in this case as where DFT is being 'read in' we don't have a stage 'DFTMDparas'.
                DFTsetup.setupDFTinput(var.DFTinputdir, var.DFTinputpath)
        elif var.stage == 5 or var.stage == 8 or var.stage == 9:
            # For stage 5 this is for generating training data based on configs from stage 4. Electronic parameters are
            # either MP defaults or optimized parameters. For stage 8 important to have higher elec paras as we set up 
            # the relabelling jobs in 5 'remotely' from stage 8, so need the appropriate DFT pars.
            if var.dft_energy_tol is None:
                DFTsetup.setupDFTinput(var.DFTinputdir, var.DFTinputpath)
            else:
                DFTsetup.setupDFTinput(var.DFTnonMDhighElecParasdir, var.DFTnonMDhighElecParaspath)
            # (for stage 5, configs themselves will be read in directly from stage 3 (though we will rescale the k-points beforehand, though not using divisor from stage 3)


def atomsFromRelaxedGeom():

    # Read relaxed geometry from var.DFTnonMDgeomOptdir into var.atoms object. For use in setting up energy calculation for relaxed
    # geometry when using the grid method for geometry optimization.

    # Switch to root directory to access castep keywords file, ensuring consistent atom positions during read.
    os.chdir(var.dirpath)

    # magmomsPrev=var.atoms.get_initial_magnetic_moments()
    if var.dft_engine == "VASP":
        # Take from POSCAR file
        POSCARfile = open(var.DFTnonMDgeomOptdir + "/" + var.DFTinput[1], "r")
        atomsRelaxed = ase.io.read(POSCARfile,format='vasp')
    elif var.dft_engine == "CASTEP":
        # Take from .cell file
        cellFile = open(var.DFTnonMDgeomOptdir + "/" + var.DFTinput[1], "r")
        atomsRelaxed = ase.io.castep.read_castep_cell(cellFile)
        #  var.atoms.set_initial_magnetic_moments=var.atoms.get_magnetic_moments # we need to do this because writeDFTinputfiles uses var.atoms.get_initial_magnetic_moments() to write out the .cell file
        # update: above doesn't work because apparently atoms isn't even given 'get_magnetic_moments' as a method... just reset the magnetic moments to the 'initial' ones we read in, after all we are using these as the starting point for every other DFT calculation...
        # var.atoms.set_initial_magnetic_moments(magmoms=magmomsPrev)
    var.atoms.set_cell(atomsRelaxed.get_cell())
    var.atoms.set_positions(atomsRelaxed.get_positions())
    var.atoms.set_chemical_symbols(atomsRelaxed.get_chemical_symbols())

    return


def removeUncorrelatedConfigs(path, corrTime, filename = None):

    # Remove uncorrelated configs from .castep or vasprun.xml files

    # First move to another file to enable selective copying to var.DFToutput[0]
    if filename == None:
        filename = var.DFToutput[1]
    shutil.move(path + "/" + filename, path + "/" + filename + ".bkp")

    mdStep = 0
    DFToutputBkpFile = open(path + "/" + filename + ".bkp", "r")
    DFToutputFile = open(path + "/" + filename, "w")
    write = True
    while True:
        lineReadin = DFToutputBkpFile.readline()
        # we want to read before first iteration and then only iterations where we have uncorrleated snapshots
        if not lineReadin:
            break
        #if (var.dft_engine == "CASTEP" and "Starting MD iteration" in lineReadin) or (
        #    var.dft_engine == "VASP" and "<calculation>" in lineReadin
        #):
        if var.stringNewMDstepForDFToutput1 in lineReadin:
            mdStep = mdStep + 1
            beforeFirstMDitn = False
            if ((mdStep - 1) % corrTime) == 0:
                write = True
            else:
                write = False
        if write == True:
            DFToutputFile.write(lineReadin)
    DFToutputBkpFile.close()
    DFToutputFile.close()

    os.system("rm " + path + "/" + filename + ".bkp")


def is_geom_constant(val):
    # Check if the given value is close to any of the predefined geometric constants.

    # geom_consts = [np.sqrt(2), np.sqrt(3), np.sqrt(3)/2, 2, 3, 4, 0.5, 1/3]
    geom_consts = [1 / 3, 1 / 2, np.sqrt(2) / 2, np.sqrt(3) / 2]
    tol = 1e-3
    for gc in geom_consts:
        for multiplier in range(-10, 10):
            gc2 = gc * multiplier
            # print("   checking against:", gc2)
            if abs(val / gc2 - 1) < tol:
                return gc2
    return None


def identify_independent_parameters(cell):
    # This function identifies independent parameters within a given cell.
    # It iterates over three vectors and checks for relationships between their values.
    # The function returns dictionaries of independent parameters and prefactors.

    a, b, c = cell
    tol = 1e-5
    independent_parameters = [None] * 9
    prefactors = [None] * 9
    unique_values = {}
    next_index = 1
    i = 0
    for vec, vec_name in zip([a, b, c], ["a", "b", "c"]):
        outf.printt(2,'    vec='+str(vec)+', vec_name='+str(vec_name))
        for val in vec:
            outf.printt(2,'    i='+str(i)+', val='+str(val))
            if val == 0:
                i = i + 1
                continue
            ind_param = None
            factor = 1
            for known_val, known_index in unique_values.items():
                if known_val == 0:
                    continue
                ratio = val / known_val
                outf.printt(2,"    checking if component:"+str(val)+" is related by a factor to "+str(known_val))
                outf.printt(2,"    (ratio="+str(val)+"/"+str(known_val)+"="+str(val / known_val)+")")
                geom_const = is_geom_constant(ratio)
                outf.printt(2,"    found geometric constant: "+str(geom_const))
                if geom_const or abs(val - known_val) < tol:
                    outf.printt(2,'    determining factor... val='+str(val)+' known_val='+str(known_val)+' factor=abs(Val/know..)='+str(abs(val/known_val)))
                    factor = val / known_val
                    ind_param = known_index
                    break
            if ind_param is None:
                ind_param = next_index
                unique_values[val] = ind_param
                next_index += 1
            outf.printt(2,'    ind_param='+str(ind_param)+', i='+str(i))
            independent_parameters[i] = ind_param
            prefactors[i] = factor#np.sign(val) * factor
            outf.printt(2,'    prefactors['+str(i)+']='+str(prefactors[i]))
            outf.printt(2,'      '+str(np.sign(val))+', '+str(factor))
            i = i + 1
    return independent_parameters, prefactors


def create_grid_ranges(n_independent_parameters, independent_parameters, lattice_vectors, delta=0.1, step=0.02):
    print('n_independent_parameters=',n_independent_parameters,', independent_parameters=',independent_parameters,', lattice_vectors=',lattice_vectors)
    grid_ranges = [None] * n_independent_parameters
    grid_ranges_dirs = [None] * n_independent_parameters
    for ind_param, component_value in zip(independent_parameters, lattice_vectors.ravel()):
        print('ind_param, component_value=',ind_param, component_value)
        new_range = np.arange(component_value - delta, component_value + delta + step, step)
        new_range_str = np.array2string(new_range, precision=2)
        if grid_ranges[ind_param - 1] is None:
            grid_ranges[ind_param - 1] = new_range
            grid_ranges_dirs[ind_param - 1] = new_range_str
            outf.printt(2, "setting grid_ranges["+str(ind_param - 1)+"]="+str(new_range))

    # grid ranges should be paired e.g., given [1.5,1.6,...2.0] and [3.1,3.2,..3,6] make [[1.5,3.1],
    # [1.5,3.2], ..., [1.5,3.6],[1.6,3.1],[1.6,3.2], ..., [1.6,3.6], etc]
    grid_ranges = np.array([list(grid_ranges) for grid_ranges in itertools.product(*grid_ranges)])

    return grid_ranges, grid_ranges_dirs


def setupDFTinputdir(xcFunc_local):

    # Using xcFunc_local rather than var.xcFunc because this function can also be called to setup a subset of var.xcFunc (see APD.py)

    from pymatgen.ext.matproj import MPRester
    # from mp_api.client import MPRester <-- the new one. but requires new package install and api isn't finalized yet
    from pymatgen.io.vasp.sets import MPRelaxSet   # , MITMDSet <- don't use the latter as it messes with INCAR settings too much, e.g. sets ISPIN=1 unless explictly passed as a argument

    if var.mp_id is not None:
        # Query Materials Project to structure
        outf.printt(1, "  Querying materials project for structure...")
        outf.printt(2, "  Issuing command 'mpr = MPRester("+str(var.mapi_key)+")'")
        mpr = MPRester(var.mapi_key)  # object for connecting to MP Rest interface
        outf.printt(2, "  Issuing command 'var.structure = mpr.get_structure_by_material_id("+str(var.mp_id[0])+")'")
        var.structure = mpr.get_structure_by_material_id(var.mp_id[0])
        outf.printt(1, "  ...Structure retrieved.")
    else:
        # Read POSCAR, .cell or .in from job dir and convert to structure
        if var.dft_engine == "VASP":
            #POSCARfile = open(var.dirpath + "/POSCAR", "r")
            #atoms = ase.io.read(POSCARfile,format='vasp')
            poscar = Poscar.from_file(var.dirpath + "/POSCAR")
            var.structure = poscar.structure
        elif var.dft_engine == "CASTEP":
            cellFile = open(var.dirpath + "/castep.cell", "r")
            atoms = ase.io.castep.read_castep_cell(cellFile)
            var.structure = aio.AseAtomsAdaptor.get_structure(atoms)
        else:
            outf.printt(0,'  ERROR: User-specified structure/parameters not yet supported for QE, STOPPING')
            quit()

    var.speciesInitial = []
    for species in var.structure.species:
        if species not in var.speciesInitial:
            var.speciesInitial.append(species)
    var.nSpeciesInitial = [None] * len(var.speciesInitial)
    for iSpecies, species in enumerate(var.speciesInitial):
        var.nSpeciesInitial[iSpecies] = var.structure.species.count(species)
    # var.speciesInitial=list(set(var.structure.species)) # no. of unique species. 'set' ensures only unique elements from the list of all species. THIS DOESNT PRESERVE ORDER
    outf.printt(2, "  Species: " + str(var.speciesInitial))

    # Set up other inputs (here we could query INCAR etc directly from mp, c.f. "query the INCAR file directly" in notes_tetrahyrdrite.txt), but instead we use the
    # MP sets. I had thought the first option would be better (since these inputs should be closer to the reported predictions on mp) but I encountered at least one
    # example-- Fe2O3 --where the MP set gives the correct AFM solution whereas the INCAR from the webpage gives the (wrong) FM solution. May need to reconsider this later.

    # In the following I originally tried MPMDSet, but it doesn't seem to retain the LDAU and MAGMOM selections from 
    #the base MPRelaxSet.
    outf.printt( 1, "  Setting up INCAR settings based on MPRelaxSet; adapting for initial non-MD calculations)" )
    if var.dft_engine == "CASTEP":
        outf.printt(1, "  (will convert to CASTEP inputs after)")
        # Although the MPMDSet is meant to be uniquely determined by the 'CONFIG' block under 'MPRelaxSet' on https://pymatgen.org/pymatgen.io.vasp.sets.html,
        # I think there are additional rules applied (e.g. Fe2O3 is set up as AFM) which are possibly updated based on feedback. Therefore convert to CASTEP from the VASP
        # parameters rather than attempt to code the rules for CASTEP.

    # APDworkdir/ will contain initially DFT/xc/mat/1_... Here xc will either be PBE (default from materials project) or xc-functionals contained in xcFunc
    
    for xc in xcFunc_local:
        # Notes: ISMEAR=0 is 'safe' for metals, insulators and semiconductors. c.f. https://www.vasp.at/wiki/index.php/ISMEAR
        custom_settings = {
            "IBRION": 0,
            "ISMEAR": 0,
            "SIGMA": 0.04,
            "NSW": 0,
            "TEBEG": 300,
            "LCHARG": False,
            "LWAVE": False,
            "SMASS": 0,
            "PREC": "Normal",
            "ADDGRID": True,
            "ISIF": 0,
            "LREAL": False,
            "MDALGO": 3,
            "MAXMIX": 20,
            "NELM": 500,
            "NELMIN": 4,
        }  # Added on 21/04/2022 : SMASS=0 (was defaulting to -3, which _might_ (but probably doesn't) correspond somehow to NVE); Here we use EDIFF_PER_ATOM, which is recognized by pyMatGen, but gets convert to EDIFF. We would like to use EDIFF_PER_ATOM in the main code (e.g. to make it easier when changing size of cells), but because it gets converted to EDIFF we instead use a global variable var.ediff_per_atom. UPDATE: removed "EDIFF_PER_ATOM": 0.00001, as this is handled in the line below.
        var.ediff_per_atom = 1e-9 # ensures accurate forces and stresses (geom opt, phonons, elastic constants etc)
        # If we want to try MPMDSet again, the following seem sensible custom_settings:
        # custom_settings = {"LPLANE":None, 'ENCUT': 520} # For some reason ENCUT does not appear unless I specify it here. Set to the value it is supposed to have
        # according to https://pymatgen.org/pymatgen.io.vasp.sets.html (since MPRelaxSet is base)

        # Add to custom_settings based on xc: (c.f. https://www.vasp.at/wiki/index.php/VdW-DF_functional_of_Langreth_and_Lundqvist_et_al. If it doesn't work
        # search LUSE_VDW and click second link)
        var.vdw = False
        if xc.lower() == "pbesol":
            custom_settings["GGA"] = "PS"
        if xc.lower() == "revpbe":
            custom_settings["GGA"] = "RE"
            custom_settings["LUSE_VDW"] = True
            custom_settings["AGGAC"] = 0.0000
            custom_settings["LASPH"] = True
            var.vdw = True
        elif xc.lower() == "optpbe":
            custom_settings["GGA"] = "OR"
            custom_settings["LUSE_VDW"] = True
            custom_settings["AGGAC"] = 0.0000
            custom_settings["LASPH"] = True
            var.vdw = True
        elif xc.lower() == "optb88":
            custom_settings["GGA"] = "BO"
            custom_settings["LUSE_VDW"] = True
            custom_settings["AGGAC"] = 0.0000
            custom_settings["LASPH"] = True
            custom_settings["PARAM1"] = 0.1833333333
            custom_settings["PARAM2"] = 0.2200000000
            var.vdw = True
        elif xc.lower() == "optb86b":
            custom_settings["GGA"] = "MK"
            custom_settings["LUSE_VDW"] = True
            custom_settings["AGGAC"] = 0.0000
            custom_settings["LASPH"] = True
            custom_settings["PARAM1"] = 0.1234
            custom_settings["PARAM2"] = 1.0000
            var.vdw = True
        if var.spin_pol==False:
            custom_settings["ISPIN"] = 1
        elif var.spin_pol==True:
            custom_settings["ISPIN"] = 2
        relax = MPRelaxSet(var.structure, user_incar_settings=custom_settings, user_potcar_settings = {"W": "W_sv"})
        var.incar = relax.incar
        var.incar.pop("ISYM", None) # Removing this results in ISYM>0, I.e. using symmetry
        #  print('relax.incar["ISYM"]=',relax.incar["ISYM"],' after')
        if var.hubbard_U == False:
           outf.printt(2, f"  hubbard_U == False: removing LDAU and related parameters")
           var.incar.pop("LDAU")#, None)
           var.incar.pop("LDAUU")#, None)
           var.incar.pop("LDAUJ")#, None)
           var.incar.pop("LDAUL")#, None)
           var.incar.pop("LDAUPRINT")#, None)
           var.incar.pop("LDAUTYPE")#, None)
        #     if "LDAU" in relax.incar:
        #        print('LDAU _is_ in relax.incar')
        #     else:
        #        print('LDAU is _not_ in relax.incar')
        #     del relax.incar["LDAU"]
        #     print('relax.incar["LDAU"]=',relax.incar["LDAU"])
        #     del relax.incar["LDAUTYPE"]
        #     print('relax.incar["LDAUTYPE"]=',relax.incar["LDAUTYPE"])

        var.kpoints = relax.kpoints
        var.poscar = relax.poscar
        if var.dft_engine == "VASP":
            var.potcar = relax.potcar

        if var.dft_engine == "CASTEP":
            var.calc = ase.calculators.castep.Castep()  # (keyword_tolerance=3)
            var.calc.param.xc_functional = xc
            if xc.lower() != "pbe" and xc.lower() != "lda" and xc.lower() != "pbesol":
                print("  ERROR: xc not equal to PBE or LDA, and CASTEP dft_engine selected, STOPPING")
                quit()

            var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure)
            convertVASPtoCASTEP()
            var.calc._set_atoms = True

        if (
            var.dft_engine == "VASP"
            and var.vdw == True
            and os.path.isfile(var.dirpath + "/vdw_kernel.bindat") == False
        ):
            # Check this first to avoid generating directories if this file is not present
            outf.toAction(
                "You are using vdw xc-functionals: Please download or copy vdw_kernel.bindat (a prerequesite for running VASP using vdw) to: "
                + str(var.dirpath)
            )
            print("  ERROR: no vdw_kernel.bindat, STOPPING")
            quit()

        # Create directories
        filesys.createWorkdir(var.APDworkdir)
        filesys.createWorkdir(var.DFTdir)
        filesys.createWorkdir(var.DFTdir + "/" + xc)
        workdir = var.DFTdir + "/" + xc + "/" + var.mp_id[0] + "/" # Currently APD supports just one material type
        filesys.createWorkdir(workdir)
        var.DFTinputdir = workdir + "1_input"
        var.DFTinputpath = var.dirpath + "/" + var.DFTinputdir
        filesys.createWorkdir(var.DFTinputdir)

        # relax = MPMDSet(structure, start_temp=300, end_temp=300, nsteps=1000, spin_polarized=True, user_incar_settings=custom_settings)
        if var.dft_engine == "CASTEP" and var.species_pot:
            # Set up species_pot. For USP do not specify species_pot in setting.APD, then it will not be set here, and
            # will not appear in the .cell file (CASTEP will then assume USPP by default).
            outf.printt(1, f"  Setting species_pot to: {var.species_pot}")
            if var.species_pot == 'NCP':
                species = [str(x).replace("Element ", "") for x in var.speciesInitial]
                species_NCP_list = [] # ASE requires a list of tuples, each containing two elements: species, file
                for species_ind in species:
                   species_NCP_list.append((species_ind,"NCP"))
                var.calc.cell.species_pot = species_NCP_list

        writeDFTinputFiles(var.DFTinputdir, var.DFTinputpath, "", write_frac_coords=True) # the latter is needed for
            # using the phonopy castep interface which requires fractional coords

        if var.dft_engine == "CASTEP":
            # See if species_pot needs adjusting based on method of phonon calculation
            outf.printt( 1, f"  Determining method for calculating phonon properties - first determine number of "
                "independent displacements")
            # Need to determine if we are going to use finite displacement calculations or DFPT. If latter need to use 
            # norm-conserving pseudopotentials (ultrasoft pseudopotentials cannot be used for DFPT calculations in 
            # CASTEP)
            if var.phonon_mode == "DFPT" and not var.species_pot: # if species_pot have been provided assume the user
                    # knows what they are doing... (they should be norm conserving if DFPT is being used)
                outf.printt( 1, f"    phonon_mode = 'DFPT' set by user. - setting NCP pseudopotentials, as "
                    "these are required when using DFPT with CASTEP" )
                species = [str(x).replace("Element ", "") for x in var.speciesInitial]
                species_NCP_list = [] # ASE requires a list of tuples, each containing two elements: species, file
                for species_ind in species:
                   species_NCP_list.append((species_ind,"NCP"))
                var.calc.cell.species_pot = species_NCP_list
                writeDFTinputFiles(var.DFTinputdir, var.DFTinputpath, "")
            elif var.phonon_mode == "FINITE_DISP":
                outf.printt( 1, f"    phonon_mode = 'FINITE_DISP' set by user; will use finite displacement method" )
            elif var.phonon_mode == "AUTO":
                n_indep_disp = setupFiniteDispCalcs(var.DFTinputpath)
                outf.printt( 1, f"  Number of displacements: {n_indep_disp}")
                if n_indep_disp > var.n_indep_disp_crossover:
                    outf.printt( 1, f"    ... > {var.n_indep_disp_crossover}; will use DFPT - setting NCP pseudopotentials, as "
                        "these are required when using DFPT with CASTEP" )
                    if var.species_pot:
                        outf.printt( 1, "ERROR: Cannot do the above because species_pot has been fixed in settings.APD:"
                            " Please remove APDworkdir/, and then either remove species_pot tag, or else set" 
                            " phonon_mode = finite_disp. Then run APD again" ) 
                        quit()
                    else:
                        species = [str(x).replace("Element ", "") for x in var.speciesInitial]
                        species_NCP_list = [] # ASE requires a list of tuples, each containing two elements: species, file
                        for species_ind in species:
                           species_NCP_list.append((species_ind,"NCP"))
                        var.calc.cell.species_pot = species_NCP_list
                        writeDFTinputFiles(var.DFTinputdir, var.DFTinputpath, "")
                else:
                    outf.printt( 1, f"    ... <= {var.n_indep_disp_crossover}; will use finite displacement method")

        # Also write out conventional cell with suffix _conv
        SGA = SpacegroupAnalyzer(var.structure)
        structure_conv = SGA.get_conventional_standard_structure()
        if var.dft_engine == "VASP":
            poscar_conv = Poscar(structure_conv)
            poscar_conv.write_file(var.DFTinputpath + "/POSCAR_conv", direct = False)
        elif var.dft_engine == "CASTEP":
            atoms_conv = aio.AseAtomsAdaptor.get_atoms(structure_conv)
            with open(var.DFTinputpath + "/castep_conv.cell", "w") as CASTEPcellFile:
                ase.io.castep.write_castep_cell(CASTEPcellFile, atoms_conv)

    DFTobjectsInitialized = True


def setupHighElecParadir():

    # Setup directory for optimizing DFT parameters (energy cutoff and kpoints)
    # Notes: seemed to achieve convergence for LLZO, target 1 meV/atom, but if there are issues consult:
    # http://www.castep.org/files/workshop_lectures_2012/Convergence-print-1.pdf
    # and mark: 'Don’t be fooled into choosing too high a plane-wave cutoff when you are really just converging fine FFT grid'
    # in particular consider including ADDGRID=true (for VASP); grid_scale or peraps fine_grid_scale/fine_gmax (CASTEP)

    filesys.createWorkdir(var.DFTnonMDdir)
    filesys.createWorkdir(var.DFTnonMDhighElecParasdir)

    var.incar["ISIF"] = 0
    var.incar.pop("ISYM", None) # use symmetry
    var.incar["NSW"] = 0
    var.ediff_per_atom = 0.1 * 0.001 * var.dft_energy_tol # Use slightly less stringent tolerance as we only need to converge the energy (meV, then factor 1/10th to be on safe side)

    # Cutoff test
    # Adjust k-points if necessary:
    print('kpoints from 1_/=',var.kpoints.kpts)
    # kpnts_prev = copy.deepcopy(var.kpoints.kpts) # we used to reduce kpoints for efficiency, but this can affect reliability of the kpoint convergence
    # var.kpoints.kpts[0] = [1, 1, 1]
    # If no mp_id specified (I.e. we are reading user-supplied structure and parameters) then need to alter baseline
    # k-points from the value specified by user to that from MP
    if var.mp_id == None:
       # I think we already have atoms (as well as structure) objects to hand... other wise add following later: var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure) 
       from pymatgen.io.vasp.sets import MPRelaxSet
       relax = MPRelaxSet(var.structure)
       var.kpoints = relax.kpoints 

    cut_off_prev = var.incar["ENCUT"]
    for encut in range(300,1100,100): # this means max 1000 eV. tried max 800 eV for a while but no good for DFPT. perphaps in future make choice dependent on whether DFPT is being used.
       encutDirec = var.DFTnonMDhighElecParasdir+'/encut' + str(encut)
       encutPath = var.dirpath + "/" + encutDirec
       filesys.createWorkdir(encutDirec)
       var.incar["ENCUT"] = int(encut)
       if var.dft_engine == "CASTEP":
           convertVASPtoCASTEP2()
           # For energy convergence if encut is too high, finite_basis_spacing may need increasing. Otherwise the
           # default (5) can be too small and result in no new plane waves being added as encut is increased, so that
           # the energy doesn't change (causes error in CASTEP). Note we only do this for energies - for stresses it
           # can result in less accurate values
           if var.incar["ENCUT"] >= 700:
               var.calc.param.finite_basis_spacing = 10
           else:
               var.calc.param.finite_basis_spacing = None # Will need to check this line works. i imagine it does, but if not set to 5 explicitly

       writeDFTinputFiles(encutDirec, encutPath, var.DFTinputpath)
       if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
           script = "script_" + var.dft_engine + "_singleShot"
           outf.printt(2, "  " + script
               + " job submission script present in run directory. Copying to stage sub-directories and renaming as script",
           )
           copyVASPscriptAdjustINCAR(script, encutPath)

    # Kpoints
    # var.kpoints.kpts = kpnts_prev
    print('kpoints=',var.kpoints.kpts)
    # Ideally have encut at a low value for this test, but for both CASTEP and VASP if use e.g. 520 eV this results in much
    # too large kpoints from the convergence test. Choose a safe 'large' value (700 eV) for this test. 
    # var.incar["ENCUT"] = cut_off_prev # 700 seems sufficient, but if not robust enough perhaps reinstate this instead
    var.incar["ENCUT"] = 700 
    if var.dft_engine == "CASTEP":
       var.calc.param.finite_basis_spacing = None
    kpoint_factors = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0] # temporarily increased kpoints factors (was up to 2.0 before)
    initial_kpoints = var.kpoints.kpts[0][0:3]
    unique_kpoints = set() # Set to store unique kpoints
    for factor in kpoint_factors:
       scaled_kpoints = tuple(int(math.floor(x * factor)) for x in initial_kpoints) # changed from 'round' to math.floor as latter always rounds down

       # Skip if these kpoints have been processed already
       if scaled_kpoints in unique_kpoints or scaled_kpoints == (0, 0, 0):
           continue

       # Mark these kpoints as processed
       unique_kpoints.add(scaled_kpoints)

       kpointsDirec = var.DFTnonMDhighElecParasdir + '/kpnts' + "_".join(map(str, scaled_kpoints))
       kpointsPath = var.dirpath + "/" + kpointsDirec
       filesys.createWorkdir(kpointsDirec)

       var.kpoints.kpts = [list(scaled_kpoints)]
       if var.dft_engine == "CASTEP": 
           var.calc.cell.kpoint_mp_grid = var.kpoints.kpts[0][0:3]

       if var.dft_engine == "CASTEP":
           convertVASPtoCASTEP2()
       writeDFTinputFiles(kpointsDirec, kpointsPath, var.DFTinputpath)

       if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
           script = "script_" + var.dft_engine + "_singleShot"
           # Assuming a function exists to copy and adjust script
           copyVASPscriptAdjustINCAR(script, kpointsPath)

    var.incar["ENCUT"] = cut_off_prev


def setupGeomOptdir():

    # Setup directory for DFT geometry optimization

    # Type of geometry optimization will depend on number of independent lattice parameter components.
    # We check this here first to see if we can rely on VASP/CASTEP internal optimization (reasonably
    # trustworthy for isotropic systems) or we need to do a geometry optimization ourselves using a grid
    # search.

    filesys.createWorkdir(var.DFTnonMDgeomOptdir)

    # The following parameters are required whether we use internal geometry optimization or grid method:
    var.incar.pop("ISYM", None) # use symmetry
    var.incar["IBRION"] = 2
    var.incar["NSW"] = 1000 # even for grid, optimization of internal DoF required
    var.incar["EDIFFG"] = -0.001 # High precision geometry needed for phonons
    if var.dft_engine == "CASTEP":
        var.calc.param.finite_basis_spacing = None # This will have been altered by the high elec paras stage,
                # but should here be reset to the default (5), else the finite_basis correction will be inaccurate

    independent_parameters, prefactors = identify_independent_parameters(var.atoms.get_cell())
    print('vectors: ',var.atoms.get_cell())
    print('independent_parameters: ',independent_parameters)
    print('prefactors: ',prefactors)
    n_independent_parameters = max(x for x in independent_parameters if x is not None)
    print('n_independent_parameters: ',n_independent_parameters)
    outf.printt(1, "  Number of independent lattice parameter components: " + str(n_independent_parameters))
    if n_independent_parameters>2: # this could be increased to 3; need to change 'optDirec=...' line and use fewer grid points
        outf.printt(1,
                    "ERROR: too many independent degrees of freedom to optimize geometry using grid method, STOPPING")
        quit()

    # Set up directories for geometry optimization
    if n_independent_parameters == 1 and not var.force_grid_opt:

        # additional parameters for using internal geometry optimization of the DFT codes
        var.incar["ISIF"] = 3
        if var.dft_engine == "CASTEP":
            convertVASPtoCASTEP2()  # for ISIF=2 above this will add the relevant extra commands to the .cell file
            var.calc.param.task = "GeometryOptimization" # both grid and internal methods use this, but for grid additionally fix_all_cell=True
            var.calc.param.geom_energy_tol = [2e-9, "eV"] # In VASP can't have both geom_energy_tol and geom_force_tol
                    # (both controlled by EDIFFG), so one of these needs to be added post-conversion from VASP to 
                    # CASTEP
            var.calc.param.geom_max_iter = 500
            var.calc.param.continuation = "default"
        writeDFTinputFiles(var.DFTnonMDgeomOptdir, var.DFTnonMDgeomOptpath, var.DFTinputpath)

        if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
            script = "script_" + var.dft_engine + "_singleShot"
            outf.printt(
                2,
                "  "
                + script
                + " job submission script present in run directory. Copying to stage sub-directories and renaming as script",
            )
            copyVASPscriptAdjustINCAR(script, var.DFTnonMDgeomOptpath)
    else:
        # grid search
        lattice_vectors = var.atoms.get_cell()
        grid_ranges, grid_ranges_dirs = create_grid_ranges(
            n_independent_parameters, independent_parameters, lattice_vectors
        )
        # additional parameters for DFT codes
        var.incar["ISIF"] = 2
        # other possibles for castep? :
        # var.calc.param.task = "SinglePoint"
        # var.calc.param.fix_all_cell = 'True' # FIX_ALL_CELL : TRUE. This fixed lattice parameters but positions can vary <- this needs to be in .cell
        # var.calc.param.md_num_iter = 1 # shouldn't be needed since 'SinglePoint' should just do one step

        # Set up dirs for each combination of independent lattice vector component
        # E.g., for -6.618035  6.618035  6.350853 6.618035 -6.618035  6.350853 6.618035  6.618035 -6.350853
        # We need directories of the form: 2_nonMD/geomOpt/{-6.71_5.35 , -6.71_5.45, ... , -6.51_7.25}
        print('lattice_vectors to change:',lattice_vectors)

        # Set up independent parameter sets (e.g. -6.71, 5.35)
        # print('grid_ranges=',grid_ranges) # debug
        indep_para_val=[0]*n_independent_parameters
        for indep_para_val in grid_ranges:
           # print('indep_para_val=',indep_para_val) # debug
           # print('independent_parameters=',independent_parameters) # debug
           new_cell = np.zeros((3, 3))
           for i, val in enumerate(independent_parameters):
              # print('i, val=',i,val) # debug
              # var.atoms.cell[i // 3, i % 3] = prefactors[i] * indep_para_val[val-1]
              new_cell[i // 3, i % 3] = prefactors[i] * indep_para_val[val-1]
           var.atoms.set_cell(new_cell, scale_atoms=True)
           # print('  lattice vectors=',var.atoms.cell) # debug
           # print('len(grid_ranges)=',len(grid_ranges),', grid_ranges=',grid_ranges)
           if n_independent_parameters==1:
              optDirec=var.DFTnonMDgeomOptdir+'/'+"{:.10f}".format(indep_para_val[0])
           elif n_independent_parameters==2:
              optDirec=var.DFTnonMDgeomOptdir+'/'+"{:.10f}_{:.10f}".format(indep_para_val[0], indep_para_val[1])
           elif n_independent_parameters==3:
              optDirec=var.DFTnonMDgeomOptdir+'/'+"{:.10f}_{:.10f}_{:.10f}".format(indep_para_val[0], indep_para_val[1], indep_para_val[2])
           optPath=var.dirpath + "/" +optDirec
           filesys.createWorkdir(optDirec)
           if (
               var.dft_engine == "VASP"
           ):  # for VASP, writeDFTinputFiles() below writes from var.structure, var.poscar etc, not atoms objects
               var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
               var.poscar = Poscar(var.structure)
           if var.dft_engine == "CASTEP":
               convertVASPtoCASTEP2()  # for ISIF=2 above this will add the relevant extra commands to the .cell file
               var.calc.param.task = "GeometryOptimization" # both grid and internal methods use this, but for grid additionally fix_all_cell=True
               var.calc.param.geom_energy_tol = [2e-9, "eV"] # see earlier comment ('In VASP can't...')
               var.calc.param.continuation = "default"
           writeDFTinputFiles(optDirec, optPath, var.DFTinputpath)

           if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
               script = "script_" + var.dft_engine + "_singleShot"
               outf.printt(2,"  " + script
                   + " job submission script present in run directory. Copying to stage sub-directories and renaming as script")
               copyVASPscriptAdjustINCAR(script, optPath)


def setupEnergydir():

    # Setup directory for single-shot energy calculation of optimized geometry
    # Note: the calculation itself is only necessary if using the grid method of geometry optimization
    # If we used internal optimization (internal to the DFT code) we just copy over the outputs from the geomOpt dir

    filesys.createWorkdir(var.DFTnonMDenergydir)

    # Should be able to remove following as it should already have been reset in 'setupGeom...'. (Do need to check as
    # we shouldn't be adjusting this for EFS generation as it results in less accurate stresses. The slightly less
    # accurate energy is not important as it is a fixed offset)
    # if var.dft_engine == "CASTEP":
    #     var.calc.param.finite_basis_spacing = None # This will have been altered by the high elec paras stage,

    if DFTanalysis.geom_opt_used_grid():

       # Read relaxed geometry from var.DFTnonMDgeomOptdir into var.atoms object

       # Switch to root directory to access castep keywords file, ensuring consistent atom positions during read.
       os.chdir(var.dirpath)

       # magmomsPrev=var.atoms.get_initial_magnetic_moments()
       if var.dft_engine == "VASP":
           # Take from POSCAR file
           POSCARfile = open(var.DFTnonMDgeomOptdir + "/POSCAR_rlx", "r")
           atomsRelaxed = ase.io.read(POSCARfile,format='vasp')
       elif var.dft_engine == "CASTEP":
           # Take from .cell file
           cellFile = open(var.DFTnonMDgeomOptdir + "/castep_rlx.cell", "r")
           atomsRelaxed = ase.io.castep.read_castep_cell(cellFile)
           #  var.atoms.set_initial_magnetic_moments=var.atoms.get_magnetic_moments # we need to do this because writeDFTinputfiles uses var.atoms.get_initial_magnetic_moments() to write out the .cell file
           # update: above doesn't work because apparently atoms isn't even given 'get_magnetic_moments' as a method... just reset the magnetic moments to the 'initial' ones we read in, after all we are using these as the starting point for every other DFT calculation...
           # var.atoms.set_initial_magnetic_moments(magmoms=magmomsPrev)
       var.incar["IBRION"] = 2
       var.incar.pop("ISYM", None) # use symmetry
       var.incar["ISIF"] = 2
       var.incar["EDIFFG"] = -0.001 # High precision geometry needed for phonons
       var.incar["NSW"] = 1000 # even for fixed latt paras, optimization of internal DoF required

       # Cut off and energy from highElec (or geomopt?)
       # var.calc.param.cut_off_energy = [800, "eV"] # these are for consistency with geom opt where, for now, i have them hardwired
       # kpoints...

       # Need to make sure the following are properly set when we call convertVASPtoCASTEP2() below:
       # var.calc.param.task = "GeometryOptimization"

       var.atoms.set_cell(atomsRelaxed.get_cell())
       var.atoms.set_positions(atomsRelaxed.get_positions())
       var.atoms.set_chemical_symbols(atomsRelaxed.get_chemical_symbols())

       if (
           var.dft_engine == "VASP"
       ):  # for VASP, writeDFTinputFiles() below writes from var.structure, var.poscar etc, not atoms objects
           var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
           var.poscar = Poscar(var.structure)
       elif var.dft_engine == "CASTEP":
           convertVASPtoCASTEP2()
           var.calc.param.task = "GeometryOptimization"
           var.calc.param.geom_energy_tol = [2e-9, "eV"] # see earlier comment ('In VASP can't...')
           var.calc.param.continuation = "default"
           #var.calc.param.finite_basis_corr = 2 # for a geom opt the default would be to turn this off, which would result
                   # in finite stresses after the geom opt (when they should be nearly zero). UPDATE: now done for all castep
                   # jobs in 'convertVASPtoCASTEP2'
       writeDFTinputFiles(var.DFTnonMDenergydir, var.DFTnonMDenergypath, var.DFTinputpath)

       if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
           script = "script_" + var.dft_engine + "_singleShot"
           outf.printt(
               2,
               "  "
               + script
               + " job submission script present in run directory. Copying to stage sub-directories and renaming as script",
           )
           copyVASPscriptAdjustINCAR(script, var.DFTnonMDenergypath)

    else:

       # In this case don't need to recompute energy as it will part of last step of the geometry optimization internal to DFT code
       deepest_dir = find_deepest_cont_dir(var.DFTnonMDgeomOptpath) # find deepest cont/ dir
       if var.dft_engine == "VASP":
           os.system("cp " + deepest_dir + "/{vasprun.xml,OUTCAR} " + var.DFTnonMDenergypath)
       elif var.dft_engine == "CASTEP":
           os.system("cp " + deepest_dir + "/castep.castep " + var.DFTnonMDenergypath)


def setupElasticdir():

    # Set up directory for elastic calculations
    # (kpoints? necessary? only change C11 etc to 0.01 or 0.02 for BaZrO3 so don't implement, at least until i find evidence they are needed)

    # Switch to root directory to access castep keywords file, ensuring consistent atom positions during read.
    os.chdir(var.dirpath)

    filesys.createWorkdir(var.DFTnonMDelasticdir)

    # DFT settings
    var.incar["ISYM"] = 0  # presumably this needs turning off again since we will displace cell paramters
    var.incar["NSW"] = 1000 # needed for both DFT codes - for CASTEP for internal DoF opt; for VASP, multiple configs in one run
    if var.dft_engine == "CASTEP":
        var.incar["ISIF"] = 2
        castepCellFile = open(var.DFTnonMDphononpath + "/castep.cell", "r") # don't take .cell from energy/ because where internal geom opt was used the geometry-relaxed .cell is not present here
        var.atoms = ase.io.castep.read_castep_cell(castepCellFile)
        # For CASTEP we use script for elastic constants. This requires the .castep for perfect structure
        os.system("cp " + var.DFTnonMDenergypath + "/castep.castep " + var.DFTnonMDelasticpath)
        convertVASPtoCASTEP2()
        var.calc.param.task = "GeometryOptimization"
        var.calc.param.continuation = "default"
        var.calc.param.num_backup_iter = 5
    if var.dft_engine == "VASP": # VASP uses internal algorithm for calculating elastic constants, requiring following:
        var.incar["ISIF"] = 3
        var.incar["IBRION"] = 6
        var.structure = Structure.from_file(var.DFTnonMDphononpath + "/POSCAR")
        var.poscar = Poscar(var.structure)
    writeDFTinputFiles(var.DFTnonMDelasticdir, var.DFTnonMDelasticpath, var.DFTinputpath, write_frac_coords=True)
    # Relaxed cell from last sub-stage (phonons)
    # Should be able to remove this, as we now write structure direct from the atoms/structure object
    # if var.dft_engine == "CASTEP":
    #     # For CASTEP we use script for elastic constants. This requires the .castep for perfect structure
    #     os.system("cp " + var.DFTnonMDenergypath + "/castep.castep ./")
    #     os.system("cp " + var.DFTnonMDphononpath + "/castep.cell ./") # don't take .cell from energy/ because where internal geom opt was used the geometry-relaxed .cell is not present here
    # else:
    #     os.system("cp " + var.DFTnonMDphononpath + "/POSCAR ./")

    if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
        script = "script_" + var.dft_engine + "_singleShot"
        outf.printt(2, "  " + script + " job submission script present in run directory. Copying to stage "
                "sub-directories and renaming as script")
        copyVASPscriptAdjustINCAR(script, var.DFTnonMDelasticpath)

    if var.dft_engine == "CASTEP":
        # Use 'elastic-constants' script to set up calculations for elastic constants
        os.chdir(var.DFTnonMDelasticpath)
        os.system("/$APDdir/otherCodes/elastic-constants/generate_strain.py castep")
        os.system("/$APDdir/tools/bash/setupelasticconstantsdirsCASTEP.sh")


def setupEvdir():

    # Set up directory for E-V curve calculations
    # Read relaxed geometry from var.DFTnonMDenergydir into var.atoms object

    # Switch to root directory to access castep keywords file, ensuring consistent atom positions during read.
    os.chdir(var.dirpath)

    if var.dft_engine == "VASP":
        # Take from POSCAR file
        POSCARfile = open(var.DFTnonMDelasticdir + "/" + var.DFTinput[1], "r")
        atomsRelaxed = ase.io.read(POSCARfile,format='vasp')
    elif var.dft_engine == "CASTEP":
        # Take from .cell file
        cellFile = open(var.DFTnonMDelasticdir + "/" + var.DFTinput[1], "r")
        atomsRelaxed = ase.io.castep.read_castep_cell(cellFile)
    var.atoms.set_cell(atomsRelaxed.get_cell())
    var.atoms.set_positions(atomsRelaxed.get_positions())
    var.atoms.set_chemical_symbols(atomsRelaxed.get_chemical_symbols())

    filesys.createWorkdir(var.DFTnonMDevdir)

    var.incar.pop("ISYM", None) # use symmetry
    var.incar["ISIF"] = 2
    var.incar["IBRION"] = -1
    var.incar["NSW"] = 1000 # be sure to optimize internal D of F!
    var.incar["ISIF"] = 2 # ...but remember to keep lattice parameters fixed
    if var.dft_engine == "CASTEP":
        convertVASPtoCASTEP2()
        var.calc.param.task = "GeometryOptimization"

    # setup up jobs for 10 volumes around the equilibrium volume. Take steps of 0.1*vol_eqbr/natoms
    volEqbr = var.atoms.get_volume()
    natoms = len(var.atoms)
    volOverAtomsEqbr = volEqbr / natoms
    cellEqbr = var.atoms.get_cell()
    step = 0.1
    volsOverAtoms = []
    for i in range(-5, 6):
        volsOverAtoms.append(round(volOverAtomsEqbr + float(i) * step, 2))  # Round to 8 d.p.
    outf.printt(2, "  Volumes to consider: " + str(volsOverAtoms))

    for volOverAtoms in volsOverAtoms:
        filesys.createWorkdir(var.DFTnonMDevdir + "/VoverN" + str(volOverAtoms))
        # var.atoms.set_volume(vol)
        var.atoms.set_cell(cellEqbr * ((volOverAtoms / volOverAtomsEqbr) ** (1 / 3)), scale_atoms=True)
        if (
            var.dft_engine == "VASP"
        ):  # for VASP, writeDFTinputFiles() below writes from var.poscar, not atoms objects
            var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
            var.poscar = Poscar(var.structure)
        subdir = "/VoverN" + str(volOverAtoms)
        writeDFTinputFiles(var.DFTnonMDevdir + subdir, var.DFTnonMDevpath + subdir, var.DFTinputpath)
        if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
            script = "script_" + var.dft_engine + "_singleShot"
            outf.printt( 2, f"  {script} job submission script present in run directory. Copying to stage "
                "sub-directories and renaming as script" )
            copyVASPscriptAdjustINCAR(script, var.DFTnonMDevpath + subdir)


def setupFiniteDispCalcs(pathToRun):

    """
    Use Phonopy and custom script to set up finite displacement calculations, I.e. directories with calculations for 
    displaced atom configurations
    """

    #os.chdir(pathToRun)
    phonopyCommand = f'phonopy -d --dim="{var.cell_for_phonons}"'
    if var.dft_engine == "CASTEP":
        phonopyCommand += ' --castep -c castep.cell'
    phonopy_dir_script = os.path.join(os.environ['APDdir'], "tools/bash/setupphonopydirs" + var.dft_engine + ".sh")
    try:
        # Run Phonopy
        outf.printt(2, f"  About to run {phonopyCommand} in path: {pathToRun}")
        subprocess.run(phonopyCommand, shell=True, check=True, cwd=pathToRun)
        #os.system(phonopyCommand)

        # Run Phonopy directory setup script
        outf.printt(2, "  Setting up directories for displacement calculations...")
        outf.printt(2, f"  Running: {phonopy_dir_script}")
        result = subprocess.run([phonopy_dir_script], check=True, capture_output=True, text=True, cwd=pathToRun)
        n_indep_disp = int(result.stdout.strip())
    except subprocess.CalledProcessError:
        outf.printt(0, "  Bash script failed, STOPPING")
        quit()

    return n_indep_disp


def resize_magmom(natoms_target):

    magmom = var.incar["MAGMOM"]
    magmomEqual = all(ele == magmom[0] for ele in magmom)
    if magmomEqual == True:
        var.incar["MAGMOM"] = [magmom[0]] * natoms_target
        resize_magmom_success = True
    else: 
        resize_magmom_success = False

    return resize_magmom_success


def find_deepest_cont_dir(base_dir):
    current_dir = base_dir
    while True:
        #print('os.listdir(current_dir) = os.listdir(',current_dir,') =',os.listdir(current_dir))
        cont_dir = [d for d in os.listdir(current_dir) if d == 'cont' and os.path.isdir(os.path.join(current_dir, d))]
        if not cont_dir:
            break
        current_dir = os.path.join(current_dir, cont_dir[0])
    return current_dir


def setupPhonondir(finiteDisp):

    """
    Sets up the directory for phonon calculations. Phonopy is called from command-line, however phonopy does not 
    adjust properties such as kpoints and extensive properties such as magmoms so we adjust these separately in-code.

    Parameters:
    - finiteDisp (bool): A flag to indicate whether to perform a finite displacement calculation.
    
    Returns:
    - n_indep_disp (int): Number of independent displacements for phonon calculations.
    
    The function performs the following operations:
    - Reads the relaxed atomic structure from the appropriate files.
    - Sets up the working directory for phonon calculations.
    - Configures INCAR settings based on whether a finite displacement calculation is to be performed.
    - Sets up the working directory for phonon calculations

    In addition just for finite displacement calculations:
    - Adjusts MAGMOM in INCAR when resizing the cell for finite displacement calculations in VASP.
    - Sub-directories for displacement calculations are set up
    - For CASTEP, phonopy will not write out adjusted k-points etc to end of .cell files. We do this manually at the end.

    Notes: if NBANDS is present in INCAR (it shouldn't be from queried files but if hand-made it might
    be) then it should be removed here as when the cell is scaled is might have to be larger. (the default
    VASP value takes care of this)
    """

    # Get relaxed structure. For 'internal'/'grid' optimizations we read the last config from geom/ or energy/ dirs
    # respectively. The last config in energy/ is required for grid-based optimizations because it is here we
    # do the atom relaxation (in geom/ we just do the latt para optimization for the 'grid' case)
    if var.dft_engine == "VASP":
        if DFTanalysis.geom_opt_used_grid():
           deepest_dir = find_deepest_cont_dir(var.DFTnonMDenergypath)
        else:
           deepest_dir = find_deepest_cont_dir(var.DFTnonMDgeomOptpath)
        CONTCARfile = open(os.path.join(deepest_dir, var.DFToutput[4]), "r")
        atomsRelaxed = ase.io.read(CONTCARfile,format='vasp')
    elif var.dft_engine == "CASTEP":
        if DFTanalysis.geom_opt_used_grid():
           deepest_dir = find_deepest_cont_dir(var.DFTnonMDenergypath)
        else:
           deepest_dir = find_deepest_cont_dir(var.DFTnonMDgeomOptpath)
        DFToutputFile = open(os.path.join(deepest_dir, var.DFToutput[5]), "r") # .geom file
        atomsAll = ase.io.castep.read_castep_geom(DFToutputFile)
        atomsRelaxed = atomsAll[-1]

    var.atoms.set_cell(atomsRelaxed.get_cell())
    var.atoms.set_positions(atomsRelaxed.get_positions())
    var.atoms.set_chemical_symbols(atomsRelaxed.get_chemical_symbols())

    filesys.createWorkdir(var.DFTnonMDphonondir)

    # # Set up parameters
    # # KPOINTS : Currently read in from 1_ and corresponding to the 1x1x1sc. Need to multiply by factor stored in
    # # 2_nonMD/highElecParas/stageOutput as 'Kpoints factor: 2.0'
    # with open(os.path.join(var.DFTnonMDhighElecParaspath, 'stageOutput'), 'r') as file:
    #     lines = file.readlines()
    # kpoint_factor = None
    # for line in lines:
    #     if 'Kpoints factor:' in line:
    #         end_of_line = line.split("Kpoints factor:")[-1].strip() # e.g., 2.0
    #         print('end_of_line=',end_of_line)
    #         kpoint_factor = int(float(end_of_line))
    # if kpoint_factor == None:
    #     outf.printt(0, '  ERROR: Kpoints factor not found in high elec stageOutput file, STOPPING')
    #     quit()
    # for i in range(len(var.kpoints.kpts[0])):
    #     var.kpoints.kpts[0][i] = var.kpoints.kpts[0][i] * kpoint_factor
    # outf.printt(2, f'  New kpoints after adjusting by factor in high_elec stage (for 1x1x1sc): {var.kpoints.kpts[0]}')

    var.incar["NSW"] = 0
    if finiteDisp:

        #var.incar["ISYM"] = 0 # this needs turning off again since we will displace atoms. Update: suggested paras for
        #        phonon calcs have SYMMETRY_GENERATE for CASTEP. c.f. : https://www.tcm.phy.cam.ac.uk/castep/Phonons_Guide/Castep_Phononsch2.html#:~:text=The%20setup%20for%20a%20CASTEP,_FRAC%20or%20%25BLOCK%20POSITIONS_ABS.
        #        the following setting results in SYMMETRY_GENERATE = True when convertVASPtoCASTEP2() is called later
        var.incar["ISYM"] = 2 # apparently = 1 default for uspp though in VASP?
        var.incar["ISIF"] = 2 # stress tensors... is this necessary?
        var.incar["IBRION"] = -1

        # Adjust k-points and extensive properties ahead of calling phonopy (phonopy does not adjust these by itself).

        # KPOINTS : reduce by values in var.cellFactors (contains e.g. [2, 2, 1])
        kpoint_factors = [1.0 / factor for factor in var.cellFactors]
        initial_kpoints = var.kpoints.kpts[0][0:3]
        # Adjust the kpoints based on the cell factors
        for i in range(len(initial_kpoints)):
            var.kpoints.kpts[0][i] = int(math.floor(initial_kpoints[i] * kpoint_factors[i]))

        # LDAU : to do

        # MAGMOMS:
        cellFactor = np.prod(var.cellFactors)
        resize_magmom_success = resize_magmom( len(var.incar["MAGMOM"]) * cellFactor )
        if not resize_magmom_success:
            # Unlike later, here we have a simple 2x2x2 supercell so can easily adjust magmoms. 
            outf.printt( 1, f"    Magnetic moments in INCAR file are not all equal; updating MAGMOM for new "
                "{var.cell_for_phonons} supercell" ) 
            # First we need the number of species however because VASP groups atom types together in POSCAR.
            # (is there a simpler way in ASE for this?)
            nAtomsPerSpecies = []
            prevAtomicNum = 0
            nAtomsCounter = 1
            nSpeciesCounter = 0
            for number in var.atoms.numbers:
                if prevAtomicNum != 0:
                    if number != prevAtomicNum:
                        nSpeciesCounter = nSpeciesCounter + 1
                        nAtomsPerSpecies.append(nAtomsCounter)
                        nAtomsCounter = 1
                    else:
                        nAtomsCounter = nAtomsCounter + 1
                prevAtomicNum = number
            nSpeciesCounter = nSpeciesCounter + 1
            nAtomsPerSpecies.append(nAtomsCounter)
            print("nAtomsPerSpecies=", nAtomsPerSpecies)
            print("original MAGMOM=", var.incar["MAGMOM"])
            nAtomsCounter = 0
            magmomNew = []
            for nSpecies in range(0, nSpeciesCounter):
                magmomToCopy = var.incar["MAGMOM"][nAtomsCounter : nAtomsCounter + nAtomsPerSpecies[nSpecies]]
                magmomNew.extend(cellFactor * magmomToCopy)  # e.g cellFactor=8 for var.cell_for_phonons='2 2 2'
                nAtomsCounter = nAtomsCounter + nAtomsPerSpecies[nSpecies]
            var.incar["MAGMOM"] = magmomNew
            print("new MAGMOM=", var.incar["MAGMOM"])

    else:
        # Note DFPT is only used when CASTEP is being used, but as elsewhere we use vasp variables where possible and
        # convert to CASTEP variables later
        var.incar["ISYM"] = 2
        var.incar["IBRION"] = 8 # for DFPT (8 rather than 7: uses symmetry to reduce the number of displacements)
        var.incar["NSW"] = 300 # this will be converted to 'PHONON_MAX_CYCLES' later on (for castep)

    if not finiteDisp:
        if var.dft_engine == "CASTEP":
            convertVASPtoCASTEP2()
            var.calc.param.phonon_fine_method = "interpolate"
            var.calc.param.backup_interval = 1800
            var.calc.param.num_backup_iter.clear()
            var.calc.param.continuation = "default"
            var.calc.param.elec_energy_tol = [1.0e-10, "eV"]
            var.calc.cell.phonon_kpoint_mp_grid = [3, 3, 3] # odd, so as to include gamma point
            var.calc.cell.phonon_fine_kpoint_path_spacing = [0.03, '1/ang']
            print('var.phonon_fine_kpoint_path=',var.phonon_fine_kpoint_path)
            if var.phonon_fine_kpoint_path:
                outf.printt(2, f'  Using user-supplied path to set: phonon_fine_kpoint_path')
                kpoint_string = str(var.phonon_fine_kpoint_path)
                print('kpoint_string=',kpoint_string)
                #kpoint_list = kpoint_string.split('\n') # with this the '\n' are not correctly identified
                kpoint_list = eval("'''{}'''".format(kpoint_string)).split('\n')
                print('kpoint_list:',kpoint_list)
                #quit()
                var.atoms.calc.cell.phonon_fine_kpoint_path = kpoint_list
            else:
                # Use seekpath to get kpoint path (phonopy also does it this way)
                outf.printt(2, f'  Determining Kpoint path using seekpath')
                numbers = var.atoms.get_atomic_numbers() # hopefully this is the right primitive atoms object
                inp = (var.atoms.cell, var.atoms.get_scaled_positions(), numbers)
                point_coords = seekpath.get_path(inp)['point_coords']
                coords_string = "\n".join([" ".join(map(str, coords)) for coords in point_coords.values()])
                outf.printt(2, f'    Kpoint path determined from seekpath:"{coords_string}\n"')
                var.atoms.calc.cell.phonon_fine_kpoint_path = [coords_string]
            writeDFTinputFiles(var.DFTnonMDphonondir, var.DFTnonMDphononpath, var.DFTinputpath, write_frac_coords = True)
        else:
            # DFPT not supported for VASP (finite_disp_mode = True in that case in APD.DFTnonMD, so shouldnt get here)
            print('ERROR: DFPT for phonons shouldnt proceed when using VASP, STOPPING')
            quit()
            var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
            var.poscar = Poscar(var.structure)
            writeDFTinputFiles(var.DFTnonMDphonondir, var.DFTnonMDphononpath, var.DFTinputpath)
    else:
        # Write the adjusted parameters to DFT input files so they can be copied over to the disp#/ dirs once they have 
        # been generated. 
        if var.dft_engine == "CASTEP":

            # For CASTEP when we write out the adjusted parameters we also have to transform the atoms object to the 
            # supercell internally, because ase won't allow e.g. magmom to be extended beyond the current number of atoms 
            # in the atoms object. After reading the tail end of .cell we then reset the atoms object to its original form 
            # and re-write out the .cell file.
            original_atoms = var.atoms.copy()
            scaling_matrix = np.diag(var.cellFactors)
            supercell = make_supercell(var.atoms, scaling_matrix)
            var.atoms = supercell
            convertVASPtoCASTEP2()
            writeDFTinputFiles(var.DFTnonMDphonondir, var.DFTnonMDphononpath, var.DFTinputpath, write_frac_coords = True)

            # For CASTEP, unlike VASP, various properties are stored in the .cell file rather than along with other
            # parameters (in the .param in CASTEP case), so we have to read the tail end of the .cell file which contains 
            # the adjusted parameters, storing it internally, then appending it to the phonopy generated .cell files
            # (Properties at end of .cell file are 'wiped' by phonopy when it generates the new .cell files).
            additional_info = []
            with open( os.path.join(var.DFTnonMDphononpath, var.DFTinput[1]), 'r') as file:
                copy = False
                for line in file:
                    if line.strip() == "%ENDBLOCK POSITIONS_FRAC":
                        copy = True
                    elif copy:
                        additional_info.append(line)
            outf.printt(2,"  Tail of .cell file to append to displaced .cell files:")
            if var.verbosity == 2:
                print("  " + str(additional_info))

            # Now reset the .cell file so phonopy can operate on it afresh
            var.atoms = original_atoms
            # convertVASPtoCASTEP2() needed again?
            writeDFTinputFiles(var.DFTnonMDphonondir, var.DFTnonMDphononpath, var.DFTinputpath, write_frac_coords = True, 
                writeINCAR = False)

        elif var.dft_engine == "VASP":

            var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
            var.poscar = Poscar(var.structure)
            writeDFTinputFiles(var.DFTnonMDphonondir, var.DFTnonMDphononpath, var.DFTinputpath)

            # Following is a bit of a hack. 'writeDFTinputFiles' is designed to take the number of atoms from var.poscar.
            # structure.num_sites and combined with var.ediff_per_atom write out an appropriate EDIFF. However in present
            # case we are setting up inputs so that phonopy can afterwards enlarge the cells, so the EDIFF written to INCAR
            # is too small. To correct this adjust EDIFF here.
            incar_path = os.path.join(var.DFTnonMDphononpath, "INCAR")
            with open(incar_path, 'r') as file:
                lines = file.readlines()
            new_ediff_value = var.ediff_per_atom * var.poscar.structure.num_sites * cellFactor
            for i, line in enumerate(lines):
                if line.strip().startswith('EDIFF'):
                    lines[i] = f"EDIFF = {new_ediff_value}\n"
                    break
            with open(incar_path, 'w') as file:
                file.writelines(lines)

    # Copy scripts
    if finiteDisp:
        scriptType = "phonon"
    else:
        scriptType = "MD" # For DFPT jobs longer than typical single shot calcs (I think...) therefore use MD script
    script = "script_" + var.dft_engine + "_" + scriptType
    if os.path.isfile(var.dirpath + "/" + script):
        outf.printt(2, "  " + script
            + " job submission script present in run directory. Copying to stage sub-directories and renaming as "
            + "script")
        copyVASPscriptAdjustINCAR(script, var.DFTnonMDphononpath)

    if finiteDisp:
        n_indep_disp = setupFiniteDispCalcs(var.DFTnonMDphononpath)
        if var.dft_engine == "CASTEP":
            # When phonopy generates displaced configs in .cell files it strips off all extra info like kpoints,
            # magmomgs, ldau etc. We have already adjusted these based on the new cell size, but need to add them
            # manually now to the displaced .cell files and supercell file
            for dir_name in os.listdir(var.DFTnonMDphononpath):
                path_name = os.path.join(var.DFTnonMDphononpath, dir_name)
                print("  dir_name.startswith('disp')=",dir_name.startswith('disp'))
                print("  os.path.isdir(path_name)=",os.path.isdir(path_name))
                if dir_name.startswith('disp') and os.path.isdir(path_name):
                    # Append to each .cell file in the directory
                    for file_name in os.listdir(path_name):
                        if file_name.endswith('.cell'):
                            file_path = os.path.join(path_name, file_name)
                            outf.printt(2, f'  ...about to append to {file_path}')
                            with open(file_path, 'a') as file:
                                file.writelines(additional_info)
            
    else:
        n_indep_disp = 0

    return n_indep_disp



def PWSCFinToIncar(inFile):
    """
    Reads parameters from a PWscf .in file and assigns them to a VASP-like INCAR dictionary.
    If a parameter is not used/altered in APD, and there is no obvious VASP equivalent, keep the same name for the
    variable in the var.incar space (these are those in lower-case below). It will then not be touched by APD and later
    will be converted back to the relevant parameter in subsequent .in write-outs.

    TO DO: later when we implement PWscf-MD in APD will need to decide what to do with paras like:
    pot_extrapolation, wfc_extrapolation, and ion_temperature.

    Args:
        inFile (str): Path to the PWscf input file.

    Returns:
        dict: VASP-like INCAR dictionary with converted parameters.
    """
    var.incar = {}  # Initialize the INCAR dictionary

    # Define mappings from PWscf parameters to VASP-like INCAR tags
    pwscf_to_incar_map = {
        'ecutwfc': 'ENCUT',      # Plane wave cutoff energy
        'ecutrho': 'encutrho',   # Charge density cutoff
        'dt': 'POTIM',           # Time step for molecular dynamics
        'nstep': 'NSW',          # Number of ionic steps
        'conv_thr': 'EDIFF',     # Convergence threshold
        'tempw': 'TEBEG',        # Initial temperature
        'ion_temperature': 'MDALGO',  # MD algorithm
        'nspin': 'ISPIN',        # Spin polarization
        'press': 'PSTRESS',      # Pressure stress
        'nbnd': 'NBANDS',        # Number of bands
        'nosym': 'SYMM',         # Symmetry
        'electron_maxstep': 'NELM', # Max number of electronic steps
        'smearing': 'smearing',  # Parameters like these aren't altered by APD
        'degauss': 'degauss',
        'occupations': 'occupations',
        'mixing_beta': 'mixing_beta',
        'prefix' : 'prefix'
    }

    # Reverse map for INCAR to PWscf
    var.incar_to_pwscf_map = {v: k for k, v in pwscf_to_incar_map.items()}

    # Read the PWscf input file
    with open(inFile, 'r') as file:
        lines = file.readlines()

    # Dictionaries to store starting magnetizations per species, psuedopotentials, etc
    magnetizations = {}
    var.incar['pseudopotentials'] = {}

    # Flag to indicate if we are inside the ATOMIC_SPECIES block
    inside_atomic_species = False

    # Parse each line and extract parameters
    for line in lines:

        stripped_line = line.strip()

        # Detect ATOMIC_SPECIES section
        if stripped_line.lower().startswith('atomic_species'):
            inside_atomic_species = True
            continue

        # Process the ATOMIC_SPECIES block
        if inside_atomic_species:
            # If the line is empty or doesn't have three parts, we assume the block has ended
            if stripped_line == '' or len(stripped_line.split()) < 3:
                inside_atomic_species = False
                continue

            # Extract species data from the line
            species_data = stripped_line.split()
            element_symbol = species_data[0]  # e.g., 'Ni'
            pseudo_file = species_data[2]     # e.g., 'Ni.pbe-nd-rrkjus.UPF'

            # Store in the pseudopotential dictionary
            var.incar['pseudopotentials'][element_symbol] = pseudo_file

        # Split the line by '=' to parse key-value pairs
        if '=' in line:
            key, value = line.split('=', 1)
            key = key.strip().lower()  # Normalize the key to lowercase
            value = value.split('!')[0].strip()  # Remove comments and whitespace

            # Remove trailing commas or full stops
            value = value.rstrip(',').rstrip('.')

            # Handle conversion to float or integer if possible
            try:
                value_lower = value.lower()  # Convert to lowercase once for all checks
                if value_lower in ['.true.', '.false.', 'true', 'false']:
                    value = (value_lower == '.true.')  # Convert to True/False
                elif '.' in value or 'd' in value_lower:
                    value = float(value.replace('d', 'e'))  # Convert scientific notation with 'd' to 'e'
                else:
                    value = int(value)  # Otherwise, assume it's an integer
            except ValueError:
                #value = value.strip().strip("'").strip('"')  # Retain as string if not numeric
                pass

            # Special handling for starting magnetization values with indices
            if key.startswith('starting_magnetization('):
                species_index = int(key.split('(')[1].split(')')[0])  # Extract species index
                magnetizations[species_index] = value  # Store the value in the magnetizations dict
            else:
                # Map PWscf parameter to VASP INCAR equivalent
                incar_key = pwscf_to_incar_map.get(key)
                if incar_key:
                    var.incar[incar_key] = value

        # Special handling for K_POINTS section
        elif line.strip().lower().startswith('k_points'):
            kpoints_arg = line.split()[1].lower()  # e.g., gamma, automatic, tpiba
            var.kpoints = SimpleNamespace()  # Initialize kpoints namespace
            idx = lines.index(line)
            if kpoints_arg == 'gamma':
                var.kpoints.kpts = [[0, 0, 0] for _ in range(3)]
            elif kpoints_arg == 'automatic':
                kpoints_values = lines[idx + 1].split()
                nk1, nk2, nk3 = map(int, kpoints_values[:3])
                sk1, sk2, sk3 = map(int, kpoints_values[3:])
                var.kpoints.kpts = [[nk1, nk2, nk3]] + [[0, 0, 0] for _ in range(2)]
                if sk1 != 0 or sk2 != 0 or sk3 != 0:
                    print('ERROR: Extend code for k-points offset for PWscf, STOPPING')
                    quit()
            elif kpoints_arg == 'tpiba':
                # The was ase qe handles kpoint lists is to store them as a numpy array. c.f: https://wiki.fysik.dtu.dk/ase/ase/calculators/espresso.html
                npoints = int(lines[idx + 1].strip())
                kpts_list = []
                for i in range(npoints):
                    kpts_list.append(list(map(float, lines[idx + 2 + i].split())))
                var.kpoints.kpts = np.array(kpts_list)


    # I discovered later that magnetization is actually stored in the atoms object, so the following is commented out.
    # For future reference it is stored in: atoms.get_initial_magnetic_moments()
    # # Calculate MAGMOM based on species counts
    # if magnetizations:
    #     var.incar['MAGMOM'] = [magnetizations[ispc] for ispc in sorted(magnetizations)]
    #     print("var.incar['MAGMOM'] = ",var.incar['MAGMOM'])
    #     # I originally had plan (see below) to assign to full VASP-like MAGMOM list. But then changes to the
    #     # latter within a given species would not be transferable back to the magnetizations PWscf tag, so seemed
    #     # little point. If I need to adjust magnetization for PWscf will just need to use an 'if PWscf' conditional

    #     # # Count the number of atoms per species
    #     # species_counts = Counter(var.atoms.get_chemical_symbols())
    #     # magmom_values = []  # List to store the computed MAGMOM values
    #     # species_list = list(species_counts.keys())  # Ordered list of species

    #     # # Compute MAGMOM values
    #     # for i, species in enumerate(species_list):
    #     #     if i + 1 in magnetizations:  # Match species index to magnetization index
    #     #         magmom_values.extend([magnetizations[i + 1]] * species_counts[species])

    #     # var.incar['MAGMOM'] = magmom_values

    # Post-processing specific mappings
    # Convert MD ensemble types to ISIF equivalent
    if 'md_ensemble' in var.incar:
        if var.incar['md_ensemble'].upper() == 'NVT':
            var.incar['ISIF'] = 2
        elif var.incar['md_ensemble'].upper() == 'NPT':
            var.incar['ISIF'] = 3

    # nosym (True, False) -> SYMM (0, 1 respectively)
    if 'SYMM' in var.incar:
        var.incar['SYMM'] = 0 if var.incar['SYMM'] else 1

    # Print for debugging
    print("INCAR Parameters:")
    for key, val in var.incar.items():
        print(f"{key} = {val}")

    # Particular settings we would like for all (or at least most) DFT calculations to be performed through APD
    var.incar['tprnfor'] = True
    var.incar['tstress'] = True
    var.incar['pseudo_dir'] = f"'{var.dirpath}'"
    var.incar['prefix'] = 'PWSCF'

    print("var.incar['pseudopotentials']=",var.incar['pseudopotentials'])

    return



def convertParamToIncar():

    # Sets up a dictionary, var.incar (rather than in the case of VASP, the actual object, var.incar, instantiated from pymatgen.io.vasp.inputs.Incar) to allow to
    # retain much of the same code as for the VASP case.

    # Set up an empty incar dictionary:
    var.incar = {}
    if var.calc.param.md_ion_t.value is not None:
        var.incar["LANGEVIN_GAMMA"] = float(var.calc.param.md_ion_t.value.split()[0])
    if var.calc.param.md_cell_t.value is not None:
        var.incar["LANGEVIN_GAMMA_L"] = float(var.calc.param.md_cell_t.value.split()[0])
    if var.calc.param.md_delta_t.value is not None:
        var.incar["POTIM"] = float(var.calc.param.md_delta_t.value.split()[0])
    if var.calc.param.md_ensemble is not None:
        if var.calc.param.md_ensemble.value == "NVT":
            var.incar["ISIF"] = 2
        elif var.calc.param.md_ensemble.value == "NPT":
            var.incar["ISIF"] = 3
    if var.calc.param.cut_off_energy.value is not None:
        var.incar["ENCUT"] = float(var.calc.param.cut_off_energy.value.split()[0])
    if var.calc.param.elec_energy_tol is not None:
        var.incar["EDIFF_PER_ATOM"] = float(
            var.calc.param.elec_energy_tol.value.split()[0]
        )  # this just ends up defining a 'var.incar["EDIFF"]' (and we lose var.incar["EDIFF_PER_ATOM"]). So we also define a var.ediff_per_atom below to use in-code (this is convenient e.g. when we change cell size).
        var.ediff_per_atom = float(var.calc.param.elec_energy_tol.value.split()[0])
    if var.calc.param.md_num_iter is not None:
        var.incar["NSW"] = int(var.calc.param.md_num_iter.value)

    # Hubbard U
    # It is not clear if the atom object retains any information on the lda+u settings. From https://wiki.fysik.dtu.dk/ase/ase/calculators/castep.html it appears not.
    # Therefore it might be necessary to store this information either as var.ldauu etc or in the var.incar dictionary, so we can write it out again to the .cell file
    # later.

    return

def removeOutputs(direc):

    """
    Removes files from directory, direc/, that are not in var.DFTinput and renames files in var.DFToutput by prepending
    'failed_'
    """

    path = var.dirpath + "/" + direc

    # List all files in the directory
    try:
        files_in_directory = os.listdir(path)
    except OSError as e:
        outf.printt(2, f"    Error accessing directory {path}: {e}")
        return

    # If pseudopotential files have been specified (CASTEP) these should be retained
    species_pot_files = []
    if var.species_pot:
        for species_pot_ind in var.species_pot: # e.g. species_pot = [("O", "o-optgga1.recpot"), ... ]
            species_pot_files.append(species_pot_ind[1])
    print('species_pot_files=',species_pot_files)

    # Delete files not in var.DFTinput and var.species_pot
    for file in files_in_directory:
        if (file not in var.DFTinput and file not in species_pot_files and file != "script" 
                and not file.startswith('failed')):
            if (file in var.DFToutput or file.endswith('.out') or file.endswith('.err')) and file:
                new_name = "failed_" + file
                os.rename(os.path.join(path, file), os.path.join(path, new_name))
                outf.printt(2, f"    Renamed {file} to {new_name}")
            else:
                os.remove(os.path.join(path, file))
                outf.printt(2, f"    Removed {file}")
    

def contJobs(direcs):

    # Set up continuation jobs for DFT calculations in directories: direcs.

    outf.printt(1, "  Setting up cont/ directories for:")
    for direc in direcs:

        outf.printt(1, "    " + direc)
        path = var.dirpath + "/" + direc
        setupDFTinput(direc, path) # Be sure to initialize DFT paras according to previous geom opt job. This will just
                # be same as high elec paras except NSW will be 1000, or 1000 minus completed jobs.
        os.mkdir(path + "/cont")

        # Copy all files except INCAR
        if ( var.dft_engine == "CASTEP" ):  # equivalently code use:   if fileOutput=='castep.castep': (here fileOutput needs to be provided as argument to subroutine)
            toCopy = "script,castep.cell,castep.param"
            if os.path.exists(f"{path}/castep.check"):
                toCopy += f",castep.check" # Ccheck this as there for edge case where .check hasn't been created
            os.system("cp " + path + "/{" + toCopy + "} " + path + "/cont/")
            if var.species_pot:
                for species_pot_ind in var.species_pot: # e.g. species_pot = [("O", "o-optgga1.recpot"), ... ]
                    shutil.copy2(os.path.join(path, species_pot_ind[1]), os.path.join(path, "cont"))
        elif var.dft_engine == "VASP":  # equivalently code use:   if fileOutput=='OUTCAR':
            # Note here we need to decide if we copy CONTCAR to POSCAR (as for MD) or not (as for single point, e.g. trainingData stage for 'reduced' in config_gen)
            #   -- if single point then CONTCAR (if present) will surely just be equal to POSCAR, so not an issue?
            os.system("cp " + path + "/{script,POSCAR,INCAR,KPOINTS,POTCAR} " + path + "/cont/")
            if os.path.exists(path + "/CONTCAR"):
                # Check if CONTCAR is empty or has only one line
                with open(path + "/CONTCAR", "r") as file:
                    lines = file.readlines()
                if len(lines) <= 1:  # Empty or insufficient content
                    print("CONTCAR is empty or missing content. Copying POSCAR instead.")
                    os.system(f"cp {path}/POSCAR {path}/cont/POSCAR")
                else:
                    os.system(f"cp {path}/CONTCAR {path}/cont/POSCAR")
            else:
                print("CONTCAR does not exist. Copying POSCAR instead.")
                os.system(f"cp {path}/POSCAR {path}/cont/POSCAR")

            if os.path.exists(path + "/vdw_kernel.bindat"):
                os.system("cp " + var.dirpath + "/vdw_kernel.bindat " + path + "/cont/")

        if 'phonon' not in direc:
            # Compute number of ionic steps already computed:
            completed_steps = 0
            sub_path = path
            # UPDATE: we used to sum over all steps from previous cont/ dirs (including the first directory). But this
            # isn't necessary since the var.incar["NSW"] from the latest job already accounts for the progress made
            # earlier...
            # while True: #sub_path.endswith('cont/'):
            if var.dft_engine == "CASTEP":
                if 'geomOpt' in direc: # CASTEP unusual in that trajectory file has a different name for geom opt than
                        # for md...
                    castep_geom_file = os.path.join(sub_path, "castep.geom")
                    if os.path.exists(castep_geom_file):
                       try:
                          with open(castep_geom_file, "r") as castep_file:
                              traj = ase.io.castep.read_castep_geom(castep_file)
                       except Exception as e:
                          print(f"Error reading CASTEP geom file: {castep_geom_file}, Error: {e}")
                          traj = None
                    else:
                       traj = None
                else: 
                    castep_md_file = os.path.join(sub_path, var.DFToutput[2])
                    if os.path.exists(castep_md_file):
                        try:
                            with open(castep_md_file, "r") as castep_file:
                                traj = ase.io.castep.read_castep_md(castep_file)
                        except Exception as e:
                            print(f"Error reading CASTEP MD file: {castep_md_file}, Error: {e}")
                            traj = None
                    else:
                        traj = None

                # Calculate completed_steps only if traj is valid
                if traj:
                    try:
                        print('var.calc.param.num_backup_iter=', var.calc.param.num_backup_iter)
                        num_backup = int(var.calc.param.num_backup_iter.value)
                        len_traj = len(traj)
                        print('len_traj=', len_traj)
                        completed_steps += (len_traj // num_backup) * num_backup
                        print('completed_steps updated to =', completed_steps)
                    except Exception as e:
                        print(f"Error processing trajectory data: {e}")

            else:
                vasp_file = os.path.join(sub_path, var.DFToutput[2])
                if os.path.exists(vasp_file):
                    try:
                        traj = ase.io.read(vasp_file)
                        completed_steps += len(traj)
                        print('completed_steps updated to =', completed_steps)
                    except Exception as e:
                        print(f"Error reading VASP file: {vasp_file}, Error: {e}")
                else:
                    print(f"VASP file not found: {vasp_file}")

            # if not sub_path.endswith('cont/'):
            #     break
            # sub_path = os.path.dirname(sub_path.rstrip('/'))  # Go up one directory level

            # Write INCAR using new NSW: (to keep this function agnostic to the type of calc, i.e. configs vs MDparas etc)
            # easiest here to just find-replace the NSW / MD_NUM_ITER
            print('old_NSW=',var.incar["NSW"],', completed_steps (from previous runs) =',completed_steps)
            new_NSW = var.incar["NSW"] - completed_steps
            print('new NSW=',new_NSW)
            # Change in file: direc + "/cont/" , path + "/cont/", var.DFTinputpath
            if var.dft_engine == "CASTEP":
                input_filename = os.path.join(direc + "/cont/",  "castep.param")
                replacement_string = f"MD_NUM_ITER: {new_NSW}"
                keyword = "MD_NUM_ITER"
            else:
                input_filename = os.path.join(direc + "/cont/", 'INCAR')
                replacement_string = f"NSW = {new_NSW}"
                keyword = "NSW"
            
            # Read the file and replace the line
            with open(input_filename, 'r') as file:
                lines = file.readlines()
            
            with open(input_filename, 'w') as file:
                for line in lines:
                    if line.strip().startswith(keyword):
                        file.write(replacement_string + "\n")
                    else:
                        file.write(line)
 
        # DFTsetup.writeDFTinputFiles( direc + "/cont/" , path + "/cont/", var.DFTinputpath, writePOSCAR=False ) <-- no longer want to use this as can't assume all parameters are correctly set. (e.g. temp# is not set correctly when setting up cont jobs for DFTMDconfig calcs)


def convertVASPtoCASTEP():
    # Converts VASP INCAR parameters stored in var.incar to CASTEP input parameters in var.calc. This is called when the VASP files are queried from Materials Project.
    # Some default settings not present in the INCAR are also set here.
    # This function is used both for the MD and single shot energies, for NVT and NPT. NSW determines MD vs single shot, ISIF determine NVT/NPT

    # Conversion from INCAR: (for more, see 'convertVASPtoCASTEP2' call below)
    if var.incar["ISPIN"] == 1:
        var.calc.param.spin_polarised = False
    elif var.incar["ISPIN"] == 2:
        var.calc.param.spin_polarised = True

    # Some other values of CASTEP parameters we will used, which are appropriate for MD simulations
    var.calc.param.elec_energy_tol = [1.0e-5, "eV"]  # eV / atom
    var.calc.param.md_elec_convergence_win = (
        2  # 2 consequetive values within tolerance for convergence rather than 3, for faster MD
    )
    var.calc.param.calculate_stress = True
    var.calc.param.md_thermostat = "Langevin"
    # var.calc.param.fix_com=True # SCARF CASTEP complains this isn't in keyword list, so omit this (should default to true anyway)
    var.calc.param.max_scf_cycles = 500
    # var.calc.param.md_temperature=[None]*2
    # var.calc.param.md_temperature[0]=300
    # var.calc.param.md_temperature[1]='K'
    var.calc.param.md_temperature = [300, "K"]
    var.calc.param.continuation = "default"  # continuation file written out
    var.calc.param.num_backup_iter = 10
    var.calc.param.nextra_bands = 60  # We use this initially for stage 2_, but as soon as cell is resized we start using 'NBANDS' instead. Hence if var.INCAR("NBANDS") present then in convertVASPtoCASTEP2() we will pop this and instead set var.calc.param.nbands instead. Note that the use of NBANDS directly (with sufficiently large value) is needed for finite temperature MD to ensure sufficient bands. UPDATE: recently increased this to 40 as some phonon calcs had insufficient bands. (CASTEP recommended this value. It may be in fact that we can just use this setting also for DFT MD, but I have not tested that yet)

    convertVASPtoCASTEP2()  # the remaining CASTEP variables (e.g. md_ion_t) all belong in a set that I use to convert CASTEP -> VASP -> CASTEP in order to retain the
    # already coded VASP logic of the code. Make the remaining conversions through this call

    return


def convertVASPtoCASTEP2():
    # Converts VASP INCAR parameters stored in var.incar to CASTEP input parameters in var.calc (as well as to some general global variables inc. magmoms, ldauu, etc
    #
    # This is called to : i) convert back from VASP to CASTEP for writing out CASTEP files (we often prefer VASP variables in-code to retain previously coded algorithms)
    #                     ii) in the initial querying phase, which queries VASP data. This routine converts the remaining VASP data that isn't included in
    #                         'convertVASPtoCASTEP'.

    if var.verbosity == 2:
        print("  Converting VASP to CASTEP settings (in convertVASPtoCASTEP2): ", end="")
    if "LANGEVIN_GAMMA" in var.incar:
        langevinGammaOrdered = var.incar["LANGEVIN_GAMMA"]
        langevinGammaOrdered.sort()
        var.calc.param.md_ion_t = [
            1 / langevinGammaOrdered[0],
            "ps",
        ]  # In CASTEP the damping time is instead used, which is 1/friction para. if we are querying a POSCAR with different values of LANGEVIN_GAMMA per species, take the smallest value (largest damping time) for the CASTEP value.
        if var.verbosity == 2:
            print(
                "LANGEVIN_GAMMA=",
                var.incar["LANGEVIN_GAMMA"],
                " to md_ion_t=",
                var.calc.param.md_ion_t.value,
                ", ",
                end="",
            )
    if "POTIM" in var.incar:
        var.calc.param.md_delta_t = [var.incar["POTIM"], "fs"]
        if var.verbosity == 2:
            print("POTIM=", var.incar["POTIM"], " to delta_t=", var.calc.param.md_delta_t.value, ", ", end="")
    if "EDIFFG" in var.incar:
        if var.incar["EDIFFG"] < 0:
            var.calc.param.geom_force_tol = [-var.incar["EDIFFG"], "eV/ang"]
        else:
            var.calc.param.geom_energy_tol = [var.incar["EDIFFG"], "eV"]
    if "MDALGO" in var.incar:
        if var.incar["MDALGO"] == 3:
            var.calc.param.md_thermostat = "Langevin"
            if var.verbosity == 2:
                print(
                    "MDALGO=",
                    var.incar["MDALGO"],
                    " to md_thermostat=",
                    var.calc.param.md_thermostat.value,
                    ", ",
                    end="",
                )
        else:
            print("  Please update convertVASPtoCASTEP2 - unknown value for MDALGO")
    if "NSW" in var.incar:
        if var.incar["NSW"] > 0:
            var.calc.param.md_num_iter = var.incar["NSW"]
        elif var.incar["NSW"] == 0:
            var.calc.param.md_num_iter = 1
        else:
            print("STOPPING: NSW<0")
            quit()
        if var.verbosity == 2:
            print("NSW=", var.incar["NSW"], " to md_num_iter=", var.calc.param.num_iter.value, ", ", end="")
        if var.incar["NSW"] == 0 or var.incar["NSW"] == 1:
            var.calc.param.task = "SinglePoint"
        else:
            var.calc.param.task = "MolecularDynamics"
        if var.verbosity == 2:
            print("setting task=", str(var.calc.param.task.value), ", ", end="")
    if "TEBEG" in var.incar:
        var.calc.param.md_temperature = [var.incar["TEBEG"], "K"]
        if var.verbosity == 2:
            print(
                "TEBEG=", var.incar["TEBEG"], " to md_temperature=", var.calc.param.md_temperature.value, ", ", end=""
            )
    var.calc.param.elec_energy_tol = [var.ediff_per_atom, "eV"]
    if var.verbosity == 2:
        print(
            "EDIFF_PER_ATOM=",
            var.ediff_per_atom,
            " to elec_energy_tol=",
            var.calc.param.elec_energy_tol.value,
            ", ",
            end="",
        )  # i don't think EDIFF_PER_ATOM is ever in var.incar. even if we set it, it gets converted to EDIFF. Thus we use an alternate global variable to keep track of ediff per atom. So we can probably remove this conditional
    if "ISIF" in var.incar:
        if var.incar["ISIF"] == 0 or var.incar["ISIF"] == 2:
            var.calc.param.md_ensemble = "NVT"
            var.calc.param.md_barostat = "Andersen-Hoover"  # default. but isn't going to matter in this case.
        elif var.incar["ISIF"] == 3:
            var.calc.param.md_ensemble = "NPT"
            var.calc.param.md_barostat = "Parrinello-Rahman"
        if var.verbosity == 2:
            print(
                "ISIF=",
                var.incar["ISIF"],
                " to md_ensemble=",
                var.calc.param.md_ensemble.value,
                ", md_barostat=",
                var.calc.param.md_barostat.value,
                ", ",
                end="",
            )
    if "ENCUT" in var.incar:
        var.calc.param.cut_off_energy = [int(var.incar["ENCUT"]), "eV"]
        if var.verbosity == 2:
            print(
                "ENCUT=",
                var.incar["ENCUT"],
                " to param.cut_off_energy=",
                var.calc.param.cut_off_energy.value,
                ", ",
                end="",
            )
    if "NBANDS" in var.incar:
        var.calc.param.nextra_bands.clear()
        var.calc.param.nbands = var.incar["NBANDS"]
        if var.verbosity == 2:
            print(f'NBANDS={var.incar["NBANDS"]} to param.nbands={var.calc.param.nbands}, (and popping '
                    'param.nextra_bands), ', end='' )  # if that doesn't work try : var.calc.param.nbands.value

    # Specific parameters just for CASTEP (no equivalent for VASP or else not necessary to specify for VASP):
    
    # finite_basis_correction:
    #
    # This should be set to 'Automatic' by CASTEP whenever it is 'needed' (and it _is_ needed to get accurate stress
    # tensors). For most cases it is the case (MD-NPT; single shot DFT where calculate_stress = True; ...). A
    # seeming blind spot is where a geom opt is specified, calculate_stress = True, and fix_cell = True. This is the
    # case for elastic and phonon calculations. In this case we need accurate stresses as the EFS are used in the 
    # fitting, but CASTEP defaults the value of finite_basis_corr to 0. Since these are the only two places where
    # we would end up with '0', and we actually want them '2', it is easiest to just make the blanket setting below:
    var.calc.param.finite_basis_corr = 2

    # kpoints
    # need to map var.kpoints.kpts[0][0:3]) to var.atoms.calc.cell.kpoint_mp_grid , so that the ..._value[0:2] provides access to same quantity
    print('var.kpoints.kpts=',var.kpoints.kpts)
    print('var.kpoints.kpts[0]=',var.kpoints.kpts[0])
    if "ISYM" in var.incar:
        if var.incar["ISYM"] > 0: 
            var.calc.cell.symmetry_generate = "TRUE" 
        else:
            var.calc.cell.symmetry_generate = None # used "FALSE" before but None is better? i.e. to unset it so it simply doesn't appear in the .cell (isn't that the desired behaviour?)
    else: # if ISYM not in INCAR VASP would use default value (1,2 or 3) which corresponds to use of symmetry
        var.calc.cell.symmetry_generate = "TRUE" 
    if "ISIF" in var.incar and "NSW" in var.incar:
        if var.incar["ISIF"] <= 2 and var.incar["NSW"] > 1:
            var.calc.cell.fix_all_cell = "TRUE" # added recently
        else:
            #print('var.calc.cell.fix_all_cell from before = ',var.calc.cell.fix_all_cell.value)
            #quit()
            var.calc.cell.fix_all_cell = None # had "FALSE" before, but then it writes FIX_ALL_CELL = False to .cell which seems like too much info given that 'False' is the default behaviour anyway
    var.calc.cell.kpoint_mp_grid = var.kpoints.kpts[0][0:3]

    # Magnetic moment
    if "MAGMOM" in var.incar:
        var.magmoms = var.incar["MAGMOM"]
        magmoms_get = var.atoms.get_initial_magnetic_moments()
        var.atoms.set_initial_magnetic_moments(magmoms=var.magmoms)
        if var.verbosity == 2:
            print("MAGMOM to intermediary atoms object (var.atoms.get_initial_magnetic_moments)", end="")

    # DFPT for phonons
    if "IBRION" in var.incar:
        if var.incar["IBRION"] == 8:
            var.calc.param.task = "Phonon" # In general we don't ascribe task based on vasp settings, here is exception
            if "NSW" in var.incar:
                var.calc.param.phonon_max_cycles = var.incar["NSW"]

    # Hubbard U
    if "LDAU" in var.incar:
        if var.incar["LDAU"] == True:
            print('var.incar["LDAUTYPE"]=', var.incar["LDAUTYPE"])
            if var.incar["LDAUTYPE"] != 2:
                outf.printt( 0, "    ERROR: LDAUTYPE in VASP INCAR not equal to 2 - CASTEP only supports simplified "
                    "Dudarev approach (2), STOPPING." )
                quit()
            var.ldauu = var.incar["LDAUU"]
            var.ldauj = var.incar["LDAUJ"]
            if set(var.ldauj) != {0}:
                var.ldauu = var.ldauu - var.ldauj
                if var.verbosity == 2:
                    print( f"LDAUJ non zero (={var.ldauj}), setting LDAUU=LDAUU-LDAUJ={var.ldauu},", end="" )
            var.ldaul = var.incar["LDAUL"]

            if set(var.ldauu) != {0}:  # if ldauu contains more than just zeros...
                LDAUstring = "  eV\n"
                speciesTypes = list(dict.fromkeys(var.structure.species))
                l_numToLett = ["s", "p", "d", "f"]
                for speciesTypeIndex, speciesType in enumerate(speciesTypes):
                    if var.ldauu[speciesTypeIndex] > 0 and var.ldaul[speciesTypeIndex] != -1:
                        LDAUstring = LDAUstring + ( f"    {speciesType}  {l_numToLett[var.ldaul[speciesTypeIndex]]}: "
                                + f"{var.ldauu[speciesTypeIndex]}\n" )
                var.calc.cell.hubbard_u = [LDAUstring]
                if var.verbosity == 2:
                    print(f'LDAUU={var.incar["LDAUU"]}, LDAUJ={var.incar["LDAUJ"]} to cell.hubbard_u={var.ldauu} ',
                        end='')

    if var.verbosity == 2:
        print("")

    return


def adjustLattParas(temperatures):
    # Used averaged cell volumes from ISIF3 (stage3) runs to set up ISIF2 (stage4) jobs
    for tempIndex, temp in enumerate(temperatures):
        # Read in lattice parameters from NPT simulation of stage 3
        thermalExpansionFile = open(var.DFTconfigsDFTNPTpath + "/temp" + str(temp) + "/thermalExpansion.dat", "r")
        lines = thermalExpansionFile.readlines()
        lattParaLines = lines[4:7]
        averagedLattParas = []
        for i in range(0, 3):
            p = re.compile(r"[-+]?\d*\.\d+|\d+")
            numbersFromLine = [float(i) for i in p.findall(lattParaLines[i])]  # Convert strings to float
            averagedLattParas.append(numbersFromLine[0])
            averagedLattParas.append(numbersFromLine[1])
            averagedLattParas.append(numbersFromLine[2])

        # Write adjusted lattice parameters to POSCAR file of this stage
        lines = open(var.DFTconfigsDFTNVTpath + "/temp" + str(temp) + "/POSCAR", "r").readlines()

        lines[2] = str(averagedLattParas[0]) + " " + str(averagedLattParas[1]) + " " + str(averagedLattParas[2]) + "\n"
        lines[3] = str(averagedLattParas[3]) + " " + str(averagedLattParas[4]) + " " + str(averagedLattParas[5]) + "\n"
        lines[4] = str(averagedLattParas[6]) + " " + str(averagedLattParas[7]) + " " + str(averagedLattParas[8]) + "\n"
        outputFile = open(var.DFTconfigsDFTNVTpath + "/temp" + str(temp) + "/POSCAR", "w")
        outputFile.writelines(lines)
        outputFile.close()


def detectLattice():
    # See if we can determine the type of lattice vectors: sc, fcc or bcc. This is to save time having to run the find_optimal_cell_shape routine.
    # This routine is effectively a stub and is currently very restrictive in the lattice vectors it can detect the lattice type from. E.g.
    # 0 1 0; 1 0 0; 0 0 1 will not be detected as cubic). Routine should be written properly.

    lattice = str(var.structure.lattice)
    l = [float(x) for x in lattice.split()]
    print("l=", l)
    if (
        l[1] == 0.0
        and l[2] == 0.0
        and l[3] == 0.0
        and l[4] == l[0]
        and l[5] == 0.0
        and l[6] == 0.0
        and l[7] == 0.0
        and l[8] == l[0]
    ):
        latticeType = "sc"
    elif l[1] == l[0] and l[2] == 0.0 and l[3] == 0.0 and l[5] == l[4] and l[6] == l[9] and l[8] == 0.0:
        latticeType = "fcc"
    elif (
        l[1] == l[0]
        and l[2] == l[0]
        and l[3] == l[0]
        and l[4] == l[0]
        and l[5] == -l[0]
        and l[6] == -l[0]
        and l[7] == l[0]
        and l[8] == l[0]
    ):
        latticeType = "bcc"
    else:
        latticeType = "unknown"

    return latticeType


def resizeCell(natomTargetMin, natomTargetMax, otherVASP):

    from pymatgen.transformations.advanced_transformations import CubicSupercellTransformation
    from pymatgen.io.vasp.sets import MPRelaxSet

    """
    Resize structure so number of atoms in range natomTargetMin - natomTargetMax, and rescale kpoints. (Changes made 
    to var.structure and var.poscar, not written out to file.) If 'otherVASP' is True, INCAR and KPOINTS will also 
    be adjusted, if False, only the POSCAR will be changed.
    As elsewhere, if CASTEP is being used, we still use the VASP PyMatGen objects, var.kpoints, var.incar (e.g. 
    "MAGMOM") etc to temporarily carry quantities to later convert back to CASTEP.
    """

    outf.printt(1, "  Resize cell if necessary...")
    outf.printt(2, "    Number of sites in (assumed primitive) cell: " + str(var.structure.num_sites))
    natomInitial = var.structure.num_sites
    foundNewCell = False

    # If too few or too many atoms in primitive cell consider either a conventional cell or a supercell made up of conventional or primitive cells
    if var.structure.num_sites > natomTargetMin and var.structure.num_sites < natomTargetMax:
        outf.printt(2, "    " + str(var.structure.num_sites) + " atoms ...within range: " + str(natomTargetMin)
            + "-" + str(natomTargetMax))

    elif var.structure.num_sites < natomTargetMin:
        outf.printt(2, "    Less than natomTargetMin = " + str(natomTargetMin) + 
            ". Considering cubic or fcc supercells." )

        outf.printt( 1, "    Searching for cubic or fcc supercells in range: " + str(natomTargetMin)
            + "-" + str(natomTargetMax) )
        # First try CubicSupercellTransformation, which is faster than the ase method below, but only works for 
        # generating cubic supercells, and also will only find exact matches
        outf.printt(1, "    Try to generate cubic cell using pymatgen CubicSupercellTransformation") 
        supercell_generator = CubicSupercellTransformation(
            min_atoms=natomTargetMin, max_atoms=natomTargetMax, min_length=1.0
        )  # don't think i need last parameter...
        try:

            supercell = supercell_generator.apply_transformation(var.structure)
            var.structure = supercell # this was missing before, but presumably is necessary. also changed in line below
            outf.printt(1, "      Generated cubic cell. No. atoms: " + str(var.structure.num_sites)) # was supercell.num_sites
            foundNewCell = True

        except:

            outf.printt(1, "      Supercell not found using pymatgen CubicSupercellTransformation")
            outf.printt(1, "    Try to generate supercell using ase find_optimal_cell_shape")
            var.atoms = aio.AseAtomsAdaptor.get_atoms(
                var.structure
            )  # remember to convert back from atoms (ASE) to structure (pymatgen) object after generating supercell..
            print(' var.atoms.positions before = ',var.atoms.positions)
            outf.printt(1, "      Checking primitive supercell type:")
            sp = get_spacegroup(var.atoms, symprec=1e-5)
            if sp.symbol == "P m -3 m":
                print("        Simple cubic primitive cell detected")
            elif sp.symbol == "F m -3 m":
                print("        fcc primitive cell detected")
            elif sp.symbol == "L m -3 m":
                print("        bcc primitive cell detected")
            smallestDev = {}
            natomsSmallestDev = {}
            supercellPerCellType = {}
            for cellType in ["fcc", "sc"]:
                outf.printt(1, "      Looking for " + cellType + " supercells:")
                # Check if lattice is cubic, fcc or bcc in which case we can skip the slow 'find_optimal...' bit below, and access the 'dev1' from one of the files
                # Popt-bcc2fcc.json  Popt-bcc2sc.json  Popt-fcc2fcc.json  Popt-fcc2sc.json  Popt-sc2fcc.json  Popt-sc2sc.json
                PoptFile = None
                if sp.symbol == "P m -3 m":
                    # cubic
                    if cellType == "fcc":
                        PoptFile = "Popt-sc2fcc.json"
                    elif cellType == "sc":
                        PoptFile = "Popt-sc2sc.json"
                    PoptFileObject = open(var.APDdir + "/inputFiles/" + PoptFile)
                    PoptData = json.load(PoptFileObject)
                elif sp.symbol == "F m -3 m":
                    # fcc
                    if cellType == "fcc":
                        PoptFile = "Popt-fcc2fcc.json"
                    elif cellType == "sc":
                        PoptFile = "Popt-fcc2sc.json"
                    PoptFileObject = open(var.APDdir + "/inputFiles/" + PoptFile)
                    PoptData = json.load(PoptFileObject)
                elif sp.symbol == "L m -3 m":
                    # bcc
                    if cellType == "fcc":
                        PoptFile = "Popt-bcc2fcc.json"
                    elif cellType == "sc":
                        PoptFile = "Popt-bcc2sc.json"
                    PoptFileObject = open(var.APDdir + "/inputFiles/" + PoptFile)
                    PoptData = json.load(PoptFileObject)
                if PoptFile is None: outf.printt(2,'      (No Popt file identified; computing deviations)')

                ncellMin = int(natomTargetMin / var.structure.num_sites)
                ncellMax = int(natomTargetMax / var.structure.num_sites)
                smallestDev[cellType] = 1000
                outf.printt(2,f'      (checking in range {ncellMin}-{ncellMax})')
                for ncells in range(ncellMin, ncellMax+1):  # 5 atom primitive, so corresponds to natoms: 50-100
                    if PoptFile == None:
                        P1 = find_optimal_cell_shape(var.atoms.cell, ncells, cellType)
                        deviation = get_deviation_from_optimal_cell_shape(np.dot(P1, var.atoms.cell))
                    else:
                        if str(ncells) in PoptData:
                            deviation = PoptData[str(ncells)]["dev"][0]
                            P1 = PoptData[str(ncells)]["P"]
                        else:
                            print('      ERROR: deviation data not found in: ' + PoptFile + ' for ncells='
                                + str(ncells) + ' - shouldnt happen. STOPPING')
                            quit() # check this. perhaps need to do the 'get_deviation_from...' in this case...
                    outf.printt(2, "      ncells=" + str(ncells) + ", natoms="
                        + str(ncells * var.structure.num_sites) + # ", transformation matrix=" + str(P1) + 
                        ", similarity measure to sc=" + str(deviation))
                    if deviation < smallestDev[cellType]:
                        smallestDev[cellType] = deviation
                        natomsSmallestDev[cellType] = ncells * var.structure.num_sites
                        supercellPerCellType[cellType] = make_supercell(var.atoms, P1)
                        supercellPerCellType[cellType] = aio.AseAtomsAdaptor.get_structure(
                            supercellPerCellType[cellType] )  # convert back to structure type for pymatgen...
                print( "    Smallest deviation for " + cellType + " supercells=", smallestDev[cellType],
                    " for natoms=", natomsSmallestDev[cellType] )

            minSmallestDev = min(smallestDev.values())
            cellType_minSmallestDev = min(smallestDev, key=smallestDev.get)
            outf.printt(1,f"      Best supercell found has deviation: {minSmallestDev} from perfect cubic/fcc for "
                          f"{cellType_minSmallestDev} (< ~ 0.03 = perfect)")
            var.structure = supercellPerCellType[cellType_minSmallestDev]
            foundNewCell = True

    elif var.structure.num_sites > natomTargetMax:
        outf.printt(1, "    Greater than natomTargetMax=" + str(natomTargetMax) + ".")
        outf.printt(1, "    ATTENTION: this is a large cell - no need to build a supercell (or likely insufficient "
            + "computing resources).")
        outf.printt(1, "    WARNING: if this is unexpected for this material it may be worth checking that the input "
            + "structure is indeed the primitive cell.")

    natomFinal = var.structure.num_sites
    var.poscar = Poscar(var.structure)

    if foundNewCell == True:

        if var.dft_engine == "VASP":

            # The above transformation repeats the primitive cell multiple times without regrouping species. For VASP it is
            # better to group in units of the same species

            outf.printt( 2, "  No. species in POSCAR > than those in original queried POSCAR: attempting to pack all "
                + "species of same type together" )
            # First check that the ordering of species in var.poscar is equal to that of the original poscar file, repeated a number of times. e.g 2 Fe, Mn, O, 2Fe, Mn, O...
            for iatom, atom in enumerate(var.poscar.site_symbols):
                # print('var.speciesInitial=',var.speciesInitial)
                # print('var.speciesInitial[',iatom % len(var.speciesInitial),']=',var.speciesInitial[iatom % len(var.speciesInitial)],', var.poscar.site_symbols[',iatom,']=',var.poscar.site_symbols[iatom],')')
                if (
                    var.poscar.site_symbols[iatom].strip()
                    != str(var.speciesInitial[iatom % len(var.speciesInitial)]).strip()
                ):
                    print(
                        "    ERROR: ordering of species in enlarged POSCAR does not match ordering in original POSCAR ( var.speciesInitial[",
                        iatom % len(var.speciesInitial),
                        "]=",
                        var.speciesInitial[iatom % len(var.speciesInitial)],
                        " != var.poscar.site_symbols[",
                        iatom,
                        "]=",
                        var.poscar.site_symbols[iatom],
                        "), STOPPING",
                    )
                    quit()
            # now check through var.poscar.natoms to make sure it is of form e.g. 1 1 3 1 1 3... if original number of atoms per species is 1 1 3
            for iList, natoms in enumerate(var.poscar.natoms):
                # print('var.nSpeciesInitial[',iList % len(var.nSpeciesInitial),']=',var.nSpeciesInitial[iList % len(var.nSpeciesInitial)],', var.poscar.natoms[',iList,']=',var.poscar.natoms[iList],')')
                if var.poscar.natoms[iList] != var.nSpeciesInitial[iList % len(var.nSpeciesInitial)]:
                    print( "    ERROR: ordering of numbers of species in enlarged POSCAR does not match ordering numbers "
                        "of species in original POSCAR ( var.nSpeciesInitial[",iList % len(var.nSpeciesInitial),
                        "]=",var.nSpeciesInitial[iList % len(var.nSpeciesInitial)]," != var.poscar.natoms[",iList,"]=",
                        var.poscar.natoms[iList],"), STOPPING" )
                    quit()
            nRepetitions = int(len(var.poscar.site_symbols) / len(var.speciesInitial))

            coordsNew = []
            coordsNew = [None] * len(var.speciesInitial)
            coordsNew = [[] for i in itertools.repeat(None, len(var.speciesInitial))]
            nPrim = 0
            nAtomsPerPrim = sum(var.nSpeciesInitial)
            nAtomsSum = 0
            for iRep in range(1, nRepetitions + 1):
                nAtomsSum = 0
                for iSpc in range(1, len(var.speciesInitial) + 1):
                    # print('attempting to append to sitesNew[',iSpc-1,']=',sitesNew[iSpc-1],' with nAtomsSum=',nAtomsSum)
                    for elemCount in range(
                        (iRep - 1) * nAtomsPerPrim + nAtomsSum,
                        (iRep - 1) * nAtomsPerPrim + nAtomsSum + var.nSpeciesInitial[iSpc - 1],
                    ):
                        coordsNew[iSpc - 1].append(var.poscar.structure.sites[elemCount].frac_coords)
                    # print('incrementing natomsSum by var.nSpeciesInitial[',iSpc-1,'] (=',var.nSpeciesInitial[iSpc-1],') +1')
                    nAtomsSum = nAtomsSum + var.nSpeciesInitial[iSpc - 1]
            elemCountPrev = 0
            for iSpc in range(1, len(var.speciesInitial) + 1):
                for elemCount in range(1, len(coordsNew[iSpc - 1]) + 1):
                    # print('elemCount=',elemCount,', adjusting var.poscar.structure[',elemCount-1+elemCountPrev,']')
                    var.poscar.structure[elemCount - 1 + elemCountPrev] = ( var.speciesInitial[iSpc - 1],
                        coordsNew[iSpc - 1][elemCount - 1] )  # assign with e.g.: structure[1] = "Cl", [0.51, 0.51, 0.51]
                elemCountPrev = elemCountPrev + len(coordsNew[iSpc - 1])

        if otherVASP == True:

            # Adjust incar settings (kpoints, magmom, ediff) accordingly

            # Adjust kpoints based on MPRelaxSet. Note that if we are running MD with reduced DFT sets we further 
            # reduce the kpoints after this call by the previously determined 'divisor'. Note that we adjust INCAR 
            # MAGMOM setting manually rather than taking new values from relax.incar (see below) because magnetic 
            # moments are lost when we convert: structure->atom->structure. Note there is no need to alter EDIFF 
            # because we only work internally with EDIFF_PER_ATOM
            relax = MPRelaxSet(var.structure)
            var.kpoints = relax.kpoints
            # var.kpoints = Kpoints.automatic_density_by_vol(var.structure, 64)   # <- this would be another way of doing the same thing...
            # print('using automatic_density_by_vol method, with kppvol (k-points per volume)=64 : ',var.kpoints.kpts[0][0],var.kpoints.kpts[0][1],var.kpoints.kpts[0][2])
            outf.printt( 1, "    Rescaled kpoints: " + str(var.kpoints.kpts[0][0]) + "x" + str(var.kpoints.kpts[0][1])
                + "x" + str(var.kpoints.kpts[0][2]) )

            # MAGMOM
            # var.incar["MAGMOM"]=relax.incar["MAGMOM"] 'relax' has lost the original info on magnetic moments as queried from mat proj therefore would always get default
            #                                            magmoms (0.6) if we include this line (which would be wrong in some cases)
            resize_magmom_success = resize_magmom(natomFinal)
            if not resize_magmom_success:
                # this needs to go at the point where the POSCAR is written. Possibly with MAGMOM = ? in the INCAR
                outf.toAction(
                    "    ERROR: Magnetic moments in INCAR file are not all equal. Please manually alter the MAGMOM setting in the INCAR files to coincide with new supercell size."
                )
                var.norun = True
            outf.printt(2, "    Extended MAGMOM list: " + str(var.incar["MAGMOM"]))

            # NBANDS. (We use the VASP default here because it is more conservative than the CASTEP one, and I had some crashes at
            # high temp with CASTEP defaults (even with NEXTRA_BANDS=20). Obviously we only need to set this for CASTEP since for VASP
            # it is already the default. (as elsewhere however we use VASP variables and only convert to CASTEP on file write-out)
            if var.dft_engine == "CASTEP":
                highElecParasStageOutputFile = open(var.DFTnonMDhighElecParaspath + "/stageOutput", "r")
                matches = [line for line in highElecParasStageOutputFile if "Number of electrons per atom:" in line]
                p = re.compile(r"[-+]?\d*\.\d+|\d+")
                numbersFromLine = [float(i) for i in p.findall(matches[0])]
                nElecs = numbersFromLine[0] * var.structure.num_sites  # number of electrons for our larger cell
                # According to VASP: MAX(NELECTRONS/2 + NIONS/2, NELECT*0.6)
                var.incar["NBANDS"] = max(nElecs / 2 + var.structure.num_sites / 2, nElecs * 0.6)
                # NEED TO CHECK THIS WORKS AS I HAVE NOT TRIED IT YET (added by hand to .param previously)


def copyStrucsFromDFTconfigs():

    script = "script_" + var.dft_engine + "_singleShot"
    if os.path.isfile(var.dirpath + "/" + script):
        outf.printt( 2, f"  {script} job submission script present in run directory. Copying to stage sub-directories "
            "and renaming as script" )
        copyScript = True
    else:
        copyScript = False

    # Determine directories and/or subdirectories of MD runs (temp#/ in former, temp#/defect# in latter)
    dir_list = [ name for name in os.listdir(var.DFTconfigsDFTNPTpath) if os.path.isdir(os.path.join(var.DFTconfigsDFTNPTpath, name)) ]
    all_dirs = []
    for dir in dir_list:
       subdir_path = os.path.join(var.DFTconfigsDFTNPTpath, dir)
       subdirs = [os.path.join(dir, name) for name in os.listdir(subdir_path) if os.path.isdir(os.path.join(subdir_path, name))]
       if subdirs:
           all_dirs.extend(subdirs)
       else:
           all_dirs.append(dir)    
    print('all_dirs=',all_dirs)

    for direc in all_dirs:
        # reading data from:
        DFTconfigsTempdir = var.DFTconfigsDFTNPTdir + "/" + direc
        DFTconfigsTemppath = var.dirpath + "/" + DFTconfigsTempdir
        # ...writing out to:
        DFTtrainingDataInitialTempdir = var.DFTtrainingDataInitialdir + "/" + direc
        DFTtrainingDataInitialTemppath = var.dirpath + "/" + DFTtrainingDataInitialTempdir
        filesys.createWorkdir(DFTtrainingDataInitialTempdir)
        cells = glob.glob(DFTconfigsTemppath + "/castep.cell_*")  # this returns a list of paths
        for cell in cells:
            cell = os.path.basename(cell)
            cellFile = open(DFTconfigsTemppath + "/" + cell, "r")
            var.atoms = ase.io.castep.read_castep_cell(cellFile)
            resize_magmom_success = resize_magmom( len(var.atoms) )
            if not resize_magmom_success:
                print('ERROR: magmoms not identical when trying to resize magmoms, STOPPING')
                quit()
            var.structure = aio.AseAtomsAdaptor.get_structure(
                var.atoms
            )  # we should be using structure in main code. it is converted back to atoms by convertVASPtoCASTEP2

            cellNum = re.sub("[^0-9]", "", cell)
            DFTtrainingDataInitialTempCelldir = DFTtrainingDataInitialTempdir + "/config" + cellNum
            DFTtrainingDataInitialTempCellpath = var.dirpath + "/" + DFTtrainingDataInitialTempCelldir
            filesys.createWorkdir(DFTtrainingDataInitialTempCelldir)

            if var.dft_engine == "CASTEP":
                convertVASPtoCASTEP2()  # updates var.calc, and defines var.magmoms, and var.ldauu etc.
            DFTsetup.writeDFTinputFiles(
                DFTtrainingDataInitialTempCelldir, DFTtrainingDataInitialTempCellpath, var.DFTinputpath
            )  # Save the adjusted INCAR (or .param in case of CASTEP) file, and the non-adjusted POSCAR and KPOINTS (or .cell file) files, to stage 2 root directory
            if copyScript:
                copyVASPscriptAdjustINCAR(script, DFTtrainingDataInitialTempCellpath)


def get_order(file):
    file_pattern = re.compile(r".*?(\d+).*?")  # ought to put this line somewhere else for efficiency
    match = file_pattern.match(Path(file).name)
    if not match:
        return math.inf
    return int(match.groups()[0])


def concatenateDFT(tempdir):
    temppath = var.dirpath + "/" + tempdir
    concatDFToutputFile = open(temppath + "/castep.castep", "w")
    sortedFiles = sorted(glob.glob(temppath + "/config*/"), key=get_order)
    for path in sortedFiles:  # sorted(glob.glob(temppath+"/config*/"), key=len):
        print("reading to path=", path)
        lines = open(path + "/" + var.DFToutput[0]).readlines()
        concatDFToutputFile.writelines(lines)
    concatDFToutputFile.close()


def alterKpoints(path, kpoints, kpnts1, kpnts2, kpnts3):
    # Write a KPOINTS file to path 'path' from the kpoints object 'kpoints' but with new kpoints values, kpnts1..3 on line 3
    kpointsStyle = str(kpoints.style)
    if kpointsStyle[0] == "G" or kpointsStyle[0] == "g" or kpointsStyle[0] == "M" or kpointsStyle[0] == "m":
        replace_line(path + "/KPOINTS", 3, str(kpnts1) + " " + str(kpnts2) + " " + str(kpnts3) + "\n")
    else:
        print(
            "  ERROR: unable to automatically adjust KPOINTS (Gamma or Monkhorst style required). Please make the adjustment manually and restart, STOPPING."
        )


def replace_line(file_name, line_num, text):
    lines = open(file_name, "r").readlines()
    lines[line_num] = text
    outputFile = open(file_name, "w")
    outputFile.writelines(lines)
    outputFile.close()


def timestepGammaEdiffDirs(DFTMDparastempKptsEncutdir, timesteps, gammas, ediffs_per_atom, DFToriginpath):

    jobdirsLocal = []
    for timestep in timesteps:
        for gamma in gammas:
            for ediff_per_atom in ediffs_per_atom:
                jobdir = DFTsetup.setupMDparasTestdir( DFTMDparastempKptsEncutdir, timestep, gamma, ediff_per_atom, 
                    DFToriginpath )
                jobdirsLocal.append(jobdir)
    return jobdirsLocal


def setupMDparasTestdir(rootdir, timestep, gamma, ediff_per_atom, DFToriginpath):  

    # Sets up directory stagePath/ and writes DFT input files to the directory. POSCAR, KPOINTS and INCAR are written
    # from objects, POTCAR is copied from DFToriginpath/. Adjusts the parameter 'param' in the INCAR file to 'value'

    if os.path.exists(var.dirpath + "/" + rootdir + "/timestep" + str(timestep)) == False:
        os.mkdir(var.dirpath + "/" + rootdir + "/timestep" + str(timestep))
    if os.path.exists(var.dirpath + "/" + rootdir + "/timestep" + str(timestep) + "/gamma" + str(gamma)) == False:
        os.mkdir(var.dirpath + "/" + rootdir + "/timestep" + str(timestep) + "/gamma" + str(gamma))
    if os.path.exists(var.dirpath + "/" + rootdir + "/timestep" + str(timestep) + "/gamma" + str(gamma)) == False:
        os.mkdir(var.dirpath + "/" + rootdir + "/timestep" + str(timestep) + "/gamma" + str(gamma))
    direc = rootdir + "/timestep" + str(timestep) + "/gamma" + str(gamma) + "/ediff" + str(ediff_per_atom)
    path = var.dirpath + "/" + direc
    os.mkdir(path)
    # Adjust param values in INCAR file
    var.incar["POTIM"] = timestep
    if gamma != "Default":
        var.incar["LANGEVIN_GAMMA"] = [gamma] * var.structure.ntypesp
    var.ediff_per_atom = ediff_per_atom
    if var.dft_engine == "CASTEP":
        var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure) # MD paras test stage used 'structure', but CASTEP input write needs 'atoms'
        convertVASPtoCASTEP2()
        var.calc.param.continuation = "default"
    writeDFTinputFiles(direc, path, DFToriginpath)
    if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_MDparas"):
        script = "script_" + var.dft_engine + "_MDparas"
        outf.printt( 2, "  " + script + " job submission script present in run directory. Copying to stage "
            + "sub-directories and renaming as script" )
        copyVASPscriptAdjustINCAR(script, path)

    return direc


def generate_defected_structure(structure, defect_spec):

    from pymatgen.analysis.defects.core import Substitution, Vacancy, DefectComplex
    from pymatgen.core import PeriodicSite

    defect_complex = []  # Initialize an empty list to hold defect objects
   
    prev_indices = [] # only allow one defect per site
    for defect_dict in defect_spec:
        defect_type = defect_dict['type']
        count = defect_dict['count']
        mode = defect_dict['mode']
        
        if mode.upper() == 'REL':
            # Convert relative count to absolute count by multiplying with the total number of atoms
            count = round(count * len(structure))
        print('A')

        if 'V_' in defect_type:  # Check if the defect is a vacancy
            species_string = defect_type.split('_')[1]  # Extract species from defect type
            for _ in range(count):
                print('  ',_,' out of ',range(count))
                print('  checking for match: ',species_string)
                indices = [i for i, site in enumerate(structure) if site.species_string == species_string 
                    and i not in prev_indices]
                if not indices:
                    print(f"No sites found for species: {species_string}")
                    quit()
                print('  indices= ',indices)
                random_index = random.choice(indices)
                print('  random_index=',random_index)
                prev_indices.append(random_index)
                #print('structure=',structure)
                vacancy = Vacancy(structure, structure[random_index], oxi_state=0)#user_charges=user_charges)
                defect_complex.append(vacancy)
        
        elif '_' in defect_type:  # Check if the defect is a substitution
            final_species, initial_species = defect_type.split('_')
            for _ in range(count):
                print('  checking for match: ',initial_species)
                indices = [i for i, site in enumerate(structure) if site.species_string == initial_species
                    and i not in prev_indices]
                print('  indices=',indices)
                if not indices:
                    print(f"  No sites found for species: {initial_species}")
                    quit()
                random_index = random.choice(indices)
                print('  random_index=',random_index)
                prev_indices.append(random_index)
                print('  structure[',random_index,']=',structure[random_index])
                print('  structure[',random_index,'].frac_coords=',structure[random_index].frac_coords)
                #final_species="Nb"
                print('  final_species=',final_species,', as string=',str(final_species))
                subs_site = PeriodicSite( species=final_species, coords=structure[random_index].frac_coords, 
                    lattice=structure.lattice )
                substitution = Substitution(structure, subs_site, oxi_state=0)
                defect_complex.append(substitution)

    # Combine all defect objects into a DefectComplex
    def_comp = DefectComplex(defects=defect_complex)
    print('def_comp=',def_comp)
    
    # Generate the defected structure
    def_struc_unsorted = def_comp.defect_structure
    def_struc = def_struc_unsorted.get_sorted_structure() 
 
    # Adjust size of magmoms array to account for new number of atoms  
    resize_magmom_success = resize_magmom(len(def_struc))
    if not resize_magmom_success:
        print('ERROR: magmoms not identical when trying to resize magmoms, STOPPING')
        quit()
 
    return def_struc



def createDefectDir(direc, completedDirs, script, copyScript):

    """
    Sets up a new directory witin direc/ (direc might be e.g. temp3000/), either for a bulk calc or an initial defect 
    configuration (if direc/ has yet to be created), or a subsequent defect configuration if direc/ has previously 
    been created and the previous job in direc/ has been completed
    """

    # Find defect config corresponding to 'direc' from our defect configs list, and select the next one as the one to
    # be generated

    path = var.dirpath + "/" + direc
    isPath = os.path.exists(path)
    defects_completed = False
    if isPath == False:
        # First time DFT MD job to be performed: set up initial defect configuration, or if no defects, set up bulk
        os.mkdir(path)
        if var.defects:
            defect_config_index = -1 # Index of latest defect directory in var.defect_config_dirs. If none exist: -1
    else:
        # In this case setupMDconfigsdir has already been called before, but this call is to set up new directories for 
        # new defect configurations
        if not completedDirs:
            outf.printt(0,'ERROR: setupMDconfigsdir called, but all initial DFTMD dirs setup and no completedDirs',
                ' specified, STOPPING')
            quit()
        defect_config_index = None # The default, and will occur if the latest job in direc has not yet completed
        for completedDir in completedDirs:
            print('completedDir=',completedDir,', direc=',direc)
            if direc in completedDir: # E.g direc='APDworkdir/.../temp1000/'; completedDir='APDworkdir/.../temp1000/V_Nb__1'
                # Determine which defect configuration has already been completed
                defect_config = completedDir.split('/')[-1]
                print('defect_config=',defect_config)
                # Find the index of the current config in defect_config_dirs
                try:
                    defect_config_index = var.defect_config_dirs.index(defect_config)
                except ValueError:
                    raise ValueError(f"Config {defect_config} not found in defect_config_dirs")

                # Check if there's a next config
                if defect_config_index == len(var.defect_config_dirs) - 1:
                    defects_completed = True
                    print('defects completed') # debug

    if var.defects:
        if defects_completed or defect_config_index == None: # no new defect job to set up because either all 
                                                       # completed or the latest still running
            new_direc = None
        else:

            # Back-up structure and atoms because defects will be added (and possibly cell lengths changed)
            structure_orig = copy.deepcopy(var.structure)
            atoms_orig = copy.deepcopy(var.atoms)

            if defect_config_index >= 0:

                # Adjust lattice parameters of var.structure based on last ionic step of the previous defect calculation
                # Reading the thermal_expansion.dat file to get the volume at the end of the last MD run. If it is 'NaN' go
                # back to the last MD run for this temperature that gave a finite value.
                outf.printt(2, "  Obtaining lattice parameters for new defect based on previous end-of-run volume")
                vol_prev_defect = None
                for i_defect_config_index in range(defect_config_index,-1,-1):
                    print('  i_defect_config_index = ',i_defect_config_index,' (from ',defect_config_index,' to 0')
                    prev_defect_path = os.path.join(var.dirpath, direc, var.defect_config_dirs[i_defect_config_index])
                    with open(os.path.join(prev_defect_path, "thermal_expansion.dat"), "r") as lastVolFile:
                       vol_str = lastVolFile.readline().strip()
                       if vol_str != "NaN":
                           vol_prev_defect = float(vol_str.split(": ")[-1])
                           break
                if vol_prev_defect is None:
                    outf.printt(2, "  No previous defect dir completed without NaNs- keeping lattice parameters to original")
                    vol_prev_defect = var.atoms.get_volume()
                print('  vol_prev_defect from thermal_expansion.dat file = ',vol_prev_defect)
                vol_orig_cell = var.atoms.get_volume()
                vol_ratio = vol_prev_defect / vol_orig_cell
                scale_factor = vol_ratio ** (1/3)
                outf.printt( 1, f"  original lattice parameters = {str(var.atoms.cell)}" )
                new_cell = np.zeros((3, 3))
                # Adjust the lattice parameters based on the volume ratio
                for i in range(9):
                   new_cell[i // 3, i % 3] = var.atoms.cell[i // 3, i % 3] * scale_factor
                outf.printt( 1, f"  volume of original cell = {vol_orig_cell}, prev defect = {vol_prev_defect}, " 
                    "ratio = {scale_factor}" )
                var.atoms.set_cell(new_cell, scale_atoms=True)
                outf.printt( 1, f"  new lattice parameters = {str(var.atoms.cell)}" )
                var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
                # scale parameters of var.atoms (not atoms_prev_defect) according to this new volume, as a ratio of the volume of var.atoms

            # Generate new defect structure and write to new directory
            defect_config_index = defect_config_index + 1
            new_direc = os.path.join(direc, var.defect_config_dirs[defect_config_index])
            #print('before, structure=',var.structure)
            # debug
            #var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure)
            #print('var.atoms, symbols and positions (before):')
            #for symbol, position in zip(var.atoms.symbols, var.atoms.positions):
            #    print(f'{symbol}: {position}')
            ##
            print('len(var.defect_config_dirs)=',len(var.defect_config_dirs))
            defect_config_dir = var.defect_config_dirs[defect_config_index]
            defect_group = var.defect_config_dir_to_group[defect_config_dir]
            var.structure = generate_defected_structure(var.structure, defect_group)
            #print('after, structure=',var.structure)
            #print('and again, structure=',var.structure)
    else:
        print('os.path.join(',direc,', "bulk") = ',os.path.join(direc, "bulk"))
        new_direc = os.path.join(direc, "bulk")

    if new_direc:
        new_path = os.path.join(var.dirpath, new_direc)
        os.mkdir(new_path)
        print('path to write to: ',new_path,' (new_Direc = ',new_direc,')')
        if var.dft_engine == "CASTEP":
            var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure)
            # Atoms object set up at start of APD uses read_castep_cell and has 'castep_labels' array added to it with 
            # 'NULL' for each 
            # atom. After atoms->structure, if pymatgen.analysis.defects adds new atoms to the equivalent structure object 
            # new entries are not added to the equivalent array in the structure object. Accordingly when converted back to
            # atoms object there are elements with 'None' which confuses ASE/ase/ase/io/castep.py (called below in
            # writeDFTinputFiles), which expects these elements to either be 'NULL' or else something defined (probably a 
            # string). Alternatively if not atoms.has('castep_labels') all elements are set to 'NULL' by default by 
            # castep.py. To trigger this:
            if 'castep_labels' in var.atoms.arrays: del var.atoms.arrays['castep_labels']
            convertVASPtoCASTEP2()
        if var.atoms.has('castep_labels'):
            print("var.atoms.get_array('castep_labels')=",var.atoms.get_array('castep_labels'))
            print("len(var.atoms.get_array('castep_labels'))=",len(var.atoms.get_array('castep_labels')))
        print('about to call writeDTinpiutFles::::')
        writeDFTinputFiles(new_direc, new_path, var.DFTinputpath)
        if copyScript:
            copyVASPscriptAdjustINCAR(script, new_path)
        #if var.defects and defects_completed == False and defect_config_index != None and defect_config_index > 0:
        if var.defects: # remember defect_config_index has already been incremented
            print('var.defects, defects_completed, defect_config_index, defect_config_index:',var.defects, defects_completed, defect_config_index, defect_config_index)
            var.structure = structure_orig # reset as next temp#/ defect dir will require bulk to imprint new defects
            var.atoms = atoms_orig
            print('after reseting, lengths of strc and atms:',len(var.structure),len(var.atoms))

    return new_direc


def setupMDconfigsdir(stageDirec, valList, subdirName, DFToriginpath, param, completedDirs = None):

    """
    The first time this is called it sets up subdirectories stageDirec/subdirName{valList} and within each of these 
    sets up  either a 'bulk' directory (for var.defects = None) or an initial defect subdirectory. 
    Subsequently when setupMDconfigsdir is called the user should supply 'completedDirs' - a list of directories where jobs
    have completed. setupMDconfigsdir will then set up the next defect subdirectory in each of the relevant
    subdirName{valList} directories. Note: even if a given defect calculation has failed (NaN in output) we still
    proceed to the next defect in the list for that temperature. Such failed jobs are also stored in 'completedDirs'.
    In each of the defect (or bulk) dirs, POSCAR, KPOINTS and INCAR are written from objects, and POTCAR is copied 
    from DFToriginpath/. Adjusts the parameter 'param' in the INCAR file to the relevant element in valList.
    The function also returns new_direcs, to enable these jobs to be individually submitted to the cluster.
    Note that for defect calculations only one defect directory is set up at a time because the jobs corresponding 
    to different defect configs are performed in serial to allow thermal expansion to be realized (lattice parameters 
    are 'carried through' to the next defect config).
    """

    if param == "TEBEG":
        if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_MD"):
            outf.printt( 2, "  script_" + var.dft_engine + "_MD job submission script present in run directory. "
                + "Copying to stage sub-directories and renaming as script" )
            copyScript = True
            script = "script_" + var.dft_engine + "_MD"
        else:
            copyScript = False
    else:
        if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_MDparas"):
            outf.printt( 2, "  script_" + var.dft_engine + "_MDparas job submission script present in run directory. "
                + "Copying to stage sub-directories and renaming as script" )
            copyScript = True
            script = "script_" + var.dft_engine + "_MDparas"
        else:
            copyScript = False

    new_direcs = []
    for index in valList:
        print('index=',index)
        # Adjust param value in INCAR file
        if param != "LANGEVIN_GAMMA":
            var.incar[param] = index
        else:
            var.incar[param] = [index] * var.structure.ntypesp

        # Set up new directory
        subdirec = subdirName + str(index)
        direc = stageDirec + "/" + subdirec
        print('direc=',direc,' (just before createDefectdir call)')
        new_direc = createDefectDir(direc, completedDirs, script, copyScript) # set up new dirs (e.g. bulk/, V_Nb__1) witin direc
        if new_direc:
            new_direcs.append(new_direc)

    return new_direcs # Send back the directories for these new jobs (useful when new defect or temp directories
                      # have been set up)


def convertDFTtrajToSnapshots(pathDFT_MD, corrTime):

    # Read in a DFT output file file and write out castep.cell files for every 15th configuration in the trajectory

    if var.dft_engine == "CASTEP":

        #castepMDfile = open(os.path.join(pathDFT_MD, var.DFToutput[2]), "r")
        #traj = ase.io.castep.read_castep_md(castepMDfile)
        #castepMDfile.close()
        traj, nSteps, errors, contDir = DFTanalysis.readTraj(pathDFT_MD) # includes traj contributions from cont/ dirs
        print('path=',pathDFT_MD,', nSteps=',nSteps,', len(traj)=',len(traj),', contDir=',contDir)
        trajNew = traj[::corrTime]
        print('corrTime=',corrTime,', len(trajNew)=',len(trajNew))
        # code to write out atoms objects as .cell files (independentally names)
        print(' about to write to: ' + os.path.join( pathDFT_MD, var.DFTinput[1] + "_") + ' followed by number'  )
        for iatom, atoms in enumerate(trajNew):
            castepCellFile = open( os.path.join(pathDFT_MD, var.DFTinput[1] + "_" + str(iatom)), "w" )
            ase.io.castep.write_castep_cell(castepCellFile, atoms)
            castepCellFile.close()

    else:
        print("add code here")
        vaspMD = open(pathDFT_MD + var.DFToutput[1], "r")
        traj = ase.io.vasp.read_vasp_xml(pathDFT_MD + var.DFToutput[1])


def setupDFTinput(DFTorigindir, DFToriginpath):

    # Sets up structure object from DFToriginpath and corresponding DFT input objects

    from pymatgen.io.vasp.inputs import Incar

    if var.dft_engine == "VASP":

        outf.printt(2, "  Setting up objects from VASP inputs in " + DFTorigindir + "...")
        print("    ...read in: ", end="")

        # Read VASP input into var.structure, var.poscar, var.incar and var.kpoints
        print('DFToriginpath = ',DFToriginpath)
        var.structure = Structure.from_file(DFToriginpath + "/POSCAR")
        var.atoms = aio.AseAtomsAdaptor.get_atoms(
            var.structure
        )  # although APD mostly uses structure objects for VASP manipulations, in some places (e.g. in the nonMD stage) atoms are used.
        # POSCAR:
        var.poscar = Poscar(var.structure)
        if var.verbosity >= 2:
            print("POSCAR, ", end="")
        
        # INCAR:
        var.incar = Incar.from_file(DFToriginpath + "/INCAR")
        # We work internally with EDIFF_PER_ATOM. Ensure that we remove EDIFF so that when we come to rewrite the file pymatgen will recompute EDIFF from EDIFF_PER_ATOM
        # before writing EDIFF to file ('EDIFF_PER_ATOM' is only recognized by pymatgen, not by VASP itself)
        var.ediff_per_atom = var.incar["EDIFF"] / var.structure.num_sites
        var.incar.pop("EDIFF", None)
        if var.verbosity >= 2:
            print("INCAR, ", end="")

        # KPOINTS:
        var.kpoints = Kpoints.from_file(DFToriginpath + "/KPOINTS")
        if var.verbosity >= 2:
            print("KPOINTS", end="")

    elif var.dft_engine == "CASTEP":

        outf.printt(2, "  Setting up objects from CASTEP inputs in " + DFTorigindir + "...")
        print("    ...read in: ", end="")
        castepCellFile = open(DFToriginpath + "/castep.cell", "r")

        var.atoms = ase.io.castep.read_castep_cell(castepCellFile)
        if var.verbosity >= 2:
            print(".cell file, ", end="")
        var.calc = var.atoms.calc # read_castep_cell automatically attaches a calculator to atoms, but we prefer to keep calc and atoms objects separate, and only recombine them when we need to write a new file. Note, read_castep_cell attaches extra information to calc, such as kpoints (see below), so it is important to take calc from the atoms object rather than setting one up directly using: var.calc = ase.calculators.castep.Castep()
        var.structure = aio.AseAtomsAdaptor.get_structure( var.atoms ) # converts ASE's 'atom' object to PyMatGen's 'structure' object. Although writing and reading from file uses the atom object, having the structure object allows us to retain structure.num_sites in common with the VASP code
        var.calc.merge_param(DFToriginpath + "/castep.param", ignore_internal_keys=True)  # set up var.calc.param
        if var.verbosity >= 2:
            print(".param file, ", end="")
        convertParamToIncar()
        # Also read in those things from .cell that are part of INCAR
        if var.calc.cell.symmetry_generate is not None:
            if str(var.calc.cell.symmetry_generate.value).upper() == "TRUE": # strangely this variable is string type not boolean
                var.incar.pop("ISYM", None) # use symmetry
            else:
                var.incar["ISYM"] = 0
        # Magnetic moment
        magmom = var.atoms.get_initial_magnetic_moments()
        if magmom is not None:
            var.incar["MAGMOM"] = magmom
            # var.calc._set_atoms = True # <-- this should be read in or automatically set...
        # assign kpoints variables from var.calc.cell to var.kpoints (for use in APD)
        var.kpoints = types.SimpleNamespace()
        var.kpoints.kpts = [[0] * 3 for i in range(3)]
        var.kpoints.kpts[0][0] = var.calc.cell.kpoint_mp_grid._value[0]
        var.kpoints.kpts[0][1] = var.calc.cell.kpoint_mp_grid._value[1]
        var.kpoints.kpts[0][2] = var.calc.cell.kpoint_mp_grid._value[2]

    elif var.dft_engine == "PWSCF":

        outf.printt(2, "  Setting up objects from PWSCF inputs in " + DFTorigindir + "...")
        print("    ...read in: ", end="")
        in_files = [os.path.basename(f) for f in glob.glob(os.path.join(DFToriginpath, '*.in'))]
        if not in_files:
            outf.printt(0, '  ERROR: no .in file found, STOPPING')
            quit()
        elif len(in_files)>1:
            outf.printt(0, '  ERROR: multiple .in files found, STOPPING')
            quit()
        var.atoms = ase.io.read(DFToriginpath + '/' + in_files[0], format='espresso-in')
        var.structure = aio.AseAtomsAdaptor.get_structure( var.atoms )
        if var.verbosity >= 2:
            print(".in file, ", end="")
       
        # Things that would be in INCAR...
        PWSCFinToIncar(DFToriginpath + '/' + in_files[0])
        print('var.incar=',var.incar)
        print('var.kpoints.kpts=',var.kpoints.kpts)
        var.prefix_no_apost = var.incar['prefix'].strip("'\"") # This will be prefix to files, e.g. Ni.in
        var.DFTprefix = var.prefix_no_apost # This is used to copy files from the calculation root. Only for PWscf can
                # we not identify this earlier in DFTsetup.setupDFTvariables

    var.nAtomsInitial = var.structure.num_sites
    var.speciesInitial = []
    for species in var.structure.species:
        if species not in var.speciesInitial:
            var.speciesInitial.append(species)
    var.speciesInitialStrings = [
        str(x) for x in var.speciesInitial
    ]  # var.speciesInitial contains non string objects, e.g. 'Element Ba'. Convert to strings so that we can compare with species from potential file.

    var.initialSpeciesString = (
        ""  # transform to a single string. This will be of use e.g. when we come to writing potential files for LAMMPS
    )
    for species in var.speciesInitial:
        var.initialSpeciesString = var.initialSpeciesString + str(species)

    var.nSpeciesInitial = [None] * len(var.speciesInitial)
    for iSpecies, species in enumerate(var.speciesInitial):
        var.nSpeciesInitial[iSpecies] = var.structure.species.count(species)
    # var.speciesInitial=list(set(var.structure.species)) # no. of unique species. 'set' ensures only unique elements from the list of all species. THIS DOESNT PRESERVE ORDER

    if var.verbosity >= 2:
        print("")


def setupDFTvariables():
    # Set up variables specific to the DFT engine

    var.DFTinput = [None] * 4
    var.DFToutput = [None] * 6
    if var.dft_engine == "VASP":
        var.DFTinput[0] = "INCAR"
        var.DFTinput[1] = "POSCAR"
        var.DFTinput[2] = "KPOINTS"
        var.DFToutput[0] = "OUTCAR"  # we use this during analysis of DFT (e.g. to extract temperatures)
        var.DFToutput[1] = "vasprun.xml"  # this is used for fitting (at least for MEAMfit), and is the file copied to 
                # root of 3_configs/ on completion of DFT jobs
        var.stringNewMDstepForDFToutput1 = "<calculation>" # string before every MD step in vasprun.xml (used when
                # parsing configs in preparing training sets)
        var.stringTempForDFToutput1 = '<i name="TEBEG"> '
        var.DFToutput[2] = "XDATCAR"  # for trajectories
        var.DFToutput[3] = ""  # VASP doesn't have a specfic file output for errors
        var.DFToutput[4] = "CONTCAR"  # final config
        var.DFTprefix = "vasprun"
        var.DFTsuffix = ".xml"

    elif var.dft_engine == "CASTEP":
        var.DFTinput[0] = "castep.param"
        var.DFTinput[1] = "castep.cell"
        var.DFTinput[2] = "castep.cell"  # for CASTEP KPOINTS goes into .cell
        var.DFToutput[0] = "castep.castep"  # for analysis of DFT (energies, forces, stresses from here are from BO surface)
        var.DFToutput[1] = "castep.castep"  # this is used for fitting (at least for MEAMfit), and is the file copied to root of 3_configs/ on completion of DFT jobs
        var.stringNewMDstepForDFToutput1 = "Starting MD iteration" # string before every MD step in .castep
        var.stringTempForDFToutput1 = 'temperature          '
        var.DFToutput[2] = "castep.md"  # this can be used to extract trajectories but NOT energies, forces, stresses, since these include contributions from the thermostat
        var.DFToutput[3] = "castep.0001.err"  # if this file appears it means there has been some terminal error in the CASTEP job.
        var.DFToutput[4] = ""  # CASTEP doesn't have a file for final config
        var.DFToutput[5] = "castep.geom"  # Special file CASTEP uses for geom opt. Should probably go next to .md and reorder
        var.DFTprefix = "castep"
        var.DFTsuffix = ".castep"

    elif var.dft_engine == "PWSCF":
        # Note variable initialization for var.DFTprefix happens later for PWscf as it needs to be read in from a .in file first
        var.DFTinput[0] = "PWSCF.in"
        var.DFToutput[0] = "PWSCF.out"
        var.DFToutput[1] = "PWSCF.out"
        var.DFTsuffix = ".out"
        var.stringNewMDstepForDFToutput1 = "Entering Dynamics:    iteration ="
        var.stringTempForDFToutput1 = 'Starting temperature'
    print('var.DFTsuffix = ',var.DFTsuffix)    
   
    # Also some strings for output purposes for some commonly refered to settings parameters
    # Note that there is not always a one-to-one mapping, e.g. LANGEVIN_GAMMA /= md_ion_t, but in fact = 1/md_ion_t
    if var.dft_engine == "VASP":
        var.s_potim = "POTIM"
        var.s_langevin_gamma = "LANGEVIN_GAMMA"
        var.s_langevin_gamma_l = "LANGEVIN_GAMMA_L"
    elif var.dft_engine == "CASTEP":
        var.s_potim = "md_delta_t"
        var.s_langevin_gamma = "md_ion_t"
        var.s_langevin_gamma_l = "md_cell_t"


def read_ase_generated_file(in_file):
    """
    Read the ASE-generated .in file into separate blocks so we can merge new parameters into it.
    """
    #print('file=', in_file)

    # Initialize the blocks for different sections
    blocks = {
        'control': [],
        'system': [],
        'electrons': [],
        'ions': [],
        'cell': [],
        'fcp': [],
        'rism': [],
        'atomic_species': [],
        'cell_parameters': [],
        'atomic_positions': [],
        'k_points': []
    }

    current_block = None
    with open(in_file, 'r') as file:
        for line in file:
            stripped = line.strip()

            # Detect named blocks starting with '&'
            alphasand_block_just_started = False
            if stripped.startswith('&'):
                block_name = stripped[1:].split()[0].lower()
                if block_name in blocks:
                    current_block = block_name
                    alphasand_block_just_started = True
                else:
                    current_block = None

            # Detect end of blocks marked by '/'
            elif stripped == '/':
                current_block = None

            # Detect special sections like ATOMIC_SPECIES, CELL_PARAMETERS, etc.
            elif stripped.lower().startswith('atomic_species'):
                current_block = 'atomic_species'
            elif stripped.lower().startswith('cell_parameters'):
                current_block = 'cell_parameters'
            elif stripped.lower().startswith('atomic_positions'):
                current_block = 'atomic_positions'
            elif stripped.lower().startswith('k_points'):
                current_block = 'k_points'

            # Add the current line to the appropriate block
            if current_block and not alphasand_block_just_started:
                if current_block == 'atomic_species' or current_block == 'cell_parameters' or current_block == 'atomic_positions' or current_block == 'k_points':
                   blocks[current_block].append(stripped)
                else:
                   blocks[current_block].append('    '+stripped)

    return blocks


def merge_incar_with_existing(blocks):
    """
    Merges the parameters from var.incar into the blocks extracted from the existing ASE-written .in file.
    Note if we are using a kpoints grid we need to add this by hand here since it isn't dealt with by ase.io.espresso
    """

    # Reverse mapping to determine which namelist each INCAR key belongs to
    var.namelist_map = {
        'POTIM': 'control',
        'NSW': 'control',
        'ENCUT': 'system',
        'encutrho': 'system',
        'EDIFF': 'electrons',
        'TEBEG': 'ions',
        'MDALGO': 'ions',
        'ISPIN': 'system',
        'PSTRESS': 'cell',
        'NBANDS': 'system',
        'SYMM': 'system',
        'NELM': 'electrons',
        'smearing': 'system',
        'degauss': 'system',
        'occupations': 'system',
        'mixing_beta': 'electrons',
        'prefix': 'control'
    }

    # Add additional tags to blocks that may be necessary for PWSCF based on other settings
    if var.incar['NSW'] <= 1:
       # Unlike VASP or CASTEP:
       # i) NSW=0 will just do a dry run
       var.incar['NSW'] = 1
       # ii) have to explicitly tell it to still do an SCF calculation
       blocks['control'].append("    calculation = 'scf'")

    for key, value in var.incar.items():
        if key in ['pseudopotentials', 'k_points', 'atomic_positions', 'cell_parameters', 'ISIF']:
            continue  # Skip those handled separately. In case of ISIF just skip for now; may need to implement later
                      # in terms of a map to tprnfor, tstress, calculation=vc-md vs md (and -relax) etc

        # Translate the key back to PWscf parameter using the reverse map
        original_key = var.incar_to_pwscf_map.get(key, key)

        # Determine which block the parameter belongs to
        block_name = var.namelist_map.get(key, 'control')  

        #print('mapped ',key,' to ',original_key,' which is to below to block: ',block_name)

        # Necessary 'back conversions':
        # SYMM (0, 1 respectively) -> nosym (True, False)
        if original_key == 'nosym': # value is that mapped straight back from ISYM
            if value==0:
                value = '.true.'
            else:
                value = '.false.'

        # Add the parameter to the appropriate block (only if it's not already present)
        if not any(line.split('=')[0].strip() == original_key for line in blocks[block_name]):
            blocks[block_name].append(f"    {original_key} = {value}")

    #if 'MAGMOM' in var.incar:
    #    for i, magmom in enumerate(var.incar['MAGMOM']):
    #        blocks['system'].append(f"    starting_magnetization({i + 1}) = {magmom}")

    return blocks


def write_combined_file(out_file, blocks):
    """
    Write the combined blocks (ASE output + var.incar) into the final .in file.
    """
    with open(out_file, 'w') as file:
        # Write named blocks like &control, &system, &electrons, etc.
        for block_name, lines in blocks.items():
            if block_name not in ['atomic_species', 'cell_parameters', 'atomic_positions', 'k_points'] and lines:
                file.write(f"&{block_name.upper()}\n")
                for line in lines:
                    file.write(f"{line}\n")
                file.write("/\n\n")

        # Write special sections without the '&' and '/'
        if blocks['atomic_species']:
            for line in blocks['atomic_species']:
                file.write(f"{line}\n")
            file.write("\n")

        if blocks['cell_parameters']:
            for line in blocks['cell_parameters']:
                file.write(f"{line}\n")
            file.write("\n")

        if blocks['atomic_positions']:
            for line in blocks['atomic_positions']:
                file.write(f"{line}\n")
            file.write("\n")

        if blocks['k_points']:
            for line in blocks['k_points']:
                file.write(f"{line}\n")
            file.write("\n")

    print(f"Written combined .in file to {out_file}")


def writeDFTinputFiles(dirToWrite, pathToWrite, pathPotential, writePOSCAR=True, writeINCAR=True, writeKPOINTS=True, writePOTCAR=True, write_frac_coords=False):

    # Writes DFT input files to chosen directory and path (dirToWrite is just for nicer output to screen).
    # Adjust writePOSCAR etc to write only a selection of files (writeKPOINTS and writePOTCAR only apply to VASP)
    # Use pathPotential to copy POTCAR from specific location for VASP or pseudo pots (if necc) for CASTEP
    # For VASP: uses var.poscar. This is used instead of structure (and subsequent var.poscar=Poscar(var.structure)) because
    # there is at least one occurance (resizeCell) where var.poscar is manipulated directly to reorder species.
    # For CASTEP: uses var.atoms.

    outf.printt(1, "  Writing DFT input files to: " + dirToWrite)

    if var.dft_engine == "VASP":
        if writePOSCAR:
            #if write_frac_coords:
            #    var.poscar.structure.cart_coords = False # This hasn't been tested yet
            var.poscar.write_file(pathToWrite + "/POSCAR", direct = write_frac_coords)
        if writeINCAR:
            var.incar["EDIFF"] = ( var.ediff_per_atom * var.poscar.structure.num_sites ) # used to be var.structure.num_sites when we used to accept poscar and structure objects into this function. this should work fine but may need to test it. Internally in the code we use EDIFF_PER_ATOM so we don't have to scale it when we change number of atoms, for example. But need to convert back to EDIFF when writing to file because VASP does not understand EDIFF_PER_ATOM
            var.incar.pop( "EDIFF_PER_ATOM", None )  # this shouldnt be necesary since ediff_per_atom will not be present in var.incar (even if we set it, it just ends up getting converted to ediff)
            var.incar.write_file(pathToWrite + "/INCAR")
            # Now return to EDIFF_PER_ATOM in case we progress directly to the next stage (setupDFTfromPrevStage did this before entering the stage loop, but isn't called again from within the stage loop)
            var.ediff_per_atom = var.incar["EDIFF"] / var.poscar.structure.num_sites # see above
            var.incar.pop("EDIFF", None)
        if writeKPOINTS: var.kpoints.write_file(pathToWrite + "/KPOINTS")
        if writePOTCAR:
            if pathPotential != "":
                shutil.copy2(pathPotential + "/POTCAR", pathToWrite)
            else:
                var.potcar.write_file(pathToWrite + "/POTCAR")
        if var.vdw == True: # alternatively: if os.path.isfile(pathPotential + "/vdw_kernel.bindat") == True
            print("  Copying vdw_kernel.bindat to " + str(dirToWrite) + "/")
            shutil.copy2(var.dirpath + "/vdw_kernel.bindat", pathToWrite)

    elif var.dft_engine == "CASTEP":
        # Write CASTEP files
        # Tried initially using the 'calculate' method, with self._prepare_input_only = True (see below). However does not correctly write out the magmoms in the .cell file
        # # var.calc.calculate(var.atoms) # Make sure you set: self._prepare_input_only = True    in  /home/vol01/scarf781/miniconda3/envs/my_pymatgen/lib/python3.9/site-packages/ase/calculators/castep.py   (or whereever your castep.py resides)
        #print('atoms just before var.atoms.calc etc: (note, memory address=',id(var.atoms),')')
        #for symbol, position in zip(var.atoms.symbols, var.atoms.positions):
        #   print(f'{symbol}: {position}')

        var.atoms.calc = var.calc  # Reattach the calculator to the atoms ready for file write
        var.atoms.calc._directory = pathToWrite
        #var.atoms.get_potential_energy()  # i don't know if there is a var.atoms.calculate(), so try this first. Make sure you set: self._rename_existing_dir = False   in   /home/vol01/scarf781/miniconda3/envs/my_pymatgen/lib/python3.9/site-packages/ase/calculators/castep.py   (or whereever your castep.py resides)    UPDATE : Recently removed this as now we separate .cell and .param write out, the two commands write_param and write_castep_cell should together perform the same effective operation as this command 
        if writeINCAR:
            ase.io.castep.write_param(pathToWrite + "/castep.param", param=var.atoms.calc.param, force_write=True)
        if writePOSCAR:
            magmoms_get = var.atoms.get_initial_magnetic_moments()
            magmoms_get = np.array(magmoms_get)
            CASTEPcellFile = open(pathToWrite + "/castep.cell", "w")
            #print('atoms just before writing to file : (note, memory address=',id(var.atoms),')')
            #for symbol, position in zip(var.atoms.symbols, var.atoms.positions):
            #    print(f'{symbol}: {position}')
            ase.io.castep.write_castep_cell(CASTEPcellFile, var.atoms, positions_frac = write_frac_coords, 
                    magnetic_moments = magmoms_get, precision = 10)
            #quit()
            # I could not find a way to output ldauu through ase, so we manually add the relevant block
            # lda+u
            # if set(var.ldauu)!={0}: # if ldauu contains more than just zeros...
            #    CASTEPcellFile.writelines(["%BLOCK HUBBARD_U\n","  eV\n"])
            #    speciesTypes=list(dict.fromkeys(var.structure.species))
            #    l_numToLett=['s','p','d','f']
            #    for speciesTypeIndex,speciesType in enumerate(speciesTypes):
            #       if var.ldauu[speciesTypeIndex]>0 and var.ldaul[speciesTypeIndex]!=-1:
            #          CASTEPcellFile.writelines(["    "+str(speciesType)+"  "+str(l_numToLett[var.ldaul[speciesTypeIndex]])+": "+str(var.ldauu[speciesTypeIndex])+"\n"])
            #    CASTEPcellFile.writelines(["%ENDBLOCK HUBBARD_U\n","\n"])

            # the write_castep_cell call already adds the k-points to the cell file (since we have attached var.atoms.calc.cell.kpoint_mp_grid). Below is
            # how I initially did it. Note: the offset I haven't yet implemented (to get VASP's MP as opposed to VASP's Gamma mode)
            # # Add k-points to .cell file
            # kpointsStr=str(var.kpoints)
            # CASTEPcellFile.writelines(["KPOINT_MP_GRID "+str(kpointsStr.splitlines()[3])+"\n"])
            # if str(var.kpoints.style)[0]=="M" or str(var.kpoints.style)[0]=="m": # Monkhorst-Pack grid offset with respect to the origin of the Brillouin zone
            #    CASTEPcellFile.writelines(["KPOINT_MP_OFFSET 0.5 0.5 0.5\n"]) # need to check this
            # if var.incar["ISYM"] > 0 or (var.incar["ISIF"] <= 2 and var.incar["NSW"] > 1):
            #     CASTEPcellFile.write("")
            #     if var.incar["ISYM"] > 0:
            #         CASTEPcellFile.write("SYMMETRY_GENERATE\n")
            #     if var.incar["ISIF"] <= 2 and var.incar["NSW"] > 1: # 1 here, not 0, because for castep md_num_iter can't = 0
            #        CASTEPcellFile.write("FIX_ALL_CELL: TRUE\n")
            CASTEPcellFile.close()
        if writePOTCAR and var.species_pot and var.species_pot != 'NCP':
            if pathPotential == "":
                pathPotential = var.dirpath # for CASTEP unlike VASP if species_pot are specified the relevant files
                        # need to be provided in the calculation directory
            for species_pot_ind in var.species_pot: # e.g. species_pot = [("O", "o-optgga1.recpot"), ... ]
                shutil.copy2(os.path.join(pathPotential, species_pot_ind[1]), pathToWrite)

    else:
        # PWscf

        # kpoints
        kpts = None
        koffset = (0, 0, 0)  # Default to no offset

        # Check for gamma k-points (special case)
        #print('var.kpoints.kpts = ',var.kpoints.kpts)
        kpointsgrid = False
        if isinstance(var.kpoints.kpts, np.ndarray):
            kpts = var.kpoints.kpts  # Raw k-points with weights
            koffset = None
            # Update. Frustratingly at the moment ase espresson implementation can't do k-point lists. So we have to not send
            # either of these and do it manualy afterwards
            kpointsgrid = True
        elif var.kpoints.kpts == [[0, 0, 0] for _ in range(3)]:
            kpts = None  # Gamma-point only calculation
            koffset = None
        else:
            # Handle automatic k-points grid
            kpts = tuple(var.kpoints.kpts[0])  # Extract the k-points grid
            koffset = (0, 0, 0)
        #print('kpts=',kpts,', koffset=',koffset)
        #print("var.prefix_no_apost: ",var.prefix_no_apost)
        if not kpointsgrid:
            ase.io.write(f"{pathToWrite}/{var.prefix_no_apost}.in_prelim", var.atoms, format='espresso-in', pseudopotentials=var.incar['pseudopotentials'],
                kpts=kpts, koffset=koffset) 
        else:
            ase.io.write(f"{pathToWrite}/{var.prefix_no_apost}.in_prelim", var.atoms, format='espresso-in', pseudopotentials=var.incar['pseudopotentials'])
            # add code to append the kpoints manually to end of file

        # Unfortunately ase for PWscf doesn't yet write everything to file. So we need to parse what ase did,
        # add in the extra stuff in var.incar, and rewrite
        blocks = read_ase_generated_file(f"{pathToWrite}/{var.prefix_no_apost}.in_prelim")
        blocks = merge_incar_with_existing(blocks)
        if kpointsgrid:
           lines = []
           lines.append("K_POINTS tpiba")
           lines.append(str(len(var.kpoints.kpts)))
           for kp in var.kpoints.kpts:
               lines.append(" ".join(map(str, kp)))
           blocks['k_points'] = lines
        write_combined_file(f"{pathToWrite}/{var.prefix_no_apost}.in", blocks)
        if writePOTCAR and 'pseudopotentials' in var.incar:
            if pathPotential != "":
                print("var.incar['pseudopotentials'].items()=",var.incar['pseudopotentials'].items())
                for element, pseudo_file in var.incar['pseudopotentials'].items():
                    shutil.copy2(f"{pathPotential}/{pseudo_file}", pathToWrite)
            else:
                print('ERROR: no path for potential file (for PWscf) given to writeDFTinputFiles, STOPPING')
                stop


def next_iteration_pot(pot):

    # Increment iteration of pot. E.g. EAM1 -> EAM1B or EAM2C -> EAM2D.

    if pot[-1].isnumeric():
        potnew = pot + "B"
    else:
        newletter = ascii_letters[(ascii_letters.index(pot[-1]) + 1)]
        potnew = pot[:-1] + newletter
    outf.printt(2, "  Current potential=" + pot + ", new potential will be=" + potnew)

    return potnew


def setupDFTforRelabelling(potType, xc, mat, pot, tempDirec='/', badCfgs=None):

    """
    Here we convert LAMMPS configurations from:
       APDworkdir/potMD/xc/mat/pot/8_simulation/tempDirec/{dump.meam, or new_selected.cfg}, 
    ...to POSCAR files in:
       APDworkdir/DFT/xc/mat/potnew/4B_activeLearning/
    dump or .cfg files are used for M/EAM or MTP potentials respectively. For M/EAM badCfgs stores the index of configs 
    in dump.meam which need relabelling. For MTPs all configs in new_selected.cfg need relabelling.
    tempDirec is not needed for MTP as unlike M/EAM, configs for relabelling are taken from all temps, not a specific temp
    'pot' will be of the form, e.g. EAM1, EAM2, etc. potnew will be augmented with a letter to show that the DFT data 
    is for optimizing a new potential based on the old one. E.g. if pot=EAM1, potnew=EAM1B. Or if we have already done
    some active learning, pot=EAM1B, potnew=EAM1C.
    """

    if potType == 'MTP':
        # Determine species as ordered in the configs for use later (since potMD simulations are seeded using structures
        # with the same ordering as the potentials we can get this from the potential filename
        print('searching dir:',var.potTestpath,' (contents:',os.listdir(var.potTestpath))
        potFileFromPotTest = glob.glob(os.path.join(var.potTestpath, "Trained.mtp_*"))
        print('befre sfiting: ',potFileFromPotTest)
        potFileFromPotTest = [file for file in potFileFromPotTest if not file.endswith('Trained.mtp_')] # don't want
                # 'Trained.mtp_'
        print('after sfiting: ',potFileFromPotTest)
        potFileFromPotTest = potFileFromPotTest[0] # Should be just one file like 'Trained.mtp_LiOZrNbLa'...
        compositionPotentialString, speciesPotentialString, speciesPotential = potMDsetup.compositionFromPotFile(
                potFileFromPotTest, 'MTP')
    # ** NOT CLEAR IF I NEED A CONDITIONAL FOR MEAM CASE? ARE MEAM POTENTIALS STILL REORDERED ACCORDING TO DFT?

    potnew = next_iteration_pot(pot) # increment e.g. EAM1 -> EAM1B

    # Setup new directories
    simultempdir = var.potMDsimuldir + "/" + tempDirec
    simultemppath = var.dirpath + "/" + simultempdir
    potold = pot
    pot = potnew
    initialize.initDirsPaths(xc, mat, pot)
    if os.path.exists(var.DFTtrainingDataBadpath) == False:
        filesys.createWorkdir(var.DFTtrainingDataBaddir)
    filesys.createWorkdir(var.DFTtrainingDataBaddir + "/" + pot)

    os.chdir(str(var.DFTtrainingDataBadpath) + "/" + pot)
    if var.dft_engine == "CASTEP":
        os.system("cp " + var.dirpath + "/castep_keywords.json ./")

    # Set up and adjust VASP input files
    #DFTsetup.setupDFTinput(var.DFTconfigsdir, var.DFTconfigspath) # read primitive POSCAR and other VASP inputs from 
    #        # previous directory.  < already read in as high elec paras or from 1_
    DFTsetup.resizeCell(var.natomTargetMin_MD, var.natomTargetMax_MD, True) # Increase supercell size (and adjust 
            # k-points) if neccessary
    var.incar["ISIF"] = 2
    var.incar.pop("LANGEVIN_GAMMA", None)
    var.incar.pop("LANGEVIN_GAMMA_L", None)
    var.incar.pop("PMASS", None)
    var.incar.pop("POTIM", None)
    var.incar.pop("TEBEG", None)
    var.incar["NSW"] = 0

    # Determine if script to be copied
    if os.path.isfile(var.dirpath + "/script_" + var.dft_engine + "_singleShot"):
        outf.printt(2, f"  script_{var.dft_engine}_singleShot job submission script present in run directory. Copying "
                "to stage sub-directories and renaming as script")
        copyScript = True
    else:
        copyScript = False

    if var.dft_engine == "CASTEP":
        # Convert those CASTEP inputs which we have been storing in VASP objects (e.g. var.incar) back to either 
        # CASTEP objects (e.g. var.calc.param) or variables e.g. var.magmoms, which we will use subsequently here
        var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure)
        convertVASPtoCASTEP2()  # updates var.calc, and defines var.magmoms, and var.ldauu etc.
    #elif var.dft_engine == "PWSCF":
    #    # For QE we also need to do this, but a lot of the info will be assigned direct to the var.atoms object.
    #    # In order for the subsequent read-in of positions from POSCAR# not to overwrite these base settings we can
    #    # read positions into an atoms_dummy object, then just copy the positions over to var.atoms
    #    # Actually may not be necessary. I think var.atoms has already been initialized. Check:
    #    print('var.atoms=',var.atoms)
    #    quit()
    #    # If so we don't need to read it in again here. All we need to do is do the atoms_dummy bit below

    if 'EAM' in potType:
        os.system(f"cp {simultemppath}/dump2.meam {var.DFTtrainingDataBadpath}/{pot}/dump.lmc")
    else:
        os.system(f"cp {simultemppath}/new_selected.cfg {var.DFTtrainingDataBadpath}/{pot}/")
        # Set up and run command to generate POSCAR files
        MLIPcommand = f'{var.mlip_path} convert-cfg new_selected.cfg POSCAR --output-format=vasp-poscar'
        print('running using: ',MLIPcommand)
        target_path = os.path.join(var.DFTtrainingDataBadpath, pot)
        outf.printt(2, f"  About to run {MLIPcommand} in path: {target_path}")
        subprocess.run(MLIPcommand, shell=True, check=True, cwd=target_path)
        poscar_path = os.path.join(target_path, 'POSCAR')
        if os.path.exists(poscar_path):
            os.rename(poscar_path, os.path.join(target_path, 'POSCAR0'))
            outf.printt(2, f"    Renamed POSCAR to POSCAR0")
        # Populate badCfgs list with indices and add species line before species number line in POSCAR (necessary if 
        # we need to convert to .cell)
        file_pattern = os.path.join(target_path, "POSCAR*")
        badCfgs = []
        for filename in glob.glob(file_pattern):
            # Append number to badCfgs
            poscar_number = filename.split('POSCAR')[-1]
            badCfgs.append(int(poscar_number))
            # Add species names to POSCAR
            with open(filename, 'r') as file:
                lines = file.readlines()
            lines.insert(5, speciesPotentialString + '\n')
            with open(filename, 'w') as file:
                file.writelines(lines)
        outf.printt(2, f"  Species names added to POSCARs and badCfgs list populated for MTPs: {badCfgs}")

    # Convert LAMMPS dump/.cfg output to 5_trainingData/Bad/conf#/POSCAR files
    # Note I wrote the following first for CASTEP and had it that the inputs are wrote first to the new directory
    # then the config positions are read in, then the .cell is written again using these new positions.
    # For PWSCF I need to use the writeDFTinputFiles to write the .in file (ase is not fully functional for
    # PWSCF) so i had to change the atoms object first, which in any case is probably the more sensible approach...
    for cfg in badCfgs:
        cfgdir = var.DFTtrainingDataBaddir + "/" + pot + "/" + str(cfg)
        cfgpath = var.dirpath + "/" + cfgdir

        if var.dft_engine == "PWSCF":
            # In this case we read in the new positions and lattice parameters first, append them to the existing
            # var.atoms, then write them out using writeDFTinputFiles. Probably for CASTEP should alter so it is the
            # same way.
            structure_tmp = Structure.from_file(f"POSCAR{cfg}")
            atoms_tmp = aio.AseAtomsAdaptor.get_atoms(structure_tmp)
            # Add the lattice vectors and positions from atoms_tmp onto var.atoms
            var.atoms.set_cell(atoms_tmp.get_cell())  # Update the cell (lattice vectors)
            var.atoms.set_positions(atoms_tmp.get_positions())  # Update the atomic positions

        filesys.createWorkdir(cfgdir)
        # Write DFT input files to the directory. Afterwards the POSCAR/.cell/.in will be re-written based on the new
        # positions from POSCAR{cfg}. For CASTEP this allows to write the .param file now, and copy relevant psuedopot
        # files over (these things are taken care of by writeDFTinputFiles below). For PWscf maybe makes less sense
        # as the whole .in file needs rewriting again...
        if var.config_gen == 'DFT_NPT_EXISTING' or var.config_gen == 'DFT_NVT_EXISTING': # if DFT provided, potentials 
                # won't have been copied to DFTconfigs dir
           DFTsetup.writeDFTinputFiles(cfgdir, cfgpath, var.DFTinputpath) # 3rd arg: location potentials (e.g. POTCAR)
        else:
           DFTsetup.writeDFTinputFiles(cfgdir, cfgpath, var.DFTconfigspath)
        if copyScript:
            copyVASPscriptAdjustINCAR("script_" + var.dft_engine + "_singleShot", cfgpath)

        if 'EAM' in potType:
            lammpsDumpFile = open("dump.lmc", "r")
            speciesInitialStrings = [str(x) for x in var.speciesInitial]  # var.speciesInitial contains non string objects,
                    # e.g. 'Element Ba'. Convert to strings so that we can compare with species from potential file.
            atomsFromLAMMPS = ase.io.lammpsrun.read_lammps_dump_text(lammpsDumpFile, index=int(cfg) - 1, specorder=
                    speciesInitialStrings)
            structureFromLAMMPS = aio.AseAtomsAdaptor.get_structure(atomsFromLAMMPS)
            poscarFromLAMMPS = Poscar(structureFromLAMMPS)
            poscarFromLAMMPS.write_file(str(cfg) + "/POSCAR")
        else:
            shutil.copy2(f"POSCAR{cfg}", f"{cfg}/POSCAR")

        if var.dft_engine == "CASTEP":
            var.structure = Structure.from_file(str(cfg) + "/POSCAR")
            var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure)
            # convert VASP to CASTEP output
            if var.species_pot:
                var.calc.cell.species_pot = var.species_pot
            var.calc.cell.symmetry_generate = "FALSE" 
            var.calc.cell.kpoint_mp_grid = var.kpoints.kpts[0][0:3] 
            var.atoms.calc = var.calc
            castepCellFile = open(str(cfg) + "/castep.cell", "w")
            ase.io.castep.write_castep_cell(castepCellFile, var.atoms)  # ,magnetic_moments=magmoms_get)
            castepCellFile.close()


    return pot


def genUncorr(direcUncorr, pathUncorr):

    """
    Analyzes MD in directories/subdirectories of pathUncorr and determines uncorrelated snapshots. The next step
    depends on if any of the snapshots from these MD runs need recalculating using DFT (if using 'reduced' mode or if 
    dft_energy_tol has been specified). If so then uncorrelated atomic configurations are written out into the 
    directories/subdirectories so that the next stage can pick these up and relabel them. Alternatively if no new DFT
    is required DFT outputs are copied to stage root, correlated snapshots are removed, and a fitdbse file is generated
    for subsequent use by MEAMfit.
    """

    # Determine directories and/or subdirectories of MD runs (temp#/ in former, temp#/defect# in latter)
    dir_list = [ name for name in os.listdir(pathUncorr) if os.path.isdir(os.path.join(pathUncorr, name)) ]
    all_dirs = []
    for dir in dir_list:
       subdir_path = os.path.join(pathUncorr, dir)
       subdirs = [os.path.join(dir, name) for name in os.listdir(subdir_path) if os.path.isdir(os.path.join(subdir_path, name))]
       if subdirs:
           all_dirs.extend(subdirs)
       else:
           all_dirs.append(dir)    

    # Determine autocorrelation of DFT output (we only set up the relevant data for the next stage for uncorrelated snapshots)
    temperatures = []
    corrTimeVsDir = []  # correlation times for each temp#/ or temp#/defect#/ directory
    for direc in all_dirs:
        # corrTime = DFTanalysis.getCorrTime(pathUncorr + "/" + direc) # this subroutine needs improving. Currently it
        # # often under or overpredicts (sometimes = 1 for example...). Needs overhauling along with the code which
        # # determines POTIM - for next year. For now simplify and just set to 10
        corrTime = 10
        corrTimeVsDir.append(corrTime)
    outf.printt( 1, f"  directories: {all_dirs}, corrTimeVsDir:{corrTimeVsDir}" )

    # Set up relevant data for next stage. This will either be the uncorrelated POSCARs in the temp#/ or temp#/defect 
    # dirs or the uncorrelated vaspruns, brought to the stage root and renamed according to their temp#, defect#.

    if "REDUCED" in var.config_gen or var.dft_energy_tol is not None:  # read in configs
        print()
        outf.printt(1, "  Extracting positions from uncorrelated snapshots from " + var.DFToutput[1] + " for:")
        for dirIndex, direc in enumerate(all_dirs):
            outf.printt(1, f"  Direc={direc} (correlation time: {corrTimeVsDir[dirIndex]})")
            subdirpath = os.path.join(pathUncorr, direc)
            DFTsetup.convertDFTtrajToSnapshots(subdirpath, corrTimeVsDir[dirIndex])
    else:
        # Move vasprun files to the root
        # outf.printt(1,'  Removing uncorrelated snapshots from '+var.DFToutput[1]+' for:')
        # for tempIndex,temp in enumerate(temperatures):
        #     outf.printt(1,'  Temp='+str(temp)+'K (correlation time: '+str(corrTimeVsTemp[tempIndex])+'):')
        #     subdirpath=pathUncorr+"/temp"+str(temp)+"/"
        #     DFTsetup.removeUncorrelatedConfigs(subdirpath,corrTimeVsTemp[tempIndex])
        DFTanalysis.fetchDFToutput( direcUncorr, pathUncorr, tempDirList, corrTimeVsDir )  # <-- we might want to remove this and just pick up vaspruns from temp#/ dirs (as we will do POSCARs where config_gen contains reduced)
        if var.stopAPD == True:
            return  # NaN detected in vasprun
        if var.dft_engine == "VASP":
            outf.printCitation("VASP")
        else:
            outf.printCitation("CASTEP")
        potSetup.generateFitdbse(
            pathUncorr, 1.0
        )  # Construct a fitdbse in preparation for testing set purposes in the potential optimization stage.

    return corrTimeVsDir


def copyVASPscriptAdjustINCAR(script, pathTo):
    # Copies VASP job submission script from run directory to the stage directory. Adjusts INCAR based on number of cores.

    shutil.copy2(var.dirpath + "/" + script, pathTo + "/script")

    # Look for line '#SBATCH -n 24' in script. The number is the number processors.
    ncores = 0
    scriptOpen = open(var.dirpath + "/" + script)
    while True:
        lineReadin = scriptOpen.readline()
        if not lineReadin:
            break
        if "#SBATCH -n" in lineReadin:
            numbersFromLine = re.findall(r"[-+]?\d*\.\d+|\d+", lineReadin)
            if len(numbersFromLine) != 1:
                outf.printt(
                    2, "  Number of values in #SBATCH line in " + script + " not equal to 1. Not altering INCAR"
                )
                break
            ncores = int(numbersFromLine[0])
            outf.printt(2, "  Number of cores identified from " + script + ", ncores=" + str(ncores))
    scriptOpen.close()

    if var.dft_engine == "VASP":
        # Write to INCAR if ncores was found in the VASP submission script
        outf.printt(2, "  Adding appropriate NPAR flag to INCAR file...")
        if ncores > 0:
            npar = int(np.sqrt(ncores))
            # It ncores/npar must yield an integer
            while True:
                if (ncores / npar).is_integer():
                    break
                npar = npar + 1
                if npar >= ncores:
                    outf.printt(2, "  Unable to find a suitable value for NPAR in INCAR. Please do this yourself")
                    return
            outf.printt(2, "  Adding NPAR = " + str(npar) + " to INCAR")
            outf.printt(2, "  (First remove existing NPAR if present)")
            with open(pathTo + "/INCAR", "r") as INCARopen:
                lines = INCARopen.readlines()
            with open(pathTo + "/INCAR", "w") as INCARopen:
                for line in lines:
                    if "NPAR" not in line:
                        INCARopen.write(line)
            with open(pathTo + "/INCAR", "a") as INCARopen:
                INCARopen.write("NPAR = " + str(npar))
        else:
            outf.printt(
                2,
                "  Cannot find number of cores in "
                + script
                + "- not adding NPAR line to INCAR. Please do this yourself",
            )

    # CASTEP handles parallelization automatically

    return
