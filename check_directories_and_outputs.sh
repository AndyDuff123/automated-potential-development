#!/bin/bash

check_diff() {
  local file1="$1"
  local file2="$2"
  local sub_path="$3"
  if [[ ! -e $file1 || ! -e $file2 ]]; then
    echo "      Missing file in comparison: '$sub_path'."
    echo "      ($file1: $( [[ -e $file1 ]] && echo 'exists' || echo 'missing'), $file2: $( [[ -e $file2 ]] && echo 'exists' || echo 'missing' ))"
    result=1
  else
    if [[ "${file1: -5}" == ".cell" ]] || [[ "${file1: -6}" == ".param" ]]; then
      # echo "      (ignoring first 5 lines since ASE-generated .cell or .param file)"
      # .cell and .param files produced by APD are generated by ASE, and have preamble explaining this
      # that contains things like directories etc that will differ
      sed 's/\r$//' "$file1" > temp1.cell
      sed 's/\r$//' "$file2" > temp2.cell
      # Numbers that are just zero with a - at the front are equivalent to if there is a + at front
      # (the following also includes the option to round numbers down to 5 d.p (castep is 6 d.p)
      # so we can ignore differences in the last number - might be useful. change .6f to .5f and
      # remove a trailing zero from the two instances of 0.000000 and -0.000000)
      awk '{
        for(i=1;i<=NF;i++) {
          if ($i ~ /^-?[0-9.]+$/) {
            $i = sprintf("%.6f", $i);
            if ($i == "-0.000000") $i = "0.000000";
          }
        }
      } 1' temp1.cell > temp1B.cell
      awk '{
        for(i=1;i<=NF;i++) {
          if ($i ~ /^-?[0-9.]+$/) {
            $i = sprintf("%.6f", $i);
            if ($i == "-0.000000") $i = "0.000000";
          }
        }
      } 1' temp2.cell > temp2B.cell
      diff --ignore-trailing-space -q <(tail -n +5 temp1B.cell) <(tail -n +5 temp2B.cell) && echo "      '$sub_path' file correct" || { echo "      '$sub_path' file incorrect"; result=1; }
    else
      # add the above sed's and trailing... to the below, if it works
      # echo "      (considering full file in comparison)"
      diff --ignore-trailing-space -q "$file1" "$file2" && echo "      '$sub_path' file correct" || { echo "      '$sub_path' file incorrect"; result=1; }
    fi
  fi
}

check_APD_outputs() {
  local base_path="$1"
  shift  # Remove first argument
  local files=("$@")  # Remaining arguments are stored as an array
  
  echo "   Checking APD-generated output files in: ${base_path} ..."
  
  for sub_path in "${files[@]}"; do
    check_diff "../correctOutput/${base_path}/${sub_path}" "${base_path}/${sub_path}" "$sub_path"
  done
}

check_ext_code_inputs() {
  local DFTengine="$1"
  local base_path="$2"
  local script_type="$3"

  if [[ "$DFTengine" == "CASTEP" ]]; then
    file_exts=("castep.cell" "castep.param") # "phonopy_disp.yaml" "supercell.cell"
    if [[ "$script_type" == "singleShot" ]]; then
      script_name="script_CASTEP_singleShot"
    fi
  elif [[ "$DFTengine" == "VASP" ]]; then
    file_exts=("POSCAR" "INCAR" "KPOINTS")
    if [[ "$script_type" == "singleShot" ]]; then
      script_name="script_VASP_singleShot"
    fi
  else
    echo "Unknown DFT engine: $DFTengine"
    exit 1
  fi

  echo "   Checking APD-generated DFT input files in: "${base_path}

  # Check files in base_path
  for file_ext in "${file_exts[@]}"; do
    file="${base_path}/${file_ext}"
    # echo "checking file: "$file
    if [ -f "$file" ]; then
      check_diff "../correctOutput/${file}" "${file}" "${file}"
    fi
  done

  # Check files in subdirectories of base_path
  for dir in ${base_path}*; do
    if [ -d "$dir" ]; then
      for file_ext in "${file_exts[@]}"; do
        sub_path="${dir}/${file_ext}"
        # echo "sub_path="$sub_path
        if [ -f "$sub_path" ]; then
          check_diff "../correctOutput/${sub_path}" "${sub_path}" "${sub_path}"
        fi
      done
      sub_path="$dir/script"
      if [ -f "$sub_path" ]; then
        check_diff "$script_name" "${sub_path}" "${sub_path}"
      else
        echo "Error: Script $sub_path not found in correctOutput directory."
        exit 1
      fi
    fi
  done
}


i=$1
system=$2
DFTengine=$3
mp_id=$4
dft_energy_tol_check=$5
# echo "DFT engine: "$DFTengine
result=0  # Initialize result as 0 (pass)

# basic checks for all stages/sub-stages:
echo "   Location: tutorial/$system/unitTesting/"
check_diff "../correctOutput/toActionOutput/${i}_toAction.APD" "toAction.APD" "toAction.APD"

case $i in
  0)
    check_diff "script_genCastepKeyword" "script" "script"
    ;;
  1)
    check_ext_code_inputs $DFTengine "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/highElecParas/" "singleShot"
    ;;
  2)
    if [ "$dft_energy_tol_check" == "true" ]; then
      # Check output of highElecPara test. We don't normally have to deal directly with the filenames used by different DFT codes.
      # (E.g. 'check_ext_code_inputs' does it automatically using 'DFTengine'.)
      if [[ "$DFTengine" == "VASP" ]]; then
         check_APD_outputs "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/highElecParas/" "POSCAR" "INCAR" "KPOINTS" "stageOutput"
      else
         check_APD_outputs "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/highElecParas/" "castep.cell" "castep.param" "stageOutput"
      fi
    fi
    check_ext_code_inputs $DFTengine "APDworkdir/DFT/pbe/${mp_id}/1_input/" "singleShot" # checks castep.cell
    check_ext_code_inputs $DFTengine "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/geomOpt/" "singleShot" # checks castep.cell, castep.param
    ;;
  3)
    base_path="APDworkdir/DFT/pbe/${mp_id}/2_nonMD/geomOpt/"
    # Check if there are subdirs == grid-based geom opt
    if [ -n "$(find "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/geomOpt/" -mindepth 1 -maxdepth 1 -type d)" ]; then
      # Check output from geometry relaxation analysis
      if [[ "$DFTengine" == "VASP" ]]; then
         file_rlx="castep_rlx.cell"
      else
         file_rlx="POSCAR_rlx"
      fi
      check_APD_outputs "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/geomOpt/${file_rlx}stageOutput"

      # total energy - check APD outputs from setting up total energy DFT calculation
    else
      # Check APD outputs from setting up phonon DFT calculations
      base_path="APDworkdir/DFT/pbe/${mp_id}/2_nonMD/phonon/"
      check_APD_outputs ${base_path} "phonopy_disp.yaml" "supercell.cell"
      check_ext_code_inputs $DFTengine ${base_path} "singleShot" # checks castep.cell and .param in / and disp-001/ etc
    fi
    ;;
  4)
    # total energy - check APD outputs
    check_ext_code_inputs $DFTengine "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/energy" "singleShot"
    # Check APD outputs from setting up phonon DFT calculations
    base_path="APDworkdir/DFT/pbe/${mp_id}/2_nonMD/phonon/"
    check_APD_outputs ${base_path} "phonopy_disp.yaml" "supercell.cell"
    check_ext_code_inputs $DFTengine ${base_path} "singleShot" # checks castep.cell and .param in / and disp-001/ etc
    ;;
  5)
    # Phonon results are computed within APD (thru phonopy) rather than external codes, so can check these:
    check_APD_outputs "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/phonon/" "FORCE_CONSTANTS" "FORCE_SETS" "phonopy.yaml" "stageOutput"
    # Check APD outputs from setting up elastic DFT calculations
    base_path="APDworkdir/DFT/pbe/${mp_id}/2_nonMD/elastic/"
    check_ext_code_inputs $DFTengine ${base_path} "singleShot" # .cell, .param from / and cell1__1/ etc
    ;;
  6)
    # Check output of running elastic script on CASTEP calculations
    check_APD_outputs "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/elastic/" "elastic.output" # need to make this conditional on dftengine
    # Check APD-generated inputs for EvsV calculations
    # (some of the .cell's have coordinates which may differ in the last digit. might need to add feature to ignore such small numerical differences)
    base_path="APDworkdir/DFT/pbe/${mp_id}/2_nonMD/EvsV/"
    check_ext_code_inputs $DFTengine ${base_path} "singleShot" # .cell, .param from / and VoverN14.63/ etc
    ;;
  7)
    check_APD_outputs "APDworkdir/DFT/pbe/${mp_id}/2_nonMD/EvsV/" "castep_V14.63.castep" "castep_V14.73.castep" "castep_V14.83.castep" "castep_V14.93.castep" "castep_V15.03.castep" "castep_V15.13.castep" "castep_V15.23.castep" "castep_V15.33.castep" "castep_V15.43.castep" "castep_V15.53.castep" "castep_V15.63.castep" "stageOutput"
    ;;
  *)
    # Optional: Default operation for all other values of i
    ;;
esac

exit $result  # Exit with the status set in result

