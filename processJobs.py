# Automated Potential Development (APD) software
#
# Copyright (C) 2020-2023, Dr Andrew Ian Duff,
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import calendar
import copy
import datetime
import glob
import os

import outf
import re
import subprocess
import var

import DFTanalysis

def checkIfRunning(outputFile, direcs):

    # Determine if a job is no longer running on the cluster using two criterion: i) that the outputFile was last
    # updated a period of time greater than a particular threshold ago; ii) the job is not present in the SLURM queue
    # Note that we have to check the deepest lying cont/ directory.

    # Set up for queue-based approach
    outf.printt(1, "  Checking directories of jobs in SLURM queue to determine which are and are not running")
    remote_settings_file = os.path.join(var.dirpath, 'remoteComputerSettings')
    if os.path.isfile(remote_settings_file):
       outf.printt(1, "   remoteComputerSettings file present: will run jobDirs.sh remotely")
       # Parse the `remoteComputerSettings` file
       remote_info = {}
       with open(remote_settings_file, 'r') as f:
           for line in f:
               key, value = line.strip().split(' = ')
               remote_info[key.strip()] = value.strip()

       # Extract remote information
       remote_user = remote_info.get('REMOTE_USER')
       remote_host = remote_info.get('REMOTE_ADDRS')
       remote_dir = remote_info.get('REMOTE_DIR')

       ssh_command = [
           "ssh", f"{remote_user}@{remote_host}",
           f"source ~/.bashrc && cd {remote_dir} && $APDdir/tools/bash/jobDirs.sh"
       ]
       
       outf.printt(2, f"  Running: $APDdir/tools/bash/jobDirs.sh on {remote_host} in {remote_dir} (waiting for completion)")

       # Execute the command on the remote machine synchronously
       jobDirsOutput = subprocess.run(ssh_command, check=True, capture_output=True, text=True)
       jobDirsInQueueOrRunning = [
          re.sub(r'^   WorkDir=', '', line) for line in re.split(r'[\r\n]+', jobDirsOutput.stdout.strip())
          if re.search(r'/APDworkdir/', line)  # Ensure we are only picking valid paths, adjust regex as needed
       ]

       # Print the output for remote execution
       outf.printt(1, "  Remote command executed successfully:")
       outf.printt(1, f"  Output: {jobDirsInQueueOrRunning}")

    else: 

       jobDirs_script = os.path.join(os.environ['APDdir'], "tools/bash/jobDirs.sh")
       outf.printt(2, f"  Running: {jobDirs_script} in {var.dirpath}")
       jobDirsOutput = subprocess.run([jobDirs_script], check=True, capture_output=True, text=True, cwd=var.dirpath)
       jobDirsInQueueOrRunning = [re.sub(r'^   WorkDir=', '', line) for line in re.split('[\r\n]+', 
               jobDirsOutput.stdout.strip())]
       #print('jobDirsInQueueOrRunning : ',jobDirsInQueueOrRunning)

    # Set up for time-based approach
    timeCriterion = (10 + var.timestamp_leeway) * 60  # 5*60
    date = datetime.datetime.utcnow()
    utc_time = calendar.timegm(date.utctimetuple())
    outf.printt(1, "  Checking timestamps of " + str(outputFile) + " files to see which jobs are most likely no "
            "longer running:")
    outf.printt(2, "    (utc_time=" + str(utc_time) + ")")

    notRunningJobs = []
    notRunningJobsQueueApproach = []
    notRunningJobsTimeApproach = []
    for direc in direcs:

        if direc != "/":
            outf.printt(2, "    direc: " + direc)
            path = os.path.join(var.dirpath, str(direc))
        else:
            outf.printt(2, "    direc: <current directory>")
            path = var.dirpath
        # Ensure dir and path correspond to deepest cont/ directory
        while True:
            # print("os.path.isdir(os.path.join(path,'cont')) = ",os.path.isdir(os.path.join(path,'cont')))
            if os.path.isdir(os.path.join(path, "cont")):
                direc = direc + "/cont"
                path = path + "/cont"
            else:
                break
        # os.system('ls -la '+str(path)+'/'+str(var.DFToutput[0]))

        outf.printt(2, "    checking for file:" + str(path) + "/" + str(outputFile))
        if os.path.isfile(path + "/" + str(outputFile)):

            # Queue-based approach
            normalized_path = re.sub(r'/+', '/', path).rstrip('/') # need to removing trailing slashes as scontrol (slurm
                    # directive) does not output directories with a slash at the end

            if os.path.isfile(remote_settings_file):
               # We remove everything before APDworkdir/ (and the directory before that) from the path, because ADP.py
               # is running on the local machine so directories will have different preamble to on the remote
               match = re.search(r'([^/]+/APDworkdir/.*)', path)
               if match:
                   normalized_path = match.group(1)
               else:
                   print(f'ERROR: path {path} does not contain APDworkdir?, STOPPING')
                   quit()
               normalized_path = re.sub(r'/+', '/', normalized_path).rstrip('/') # make sure no trailing slashes introduced
               print('   path after removing preamble... : ',normalized_path)
               is_in_queue_or_running = any(normalized_path in job_path for job_path in jobDirsInQueueOrRunning)
            else:
               # In this case can keep full path as both jobDirs script and survey of directories was done on same machine
               print('is ',normalized_path,' in jobDirsInQueueOrRunning list?:')
               is_in_queue_or_running = normalized_path in jobDirsInQueueOrRunning
            if is_in_queue_or_running:
                outf.printt(2, "      Job in queue or running")
            else:
                outf.printt(2, "      Job not in queue or running - adding to notRunningJobsQueueApproach list")
                notRunningJobsQueueApproach.append(direc)


            # Time-based approach
            time = os.path.getmtime(path + "/" + str(outputFile))
            timeSinceModified = utc_time - time
            outf.printt(2, "    " + direc + "/" + str(outputFile) + " last modified " + str(timeSinceModified) + 
                    "s ago")
            if timeSinceModified > timeCriterion:
                outf.printt(2, "      ...older than " + str(timeCriterion) + ", adding to notRunningJobsTimeApproach "
                    "list")
                notRunningJobsTimeApproach.append(direc)
            else:
                outf.printt(2, "      ...not older than " + str(timeCriterion) + " - assuming it is still running")

        else:

            outf.printt(2, "    ..." + str(outputFile) + " file not present in " + direc + "/")
            print("WARNING: normally this should not happen (not started jobs should be handled by the: if "
                    f"{len(var.notStartedJobs)>0} conditional in the actionIncompleteDFTJobs subroutine. "
                    "An exception are jobs where the file is only written on completion of job (e.g. MTP test)")

            # Queue-based approach
            normalized_path = re.sub(r'/+', '/', path).rstrip('/')
            print('is ',normalized_path,' in jobDirsInQueueOrRunning list?:')
            if normalized_path in jobDirsInQueueOrRunning: # need to removing trailing slashes as scontrol (slurm
                    # directive) does not output directories with a slash at the end
                outf.printt(2, "      Job in queue or running")
            else:
                outf.printt(2, "      Job not in queue or running - adding to notRunningJobsQueueApproach list")
                notRunningJobsQueueApproach.append(direc)

    # Following warning probably isn't necessary now since we know the time-based approach sometimes will not work
    #if notRunningJobsTimeApproach != notRunningJobsQueueApproach:
    #    print('!!!!!!!! WARNING: time-based and queue-based approach to checking if jobs are running disagree !!!!!!!!')
    notRunningJobs = copy.deepcopy(notRunningJobsQueueApproach)

    if var.verbosity == 1:
        if notRunningJobs == "None":
            outf.printt(1, "    All jobs seem to be still running")
        else:
            outf.printt(1, "    Jobs that are stale and assumed no longer running: " + str(notRunningJobs))

    return notRunningJobs


def runOrStageJobs(direc, jobdirsLocal=""):
    # Based on the stage we either search recursively up the directory tree to find job directories, or in the case e.g. of stage=3 it is more useful to pass the
    # required jobdirs direct to this function, since there may be some subdirs for which jobs are already running (or have been submitted), and this avoids having
    # to code logic, e.g. based on timestamp, to see which directories are 'new'
    if var.norun == True:
        outf.toAction(
            "ERROR: jobs set up in "
            + direc
            + "/, but manual changes required - please edit input files and then submit jobs or restart APD"
        )
    else:
        dirDepth2 = "" # If this is set below then jobs will also be sought from this directory
        if var.stage == 0:
            outf.toAction("Please submit script in current directory")
        elif var.stage == 2:
            if direc == var.DFTnonMDhighElecParasdir:
                outf.toAction(
                    "Please run DFT in " + direc + "/"
                )  # e.g. APDworkdir/DFT/pbe/mp-22862/2_highElecParas/{encut#,kpnt#}
                dirDepth = "*/*/"
            elif direc == var.DFTnonMDgeomOptdir:
                outf.toAction("Please run DFT job in " + direc + "/")
                if DFTanalysis.geom_opt_used_grid():
                    dirDepth = "*/*/" # In this case we have directories -6.52_6.25 etc (grid based opt)
                else:
                    dirDepth = "" # In this case using the DFT codes inbuilt geom opt
            elif direc == var.DFTnonMDenergydir:
                outf.toAction("Please run DFT job in " + direc + "/")
                dirDepth = "*/"
            elif direc == var.DFTnonMDphonondir:
                outf.toAction("Please run DFT job in " + direc + "/")
                print('DFTanalysis.frozen_phonon() = ',DFTanalysis.frozen_phonon())
                if DFTanalysis.frozen_phonon():
                    dirDepth = "*/*/"  # disp-001, etc
                else:
                    stageOutput_path = os.path.join(var.DFTnonMDphononpath, "stageOutput")
                    print('stageOutput_path=',stageOutput_path)
                    if 'Dispersion job submitted.' not in open(stageOutput_path).read():
                        dirDepth = "" # DFPT initial job
                        print(' "Dispersion job submitted." is not present in the stageOutput!')
                    else:
                        print(' "Dispersion job submitted." ___is___ present in the stageOutput!')
                        dirDepth = "/*Rule*/" # DFPT post-processing interpolation job
            elif direc == var.DFTnonMDelasticdir:
                outf.toAction("Please run DFT job in " + direc + "/")
                if var.dft_engine == "VASP":
                    dirDepth = "*/"  # VASP does it with a single job
                elif var.dft_engine == "CASTEP":
                    dirDepth = "*/*/"  # cell-1__1, etc
                    if DFTanalysis.geom_opt_used_grid():
                        dirDepth2 = "/" # we need perfect cell calc also, which we won't have using grid method
            elif direc == var.DFTnonMDevdir:
                outf.toAction("Please run DFT job in " + direc + "/")
                dirDepth = "*/*/"  #  e-v
        elif var.stage == 3:
            outf.toAction(
                "Please run DFT in " + direc + "/"
            )  # e.g. APDworkdir/DFT/pbe/mp-22862/3_MDparas/temp2000/kpoints1x1x1/encut300/timestep10/gamma10/ediff0.12/castep.param, so 5 levels
            dirDepth = "*/*/*/*/"
        elif var.stage == 4:
            outf.toAction(
                "Please run DFT in " + direc + "/"
            )  # e.g. APDworkdir/DFT/pbe/mp-22862/4_configs/DFTNPT/temp1000/{bulk,V_Nb__1,etc}, so 2 level ( */* )
            dirDepth = "/*/*/"
        elif var.stage == 5:
            outf.toAction(
                "Please run DFT in " + direc + "/"
            )  # e.g. APDworkdir/DFT/pbe/mp-22862/5_trainingData/{initial,bad}/temp#/config#, so 3rd level
            dirDepth = "/*/*/*/"
        elif var.stage == 6:
            outf.toAction("Please run potential fitting jobs in " + direc + "/") # e.g. APDworkdir/pot/pbe/EAM1/
                # 6_potOpt/, so 0 levels
            dirDepth = ""
        elif var.stage == 7:
            outf.toAction("Please run potential testing jobs in " + direc + "/")
            dirDepth = ""
        elif var.stage == 8:
            if direc == var.potMDsimuldir:
                if os.path.isfile(var.potMDsimulpath + "/stageFailed"):
                    outf.toAction("Please run mlip job in " + direc + "/")
                    dirDepth = ""
                else:
                    outf.toAction("Please run potential MD jobs in " + direc + "/")
                    dirDepth = "/*/"
            else:
                outf.toAction("Please run DFT jobs in " + direc + "/")
                dirDepth = "/*/"
        elif var.stage == 9:
            if direc == var.potMDnonMDgeomOptdir:
                outf.toAction("Please run potential MD jobs in " + direc + "/")
                dirDepth = ""
            elif direc == var.potMDnonMDphonondir:
                outf.toAction("Please run potential jobs in " + direc + "/")
                dirDepth = ""
            elif direc == var.potMDnonMDelasticdir:
                outf.toAction("Please run potential jobs in " + direc + "/")
                dirDepth = ""
            elif direc == var.potMDnonMDevdir:
                outf.toAction("Please run potential jobs in " + direc + "/")
                dirDepth = "/*/"

    if jobdirsLocal == "":
        os.chdir(var.dirpath)
        print('directories to set up jobs. looking in direc + dirDepth = ',direc,', ',dirDepth)
        dirsRecursiveList = list(filter(lambda f: os.path.isdir(f), glob.glob(direc + dirDepth)))
        print('dirsRecursiveList=',dirsRecursiveList)
        if not dirsRecursiveList:
           print('ERROR: directory list empty when checking for jobs which need processing - shouldnt happen, STOPPING')
           quit()
        for direc in dirsRecursiveList:
           var.jobdirs.write(direc + "\n")  # jobdirs is a file object, so here we write direct to file
           print('added ',direc,' to jobdirs')
        if dirDepth2 != "":
           dirsRecursiveList = list(filter(lambda f: os.path.isdir(f), glob.glob(direc + dirDepth2)))
           for direc in dirsRecursiveList:
               var.jobdirs.write(direc + "\n")
               print('added ',direc,' to jobdirs')
    else:
        print(" jobdirslocal=", jobdirsLocal)
        for direc in jobdirsLocal:
            var.jobdirs.write(direc + "\n")
            print('added ',direc,' to jobdirs')


    var.stopAPD = True


def runMEAMfit(pathToRun, overrideVerbosity):

    # For verbosity=1 we wouldn't normally print output of MEAMfit, but to override this call with overrideVerbosity=True

    if var.meamfit_so: import meamfit_py

    if var.verbosity == 2 or (var.verbosity == 1 and overrideVerbosity == True):
        print(" /")
        print("|")
        print("")
    # meamfit_params = meamfit_py.read_params_py()
    if var.meamfit_so:
        meamfitrun = meamfit_py.meamfit(0, 0, 0)
    else:
        print('trying to run at :',pathToRun)
        current_directory = os.getcwd()
        print(current_directory)
        try:
            # On SCARF we used: subprocess.run("MEAMfit", shell=True, check=True, capture_output=True, text=True, cwd=pathToRun)
            # however for the Scafellpike cluster this alias wasn't recognized. We also tried:
            # result = subprocess.run(['/bin/bash', '-i', '-c', 'MEAMfit'],
            #            check=True, capture_output=True, text=True, cwd=pathToRun)
            # however that causes APD to hang (go into background) on the second call to MEAMfit.
            # In the end used var.meamfit_path which is 'MEAMfit' by default but can be used to specify the full path to
            # MEAMfit
            print('running using: ',var.meamfit_path)
            result = subprocess.run(var.meamfit_path,shell=True, check=True, capture_output=True, text=True, cwd=pathToRun)
            # Note: if this doesn't work on SCARF may need to have an explicit 'if var.meamfit_path == "MEAMfit':' then
            # do the first subprocess call mentioned above explicitly...
            print("MEAMfit executed successfully.")
            print("Standard Output:\n", result.stdout)
            print("Error Output (if any):\n", result.stderr)
        
        except subprocess.CalledProcessError as e:
            # If MEAMfit fails, catch the error and print out useful info
            print("MEAMfit execution failed.")
            print("Command:", e.cmd)
            print("Return Code:", e.returncode)
            print("Standard Output:\n", e.stdout)
            print("Error Output:\n", e.stderr)
        
        except Exception as e:
            # Catch any other unexpected exceptions
            outf.printt(0, f"  MEAMfit execution using subprocess failed with an unexpected error: {str(e)}")
            quit()	

    outf.printCitation("MEAMfit")
    # meamfit_py.end_mpi()
    if var.verbosity == 2 or (var.verbosity == 1 and overrideVerbosity == True):
        print("")
        print("|")
        print(" \\")
        print("    ...completed.")


def runExternal(string):
    outf.printt(2, " /")
    outf.printt(2, "|")
    outf.printt(2, "")
    os.system(string)
    outf.printt(2, "")
    outf.printt(2, "|")
    outf.printt(2, " \\")
    outf.printt(2, "    ...completed.")


def endStage():
    if len(var.xcFunc) == 1 and len(var.materials) == 1:
        oneXcMat = True
    else:
        oneXcMat = False

    if var.stage == 3:
        outf.toAction(
            "Please check outputs generated in "
            + var.DFTMDparasdir
            + " (stageOutput file, VASP input files, /temp#/thermalExpansion.dat files) if necessary. Re-run APD to continue DFT processing of this xc/mat"
        )
        if oneXcMat == False:
            print("  Looping to next xc/mat")
        var.stopAPD = True
    elif var.stage == 4:
        outf.toAction(
            "Please check outputs generated in "
            + var.DFTconfigsdir
            + " if necessary (e.g. fitdbse file summarizing configurational data). Re-run APD to begin potential optimization (once all materials for this xc-func have been computed using DFT)"
        )
        if oneXcMat == False:
            print("  Automation=1: looping to next xc/mat")
        var.stopAPD = True
    # elif var.stage == 6:
    #    if oneXcMat==False: print('  Looping to next xc/mat')
    #    var.stopAPD=True


def meamfitclean():
    filesToRemove = [
        "checkfiles.tmp",
        "fitted_quantities.out",
        "sepnHistogram.out",
        "vasprun*",
        "forces",
        "idsforsmallestsepns",
        "largestrho.dat",
        "smallestsepnvsstruc.dat",
    ]
    for file in filesToRemove:
        if os.path.exists(file):
            os.remove(file)
