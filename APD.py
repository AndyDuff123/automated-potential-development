# Automated Potential Development (APD) software
#
# Copyright (C) 2020-2023, Dr Andrew Ian Duff .
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import glob
import math
import os
import re
import shutil
from itertools import groupby
from math import ceil, floor
import numpy as np

# Modules
import DFTanalysis
import DFTsetup
import filesys
import initialize
import outf
import potAnalysis
import potMDanalysis
import potMDsetup
import potSetup
import processJobs
import pymatgen.io.ase as aio

# Variable containers
import var

__version__ = "0.2"

def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)


def checkGenCastepKeywords():
    # genCastepKeyword.py must be run once to generate a castep_keywords.json file if one is not already present in the run directory.
    # We run this script on a compute node, and it returns information in the genCastepKeyword.err file on where to copy this file to be able to run
    # ase/castep from any location. However I think this location is different to that which is accessed on the node from where the job is submitted because
    # it didn't work for me. Instead we keep the castep_keywords file always in the directory where ase/castep is being run. In many cases this is the job
    # directory itself, but sometimes, e.g. if we chdir to some inner directory, we need to copy the keyword file over as well if we are running ase/castep
    # there.
    #
    # Here we check:
    #   i) no genCastepKeyword.log file : set up a genCastepKeyword job to be run to do the above
    #   ii) genCastepKeyword.log file present but without 'completed...' in it : check if job is still running and suggest resubmiting if not
    #   iii) genCastepKeyword.log file present _with_ 'completed...', and either:
    #        a) castep_keywords.json file present - stage completed
    #        b) no castep_keywords.json file present - warn user that this stage failed. It may be that user already has the keywords file in a place
    #           accessible to ase, in which case to progress ask user to add 'generate_castep_keywords=False' to settings to continue (True by default)

    if os.path.isfile("castep_keywords.json") == True or (
        os.path.isfile("castep_keywords.json") == False and var.generate_castep_keywords == False
    ):
        outf.printt(1, "castep_keywords.json present (or else not required), continuing with workflow")
        var.stage = 1
        return

    outf.printt(1, "CASTEP as DFT engine but castep_keywords.json file not present")
    if os.path.isfile("genCastepKeyword.log") == False:
        outf.printt(1, "Preparing job to generate this file...")
        if os.path.isfile("script_genCastepKeyword") == False:
            outf.printt(
                0,
                "ERROR: script_genCastepKeyword not present in calculation directory (required to submit job to generate castep_keywords.json file), STOPPING",
            )
        else:
            os.system("cp script_genCastepKeyword script")
            var.norun = False
            processJobs.runOrStageJobs("", ["/"])  # second index to denote just current directory
        var.stopAPD = True
    else:
        with open("genCastepKeyword.log") as myfile:
            if "...completed" not in myfile.read():
                var.incompleteJobs = []
                var.incompleteJobs.append("/")
                var.notStartedJobs = []
                actionIncompleteJobs("genCastepKeyword.log", False)  # Check if any jobs need resubmitting
                var.stopAPD = True
            else:
                if os.path.isfile("castep_keywords.json") == True:
                    # DEPRECATED: for a while i tried copying the keywords file to a centralized location, but the location in the .err file is seemingly not appropriate. see commented out lines below
                    outf.printt(1, "castep_keywords.json present, continuing with workflow")
                    var.stage = 1
                else:
                    outf.printt(
                        1,
                        "job to generate castep_keywords.json failed, STOPPING. To run without this file (e.g. if you already have the file in an accessible location) set generate_castep_keywords=False",
                    )
                    var.stopAPD = True

def check_directories(output_file, setupContJob):

    # Stage new jobs if incomplete, or signal to exit if failed jobs

    if len(var.failedJobs) > 0:
        outf.printt(1, "  Failed jobs present, cannot continue")
        var.stopAPD = True
    if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
        actionIncompleteJobs(output_file, setupContJob) # Check if any jobs need resubmitting (and/or 'cont' directories set up)
    return


def DFTnonMD(xc, mat):

    # Optimizes DFT input parameters for 'high' calculations and conducts non-MD DFT validations.
    #
    # Note: while I just have placeholders below for the optimization of these parameters, they will be setup and launched, with no 'stopAPD=.true.' specified.
    # I.e we will proceed straight to stage 3_DFTMDparas. Only after 4_configs has finished do we demand that stage 2 is finished, I.e. DFT inputs are present
    # in 2_highElecParas/. And then we only demand it if config_gen='DFT-N{P,V}T-reduced.
    #
    # Note high elec paras determination should not be performed for each material but only for one xc (first xc encountered). The stage should be within
    # APDworkdir/DFT/pbe/mp-3834/2_highElecParas (not APDworkdir/DFT/), but whenever this function is called it first searches if the stage has already completed for
    # another xc of this material, if so it copies the VASP inputs to the current 2_highElecParas. Once we get to 5_training we take the highest paras determined
    # across the different materials to perform the high DFT (for consistency)

    # None of the calcs in this stage should be rerun as continuation jobs if they fail. For the phonons, and elastic calculations
    # this is obvious (no ionic movement) but also for geometry it is simpler to just rerun. Only CASTEP requires a particular setting
    if var.dft_engine == "CASTEP":
        var.calc.param.continuation.clear()  # no equivalent for VASP so apply directly to var.calc.param. Note: DFT jobs in 5_ will also not be continuation jobs (either 'high' jobs or else relabelling of bad DFT). Since these jobs will use the parameters saved in 2_, this setting is appropriate for this reason as well.

    # High electronic parameter determination
    if var.dft_energy_tol == None: # can skip this sub-stage
        outf.printt(
            1,
            "  dft_energy_tol=None: skipping 2_nonMDhighElecParas sub-stage (to determine electronic DFT parameters for 'high' DFT calculations)",
        )
        # In this case the 'high' electronic parameters are just those based on MPRelaxSet
        # print('var.isDFThighElecParaspath=',var.isDFThighElecParaspath)
        if var.isDFTnonMDpath == False:
            filesys.createWorkdir(var.DFTnonMDdir)
            filesys.createWorkdir(var.DFTnonMDhighElecParasdir)
            with open(var.DFTnonMDhighElecParaspath + "/stageOutput", "w") as directoriesFile:
                directoriesFile.write("High electronic parameter optimization not necessary, skipping.\n")
                directoriesFile.write(
                    "1\n"
                )  # Multiplication factor for kpoints (the factor we apply to kpoints generated by MPRelax for a given structure. This allows
                   # MP to do the hardlifting in terms of symmetry of cell while we juts apply the factor afterwards)
            DFTsetup.writeDFTinputFiles(
                var.DFTnonMDhighElecParasdir, var.DFTnonMDhighElecParaspath, var.DFTinputpath
            )  # Still save the DFT input files again in stage 2 root as these will be
            # refered to later (stage 5) for all 'high' calculations
            # WARNING: line above hasn't been tested (last time I copied the files by hand)
    # ...otherwise:
    else:
        if var.isDFTnonMDhighElecParaspath == False:
            DFTsetup.setupHighElecParadir()
            # Electronic parameter optimization set-up
            processJobs.runOrStageJobs(var.DFTnonMDhighElecParasdir, "")  # launch or instruct user to launch jobs
            if var.stopAPD == True:
                return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze
        # Electronic parameter optimization analysis
        if os.path.exists(var.DFTnonMDhighElecParaspath + "/stageOutput") == False:
           
            finisheddirs = DFTanalysis.checkDFToutput([var.DFTnonMDhighElecParasdir]) # Check if job is finished. If so
                    # add the dir to finisheddir
            if not finisheddirs:
                check_directories(var.DFToutput[0], False)
            if var.stopAPD == True:
                return
            converged_cutoff, converged_kpnts, kpnts_factor, error_value = \
                    DFTanalysis.postProcessHighElecParas(xc, mat)  # Determine cutoff and kpoints from calcs.
                            # error_value is returned if user-specified convergence criteria var.dft_energy_tol has not
                            # been met. E.g. depending on the precision specified, and the pseudopotentials being used,
                            # it may not be possible to achieve the specified precision w.r.t cutoff.
            outf.printt(2,'  Converged_cutoff, kpnts_factor ='+str(converged_cutoff)+', '+str(kpnts_factor))
            if not error_value:
                error_value = var.dft_energy_tol # In this case we achieved the accuracy specified in settings.APD.
            outf.printt(2,f'    ...Converged within +/- {error_value:.2f} meV/atom')

            # Write DFT input files with converged encut and kpoints in sub-stage directory
            var.incar["ENCUT"] = int(converged_cutoff)
            var.kpoints.kpts[0] = converged_kpnts
            if var.dft_engine == "CASTEP":
                DFTsetup.convertVASPtoCASTEP2()
                # For determining encut basis on energy optimization we had to adjust the delta E using to determine
                # the finite basis energy correction. (I think this is because we only consider the perfect structure
                # here, with relatively discreet energy eigenvalues, so the default +/-5 is insufficient to capture
                # new plane waves as we change encut, but i might be wrong.) In any case we can't use +/-10 for the
                # main calculations as it results in inaccurate stresses. Since we pick up the DFT inputs from here
                # in further stages be sure to remove it:
                var.calc.param.finite_basis_spacing = None
                # The following parameters were set before in DFTsetup.setupHighElecParadir however won't have been read 
                # in this time, and are needed to append correct keywords to end of .cell file
                var.incar["ISIF"] = 0
                var.incar.pop("ISYM", None) # Removing this results in VASP default value corresponding to use of symmetry (>0)
                var.incar["NSW"] = 0
            DFTsetup.writeDFTinputFiles(var.DFTnonMDhighElecParasdir, var.DFTnonMDhighElecParaspath, var.DFTinputpath)

            with open(var.DFTnonMDhighElecParaspath + "/stageOutput", "w") as directoriesFile:
                directoriesFile.write(f"High electronic parameter optimization complete. Converged within: "
                    f"{error_value:.2f} meV/atom / {error_value*var.structure.num_sites:.2f} meV/cell / "
                    f"{error_value*var.structure.num_sites/1000:.2f} eV/cell\n")
                directoriesFile.write(f"Cutoff: {converged_cutoff}\n")
                directoriesFile.write(f"Kpoints factor: {kpnts_factor}\n")
                directoriesFile.write("Further details to be used by later stages:\n")
                directoriesFile.write(f"Number of atoms: {var.structure.num_sites}\n")
                #subdirs = [d for d in os.listdir(var.DFTnonMDhighElecParaspath)] # old way. delete if new way works
                #if subdirs:
                try:
                    first_subdir = next(os.walk(var.DFTnonMDhighElecParaspath))[1][0] #subdirs[0]
                    subdir_path = os.path.join(var.DFTnonMDhighElecParaspath, first_subdir)
                    nElecs = DFTanalysis.nElecs(os.path.join(subdir_path, var.DFToutput[0]))
                except:
                    print('ERROR: no sub dirs present in high elec para opt? STOPPING')
                    quit()
                directoriesFile.write(f"Number of electrons: {nElecs}\n")
                directoriesFile.write("Number of electrons per atom: " + str(int(nElecs / var.structure.num_sites)) + "\n")

    if var.skip_0K_props == True:
        outf.printt(1,'  Skipping calculation of 0 K properties')
        return

    DFTsetup.setupDFTinput(
        var.DFTnonMDhighElecParasdir, var.DFTnonMDhighElecParaspath
    )  # to get the optimized DFT parameters
    # print('   after reading optimized paras, kpnts=',var.kpoints.kpts[0][0:3],', encut=',var.incar["ENCUT"]) # debug
    # quit() # debug

    if var.isDFTnonMDgeomOptpath == False:
        DFTsetup.setupGeomOptdir()
        # code to set up geom opt job then return
        # code to check on geom opt job. return with stopAPD=true if not completed (phonons & elastic constants need relaxed geometry)
        processJobs.runOrStageJobs(var.DFTnonMDgeomOptdir, "")  # launch or instruct user to launch jobs
        if var.stopAPD == True:
            return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze
    if os.path.exists(var.DFTnonMDgeomOptpath + "/stageOutput") == False:
       
        finisheddirs = DFTanalysis.checkDFToutput([var.DFTnonMDgeomOptdir], cont=True) # Check if job is finished. if
                # so add the dir to finisheddir. Note, True indicates we search for cont/ dirs within the directory
        if not finisheddirs:
            check_directories(var.DFToutput[0], True) # True: set up cont job if necessary
        if var.stopAPD == True:
            return
        DFTanalysis.postProcessGeom(xc, mat)  # Write DFT geometry information to file in outputdir
        with open(var.DFTnonMDgeomOptpath + "/stageOutput", "w") as directoriesFile:
           directoriesFile.write("Geometry optimization complete.\n")

    if var.isDFTnonMDenergypath == False:
        DFTsetup.setupEnergydir()
        if DFTanalysis.geom_opt_used_grid():
            processJobs.runOrStageJobs(var.DFTnonMDenergydir, "")  # launch or instruct user to launch jobs
            if var.stopAPD == True:
                return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze
    if os.path.exists(var.DFTnonMDenergypath + "/stageOutput") == False:
        print("var.DFTnonMDenergydir=", var.DFTnonMDenergydir)
        if DFTanalysis.geom_opt_used_grid():
            finisheddirs = DFTanalysis.checkDFToutput([var.DFTnonMDenergydir]) # Check if job is finished. If so add 
                    # the dir to finisheddirs
            if not finisheddirs:
                check_directories(var.DFToutput[0], False)
            if var.stopAPD == True:
                return
            with open(var.DFTnonMDenergypath + "/stageOutput", "w") as directoriesFile:
               directoriesFile.write("Energy calculation complete.\n")
        else:
            with open(var.DFTnonMDenergypath + "/stageOutput", "w") as directoriesFile:
               directoriesFile.write("Energy calculation copied.\n")

    if var.isDFTnonMDphononpath == False:
        if var.phonon_mode == "AUTO":
            # Determine whether to use DFPT or finite displacement based on DFT code/number displaced structures
            n_indep_disp = DFTsetup.setupPhonondir(True) # True = frozen-phonon mode
            if var.dft_engine == "VASP":
                finite_disp_mode = True # For VASP DFPT still needs a supercell for full dispersion, so no advangtage over FD
            elif var.dft_engine == "CASTEP":
                if n_indep_disp <= var.n_indep_disp_crossover:
                    finite_disp_mode = True
                else:
                    finite_disp_mode = False # For CASTEP do not need a supercell, so if too many displacements use DFPT
                    # Remove the disp#/ dirs
                    [shutil.rmtree(os.path.join(var.DFTnonMDphononpath, d)) 
                        for d in os.listdir(var.DFTnonMDphononpath)
                        if d.startswith('disp') and os.path.isdir(os.path.join(var.DFTnonMDphononpath, d))]
            # If we have established we should be using DFPT then be sure to remove directory (in a moment we will make
            # a new one for DFPT calculations) and also reset the atoms object, magmoms, etc
            if not finite_disp_mode:
               shutil.rmtree(var.DFTnonMDphononpath)
               DFTsetup.setupDFTinput(var.DFTnonMDhighElecParasdir, var.DFTnonMDhighElecParaspath)
        elif var.phonon_mode == "FINITE_DISP":
            print('Finite displacement mode')
            n_indep_disp = DFTsetup.setupPhonondir(True) # True = frozen-phonon mode
            finite_disp_mode = True
        else:
            finite_disp_mode = False

        print('not finite_disp_mode = ',not finite_disp_mode)
        if not finite_disp_mode:
            print('doing perturbation theory')
            n_indep_disp = DFTsetup.setupPhonondir(False) # perturbation theory
        with open(os.path.join(var.DFTnonMDphononpath, "stageOutput"), "w") as directoriesFile:
            directoriesFile.write("Number of independent displacements: "+str(n_indep_disp)+"\n")
            if finite_disp_mode:
                directoriesFile.write("Calculating phonons using finite displacement method\n")
            else:
                directoriesFile.write("Calculating phonons using perturbation theory\n")
        if var.skip_phonons==True:
            outf.printt(1,"  Skipping phonon calculations")
        else:
            processJobs.runOrStageJobs(var.DFTnonMDphonondir, "")  # launch or instruct user to launch jobs
            if var.stopAPD == True:
                return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze
    stageOutput_path = os.path.join(var.DFTnonMDphononpath, "stageOutput")
    if var.skip_phonons==True:
        outf.printt(1,"  Skipping phonon analysis")
    else:
        if 'complete' not in open(stageOutput_path).read():

            # Check if we are doing FD or DFPT calculations
            with open(os.path.join(var.DFTnonMDphononpath, "stageOutput"), "r") as directoriesFile:
               line = directoriesFile.readline()
               line = directoriesFile.readline()
               if 'finite displacement' in line:
                  finite_disp_mode = True
               else:
                  finite_disp_mode = False

            if finite_disp_mode or (not finite_disp_mode and 'Dispersion job submitted.' not in 
                    open(stageOutput_path).read()): # For DFPT we only need to check if the initial phonon jobs are
                            # complete only if the post-processing jobs have not been submitted
                # Check initial phonon calculations
                print("var.DFTnonMDphonondir=", var.DFTnonMDphonondir)
                if finite_disp_mode:
                    # var.DFTnonMDphonondir contains sub-dirs (disp#/); checkDFToutput will ascertain this and check
                    # they all contain finished jobs (if they do, finisheddirs = [var.DFTnonMDphonondir] is returned) 
                    finisheddirs = DFTanalysis.checkDFToutput([var.DFTnonMDphonondir])
                else:
                    # In this case a single job is checked as var.DFTnonMDphonondir contains no subdirs (again,
                    # returns finisheddirs = [var.DFTnonMDphonondir] if job issuccessful). In this case contJobs = True
                    # indicates that cont/ directories should be checked for when checking DFT outputs
                    finisheddirs = DFTanalysis.checkDFToutput([var.DFTnonMDphonondir], cont=True)  # Check if job is finished. if 
                        # so add the dir to finisheddirs. Either the job in var.DFTnonMDphonondir is checked (DFPT) or if
                        # subdirs are present (finite displacment method) the jobs in these are checked instead
                if not finisheddirs:
                    check_directories(var.DFToutput[0], not finite_disp_mode) # last index is for cont job. Yes if DFPT
                if var.stopAPD == True:
                    return

            # Proceed to analyze phonon calculation results
            if finite_disp_mode:
               DFTanalysis.postProcessPhonons(xc, mat, finite_disp_mode) # postprocess in case of FD
            if not finite_disp_mode:
                if 'Dispersion job submitted.' not in open(stageOutput_path).read():
                   # For DFPT we need to set up new jobs in subdirs with new parameters, so ensure we have first read in
                   # the parameters we used for the original phonon calcs:
                   DFTsetup.setupDFTinput(var.DFTnonMDphonondir, var.DFTnonMDphononpath)
                   DFTanalysis.postProcessPhonons(xc, mat, finite_disp_mode) # set up new jobs for DFPT
                   with open(var.DFTnonMDphononpath + "/stageOutput", "a") as directoriesFile:
                      directoriesFile.write("Dispersion job submitted.\n")
                   processJobs.runOrStageJobs(var.DFTnonMDphonondir, "")
                else:
                   DFTanalysis.checkPostProcessPhononJobs()
                   if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
                       actionIncompleteJobs( "dispersionJob.out", False ) # Check if jobs need resubmitting (no
                               # cont jobs (output file here just determines if files are backed up and behaviour for 
                               # cont jobs)
                       var.stopAPD = True
                   # also check if 'are unknown' is in the error_p.txt (which probably means the xmgrace module is not
                   # present) - if so we skip the post-processing step as it is required for outputing image files
                if var.stopAPD == True:
                    return
                # Code to post-process the dispersion output
            with open(var.DFTnonMDphononpath + "/stageOutput", "a") as directoriesFile:
               directoriesFile.write("Phonon calculation complete.\n")

    if var.isDFTnonMDelasticpath == False:
        DFTsetup.setupElasticdir()
        processJobs.runOrStageJobs(var.DFTnonMDelasticdir, "")  # launch or instruct user to launch jobs
        if var.stopAPD == True:
            return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze

    if os.path.exists(var.DFTnonMDelasticpath + "/stageOutput") == False:
        print("var.DFTnonMDelasticdir=", var.DFTnonMDelasticdir)
        finisheddirs = DFTanalysis.checkDFToutput([var.DFTnonMDelasticdir])  # Check if job is finished. If so add the 
                # dir to finisheddirs
        if not finisheddirs:
            check_directories(var.DFToutput[0], False)
        if var.stopAPD == True:
            return
        DFTanalysis.postProcessElastic(xc, mat)
        potSetup.generateFitdbse(var.DFTnonMDelasticpath, var.distorted_weight)
        with open(var.DFTnonMDelasticpath + "/stageOutput", "w") as directoriesFile:
           directoriesFile.write("Elastic calculation complete.\n")

    if var.isDFTnonMDevpath == False:
        DFTsetup.setupEvdir()
        processJobs.runOrStageJobs(var.DFTnonMDevdir, "")  # launch or instruct user to launch jobs
        if var.stopAPD == True:
            return
    if os.path.exists(var.DFTnonMDevpath + "/stageOutput") == False:
        print("var.DFTnonMDevdir=", var.DFTnonMDevdir)
        finisheddirs = DFTanalysis.checkDFToutput([var.DFTnonMDevdir])  # Check if job is finished. If so add the dir
                # to finisheddirs
        if not finisheddirs:
            check_directories(var.DFToutput[0], False)
        if var.stopAPD == True:
            return
        DFTanalysis.postProcessEv(xc, mat)
        potSetup.generateFitdbse(var.DFTnonMDevpath, var.distorted_weight)
        if var.stopAPD == True:
            return
        with open(var.DFTnonMDevpath + "/stageOutput", "w") as directoriesFile:
            directoriesFile.write("E vs V curve complete.\n")


def DFTMDparas(mat):

    """
    Optimize DFT input parameters for MD runs in stage 4_configs, with what is optimized depending on
    settings tags. This can include POTIM and LANGEVIN_GAMMA, but also ENCUT, KPOINTS and 
    EDIFF_PER_ATOM if config_gen='DFT-NPT-reduced'. Since this is just for the purposes of 
    generating configurations of atoms (snapshots will be recomputed at 'high-DFT' afterwards) the 
    criterion is just that the MD run is stable (does not crash due to a numerical error). Even if 
    config_gen='DFT-NPT', I.e. a non-reduced mode, a low ENCUT and KPOINTS will still be used 
    (and optimized if necessary) in this stage-- to speed up determination of POTIM and 
    LANGEVIN_GAMMA --they just won't be used for subsequent calculations in 4_config.
    
    Note that here, as elsewhere, if dft_engine=CASTEP, we map some CASTEP .param tags onto 
    var.incar. E.g. md_ion_t <-> LANGEVIN_GAMMA. This keeps the logic below simpler. Note that 
    md_ion_t= 1/LANGEVIN_GAMMA in actual fact, which we account for when we convert back to CASTEP 
    .param tags later on. Note that some parameters such as LANGEVIN_GAMMA_L and PMASS are not 
    present as adjustable parameters in CASTEP, but that md_cell_t takes their place. As with 
    LANGEVIN_GAMMA_L, we just give md_cell_t its default value.
    """

    # Determine if we can skip this stage
    skipStage = False
    if var.config_gen == "random":
        outf.printt(1, "  config_gen=='random': skipping 3_MDparas stage (to determine parameters for DFT MD)")
        skipStage = True
    if var.reuse_other_xc_configs == True:
        otherXcMDtrajCompleted = "complete" in var.xcFunc_DFTconfPresent[mat].values()
        if otherXcMDtrajCompleted:
            outf.printt(2, "  MD runs for other Xc completed: " + str(otherXcMDtrajCompleted))
            skipStage = True
    if skipStage == True:
        if var.isDFTMDparaspath == False:
            filesys.createWorkdir(var.DFTMDparasdir)
            with open(var.DFTMDparaspath + "/stageOutput", "w") as directoriesFile:
               directoriesFile.write("MD parameter optimization not necessary, skipping.\n")

    # ...otherwise proceed:
    else:
        if var.config_gen == "DFT_NPT" and "LANGEVIN_GAMMA" in var.incar and "POTIM" in var.incar:
            # In this special case we bypass MD parameter optimization (which for config_gen=
            # 'DFT_NPT' would just have involved POTIM, and LANGEVIN_GAMMA/_L). Note, we do not 
            # allow this override if config_gen='DFT_NPT-reduced'
            outf.printt(1,
                f"  {var.s_potim} and {var.s_langevin_gamma} values already "
                f"included in 1_input/{var.DFTinput[0]}: using these values "
                "henceforth, and overriding optimization of these parameters")
        else:
            # First determine NVT Langevin parameters (POTIM and LANGEVIN_GAMMA) using reduced ENCUT and KPOINTS. Reduced KPOINTS and ENCUT are used to determine these
            # parameters for increased efficiency. For config_gen='DFT_NPT' these reduced KPOINTS and ENCUT are only used here, whereas for 'DFT_NPT-reduced' these parameter
            # (and furthermore EDIFF) are also used in stage 3_configs to generate configurations.

            # Check on status of 2_paras/md/. The following situations are recognized by the code:
            # i) There is no 2_paras/md/ directory -> create one and setup DFT jobs: APDworkdir/DFT/pbe/mp-3834/3_MDparas/temp2000/kpoints1x1x1/encut300/potim{2,3,5,7,10}/gamma10/ediff-5
            # (Take a fixed value of gamma for this potim test. For further details see 'CASTEP md_ion_t (==1/LANGEVIN_GAMMA) notes')
            # ii) There is a 2_paras/md/ directory but without completed VASP results -> ask user to complete jobs (or wait for their completion if AUTOMATION=3)
            # iii) There is a 2_paras/md/ directory with completed VASP results -> analyze results, and place optimized INCAR, along with other VASP input files, in 2_paras/md/
            # No matter which of the above three we need:
            var.incar["MDALGO"] = 3  # Langevin thermostat
            var.incar["NSW"] = var.NSW_DFTMDparas
            var.incar["ISIF"] = 3
            var.incar["ISYM"] = 0
            if var.auto_select_temp == False:
                var.incar["TEBEG"] = var.temp_range[-1]  # the last value is appropriate either if a single or multiple values have been specified for temp_range, since we want the largest value
            else:
                var.incar["TEBEG"] = 5000  # we choose the highest temperature for the initial temperature search. if the search goes higher we perform new test calculations to refine parameters.

            DFTsetup.resizeCell(
                var.natomTargetMin_MDparaTest, var.natomTargetMax_MDparaTest, True
            )  # Increase supercell size if necessary. (K-points will be re-scaled as well, but for this stage we only need minimal kpoints so this will be overwritten below.)
            if var.dft_engine == "VASP":
                gammaDefault = 10  # unless we are optimizing the Langevin gamma friction parameter, use this default
                print("  Using default gamma: ", gammaDefault)
                var.incar["LANGEVIN_GAMMA"] = [gammaDefault] * var.structure.ntypesp
            else:
                gammaDefault = "Default"
                var.incar.pop(
                    "LANGEVIN_GAMMA", None
                )  # Use in-built CASTEP default, MD_ION_T = 100 * MD_DELTA_T for Langevin. (MD_ION_T = 1/LANGEVIN_GAMMA)

            DFTMDparasTempdir = var.DFTMDparasdir + "/temp" + str(var.incar["TEBEG"])
            DFTMDparasTemppath = var.dirpath + "/" + DFTMDparasTempdir
            # Set up low parameter DFT jobs to determine appropriate timestep and gamma settings and/or MD parameters to generate parameters as quickly as possible
            if var.isDFTMDparaspath == False:
                filesys.createWorkdir(var.DFTMDparasdir)
                # We organize in temperatures, since we may have to return to this stage if a higher temperature is sought (e.g. to get liquid). Optimized parameters are
                # always stored in DFTMDparasdir/ however
                filesys.createWorkdir(DFTMDparasTempdir)
            # We cycle over kpoints,encut pairs, starting low to begin with. If the lowest pair works we stick with it, if not gradually move along the list until
            # MD is stable
            outf.printt(
                1,
                "  Determining grid of K-points and energy cutoffs used to generate MD configs. We will start with the lowest and if necessary increase to obtain robust MD",
            )

            # Determine trial k-points
            outf.printt(2, "    K-points to consider (4th element is the divisor applied to the rescaled k-points): ")
            kptsReduced = []
            kpts = []
            kpts.append(var.kpoints.kpts[0][0])
            kpts.append(var.kpoints.kpts[0][1])
            kpts.append(var.kpoints.kpts[0][2])
            divisor = max(kpts)
            kptsReduced.append([int(math.ceil(kpts[0] / divisor)), int(math.ceil(kpts[1] / divisor)),
                    int(math.ceil(kpts[2] / divisor)), divisor])  # we append divisor to the end becase it will be of use for stage 4 when the supercell is a different size but we would like to retain the same divisor for reducing the kpoints
            if var.verbosity == 2:
                print("      " + str(kptsReduced[0]), end="")
            while True:
                divisor = divisor - 1
                if divisor == 0:
                    break
                kptsTrial = [int(math.ceil(kpts[0] / divisor)), int(math.ceil(kpts[1] / divisor)),
                    int(math.ceil(kpts[2] / divisor)), divisor]
                if kptsTrial != kptsReduced[-1]:
                    kptsReduced.append(kptsTrial)
                    if var.verbosity == 2:
                        print(" " + str(kptsTrial), end="")
            if var.verbosity == 2:
                print("")
            # Determine trial encut
            outf.printt(2, "    Energy cut-offs to consider: ")
            encutReduced = []
            encutTrial = 200
            if var.verbosity == 2:
                print("      ", end="")
            while encutTrial <= var.incar["ENCUT"]:
                if var.verbosity == 2:
                    print(str(encutTrial) + " ", end="")
                encutReduced.append(encutTrial)
                encutTrial = encutTrial + 100
            if var.verbosity == 2:
                print("")

            outf.printt(2, "    Constructing grid:")
            kptsEncutTrial = (
                []
            )  # this will have a depth 4: a list containing sublists for different job launching iterations, containing a set of kp (itself a list) and encut lists
            iteration = 0
            while iteration <= (len(kptsReduced) + len(encutReduced)):
                kptsEncutTrialIteration = []
                for kptsIndex in range(0, iteration + 1):
                    if kptsIndex < len(kptsReduced) and (iteration - kptsIndex) < len(encutReduced):
                        outf.printt(2, "      iteration=" + str(iteration) + " kptsIndex=" + str(kptsIndex))
                        outf.printt(2, "        appending kptsReduced[" + str(kptsIndex) + "]="
                            + str(kptsReduced[kptsIndex]) + " and encutReduced[" + str(iteration - kptsIndex)
                            + "]=" + str(encutReduced[iteration - kptsIndex]))
                        kptsEncutTrialIteration.append([kptsReduced[kptsIndex], encutReduced[iteration - kptsIndex]])
                if kptsEncutTrialIteration:
                    kptsEncutTrial.append(kptsEncutTrialIteration)
                iteration = iteration + 1
            print("    List of kpoints, energy cut-offs to consider, grouped in sub-lists, each of which can be "
                "launched at the same time should an earlier sublist fail",kptsEncutTrial)
            # Set up directory structure: APDworkdir/DFT/pbe/mp-3834/3_MDparas/temp2000/kpoints1x1x1/encut200/timestep#/gamma10/ediff/
            outf.printt(1, "  Setting up directory system  to contain jobs for different parameters.")
            # iterate kpoints, encut. Hopefully the first choice (kp1x1x1, 200 eV) will provide a robust MD, then no need to iterate this...
            for kptsEncutTrialIteration in kptsEncutTrial:
                # First run all sets kpts and encuts in this 'iteration'. (for iteration '1' this will just be a single set, e.g. kp1x1x1, 200 eV)
                for kptsEncutSet in kptsEncutTrialIteration:
                    kpts = kptsEncutSet[0]
                    kptsdir = str(kpts[0:3])  # omit index 3, the divisor
                    kptsdir = (kptsdir.replace("[", "(").replace("]", ")").replace("(", "").replace(")", "")
                        .replace(", ", "x"))  #  [1, 1, 1] -> 1x1x1, etc. For some reason can't do replace('[',''), so have to do this round-about way
                    # print('kpts= ',kpts,', kptsdir=',kptsdir)
                    encut = kptsEncutSet[1]
                    DFTMDparastempKptsdir = DFTMDparasTempdir + "/kpoints" + kptsdir
                    DFTMDparastempKptspath = var.dirpath + "/" + DFTMDparastempKptsdir
                    DFTMDparastempKptsEncutdir = DFTMDparastempKptsdir + "/encut" + str(encut)
                    DFTMDparastempKptsEncutpath = var.dirpath + "/" + DFTMDparastempKptsEncutdir
                    if os.path.exists(DFTMDparastempKptsdir) == False:
                        filesys.createWorkdir(DFTMDparastempKptsdir)
                    if os.path.exists(DFTMDparastempKptsEncutdir) == False:
                        filesys.createWorkdir(DFTMDparastempKptsEncutdir)
                        var.incar["ENCUT"] = encut
                        var.kpoints.kpts[0][0] = kpts[0]
                        var.kpoints.kpts[0][1] = kpts[1]
                        var.kpoints.kpts[0][2] = kpts[2]

                        jobdirsLocal = DFTsetup.timestepGammaEdiffDirs(DFTMDparastempKptsEncutdir,
                            [2, 3, 5, 7, 10],[gammaDefault],[var.ediff_per_atom],var.DFTinputpath)  # set up MD para test dirs
                        processJobs.runOrStageJobs(DFTMDparastempKptsEncutdir, jobdirsLocal)  # launch or instruct user to launch jobs

                if var.stopAPD == True:
                    print("returing...")
                    return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze

                # Timestep analysis for {kpoints,encut} of current iteration
                print(
                    "  Directories already set up for kpoints, encut sets: ",
                    kptsEncutTrialIteration,
                    ", proceeding to analyze.",
                )
                largestTimestepDivTimeSimPerSet = (
                    []
                )  # 'TimestepDivTimeSim' is the POTIM divided by simulation time to accumulate neccessary (default 200) configs.
                bestTimestepPerSet = []
                for kptsEncutSet in kptsEncutTrialIteration:
                    kpts = kptsEncutSet[0]
                    kptsdir = str(kpts[0:3])
                    kptsdir = (kptsdir.replace("[", "(").replace("]", ")").replace("(", "").replace(")", "")
                        .replace(", ", "x"))
                    encut = kptsEncutSet[1]
                    DFTMDparastempKptsdir = DFTMDparasTempdir + "/kpoints" + kptsdir
                    DFTMDparastempKptsEncutdir = DFTMDparastempKptsdir + "/encut" + str(encut)
                    DFTMDparastempKptsEncutpath = var.dirpath + "/" + DFTMDparastempKptsEncutdir
                    print("  Looking for DFT data to process in " + DFTMDparastempKptsEncutdir + "...")
                    if var.verbosity == 2:
                        print("  Reading temperature data from:")
                    elif var.verbosity == 1:
                        print("  Reading temperature data for:")
                    # print('DFTMDparastempKptsdir=',DFTMDparastempKptsdir,'os.path.exists(DFTMDparastempKptsdir)=',os.path.exists(DFTMDparastempKptsdir))
                    # print('DFTMDparastempKptsEncutdir=',DFTMDparastempKptsEncutdir,', os.path.exists(DFTMDparastempKptsEncutdir)=',os.path.exists(DFTMDparastempKptsEncutdir))
                    averageTempList, timesteplargest = DFTanalysis.averageTemp( DFTMDparastempKptsEncutdir,
                        DFTMDparastempKptsEncutpath, "timestep", "gamma", "ediff" + str(var.ediff_per_atom))
                    if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
                        actionIncompleteJobs(
                            var.DFToutput[0], True
                        )  # Check if any jobs need resubmitting (and/or 'cont' directories set up)
                    else:
                        largestTimestepDivTimeSim = 0
                        print("averageTempList=", averageTempList)
                        for averageTempListLine in averageTempList:
                            timestepDivTimeSim = averageTempListLine[0] / averageTempListLine[4]
                            print("timestep=",averageTempListLine[0]," simtime=",averageTempListLine[4],
                                " ratio=",averageTempListLine[0] / averageTempListLine[4])
                            if timestepDivTimeSim > largestTimestepDivTimeSim:
                                largestTimestepDivTimeSim = timestepDivTimeSim
                                bestTimestep = averageTempListLine[0]
                            largestTimestepDivTimeSim = max(largestTimestepDivTimeSim, timestepDivTimeSim)
                        largestTimestepDivTimeSimPerSet.append(
                            largestTimestepDivTimeSim
                        )  # max averageTempList[0]/averageTempList[4] (potim and time-simulation respectively)
                        # bestTimestepPerSet.append(bestTimestep) # COMMENTED OUT for now. The approach of using TimestepDivTimeSim to optimize POTIM only makes sense if we change the algorithm for generating configs to instead generate a fixed number of uncorrelated configs. I will make this change next year. For now revert to the old method of choosing largest working POTIM and fixing NSW=2000.
                        bestTimestepPerSet.append(timesteplargest)
                if var.stopAPD == True:
                    return  # Wait for all jobs to complete before proceeding with analysis/job set up

                # Determine highest potim/time_sim
                largestTimestepDivTimeSim = max(largestTimestepDivTimeSimPerSet)
                if ( largestTimestepDivTimeSim > 0 ):
                    # If we don't enter this loop then we go back and iterate over larger encut and kpoint
                    bestTimestepIndex = largestTimestepDivTimeSimPerSet.index(
                        largestTimestepDivTimeSim
                    )  # if there are multiple indices, eg corresponding to kp1x1x1,encut300 or kp2x2x2,encut200, this will choose the first one, I.e. where kpoints are minimized
                    bestTimestep = bestTimestepPerSet[bestTimestepIndex]
                    print("bestTimestep=", bestTimestep)
                    kpts = kptsEncutTrialIteration[bestTimestepIndex][0]
                    encut = kptsEncutTrialIteration[bestTimestepIndex][1]
                    var.incar["ENCUT"] = encut
                    var.kpoints.kpts[0][0] = kpts[0]
                    var.kpoints.kpts[0][1] = kpts[1]
                    var.kpoints.kpts[0][2] = kpts[2]

                    # Set DFTMDparastempKptsEncutTimestepdir/path to that for which highest working POTIM found. (for first iteration of kpts, encut this will hopefully just be
                    # 200eV, 1x1x1kp)
                    kptsEncutSet = kptsEncutTrialIteration[bestTimestepIndex]
                    divisor = kptsEncutSet[0][3]
                    print("divisor=", divisor)
                    kptsdir = (str(kptsEncutSet[0][0:3]).replace("[", "(").replace("]", ")").replace("(", "")
                        .replace(")", "").replace(", ", "x"))
                    encut = kptsEncutSet[1]
                    var.maxTempMDparasChecked = var.incar[
                        "TEBEG"
                    ]  # we can return to 3_MDparas after 4_configs, to generate new parameters for higher temperatures. However each
                    # time we complete this time we are always at the right temperature (the 'max' temperature we have performed
                    # simulations for so far) to generate configs for 3_configs
                    DFTMDparastempKptsEncutdir = DFTMDparasTempdir + "/kpoints" + kptsdir + "/encut" + str(encut)
                    DFTMDparastempKptsEncutpath = var.dirpath + "/" + DFTMDparastempKptsEncutdir
                    # There used to be code here for setting up jobs for determining langevin_gamma parameter.
                    # I removed it - c.f. notes in 'initialize.py'. Last commit with the code in:
                    # f13ab3e6859a199d80d86b114e4bb23ee6901a78 (other references to langevin_gamma also removed)

                    # keep langevin_gamma at its default value from the earlier tests, 10. Note: there is an argument for scaling it with timestep. c.f.
                    # CASTEP md_ion_t (==1/LANGEVIN_GAMMA) notes. But in most of my tests I found 10 to be fine, esp. since we just care about generating configs
                    bestGamma = gammaDefault
                    if "REDUCED" in var.config_gen:
                        # do ediff check
                        print()
                        # CASTEP md_elec_energy_tol is per atom while EDIFF is per cell. Therefore simplest to test by x multiples of 10.
                        ediffs_per_atom = [ format(10 * var.ediff_per_atom, '.4e'), format(100 * var.ediff_per_atom, '.4e'),
                            format(1000 * var.ediff_per_atom, '.4e') ] # without format we get ediff1.000000001e-07 etc
                        DFTMDparastempKptsEncutTimestepGammapath = ( var.dirpath + "/" + DFTMDparastempKptsEncutdir
                            + "/timestep" + str(bestTimestep) + "/gamma" + str(bestGamma) )
                        if ( os.path.exists(DFTMDparastempKptsEncutTimestepGammapath + "/ediff" + 
                            str(ediffs_per_atom[0]) ) == False ):
                            jobdirsLocal = DFTsetup.timestepGammaEdiffDirs( DFTMDparastempKptsEncutdir,
                                [bestTimestep], [bestGamma], ediffs_per_atom, var.DFTinputpath)  # set up MD para test dirs
                            processJobs.runOrStageJobs(DFTMDparastempKptsEncutdir, jobdirsLocal)
                            if var.stopAPD == True:
                                return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze

                        # Find highest EDIFF_PER_ATOM
                        bestEdiff = ""
                        var.incompleteJobs = []
                        var.notStartedJobs = []
                        for ediff_per_atom in ediffs_per_atom:
                            DFTMDparastempKptsEncutTimestepGammaEdiffdir = ( DFTMDparastempKptsEncutdir
                                + "/timestep" + str(bestTimestep) + "/gamma" + str(bestGamma) + "/ediff"
                                + str(ediff_per_atom) )
                            DFTMDparastempKptsEncutTimestepGammaEdiffpath = (
                                var.dirpath + "/" + DFTMDparastempKptsEncutTimestepGammaEdiffdir
                            )
                            temperatures, completed, nPreEqrSteps, NaNpresent, calcTime = DFTanalysis.readTemps(
                                DFTMDparastempKptsEncutTimestepGammaEdiffdir,
                                DFTMDparastempKptsEncutTimestepGammapath,
                                "ediff" + str(ediff_per_atom), "", "" )

                            if NaNpresent:
                                outf.printt(1, "      NaN detected for ediff_per_atom: " + str(ediff_per_atom))
                                break
                            # Assess stop conditions
                            if completed:
                                bestEdiff = ediff_per_atom
                        if bestEdiff != "":
                            print("  Highest EDIFF_PER_ATOM that works is: ", bestEdiff)
                        else:
                            print("  ERROR: lowest EDIFF_PER_ATOM does not work?!")
                            quit()
                        if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
                            actionIncompleteJobs(
                                var.DFToutput[0], True
                            )  # Check if any jobs need resubmitting (and/or 'cont' directories set up)
                        if var.stopAPD == True:
                            return  # Wait for all jobs to complete before proceeding with analysis.
                        # <- need to makje sure the stopapd is given out above
                    else:
                        bestEdiff = var.ediff_per_atom
                    break

            # ---- Store outputs of this stage in 3_MDparas/temp# to 'pass on' to the next stage ----
            DFTsetup.setupDFTinput(
                var.DFTinputdir, var.DFTinputpath
            )  # We want the original DFT inputs, although depending on config_gen we would like to retain certain
            # of the DFT parameters determined here
            # (to recover the original supercell, kpoints and encut), but with changes to INCAR:
            var.incar[
                "TEBEG"
            ] = (
                var.maxTempMDparasChecked
            )  # we can return to 3_MDparas after 4_configs, to generate new parameters for higher temperatures. However each
            var.incar["MDALGO"] = 3  # Langevin thermostat
            var.incar["ISIF"] = 3
            print(
                "  Lowest working kpts and encut for MD: ", kptsdir, ", ", encut
            )  # Note: we do not store the new kpoints. The reason is we want to return to the original
            # primitive cell and rescale to a new larger cell in the next stage, for which the new kpoints here would not be appropriate. Instead we just
            # store the 'divisor' determined here and use that
            print("  Highest working timestep for MD: ", bestTimestep)
            if bestTimestep > 5:
                print("    ...greater than 5 - setting to 5 (larger values may be unsafe during longer simulations)")
                bestTimestep = 5
            var.incar["POTIM"] = bestTimestep
            if var.dft_engine == "VASP":
                print("  Using default gamma: ", gammaDefault)
                var.incar["LANGEVIN_GAMMA"] = [gammaDefault] * var.structure.ntypesp
            else:
                var.incar.pop(
                    "LANGEVIN_GAMMA", None
                )  # Use in-built CASTEP default, MD_ION_T = 100 * MD_DELTA_T for Langevin. (MD_ION_T = 1/LANGEVIN_GAMMA)
            if "REDUCED" in var.config_gen:
                print("  Largest working EDIFF_PER_ATOM for MD: ", bestEdiff)
                var.incar["ENCUT"] = encut
                var.ediff_per_atom = bestEdiff
                print("  Of these, for atomic configuration generation we will use: all except gamma")
            else:
                print("  Using default EDIFF_PER_ATOM: ", var.ediff_per_atom)
                print(
                    "  Of these, for atomic configuration generation we will use: timestep. For all other parameters we will use the 'high' set"
                )

            # Below we rejoin the code from earlier, I.e. for the case where Langevin parameters for atoms are already present in the INCAR (and where we don't want to
            # compute reduced parameters for '-reduced' configuration generation). Below we set the lattice Langevin parameters.
            var.incar.pop(
                "LANGEVIN_GAMMA_L", None
            )  # Remove this in the unlikely event it is present, so we can determine it in a consistent manner to LANGEVIN_GAMMA below

        # Determine NPT Langevin parameters
        if "LANGEVIN_GAMMA_L" not in var.incar:
            outf.printt(1, "  ")
            outf.printt(1, "  " + var.s_langevin_gamma_l + " not present.")
            # No default offered by VASP, but CASTEP defaults to 10x damping time of ionic motion using Langevin thermostat, so use this (note though for the description
            # in terms of friction coefficients rather than damping times, as in VASP, this equates to friction on lattice of 0.1x that of ions).
            if var.dft_engine == "VASP":
                # VASP allows multiple friction coefficients, so choose smallest from which to determine the lattice friction coefficient
                outf.printt( 1, "  Selecting smallest " + var.s_langevin_gamma + " to set an appropriate value for "
                    + var.s_langevin_gamma_l )
                gamma = var.incar["LANGEVIN_GAMMA"]  # var.incar["LANGEVIN_GAMMA"] = [gamma] * var.structure.ntypesp
                gamma.sort()
                if "langevin_gamma_l" not in var.incar:
                    if all_equal(gamma):
                        outf.printt(1, "  LANGEVIN_GAMMA_L not present, setting to: " + str(gamma[0]))
                        var.incar["LANGEVIN_GAMMA_L"] = gamma[0]
                    else:
                        outf.printt( 1, "  LANGEVIN_GAMMA different for each element, selecting smallest value, "
                            + str(gamma[0]) + ", (longest damping time) to define LANGEVIN_GAMMA_L" )
                        outf.printt(1, "  Setting " + str(var.s_langevin_gamma_l) + " to " + str(gamma[0]) + "/10")
                        var.incar["LANGEVIN_GAMMA_L"] = gamma[0] / 10
            else:
                # Since CASTEP already has a default no need to set it here (if CASTEP devs ever changed the default it is probably better we follow their example...)
                # CASTEP has a single value across all species
                outf.printt( 1, "  CASTEP has default behaviour for " + var.s_langevin_gamma_l + " (="
                    + var.s_langevin_gamma + "*10) - not setting in .param file" )
                # outf.printt(1,'  Setting '+var.s_langevin_gamma_l+' to '+str(var.incar["LANGEVIN_GAMMA"][0])+'*10')
                # #print('var.incar["LANGEVIN_GAMMA"][0]=',var.incar["LANGEVIN_GAMMA"][0])
                # var.incar["LANGEVIN_GAMMA_L"] = var.incar["LANGEVIN_GAMMA"][0]/10 # note: for CASTEP we only convert to damping times (1/langevin_gamma) when saving input files
                # need to check that this is equivalent to: var.calc.param.md_cell_t=[10*var.calc.param.md_ion_t,'ps']
            if var.dft_engine == "VASP" and "pmass" not in var.incar:  # previously: if var.incar["PMASS"] == None:
                outf.printt(1, "  PMASS not present, setting to: " + str(var.pmass_default))
                var.incar["PMASS"] = var.pmass_default
        outf.printt( 2, "  Writing out " + str(var.dft_engine) + " input to " + DFTMDparasTempdir 
            + " (to signal stage is complete)" )
        # First convert those CASTEP inputs which we have been storing in VASP objects (e.g. var.incar) back to either CASTEP objects (e.g. var.calc.param) or variables
        # e.g. var.magmoms, which we will use subsequently here
        if var.dft_engine == "CASTEP":
            var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure) # writeDFTinputFiles requires 'atoms' where CASTEP is used
            DFTsetup.convertVASPtoCASTEP2()  # updates var.calc, and defines var.magmoms, and var.ldauu etc.
        DFTsetup.writeDFTinputFiles(
            DFTMDparasTempdir, DFTMDparasTemppath, var.DFTinputpath
        )  # Save the adjusted INCAR (or .param in case of CASTEP) file, and the non-adjusted POSCAR and KPOINTS (or .cell file) files, to stage 2 root directory
        with open(DFTMDparasTemppath + "/stageOutput", "w") as directoriesFile:
            directoriesFile.write("3_MDparas jobs completed. Divisor to use to adjust k-points:\n")
            directoriesFile.write(str(divisor) + "\n")


def actionIncompleteJobs(fileOutput, setupContJob):

    """
    Check if jobs are still running for the code which generates fileOutput (e.g. DFToutput[0] for VASP or CASTEP, output.0 for MEAMfit...) in the directories var.incompleteJobs. If not, and if setupContJob=True, set up cont directories and run. Note the output.0 is a bit generic and eventually need to change this as well as adding analogous PotOptOutput[0] etc and potOpt_engine variable once we support more than just MEAMfit...
    """

    if len(var.notStartedJobs) > 0:  # Job/s not started
        for direc in var.notStartedJobs:
            var.jobdirs_notStarted.write(direc + "\n")
        outf.toAction( "Some jobs have either not been submitted or not yet started (no castep.castep file). Please "
            "see 'jobdirs_notStarted'." )

    if len(var.incompleteJobs) > 0:  # Jobs started but incomplete
        # The following works at least on SCARF/SLURM, where the .castep file is updated every minute or so, so the timestep can be used as a check.
        # If the DFT output file is not present code assumes it has been submitted but hasn't started, and states that it is '...awaiting results'
        notRunningJobs = processJobs.checkIfRunning(fileOutput, var.incompleteJobs)
        if len(notRunningJobs) > 0:  # Job/s has stopped running
            if setupContJob == False:
                # Signal for jobs to be restarted
                for direc in notRunningJobs:
                    var.jobdirs.write(direc + "\n")  # jobdirs is a file object, so here we write direct to file
                    if fileOutput == "output_p.txt": # MLIP2 output
                        potSetup.backupOutputs(direc)
                    elif fileOutput == var.DFToutput[0] and var.dft_engine == "CASTEP":
                        # If we restart the job in the same dir, output will be appended to existing .castep. Don't
                        # want this as e.g. MEAMfit can't read concatenated .castep files
                        DFTsetup.removeOutputs(direc)
                    elif fileOutput == var.potMDoutput:
                        potMDsetup.backupOutputs(direc)

            else:
                # Set up continuation jobs
                outf.printt(
                    1,
                    "Setting up continuation jobs... (These have been generated based on the timestamp of the output file in the higher level directory in each case.)",
                )
                if fileOutput == var.DFToutput[0]:
                    DFTsetup.contJobs(notRunningJobs)  # ,fileOutput)
                elif fileOutput == "output.0":
                    potSetup.contJobs(notRunningJobs)
                var.jobsToRun = [direc + "/cont" for direc in notRunningJobs]
                for direc in var.jobsToRun:
                    var.jobdirs.write(direc + "\n")  # jobdirs is a file object, so here we write direct to file
            outf.toAction("Please run jobs in directories listed in 'jobdirs'.")

def DFTMDconfigsExisting(xc, mat):

    """
    Copy vaspruns from job directory to 4_, and prune them if necessary so not too many configs
    """

    # Determine number of steps in input DFT files, then define corrTime, which determines the separation between
    # ionic steps which are kept
    outf.printt(1, "  Reading DFT MD configurations from existing files in job directory")
    nStepsTot = 0
    DFToutputFiles = glob.glob(var.DFTprefix + "*" + var.DFTsuffix)
    if not DFToutputFiles:
        outf.printt(1, f"  ERROR: No DFT output files found with pattern {var.DFTprefix}*{var.DFTsuffix} in "
                f"calculation root directory {var.dirpath}, STOPPING")
        quit()
    outf.printt(2, "  Determine number of ionic steps...")
    temperature_file_pairs = []
    for DFToutputFile in DFToutputFiles:
        outf.printt(2, "    Checking DFT output file: " + DFToutputFile + "...")
        nSteps, temperature = DFTanalysis.countMDstepsAndTempInDFToutput1(var.dirpath, filename = DFToutputFile) # will this work, or do i need var.dirpath instead?
        temperature_file_pairs.append((temperature, DFToutputFile))
        outf.printt(2, f"      Found {nSteps} ionic steps")
        nStepsTot += nSteps
    temperature_file_pairs = sorted(temperature_file_pairs, key=lambda x: x[0])
    outf.printt(2, f"  Total number of ionic steps: {str(nStepsTot)}")
    outf.printt(2, f"  Temperature/files in increasing order of temp: {temperature_file_pairs}")
    # corrTime determines how much data we use. In general for starting point I have used about 1000 configs before.
    # (Actually ~ 600, increasing to 1000 after AL.). Here be more conservative and ensure we start with 2000 configs.
    corrTime = int(max(1, nStepsTot/2000)) # Either use all data (if < 2000), or 2000 data points
    outf.printt(2, "  Skip every "+str(corrTime)+" steps")

    filesys.createWorkdir(var.DFTconfigsdir)
    filesys.createWorkdir(var.DFTconfigsDFTNPTdir)

    outf.printt(1, "  Copying files to stage directory and reducing number of configs")
    for file in DFToutputFiles:
        outf.printt(2, "  " + file + ":")
        outf.printt(2, "    ...copying file")
        shutil.copy2(file, var.DFTconfigsDFTNPTpath)
        outf.printt(2, "    ...removing configs")
        DFTsetup.removeUncorrelatedConfigs(var.DFTconfigsDFTNPTpath, corrTime, filename = file)
        if var.dft_engine == "PWSCF":
            # Copy the .in file as well
            input_file = os.path.splitext(file)[0] + '.in'
            outf.printt(2, "    ...copying input file " + input_file)
            shutil.copy2(input_file, var.DFTconfigsDFTNPTpath)

    outf.printt(1, "  Generating fitdbse file")
    potSetup.generateFitdbse( var.DFTconfigsDFTNPTpath, 1.0 )

    outf.printt(2, "  Writing stageOutput")
    with open(os.path.join(var.DFTconfigsDFTNPTpath, "stageOutput"), "w") as stageOutputFile:
        stageOutputFile.write("Completed copying over and stripping configs from DFT MD files\n")
        for temp, filename in temperature_file_pairs:
            stageOutputFile.write(f"{int(temp)} {filename}\n")

def DFTMDconfigs(xc, mat):

    """
    Stage/run DFT jobs to generate NPT training data for potential optimization. These jobs are computationally 
    intensive and this stage is the bottleneck in efficiency, therefore ideally as few configs are generated as 
    possible. If thermal expansion manages to equilibrate for a given temperature, averages over equilibrated thermal 
    expansion and rdf will be carried out and recorded but _only_ if equilibration is achieved in the preset number
    of ionic steps.
    If auto_select_temp = True, this function determines at which temperature the liquid phase occurs. This 
    enables to choose a range of temperatures (including user specified temperatures if these have been specified)
    so that we capture the liquid phase and the high temperature solid. This can be useful to ensure a wide range of 
    configs in the initial training set, even if we do not care about modelling the liquid phase.
    Note, DFT melting point is often considerably higher than the physical melting point and depends strongly on the 
    supercell size. It is therefore difficult to predict. Here we take a practical approach, initializing DFT jobs for 
    a range of temperatures and then analyzing the rdf to try and detect the transition from solid to liquid. If the 
    liquid is not identifed using the initial range of temperatures we try higher temperatures. Once the liquid is 
    identified we may still need additional temperatures to ensure we have a good representation of the high 
    temperature solid.
    
    To do: currently a fixed number of configs is used per material (var.NSW_DFTconfigs). To improve the efficiency
    of APD this could be made variable somehow based on the following):
    i) type of potential and number of potential parameters
    ii) when i implement the scheme to adjust POTIM to minimize cpu-time for a given simulation-time, the number of
    configs will need to be adjusted accordingly (currently not implemented - we just take the largest working POTIM)
    iii) size of supercell (or more accurately total no. data points contributed by each configuration)
    iv) number of species
    v) irrespective of amount of data needed per para, length of DFT run should be sufficient to run over a range of
    lattice parameters (thermally equilibrate or at least partially equilibrate)
    
    (Implementing such a scheme would not be required for accuracy-- the active learning should provide that --but for
    efficiency)
    """

    # The following could form the basis for implementing a scheme to tune NSW according to the last comment above:
    #
    # nPotParas=potAnalysis.calcNumPotParas() # Number of potential parameters
    # nDataPoints=int(var.nDataPointsPerPotPara * nPotParas) # Number of data-points reqd given no. pot paras
    # nDataPointsPerMat=int(nDataPoints/len(var.materials)) # Split these equally across the materials
    # # Given no. atoms in cell for this material this will determine how many configs...
    # DFTsetup.resizeCell(var.natomTargetMin_MD,var.natomTargetMax_MD,True) # Increase supercell size (and adjust k-points) if neccessary
    # nDataPointsPerConfig = int(7 + var.structure.num_sites*3)  #7=1 energy, 6 stress tensor components
    # nConfigsPerMat = int(nDataPointsPerMat / nDataPointsPerConfig) # No. of atomic configs reqd for this material
    if var.defects:
        var.incar["NSW"] = var.NSW_DFTconfigs/var.num_defect_configs # DFT MD periodically restarted with diff defects
    else:
        var.incar["NSW"] = var.NSW_DFTconfigs
    var.incar["ISIF"] = 3  # Volume relaxation
    var.incar["ISYM"] = 0
    var.incar["LREAL"] = "Auto" # faster for larger cells, and should be stable here as encut not too high (c.f. highElecParas...) 
    # For config gen a low ediff can be used (unless reduced parameters have been used for config gen, in which case
    # use the value determined from stage 3:
    if ( "REDUCED" not in var.config_gen ): var.ediff_per_atom = 0.00001 

    copyPOTCARfrom = var.DFTMDparaspath + "/temp" + str(var.maxTempMDparasChecked) # Or could take from any other stage

    # Set up an initial temperature range across which to perform calculations if this is the first time this stage has been run
    if var.isDFTconfigspath == False:
        DFTsetup.resizeCell( var.natomTargetMin_MD, var.natomTargetMax_MD, True ) # Increase supercell, adjust k-points
                                                                                  # based on standard MP settings
        if ( "REDUCED" in var.config_gen ): # Reduce k-points further if using reduced DFT paras to generate configs
            # Cell may be larger here than in 3_DFTMDparas (and so KPOINTS lower- as done by resizeCell()) therefore we
            # need the factor by which we were able to reduce KPOINTS in that stage rather than absolute KPOINTS
            outf.printt(2, "  Reading in divisor from 3_DFTMDparas to rescale kpoints")
            with open(os.path.join(var.DFTMDparaspath, "temp" + str(var.maxTempMDparasChecked), 
                "stageOutput")) as stageOutputFile:
                lineReadin = stageOutputFile.readline()
                kpointsDivisor = int(stageOutputFile.readline())
            outf.printt(2, f"  kpointsDivisor: {str(kpointsDivisor)}")
            outf.printt(2, f"  Kpoints before rescaling: {var.kpoints.kpts[0][0:3]}")
            for i in range(3):
                var.kpoints.kpts[0][i] = int(math.ceil(var.kpoints.kpts[0][i] / kpointsDivisor))
            outf.printt(1, f"  Kpoints after rescaling: {var.kpoints.kpts[0][0:3]}")

        # Save structure to file in APDworkdir/DFT/pbe/mp-942733/4_configs/DFTNPT/ so we can read in next time
        # (This code needs testing)
        if var.dft_engine == "CASTEP":
            var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure) # writeDFTinputFiles requires 'atoms' where CASTEP is used
            DFTsetup.convertVASPtoCASTEP2() # Since writing a .cell some paras need converting, e.g. kpoints
        filesys.createWorkdir(var.DFTconfigsdir)
        filesys.createWorkdir(var.DFTconfigsDFTNPTdir)
        DFTsetup.writeDFTinputFiles(var.DFTconfigsDFTNPTdir, var.DFTconfigsDFTNPTpath, var.DFTinputpath) # recently adjusted this to also write out INCAR file (since if cell has changed other props like magmom, ldauu will have been changed to). Need to check it does correctly write out the INCAR file

        if var.auto_select_temp == False:
            # In this case only perform NPT simulations in the user specified temperature range
            temperatures = []
            if len(var.temp_range) == 1:  # single value
                temperatures.append(var.temp_range[0])
            else:  # lower and upper bounds
                temperatures.append(var.temp_range[1])
                temperatures.append(var.temp_range[0] + int(2 / 3 * (var.temp_range[1] - var.temp_range[0])))
                temperatures.append(var.temp_range[0] + int(1 / 3 * (var.temp_range[1] - var.temp_range[0])))
                outf.printt(1, f"  Temperatures to run NPT simulations: {temperatures}")
        else:
            # Set up initial temperature range including APD-selected and user specified temperatures
            temperatures = [1000, 2000, 3000, 4000, 5000]
            if var.temp_range != None:
                # Also set up DFT jobs for user-specified temperature range through the 'temp_range' tag in the settings.APD file
                outf.printt(2, f"  temp_range specified in settings.APD: {var.temp_range[0]} - {var.temp_range[1]}")
                # Remove those temperatures from default list that are already in this range
                for temp in temperatures:
                    if ( temp >= (var.temp_range[0] + int(1 / 3 * (var.temp_range[1] - var.temp_range[0])))
                        and temp <= var.temp_range[1] ):
                        temperatures.remove(temp)
                outf.printt(2, "  Including 3 temperatures from user-specified range")
                temperatures.insert(var.temp_range[1])
                temperatures.insert(var.temp_range[0] + int(2 / 3 * (var.temp_range[1] - var.temp_range[0])))
                temperatures.insert(var.temp_range[0] + int(1 / 3 * (var.temp_range[1] - var.temp_range[0])))
            outf.printt(2, "  Also including temperatures to scan for liquid/high temperature solid.")
            outf.printt(1, f"  Temperatures to run NPT simulations: {temperatures}")

        newJobDirs = DFTsetup.setupMDconfigsdir(var.DFTconfigsDFTNPTdir, temperatures, "temp", copyPOTCARfrom, "TEBEG")
        processJobs.runOrStageJobs(var.DFTconfigsDFTNPTdir)
        if var.stopAPD == True:
            return
    else:
        # Read in the structure (and kpoints) from before
        DFTsetup.setupDFTinput(var.DFTconfigsDFTNPTdir, var.DFTconfigsDFTNPTpath)

    if not os.path.isfile(var.DFTconfigsDFTNPTpath + "/stageOutput"):
        outf.printt(1, "  -- NPT --")

        # Check which jobs have finished; also (if no defects specified) compute rdf and thermal expansion:
        #temperatures, min_minDivAvrg = DFTanalysis.computeRdfThermExpn( xc, mat )
        temperatures = DFTanalysis.analyzeNPT()
        if var.failedJobs:
            outf.printt(0, "  ERROR: failed DFT NPT jobs, STOPPING")
            quit()
        if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
            actionIncompleteJobs( var.DFToutput[0], True ) # Check if any jobs need resubmitting (and whether cont jobs)
        if var.defects and (len(var.completeJobs) > 0 or len(var.failedJobs) > 0):
            # Defect directories within temperature dirs contain finished jobs - set up new ones (temp#/defect#/ for
            # the temperatures in question). Note that we want to set up new jobs to follow on from complete jobs as
            # well as failed jobs. 
            finishedJobs = var.completeJobs + var.failedJobs
            newJobDirs = DFTsetup.setupMDconfigsdir(var.DFTconfigsDFTNPTdir, temperatures, "temp", copyPOTCARfrom, "TEBEG", 
                    completedDirs = finishedJobs)
            if len(newJobDirs) > 0:
               processJobs.runOrStageJobs(var.DFTconfigsDFTNPTdir, jobdirsLocal=newJobDirs)
        if var.stopAPD == True:
            return

        if var.auto_select_temp == True:
            # Analyze NPT jobs to see if more temperatures are necessary to capture liquid and high temperature solid
            liquidDetected = False

            # Apply criterion on rdf descriptors to identify if we have located liquid phase
            # (This criterion is currently crude and will probably need to be replaced e.g. with:
            # detection of order of magnitude (OofM) increase in min_minDivAvrg w.r.t temperature, with no subsequent OofM increases)
            liquidDetected = min_minDivAvrg[-1] > 0.3
            print("  Liquid detected?: ", liquidDetected)
            # Also check second to last value, because we also want to include the next temp above the m.p.t
            liquidDetected = ( min_minDivAvrg[-2] > 0.3 )  
            print("  Liquid also for the second to last temperature? :", liquidDetected)

            # Need two temperatures with liquid. If not gradually increase temperature range until we do
            if not liquidDetected:

                # Check which temp dirs exist:
                subdirsAll = [ name for name in os.listdir(var.DFTconfigsDFTNPTpath) 
                   if os.path.isdir(os.path.join(var.DFTconfigsDFTNPTpath, name)) ]
                temperatures_done = [int(subdir.replace("temp", "")) for subdir in subdirsAll]

                # Choose an appropriate list of temperatures from:
                temperature_sets = [ [7500, 10000], [12500, 15000, 17500, 20000] ]
                selected_temperatures = None
                for temperature_set in temperature_sets:
                    if all(temp in temperatures_done for temp in temperature_set):
                        continue
                    else:
                        selected_temperatures = temperature_set
                        break
                if selected_temperatures is None:
                    print("  ERROR: liquid not detected for any considered high temperatures, STOPPING")
                    quit()

                print("  Increasing temperature range to find liquid:", selected_temperatures)
                newJobDirs = DFTsetup.setupMDconfigsdir(var.DFTconfigsDFTNPTdir, selected_temperatures, "temp", 
                    copyPOTCARfrom, "TEBEG")
                processJobs.runOrStageJobs(var.DFTconfigsDFTNPTdir, jobdirsLocal=newJobDirs)
                if var.stopAPD == True:
                    return

            # Need high temperature solid.First determine 'liquidTemp', the lowest temperature where liquid occurs
            for liquidIndex, liquidTemp in enumerate(temperatures):
                if (
                    min_minDivAvrg[liquidIndex] > 0.3
                ):  # Use minimum value of minDivAvrg across all species combinations to ensure we are not just in a situation where
                    # some atoms are diffusing while others are still vibrating around their solid positions
                    break
            print("  Lowest temperature at which liquid appears in DFT:", liquidTemp)

            # Ensure at least one temperature is in range: (var.fracLiqTemp-0.1)*liquidTemp > temp > 
            # (var.fracLiqTemp+0.1)*liquidTemp (a 'high temperature' solid)
            highTempSolidDetected = False
            print("  Checking if we have a high temperature solid...")
            for temp in temperatures:
                print( "  Comparing ", temp, ", with lower and upper limits: ",
                    int((var.fracLiqTemp - 0.1) * liquidTemp), " and ", int((var.fracLiqTemp + 0.1) * liquidTemp) )
                if temp >= int((var.fracLiqTemp - 0.1) * liquidTemp) and temp <= int(
                    (var.fracLiqTemp + 0.1) * liquidTemp ):
                    highTempSolidDetected = True
                    highTempSolidTemp = temp
                    print("  High temp solid detected")
                else:
                    print("  No high temp solid detected")

            if highTempSolidDetected == False:
                temperatures = [var.fracLiqTemp * liquidTemp]
                DFTsetup.setupMDconfigsdir(var.DFTconfigsDFTNPTdir, temperatures, "temp", copyPOTCARfrom, "TEBEG")
                print( "  No high temperature solid calculation detected: generating new high temperature DFT job" \
                    " for solid phase..." )
                processJobs.runOrStageJobs(var.DFTconfigsDFTNPTdir)
                if var.stopAPD == True:
                    return
            else:
                print("  High temperature solid calculation detected: no need for further jobs in this stage")

        corrTimeVsTemp = DFTsetup.genUncorr(var.DFTconfigsDFTNPTdir, var.DFTconfigsDFTNPTpath)

        # Record to file the name of subdirectories to be passed onto stage 5 (this writes to: var.DFTconfigsDFTNPTpath+"/stageOutput")
        if var.auto_select_temp == True:
            if len(var.temp_range) == 1:
                filesys.recordTemps(
                    var.temp_range[0],
                    0,
                    highTempSolidTemp,
                    temperatures[liquidIndex],
                    temperatures[liquidIndex + 1],
                    corrTimeVsTemp,
                )
            else:
                filesys.recordTemps(
                    var.temp_range[0],
                    var.temp_range[1],
                    highTempSolidTemp,
                    temperatures[liquidIndex],
                    temperatures[liquidIndex + 1],
                    corrTimeVsTemp,
                )
        else:
            if len(var.temp_range) == 1:
                filesys.recordTemps(var.temp_range[0], 0, 0, 0, 0, corrTimeVsTemp)
            else:
                filesys.recordTemps(var.temp_range[0], var.temp_range[1], 0, 0, 0, corrTimeVsTemp)
        # Also write file 'stageOutput' to var.DFTconfigsDFT{NPT,NVT}path. This signals to other xc of the same material that we can use the same configs to generate
        # training data (we do not use stageOutput in main stage directory. instead we look for it in each subdirectory present. hence if potMD level prompts a return to this stage if new configs need relabelling with DFT it just has to make a new directory

        # Write POSCAR, INCAR, KPOINTS and POTCAR. Write this to the base directory for 4_configs. This is to enable ready access in case
        # we need later to set up 'bad configs' from lammps QbyC or failed lammps runs. Having the DFT inputs here for the correct cell
        # size (the larger size we use for both DFT and potential-MD) allows easy set up of the DFT parameters (KPOINTS and INCAR for VASP)
        # corresponding to this size. c.f. DFTsetup.setupDFTforRelabelling(). (orignally i had this a level of indentation back... not sure why...)
        # First convert those CASTEP inputs which we have been storing in VASP objects (e.g. var.incar) back to either CASTEP objects (e.g. var.calc.param) or variables e.g. var.magmoms, which we will use subsequently here
        if var.dft_engine == "CASTEP":
            var.atoms = aio.AseAtomsAdaptor.get_atoms(var.structure) # writeDFTinputFiles requires 'atoms' where CASTEP is used
            DFTsetup.convertVASPtoCASTEP2()  # updates var.calc, and defines var.magmoms, and var.ldauu etc.
        DFTsetup.writeDFTinputFiles(
            var.DFTconfigsdir, var.DFTconfigspath, var.DFTinputpath
        )  # used to take POTCAR from: var.DFTMDparaspath, but there is no POTCAR there... (only in subdirs). from DFTinputpath is same
        outf.printt(2, "  Written vasp input files to " + var.DFTconfigsdir)

    # some stuff maybe useful:
    #corrTimeVsTemp = DFTsetup.genUncorr(var.DFTconfigsDFTNVTdir, var.DFTconfigsDFTNVTpath)

    #outf.printt(1, "  Sufficient configurations to begin fitting.")
    #directoriesFile = open(var.DFTconfigsDFTNVTpath + "/stageOutput", "w")
    #directoriesFile.write("DFT NVT MD jobs completed.\n")
    #directoriesFile.write("Correlation time per temp: " + str(corrTimeVsTemp) + "\n")
    #directoriesFile.close()
    #outf.printt(2, "  Generated " + var.DFTconfigsDFTNVTdir + "/stageOutput file")


def DFTtrainingData(xc, mat):
    
    """
    This stage calculates DFT training data using 'high DFT parameters' (either optimized in stage 2 or taken from our 
    adapted MPRelax parameters). First, DFT calculations using the results of 4_configs are performed (by default MD 
    runs using the adpated MPRelax settings rather than reduced DFT parameters). Next we look for structures sent back
    from the potMD level for recalculation using DFT.
    """

    var.incar["LREAL"] = False # faster for larger cells, and should be stable here as encut not too high (c.f. highElecParas...) 

    # ---- Initial training data ----
    if var.isDFTtrainingDatapath == False:  # Set up stage

        outf.printt( 1, f"  Training data to be read-in from: {var.DFTconfigsdir}" )

        filesys.createWorkdir(var.DFTtrainingDatadir)
        filesys.createWorkdir(var.DFTtrainingDataInitialdir)

        # Read configs from stage 4 for re-calculation using higher parameters. Alternatively if new calcs aren't 
        # necessary just copy the DFT output files over from var.DFTconfigsDFTNPTpath (if using reduced method and/or
        # if dft_energy_tol specified) or from the calculation directory (if var.config_gen = "DFT_NPT_EXISTING")

        if "REDUCED" in var.config_gen or var.dft_energy_tol is not None:  # read in configs

            # Resize cell and kpoints (but unlike 4_configGen here we don't apply the divisor to the kpoints)
            DFTsetup.resizeCell( var.natomTargetMin_MD, var.natomTargetMax_MD, True )
            # Increase supercell size (and adjust k-points) if neccessary. Note unlike the last stage
            # we do not apply the divisor in reduced case.

            # Adjust INCAR for single shot jobs (ISIF=2)
            var.incar["ISIF"] = 0
            var.incar["NSW"] = 0
            var.incar.pop("LANGEVIN_GAMMA", None)
            var.incar.pop("LANGEVIN_GAMMA_L", None)
            var.incar.pop("PMASS", None)

            if var.dft_energy_tol is not None:

                # Read in energy cut off
                outf.printt(2, "  Reading in encut from 2_nonMD/highElecParas")
                stageOutputFile = open(os.path.join(var.DFTnonMDhighElecParaspath, "stageOutput"))
                lineReadin = stageOutputFile.readline()
                lineReadin = stageOutputFile.readline()
                parts = lineReadin.split()
                print('lineReadin=',lineReadin,', parts=',parts)
                var.incar["ENCUT"] = int(parts[1])
                outf.printt(2, f'    ...set to: {var.incar["ENCUT"]}')

                # Read in kpoints multiplier
                outf.printt(2, "  Reading in multiplier from 2_nonMD/highElecParas to rescale kpoints")
                lineReadin = stageOutputFile.readline()
                stageOutputFile.close()
                parts = lineReadin.split()
                kpointsMultiplier = float(parts[2])
                print("kpointsMultiplier=", kpointsMultiplier)
                outf.printt(2, "  Kpoints before rescaling: " + str(var.kpoints.kpts[0][0:3]))
                for i in range(3):
                    var.kpoints.kpts[0][i] = int(math.ceil(var.kpoints.kpts[0][i] * kpointsMultiplier))
                outf.printt(1, "  Kpoints after rescaling: " + str(var.kpoints.kpts[0][0:3]))

            DFTsetup.copyStrucsFromDFTconfigs()
            processJobs.runOrStageJobs(var.DFTtrainingDataInitialdir)
            if var.stopAPD == True:
                return

        else:

            for file in glob.glob(var.DFTconfigsDFTNPTpath + "/" + var.DFTprefix + "*"):
                shutil.copy2(file, var.DFTtrainingDataInitialpath)
            shutil.copy2(var.DFTconfigsDFTNPTpath + "/fitdbse", var.DFTtrainingDataInitialpath)

    # -------------------------------------------------------------

    # ---- Process computed data ----
    if (not os.path.isfile(os.path.join(var.DFTtrainingDataInitialpath, "stageOutput")) and 
            ("REDUCED" in var.config_gen or var.dft_energy_tol is not None) and var.config_gen != "DFT_NPT_EXISTING"
            and var.config_gen != "DFT_NVT_EXISTING"):
        # read in configs
        outf.printt(1, "  Processing DFT data in: " + var.DFTtrainingDatadir)
        # Check which temp/ directories have incompleted config/ jobs which need resubmitting, and which temp/
        # directories have a completed set of config/ jobs
        # format of directories is e.g: 5_trainingData/initial/temp1000/bulk/config11, and we want to send the list:
        # containing ...temp#/bulk/ to checkDFToutput (the latter will then treat config# as subdirs and process them)
        subdirs = [subsubdir for subdir in glob.glob(os.path.join(var.DFTtrainingDataInitialdir, "temp*/")) for subsubdir in glob.glob(os.path.join(subdir, "*/"))]
        print('  subdirs identified: ',subdirs)
        var.tempDefectdirs = DFTanalysis.checkDFToutput(subdirs) # just made this change
        if var.incompleteJobs or var.notStartedJobs:
            outf.printt(1, "  Incomplete jobs. Determining which jobs need resubmitting, then APD will stop.")
            var.stopAPD = True
        # Stage new jobs if incomplete, or exit if failed jobs
        check_directories(var.DFToutput[0], False)
        if var.stopAPD == True:
            return

        # Concatenate DFT
        for tempDefectdir in var.tempDefectdirs:
            DFTsetup.concatenateDFT(tempDefectdir)
        # now fetch
        # we have var.tempdirs=APDworkdir/DFT/pbe/mp-3834/5_trainingData/initial/temp{1000,2000,...} but following routine needs just temp{1000,2000,...}/
        var.tempDefectdirsReduced = []
        for direc in var.tempDefectdirs:
            split = direc.split("initial/")
            var.tempDefectdirsReduced.append(split[1][:-1])  # split[1] gives temp#/defect#/, [:-1] to remove the final '/'.
        DFTanalysis.fetchDFToutput( var.DFTtrainingDataInitialdir, var.DFTtrainingDataInitialpath, 
            var.tempDefectdirsReduced, None ) # this sub needs adapting for: CASTEP; remove error checking 
                # (actually, probably retain it, since this code will also act on castep files from high DFT MD 
                # jobs which wouldn't have been checked yet...)
        if var.stopAPD == True:
            return

        potSetup.generateFitdbse(var.DFTtrainingDataInitialpath, 1.0)
        if var.stopAPD == True:
            return

    outf.printt(1, "  Initial DFT training data set up.")
    with open(os.path.join(var.DFTtrainingDataInitialpath, "stageOutput"), 'w') as directoriesFile:
        directoriesFile.write("DFT training data complete.\n")
    outf.printt(2, "  Generated " + var.DFTtrainingDataInitialdir + "/stageOutput file")
    # -------------------------------

    # ---- 'Bad' DFT configs returned from the potMD level ----

    # Check if there are any stage 5 single shot DFT jobs to complete. These are only required on a per potential basis if a given
    # potential fails during an MD run, or later, as part of active learning
    var.singleShotdirs = []  # Directories with single shot DFT calculations are added to this
    for potType in var.pot_types:
        outf.printt(
            2,
            "  Initial configs training-data calculation completed. Checking for incomplete single shot DFT jobs returned from the potMD level...",
        )
        for path in glob.glob(var.DFTtrainingDataBadpath + "/" + potType + "*/"):
            if not os.path.isfile(path + "/stageOutput"):
                split = path.split("APDworkdir")
                direc = "APDworkdir" + split[1]
                var.singleShotdirs.append(direc)

    if not var.singleShotdirs:  # If list is empty
        outf.printt(2, "    ...none found; DFT level completed for this xc and mat")
        return

    var.singleShotdirs = DFTanalysis.checkDFToutput(var.singleShotdirs)  # Remove potential directories without a complete set of vaspruns in their subdirectories
    if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
        actionIncompleteJobs(
            var.DFToutput[0], False
        )  # Check if any jobs need resubmitting (and/or 'cont' directories set up)
    if var.stopAPD == True:
        return
    outf.printCitation("VASP")

    # The jobs here have already been staged/scheduled by stage 8. Only need to copy and rename vaspruns and set up a fitdbse file

    # Post analysis on vasprun files to ensure no NaN's, and that there are sufficient DFT data. Copy and rename vasprun files to the DFTconfigsdir/ and generate
    # a fitdbse (MEAMfit) training set file in the process

    for singleShotdir in var.singleShotdirs:  # iterate over var.DFTtrainingDataBaddir/EAM1/ etc
        words = singleShotdir.split("/")
        print("(words=", words, ")")
        if var.verbosity > 1:
            outf.underline(words[-2] + ":")

        # cp 5_trainingData/bad/EAM2B/1659/vaspruns to 5_trainingData/bad/EAM2B/vasprun_1659.xml etc
        # setup subdirs of files of form {1659,1660,...}
        subdirs = [
            name
            for name in os.listdir(var.dirpath + "/" + singleShotdir + "/")
            if os.path.isdir(var.dirpath + "/" + singleShotdir + "/" + name)
        ]
        print("  List of directories containing bad configs:", subdirs)
        DFTanalysis.fetchDFToutput(singleShotdir, var.dirpath + "/" + singleShotdir, subdirs, None)  # The 'None' is just meant to signal that we are not concerned with correlation (files copied like-for-like). used to have "" as second-last argument
        if var.stopAPD == True:
            return

        potSetup.generateFitdbse(var.dirpath + "/" + singleShotdir, 1.0)

        directoriesFile = open(var.dirpath + "/" + singleShotdir + "/stageOutput", "w")
        directoriesFile.write("Single-shot DFT jobs completed.\n")
        directoriesFile.close()
        outf.printt(2, "  Generated " + singleShotdir + "/stageOutput file")
    # ---------------------------------------------------------


def potFitOrTest(xc, pot, potItn, jobType):

    # Stage a potential fitting job (jobType='fit') or evaluate an existing potential on testing-set data (jobType='test'). For fitting, ISIF=2 data is use to fit,
    # for testing, ISIF=3 data is used. xc denotes the exchange-correlation of DFT which we are fitting to, pot is the type of potental (e.g. 'EAM', 'MEAM').

    if var.meamfit_so: import meamfit_py

    if jobType == "fit":
        direc = var.potOptdir
        path = var.potOptpath
        ispath = var.ispotOptpath
    elif jobType == "test":
        direc = var.potTestdir
        path = var.potTestpath
        ispath = var.ispotTestpath
        # We skip this stage at present for the case where sep_coulomb_pot is True, because in that case ISIF=3 jobs cannot be processed by LAMMPS
        # without further coding to adjust the non-diagonal lattice parameters (and LAMMPS is needed to calculate the Coulomb part).
        # UPDATE: this shouldn't be the case anymore since we now routinely use LAMMPS on non-orthogal cells; and we need 7_
        # as the renamed potentials stored there are called upon later.
        # skipStage = False
        # if var.sep_coulomb_pot_specified == True:
        #     if var.sep_coulomb_pot == True:
        #         print("Skipping testing of potential in ISIF=3 data (sep_coulomb_pot=True)")
        #         filesys.createWorkdir(direc)
        #         stageOutputFile = open(path + "/stageOutput", "w")
        #         stageOutputFile.write(
        #             "Potential optimization complete - skipping test set analysis (sep_coulomb_pot=True)\n"
        #         )
        #         stageOutputFile.close()
        #         return

    if ispath == False:
        # Stage and run fitting/analysis jobs
        filesys.createWorkdir(direc)
        os.chdir(path)
        if jobType == "fit":
            potSetup.setupFittingJob(xc, pot, potItn)
            processJobs.runOrStageJobs(direc)
        else:  # Rather than testing on holdout data we just repurpose (at least for now) the test sub-stage to do a single shot calc using MEAMfit to get for obervables for all structures (energies, forces, stresses, etc) - these currently aren't returned to the head-node in the parallel implementation (only the objective function contributions are)
            if "EAM" in pot or "MEAM" in pot: # For MEAMfit need separate call to get EFSs and their RMSEs
                potSetup.setupTestingJob( "MEAMfit" )
                outf.printt(2, "  Running MEAMfit to calculate observables for best potential...")
                if var.meamfit_so and not var.startedMPIforMEAMfit:
                    meamfit_py.start_mpi()
                    var.startedMPIforMEAMfit = True
                processJobs.runMEAMfit(path, False)
            else: # For MLIP need separate call to get EFSs
                potSetup.setupTestingJob( "MLIP2" )
                processJobs.runOrStageJobs(direc) # Unlike MEAMfit we run an external job here
        os.chdir( var.dirpath )  # return to calculation path. This is necc eg when using CASTEP because next level 
            # (potMD) may use pymatgen libraries again which require the castep_keywords.json file to be in the current 
            #directory

        if var.stopAPD == True:
            return

    # Analyse fitting/analysis jobs   FOLLOWING ought to be shunted into potAnalysis.py
    var.incompleteJobs = []
    var.notStartedJobs = []
    if jobType == "fit":

        outf.printt(2, f"  {direc} already exists, checking to see if fitting is completed:")

        optCompleted = potAnalysis.analyzeOpt(direc, path, pot)

        if optCompleted:
            return
        if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
            if 'EAM' in pot:
                actionIncompleteJobs( "output.0", True ) # Check if any cont jobs need resubmitting
            else:
                actionIncompleteJobs( "output_p.txt", False ) # Check if jobs need resubmitting (no cont jobs for MLIP)
            var.stopAPD = True
            return

    else:

        if "EAM" in pot or "MEAM" in pot:
            optimizer = "MEAMfit"
        else:
            optimizer = "MLIP2"
            # For MLIP2 we do run test jobs on the cluster, so need to check they are finished first
            potAnalysis.analyzeTest(direc, path)
            if len(var.incompleteJobs):
                actionIncompleteJobs( "temp.cfg", False )
                var.stopAPD = True
                return

        (rmsErrorEnergies, rmsErrorForces, rmsErrorStresses, rmsErrorEnergiesOverSdDFT, rmsErrorForcesOverSdDFT,
            rmsErrorStressesOverSdDFT) = potAnalysis.getObservables(optimizer, potItn)
        outf.printt( 1, f"  rms error of DFT for energies, forces, stresses: {rmsErrorEnergies}, {rmsErrorForces}, "
            f"{rmsErrorStresses}")
        outf.printt( 1, f"  rms error/s.d. of DFT for energies, forces, stresses: {rmsErrorEnergiesOverSdDFT}, "
            f"{rmsErrorForcesOverSdDFT}, {rmsErrorStressesOverSdDFT}")

        if "MTP" in pot:
           # Copy trained.mtp_ to trained.mtp_compositon (where composition is the species order in the file)
           # This provides extra bookkeeping useful for example when we need to reorder structure files in LAMMPS runs
           potSetup.amendMTPfilename(path)

        stageOutputFile = open(path + "/stageOutput", "w")
        # print('about to output to ',stageOutputFile)
        stageOutputFile.write("Potential testing complete.\n")
        stageOutputFile.write( f"rms error of DFT for energies, forces, stresses: {rmsErrorEnergies},"
            f"{rmsErrorForces}, {rmsErrorStresses}\n")
        stageOutputFile.write( f"rms error/s.d. of DFT for energies, forces, stresses: {rmsErrorEnergiesOverSdDFT},"
            f"{rmsErrorForcesOverSdDFT}, {rmsErrorStressesOverSdDFT}\n")
        stageOutputFile.close()

    return


def simulationUsingPotential(xc, mat, pot, xcFunc_potSim):

    """ 
    Stage MD simulation jobs using fitted potentials. This is for the purposes of active learning and/or evaluating 
    higher order properties, e.g. thermal expansion, melting point (not yet implemented). xc denotes the 
    exchange-correlation of DFT which we are fitting to, mat is the material we are running the simulation for, pot is 
    the type of potental (e.g. 'EAM1B', 'MEAM2').
    """

    # Define potential type
    if "MEAM" in pot:
        potType = "MEAM"
    elif "EAM" in pot:
        outf.printt(1, "  Copying lammps run script, lammpsIn_EAM_constP, from automated-potential-development and "
            "editing...")
        potType = "EAM"
    elif "MTP" in pot:
        potType = "MTP"
    else:
        outf.printt(0, "  Potential type not recognized in simulationUsingPotential, stopping")
        quit()

    # Currently when simulating using MTPs don't use query by commitee (later on for comparing different AL approaches
    # this can be generalized)
    qbc = var.query_by_committee == True and potType != "MTP"

    # Determine temperatures at which to run simulations. Consider the temperatures in 'temp_range' and also those read in from
    # user-supplied files to determine the range.
    minTempFromTempRange = min(var.temp_range)
    maxTempFromTempRange = max(var.temp_range)
    print('var.thermal_expansion=',var.thermal_expansion)
    if var.thermal_expansion == None:
        minTempForSim = minTempFromTempRange
        maxTempForSim = maxTempFromTempRange
        outf.printt( 2, f"  Temperature range for simulations from temp_range: min={minTempForSim}; max="
            f"{maxTempForSim}" )
    else:
        minTempFromExp = 10000  # arbitraly large and small vals respectively
        maxTempFromExp = -10000
        # Read temperatures and experimental thermal expansion from thermal_expansion in settings.APD
        # update: just find lowest and highest temp and use these to define a range
        for thermal_expansion_set in var.thermal_expansion:
            print('thermal_expansion_set=',thermal_expansion_set)
            for tempThermExpnPair in thermal_expansion_set:
                print('tempThermExpnPair=',tempThermExpnPair)
                if tempThermExpnPair[0] > maxTempFromExp:
                    maxTempFromExp = tempThermExpnPair[0]
                if tempThermExpnPair[0] < minTempFromExp:
                    minTempFromExp = tempThermExpnPair[0]
        minTempForSim = min(minTempFromExp, minTempFromTempRange)
        maxTempForSim = max(maxTempFromExp, maxTempFromTempRange)
        outf.printt(2, "  Temperature range for simulations determined from temp_range and temperatures of read-in "
            f"thermal expansions: min={minTempForSim}; max={maxTempForSim}")

    # Take temperatures from temp_range. Draw 20 temperatures equally spaced. If temp_range is just one value instead use that
    temperatures = []
    if minTempForSim == maxTempForSim:
        temperatures.append(minTempForSim)
    else:
        max_num_temps = 15
        total_range = maxTempForSim - minTempForSim
        step_size = max(100, ceil(total_range / max_num_temps / 100) * 100)
        upper_val = ceil(maxTempForSim / 100) * 100
        temperatures = list(range(floor(minTempForSim / 100) * 100, upper_val, step_size))
        if temperatures[0] < minTempForSim:
            temperatures[0] = minTempForSim
        if temperatures[-1] < maxTempForSim:
            temperatures.append(maxTempForSim)

    if os.path.exists(var.potMDsimulpath) == False:
        potMDsetup.setupPotMDjobs(xc, mat, pot, xcFunc_potSim, potType, temperatures) # i don't think 'mat' is needed 
            # here. also probably not needed even in the call to simulationUsingPotential, but check...
        processJobs.runOrStageJobs(var.potMDsimuldir)
        if var.stopAPD == True:
            return

    # ---- Process output ----

    # Check if jobs have finished
    temperatures.sort(reverse=True)
    directories = [var.potMDsimuldir + "/temp" + str(temp) for temp in temperatures]
    finisheddirs = potMDanalysis.checkPotMDoutput(directories, potType, True) # Check if job is finished. if so add the dir to 
        # finisheddirs. Note: this directory needs to be written similar to DFTanalysis.checkDFToutput.  True means to 
        # also check for the .rdf file
    if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
        actionIncompleteJobs(var.potMDoutput, False)
    if var.stopAPD == True:
        return

    if not os.path.isfile(os.path.join(var.potMDsimulpath, "stageFailed")): # We check this here because if we use MTPs
            # then later in this function we submit a maxvol job to the cluster, and don't need to repeat all the
            # following again on a re-call to this function on a subsequent running of APD.

        # Construct new list of temperatures based on those jobs that have completed. Note that unlike elsewhere in the code we also
        # analyze the failed jobs (those for which 'ERROR' or '...Breaking threshold...' is found above in log.lammps).
        temperatures = []
        for direc in finisheddirs:
            numbers = re.findall("[0-9]+", direc)
            temperatures.append(int(numbers[-1]))
        # following shouldn't be needed, as 'temperatures' list already returns just those for which job has completed
        # (though completion can mean 'successful' completions as well as 'failed' or 'threshold met' completion)
        #for direc in var.failedJobs: # for potMD jobs, and unlike elsewhere in code, failed jobs are also processed and 
        #        # can be used in active learning
        #    numbers = re.findall("[0-9]+", direc)
        #    temperatures.append(int(numbers[-1]))
        outf.printt(2, f"  Temperatures after removing jobs that are incomplete:{temperatures}")

        # Determine natoms and or nspecies and molMass for rescaling thermal expansion
        lines = open(var.potMDsimulpath + "/POSCAR_after_ordering.lmp", "r").readlines()
        words = lines[2].split()
        natoms = int(words[0])
        if var.thermal_expansion_units == "gcm-3":
            # Total molecular mass and nspecies required for mass density. Shouldn't this weight the mass according to 
            # species populations?
            words = lines[3].split()
            nspecies = int(words[0])
            # print('natoms=',natoms,' nspecies=',nspecies)
            molMass = 0
            masses_line_index = next((i for i, line in enumerate(lines) if "Masses" in line), None)
            for ispecies in range(nspecies):
                words = lines[masses_line_index + 2 + ispecies].split()
                molMass = molMass + float(words[1])

        os.chdir(var.potMDsimulpath)
        Lx = []
        Ly = []
        Lz = []
        thermExpnOut = ""
        if var.thermal_expansion_units == "gcm-3":
            thermExpnOut += "  temp | {Lx} | {Ly} | {Lz} | {vol} | density (g cm-3)\n"
        else:
            thermExpnOut += "  temp | {Lx} | {Ly} | {Lz} | {vol} | length (Ang)\n"
        badCfgs_QbyC = [] # These are used if query by committee is being used. Stores indices of configurations from a
                # LAMMPS run for which s.d. of predictions of energy from different potentials is beyond a threshold.
                # Starting from the highest temperature the first LAMMPS run containing such 'bad' configurations is used 
                # to populate this list (no further lower temperatures are then considered). The temperature is stored as
                # temp_qbc.
        sigma_badCfgs_QbyC = []
        # badCfgs_DoC = [] # can eventually implement this for D-optimality criterion (for MTP potentials). However here
                # the list should also index the temperature as all temperatures should be considered (MLIP2 removes
                # 'similar' atomic configurations so there is no need to artificially limit the number of considered 'bad'
                # configs prior to this screening. We could also possibly generalize badCfgs_QbyC to multiple temperatures
                # and then by comparing we would have a tool to check the efficacy of the two approaches. Note we would
                # need to manually determine the configurations indicies 'picked' my MLIP2 as it does not record them. E.g
                # the command: 
        temperatures.sort(reverse=True)
        lmpErrOverall = False # set to true if there is a LAMMPS error for at least one temp
        computeThermExpnCalled = False

        # For MTPs, do an initial check over temps to see if any configs need relabelling. This will inform if we do
        # the (expensive) S(q,w) for each temperature. Note that code should be changed so that we do this check also
        # for M/EAM, however that will need some work disentangling the QbC stuff from observable calculation in
        # computeThermExpn (see below). (At present, as written, S(q,w) will get computed in case of M/EAM for any
        # temp for which there are no bad configs, even if there are other temps with bad configs for that potential)
        configsForRelabelling = False # used for mtps
        configsForRelabellingList = []
        if potType=="MTP":
            configsForRelabelling = False
            for temp in temperatures:
                tempDirec = "temp" + str(temp)
                configsForRelabellingThisTemp = os.path.isfile(os.path.join(var.potMDsimulpath,tempDirec, 'out',
                        'preselected.cfg'))
                configsForRelabellingList.append(configsForRelabellingThisTemp)
                if configsForRelabellingThisTemp:
                    configsForRelabelling = True
        else:
            configsForRelabelling = None # Currently not used for M/EAM

        for i, temp in enumerate(temperatures):
            tempDirec = "temp" + str(temp)
            outf.printt(1, "  Checking " + tempDirec + ":")
            outf.printt(1, "    Checking LAMMPS output in: " + tempDirec)

            if potType=="MTP":
                configsForRelabellingThisTemp = configsForRelabellingList[i]

            if potType=="EAM" or potType=="MEAM" or (potType=="MTP" and not configsForRelabellingThisTemp): # only need to 
                    # compute thermal expansion for MTP if no configs have been identified for relabelling
                # The following mixes up qbyc with calculation of thermal expansion. Should rewrite at some point to
                # separate out these functionalities.
                os.chdir(tempDirec)
                # In the following we compute badCfgs by looking at the failed part of the lammps run, as well as by using QbyC
                # (badCfgs_QbyC_perTemp). We now only use the latter in determining the configs to relabel using DFT.
                (converged, LxAvrg, LyAvrg, LzAvrg, volAvrg, lmpErr, badCfgs, badCfgs_QbyC_perTemp, 
                        sigma_badCfgs_QbyC_perTemp) = potMDanalysis.computeThermExpn(qbc, xc, mat, pot, temp,
                        configsForRelabelling) # the last is used to determine if S(q,w) is calculated (only if no bad
                                # configs over all temps)
                computeThermExpnCalled = True
                if var.stopAPD == True:
                    return  # This will occur if e.g. there is no log.lammps file, or the partial.rdf file hasn't yet been 
                            # written. In this case stop the current stage and go onto stage 8 for the next xc/mat/pot 
                            # combination
                if lmpErr:
                    lmpErrOverall = True
                outf.printCitation("PymbarDetectEq")
                os.chdir("../")
                if converged == False:
                    outf.printt(1, "    LAMMPS run did not converge (configurations unlikely to be of use): moving to next "
                            "lower temperature") # If the MD is not converged (Lx or toteng not converged within first 10,000 
                             	# configs), something must have gone wrong with the MD run and the configs are unlikely to be 
                             	# useful for relabeling, therefore skip to the next lowest temp
                else:

                    if var.stopAPD == True:
                        return
                    if lmpErr == False:
                        if var.thermal_expansion_units == "gcm-3":
                            thermExpnPred = (natoms / nspecies) / (LxAvrg * LyAvrg * LzAvrg) * 10 * molMass / 6.0221409
                        else: # Thermal expansion in Ang by default
                            thermExpnPred = (volAvrg * var.nAtomsInitial / natoms) ** (1 / 3)
                            outf.printt(2,f"    Computing thermExpnPred using volAvrg={volAvrg}, var.nAtomsInitial="
                                f"{var.nAtomsInitial}, natoms={natoms} so that thermExpnPred= {thermExpnPred}")
                            # Here we want to convert to the lattice parameter of the primitive cell
                        thermExpnOut += (f"  {temp} {LxAvrg} {LyAvrg} {LzAvrg} {volAvrg} {thermExpnPred}\n")
                    if badCfgs_QbyC_perTemp:
                        # Only configs from the first temperature with bad configs gets stored for QbyC
                        badCfgs_QbyC = badCfgs_QbyC_perTemp
                        sigma_badCfgs_QbyC = sigma_badCfgs_QbyC_perTemp
                        temp_direc_QbyC = tempDirec
                        break

        if qbc and not badCfgs_QbyC and lmpErrOverall:
            print("  ERROR: Error in at least one LAMMPS file, yet no bad configs found by Q by C for any temperatures.")
            quit()
        if computeThermExpnCalled and not converged:
            outf.print(1, "  ERROR: converged LAMMPS MD not found for any temperature considered - either not enough "
                "temperatures considered, or need to reoptimize potential from scratch") # former: extend code to support 
                    # more temps by default, if experimental data is not provided. latter: implement this if it is 
                    # necessary. probably won't be; fixing former ought to solve problem...
            quit()
        outf.printt(1, "")
        outf.printt(1, "  Completed check over temperatures")
        if qbc:
            outf.printt(1, f"  Bad configs near end of failed LAMMPS runs: {badCfgs} (we will not relabel these), bad configs "
                f"detected by QbyC: {badCfgs_QbyC}")
        elif computeThermExpnCalled:
            outf.printt(1, f"  Bad configs near end of failed LAMMPS runs: {badCfgs} (we will not relabel these)")

        if badCfgs_QbyC or (potType == 'MTP' and configsForRelabelling):
            outf.printt(1, "  Bad configs detected: isolating configurations...")# and staging DFT jobs")
            if badCfgs_QbyC: outf.printt(2, "  Configs to recompute (as determined by QbyC): " + str(badCfgs_QbyC))
            stageFailedFile = open(var.potMDsimulpath + "/stageFailed", "w")
            stageFailedFile.write("Potential MD job failed: isolating configurations to be relabeled by DFT")# and staging DFT jobs")
            stageFailedFile.write(thermExpnOut)
            stageFailedFile.close()

            # Set up DFT jobs to recompute these configs (the idea is that, computed using DFT, we can use this new data 
            # to refit the potential)
            potOld = pot  # pot is changed to eg EAM1->EAM1B in the call below, when setting up the new DFT directory 
                    # (labelled according to the new potential)
            if potType == 'MTP':
                # Select from configs determined using D-optimality criterion those to relabel
                potMDanalysis.concatBadMTPconfigs(temperatures) # Concatenate out/preselected.cfg files from each temp#/
                # badCfgs_DoC_perTemp, grade_badCfgs_DoC_perTemp = potMDanalysis.getBadMTPconfigs() # eventually
                potMDanalysis.setupMaxVolJob()# Run: mlp select_add pot.mtp train_set.cfg preselected.cfg new_selected.cfg 
                processJobs.runOrStageJobs(var.potMDsimuldir) # Unlike MEAMfit we run an external job here
                if var.stopAPD == True:
                    return
            else:
                pot = DFTsetup.setupDFTforRelabelling(potType, xc, mat, pot, temp_direc_QbyC, badCfgs_QbyC)

    # Following will run if configs for relabelling have been identified. For M/EAM it will follow on directly after
    # the above, while for MTPs it will run on a restart of APD (the above block of code will result in an exit from
    # this function if configs to relabel have been detected so that the max vol job can run)
    if os.path.isfile(os.path.join(var.potMDsimulpath, "stageFailed")):

        if potType == 'MTP':
            # Check here to see if the mlip max vol job has completed...
            potMDanalysis.checkMaxVolOutput(var.potMDsimuldir)
            if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
                actionIncompleteJobs(var.maxVolOutput1, False)
            if var.stopAPD == True:
                return
            potOld = pot
            pot = DFTsetup.setupDFTforRelabelling(potType, xc, mat, pot)  # we return 'pot' here (which  e.g. if is 
                # EAM1 initially becomes EAM1B) for use in following line. Note for MTPs configs from all temps
                # are used for relabelling, and badCfgs are stored in .cfg, hence last two arguments not specified

        processJobs.runOrStageJobs(var.DFTtrainingDataBaddir + "/" + pot)  # This needs to be done after stage_8 has 
                # been called for all potential# directories, otherwise earlier potentials are double counted with 
                # */*/* etc
        pot = potOld  # return to the current potential to output stageFailed file
        initialize.initDirsPaths(xc, mat, pot)
        # Update stageFailed to include that DFT jobs have been staged
        stage_failed_path = os.path.join(var.potMDsimulpath, "stageFailed")
        with open(stage_failed_path, "r") as file:
            lines = file.readlines()
        lines[0] = "Potential MD job failed: isolated configurations to be relabeled by DFT and staged DFT jobs\n"
        with open(stage_failed_path, "w") as file:
            file.writelines(lines)
        if var.stopAPD == True:
            return

    # At this point all temp#/ directories containing finished jobs have been found to be succesfully completed and 
    # without needing DFT relabelling. However we need to first check that there weren't any unfinished jobs:
    if len(var.incompleteJobs) > 0 or len(var.notStartedJobs) > 0:
        outf.printt(2, "  ...still jobs left to finish (start?) - stopping stage without generating stageOutput file")
        var.stopAPD = True
    if var.stopAPD == True:
        return

    if (qbc and not badCfgs_QbyC): # < add second conditional for mtps. e.g. if potType=='MTP' and ... = True
        if var.verbosity > 0:
            print("    Thermal expansion:")
            print(thermExpnOut)

    directoriesFile = open(var.potMDsimulpath + "/stageOutput", "w")
    directoriesFile.write("potential MD jobs completed. Thermal expansion:\n")
    directoriesFile.write(thermExpnOut)
    directoriesFile.close()
    outf.printt(2, "  Generated " + var.potMDsimuldir + "/stageOutput file")


def staticsUsingPotential(xc, mat, pot, xcFunc_potSim):
    # Various non-MD calculations broadly termed 'statics' (although it does include geometric optimization)

    # Define potential type
    if "MEAM" in pot:
        potType = "MEAM"
    elif "EAM" in pot:
        outf.printt(1, "  Copying lammps run script, lammpsIn_EAM_constP, from automated-potential-development and "
                "editing...")
        potType = "EAM"
    elif "MTP" in pot:
        potType = "MTP"

    # Geometry optimization
    if var.ispotMDnonMDpath == False:
        potMDsetup.setupGeomOptdir(pot, potType)
        processJobs.runOrStageJobs(var.potMDnonMDgeomOptdir, "")  # launch or instruct user to launch jobs
        if var.stopAPD == True:
            return  # Make sure all sets of kpts and encuts in the current 'iteration' are running before we analyze
    if os.path.exists(var.potMDnonMDgeomOptpath + "/stageOutput") == False:
        finisheddirs = potMDanalysis.checkPotMDoutput([var.potMDnonMDgeomOptdir], potType, False)  # Check if job is 
                # finished. if so add the dir to finisheddirs. Note: this directory needs to be written similar to 
                # DFTanalysis.checkDFToutput.  False: we are not looking also for an rdf file
        if not finisheddirs:
            check_directories(var.potMDoutput, False)
        if var.stopAPD == True:
            return
        potMDanalysis.writeGeomToOutputdir(xc, mat, pot)  # Write pot geometry information to file in outputdir
        directoriesFile = open(var.potMDnonMDgeomOptpath + "/stageOutput", "w")
        directoriesFile.write("Geometry optimization complete.\n")
        directoriesFile.close()

    if var.ispotMDnonMDphononpath == False:
        potMDsetup.setupPhonondir(xc, mat, pot, potType)
        directoriesFile = open(var.potMDnonMDphononpath + "/stageOutput", "w")
        directoriesFile.write("Phonon calculation complete.\n")
        directoriesFile.close()

    if var.ispotMDnonMDelasticpath == False:
        potMDsetup.setupElasticdir(potType)
        processJobs.runOrStageJobs(var.potMDnonMDelasticdir, "")  # launch or instruct user to launch jobs
        if var.stopAPD == True:
            return
    if os.path.exists(var.potMDnonMDelasticpath + "/stageOutput") == False:
        finisheddirs = potMDanalysis.checkPotMDoutput([var.potMDnonMDelasticdir], potType, False)  # Check if job is 
                # finished. if so add the dir to finisheddirs. Note: this directory needs to be written similar to 
                # DFTanalysis.checkDFToutput.
        if not finisheddirs:
            check_directories(var.potMDoutput, False)
        if var.stopAPD == True:
            return
        potMDanalysis.postProcessElastic(xc, mat, pot)  # Write pot elastic information to file in outputdir
        directoriesFile = open(var.potMDnonMDelasticpath + "/stageOutput", "w")
        directoriesFile.write("Elastic calculation complete.\n")
        directoriesFile.close()

    if var.ispotMDnonMDevpath == False:
        potMDsetup.setupEvdir(potType)
        processJobs.runOrStageJobs(var.potMDnonMDevdir, "")  # launch or instruct user to launch jobs
        if var.stopAPD == True:
            return
    if os.path.exists(var.potMDnonMDevpath + "/stageOutput") == False:
        finisheddirs = potMDanalysis.checkPotMDoutput([var.potMDnonMDevdir], potType, False)  # Check if job is 
                # finished. if so add the dir to finisheddirs. Note: this directory needs to be written similar to 
                # DFTanalysis.checkDFToutput.
        if not finisheddirs:
            check_directories(var.potMDoutput, False)
        if var.stopAPD == True:
            return
        potMDanalysis.postProcessEv(xc, mat, pot)  # Write pot ev information to file in outputdir
        directoriesFile = open(var.potMDnonMDevpath + "/stageOutput", "w")
        directoriesFile.write("E vs V curve complete.\n")
        directoriesFile.close()


def setupAPDworkdir():
    
    if var.isAPDworkpath == False:
        outf.printt(1, "  Setting up " + var.DFTdir + " using input from settings.APD...")

        if var.mp_id == None:
            print("  No materials project id specified - please specify in settings.APD file with mp_id tag")
            print("  Alternatively you may use your own VASP inputs. If so please set up the following directory/s:")
            print("    APDworkdir/DFT/xc/mat//1_input/")
            print("  where xc denotes exchange-correlation functional and mat is material type (e.g. xc=pbe, mat=FeC)")
            print()
            print("  E.g., if you were running with PBE functional for nickel: mkdir -p APDworkdir/DFT/pbe/Ni/1_input")
            print()
            print(
                "  and where you may have multiple xc/ and multiple mat/ (but each xc/ should contain the same set of"
            )
            print("  mat/ folders). Copy the relevant DFT input files to each subsubdirectory, and rerun APD.")
            quit()
        DFTsetup.setupDFTinputdir(var.xcFunc)
        processJobs.endStage()
    else:
        outf.printt(2, "  Reading xcfuncs and materials from directory structure...")
        (
            xcFunc_fromdir,
            mat_fromdir,
        ) = initialize.determineXcMat()  # Determine xc-functionals and materials from directory structure
        outf.printt(1, "  xcFunc/s, material/s = " + str(xcFunc_fromdir) + ", " + str(mat_fromdir))
        if var.mp_id == None:
            var.materials = mat_fromdir  # Where no mp_id is specified to determine material/s, take the materials from the directory structure (which the user set up)

        outf.printt(2, "  Checking xc_func tag in settings.APD to see if new xc_funcs are to be added...")
        if var.xcFunc != None:
            outf.printt(2, "  xc_func flag found: xc_func=" + str(var.xcFunc))
            if set(var.xcFunc) != set(xcFunc_fromdir):
                # New xc-functionals have been specified in settings.APD. Set up directories for those that do not have corresponding directories.
                # (e.g. if our fitted potential yields a poor thermal expansion, new xc-funcs can be added to settings.APD to fit new potentials)
                xcFunc_toAdd = list(set(var.xcFunc) - set(xcFunc_fromdir))
                outf.printt(
                    1,
                    "  New xcFuncs specified in settings.APD: "
                    + str(xcFunc_toAdd)
                    + " - Setting up new directories in DFT phase",
                )
                DFTsetup.setupDFTinputdir(xcFunc_toAdd)
                var.xcFunc = xcFunc_fromdir + xcFunc_toAdd
            else:
                outf.printt(2, "  No new xc_funcs to be added")
        else:
            outf.printt(2, "    No xc_func tag found")
            var.xcFunc = xcFunc_fromdir
    outf.printt(1, "  Stage " + str(var.stage) + ": COMPLETED\n")


def closeDown():

    if var.meamfit_so:
        import meamfit_py
        if var.startedMPIforMEAMfit:
            meamfit_py.end_mpi()

    outf.printt(1, "")
    var.toAction.close()
    num_lines = sum(1 for line in open(var.dirpath + "/toAction.APD"))
    if num_lines > 1:
        outf.title("Please action items in toAction.APD file, if any, check inputs if necessary, then re-run APD")
    else:
        outf.title("APD finished: no jobs need to run by user")
        os.remove(var.dirpath + "/toAction.APD")
    var.jobdirs.close()
    var.jobdirs_notStarted.close()

    file_to_check = os.path.join(var.dirpath, "jobdirs")
    if os.path.isfile(file_to_check):
        if sum(1 for line in open(file_to_check)) == 0:
            os.remove(file_to_check)
    file_to_check = os.path.join(var.dirpath, "jobdirs_notStarted")
    if os.path.isfile(file_to_check):
        if sum(1 for line in open(file_to_check)) == 0:
            os.remove(file_to_check)
    outf.printt(1, "")
    quit()


def check_MTP_completion():
    
    # Check if all MTP optimizations are complete (involves checking the potMD stages to see if any more active
    # learning is necessary

    all_MTP_complete = True
    for pot_num in range(1, var.num_pots + 1):
       for xc in var.xcFunc:
           if var.mp_id == None: # user-imported DFT
               search_pattern = os.path.join(var.potMDpath, xc, '*', f'MTP{pot_num}*', '8_simulation/stageOutput')
               matching_paths = glob.glob(search_pattern)
               if not matching_paths:
                   all_MTP_complete = False
                   break 
           else:
               for mat in var.mp_id:
                   search_pattern = os.path.join(var.potMDpath, xc, mat, f'MTP{pot_num}*', '8_simulation/stageOutput')
                   matching_paths = glob.glob(search_pattern)
                   if not matching_paths:
                       all_MTP_complete = False
                       break 

    return all_MTP_complete 

# ------------#
# Code start  #
# ------------#


if __name__ == "__main__":

    var.init()  # var is an empty class which we use to contain variables, allowing to access from any function, e.g. var.stage.
    var.verbosity = 1 # Temporarily set to 1 so title can be printed. This is then adjusted in readSettings()
    outf.title("APD version: "+str(__version__)+", created by Andrew I. Duff (Copyright STFC 2024)")
    print()
    initialize.initVars()
    initialize.initMainDirsPaths()
    initialize.readSettings()
    initialize.elementLibrary()
    initialize.checkForMEAMfitso()
    var.toAction = open("toAction.APD", "w")
    var.toAction.write("Please action following items:\n")
    var.jobdirs = open("jobdirs", "w")
    var.jobdirs_notStarted = open("jobdirs_notStarted", "w")

    # Levels: DFT, potOpt, potMD

    # --- DFT level ---
    outf.printt(1, "")
    outf.title("DFT level")

    # initial setup:
    if var.verbosity > 1:
        outf.underline("Initial setup:")
    DFTsetup.setupDFTvariables()  # global variables like DFToutput[], so we can assign different names depending on the DFT engine

    # Stage 0 : generate some additional files that will be necessasry to use ASE and other libraries. Currently reserved for generating castep_keywords.json
    if var.dft_engine == "CASTEP":
        var.stage = 0
        checkGenCastepKeywords()
    else:
        var.stage = 1

    if var.stage == 1:
        # Stage 1
        var.stopAPD = False
        if var.verbosity >= 1:
            print("  Stage " + str(var.stage) + ": ", end="")
            print("  Setting up/processing " + var.APDworkdir)
        setupAPDworkdir()  # After this we will know which xc we are considering (those already which have directories and any potentially new xc's specified in settings)

    if var.stopAPD == True:
        closeDown()

    var.stage = 2

    # reorder xcFunc so PBE is front, this is so that the DFT MD parameter determination is carried out only for PBE. If not determination carried out for first in list
    if "pbe" in var.xcFunc:
        var.xcFunc.remove("pbe")
        var.xcFunc.insert(0, "pbe")
    var.firstXc = True  # This flag tells APD to perform the DFT MD parameter determination explicitly (rather than copy the results of another xc functional)
    if var.reuse_other_xc_configs == True:
        var.xcFunc_DFTconfPresent = DFTanalysis.xcFuncWithDFTconfig()

    for xc in var.xcFunc:
        var.vdw = False
        if xc.lower() == "revpbe" or xc.lower() == "optpbe" or xc.lower() == "optb88" or xc.lower() == "optb86b":
            var.vdw = True
        for mat in var.materials:
            initialize.initDirsPaths(
                xc, mat, ""
            )  # initialize directory and path names for this choice of xc and mat. potOpt stage names aren't used here so just send pot=''
            DFTobjectsInitialized = (
                False  # this is set to true when structure, poscar, incar and kpoints objects have been set up
            )
            if var.verbosity > 1:
                outf.underline(xc + "/" + mat + ":")
            initialize.determineStage(1)  # Examine presence of APDworkdir and subdirectories to determine what stage 
                    # in the APD process we are at (1 = DFT)
            outf.printt(2, "Stage determined to be " + str(var.stage) + " (DFT level)")
            var.stopAPD = False

            if var.stage == 6:
                outf.printt(2, "Nothing to be done here...")
            else:
                if var.verbosity == 1:
                    outf.underline(xc + "/" + mat + ":")
                outf.printt(2, "Initializing:")
                DFTsetup.setupDFTfromPrevStage()  # this is not in the loop below because it is only required once when we run APD and there are already completed stages

                while var.stage <= 5:
                    if var.verbosity >= 1:
                        print("Stage " + str(var.stage) + ": ", end="")
                    var.norun = False
                    if var.stage == 2:
                        if var.verbosity >= 1:
                            print("DFT electronic parameters (for high calculations)")
                        DFTnonMD(xc, mat)
                    if var.stage == 3:
                        if var.verbosity >= 1:
                            print("DFT parameters for MD runs")
                        DFTMDparas(mat)
                    elif var.stage == 4:
                        if var.verbosity >= 1:
                            print("Atomic configuration generation")
                        if var.config_gen == "DFT_NPT" or var.config_gen == "DFT_NPT_REDUCED":
                            DFTMDconfigs(xc, mat)  # Run ISIF=3 simulations at diff temps and if auto_select_temp=True
                                    # ensure we have temps for liquid and high temp solid
                        elif var.config_gen == "DFT_NPT_EXISTING" or var.config_gen == "DFT_NVT_EXISTING":
                            DFTMDconfigsExisting(xc,mat)
                        elif var.config_gen == "POTENTIAL":
                            # potMDconfigs()
                            print("ERROR: implement potential-driven configuration generation, STOPPING")
                            quit()
                        elif var.config_gen == "RANDOM":
                            # randomConfigs()
                            print("ERROR: implement random generation of configurations, STOPPING")
                            quit()
                    elif var.stage == 5:
                        if var.verbosity >= 1:
                            print("Single shot DFT jobs")
                        DFTtrainingData(xc, mat)

                    if var.stopAPD == True:
                        outf.printt(1, "  Stage " + str(var.stage) + ": HALTED, requires user-intervention")
                        break
                    else:
                        outf.printt(1, "  Stage " + str(var.stage) + ": COMPLETED\n")

                    processJobs.endStage()
                    if var.stopAPD == True:
                        break
                    if var.stage == 5:
                        outf.printt(1, f"  Completed DFT level for exchange-correlation functional: {xc}, material: "
                                f"{mat}\n")
                        break

                    var.stage += 1
                    if var.stage == 3 and (var.config_gen == "DFT_NPT_EXISTING" or 
                            var.config_gen == "DFT_NVT_EXISTING"):
                        var.stage = 4

        var.firstXc = False
    print('diretory:')
    os.system('pwd')
    # --- Potential optimization level ---
    outf.printt(1, "")
    outf.title("Potential optimization level")
    outf.printt(2, "\nInitializing:")
    # Check if requisite DFT is completed for any xcfunc (checks if any stageOutput files have been generated in stage 4 directories)
    outf.printt(2, "\n  Checking whether DFT is fully complete for any xc:")
    xcFunc_potOpt = initialize.xcFuncToOptimize()
    if len(xcFunc_potOpt) == 0:
        outf.printt(1, "\nDFT not yet complete - skipping this level")
    else:
        potSetup.checkCoulSep()

        # If we are optimizing MTPs and M/EAMs then, by default, we do the MTPs first then M/EAMs only afterwards
        var.MEAM_last = (('EAM' in var.pot_types or 'MEAM' in var.pot_types) and 
                ('MTP' in var.pot_types) and var.query_by_committee == False or var.query_by_committee == None)
        print('var.pot_types=',var.pot_types,', var.query_by_committee=',var.query_by_committee)
        pot_types_for_optimization = var.pot_types.copy()
        if var.MEAM_last: 
            outf.printt(2, f"  M/EAM optimizations to be performed after all MTP optimizations finished")
            var.all_MTP_complete = check_MTP_completion()
            outf.printt(2, f"  ...All MTP optimizations complete yet?: {var.all_MTP_complete}")
            if not var.all_MTP_complete:
                # Remove M/EAM from pot optimizations to perform for now
                pot_types_for_optimization = [pot for pot in var.pot_types if 'MEAM' not in pot and 'EAM' not in pot]

        if os.path.exists(var.potpath) == False:
            filesys.createWorkdir(var.potdir)
        for xc in xcFunc_potOpt:
            if os.path.exists(var.potpath + "/" + xc) == False:
                filesys.createWorkdir(var.potdir + "/" + xc)
            for potType in pot_types_for_optimization:
                for potNum in range(1, var.num_pots + 1):
                    pot = potType + str(potNum)
                    if os.path.exists(var.potpath + "/" + xc + "/" + pot) == False:
                        filesys.createWorkdir(var.potdir + "/" + xc + "/" + pot)

        var.pot_itns_all = [
            ""
        ]  # keep track of all iterations (including '' = first iteration; 'B' = second, etc) over all xcs and potentials
        for xc in xcFunc_potOpt:
            for potType in pot_types_for_optimization:
                for potNum in range(1, var.num_pots + 1):
                    pot = potType + str(potNum)

                    # Read DFT directory for this xc and pot and see if there are any EAM1B etc directories
                    pot_itns = [
                        ""
                    ]  # For first iteration optimization this will just contain a no-space. If new DFT has been computed to improve a pot it will contain B, C, etc
                    for mat in var.materials:
                        initialize.initDirsPaths(
                            xc, mat, ""
                        )  # initialize directory and path names for this choice of xc and mat
                        for potDir in glob.glob(
                            var.DFTtrainingDataBadpath + "/" + pot + "*"
                        ):  # glob.glob(var.DFTxcAddMatpath+pot+'*'):
                            if os.path.isfile(var.DFTtrainingDataBadpath + "/" + pot + potDir[-1] + "/stageOutput"):
                                if potDir[-1] not in pot_itns:
                                    pot_itns.append(potDir[-1])
                                if potDir[-1] not in var.pot_itns_all:
                                    var.pot_itns_all.append(potDir[-1])

                    for potItn in pot_itns:
                        pot = ( potType + str(potNum) + str(potItn) )  # e.g., EAM1B, I.e. the second iteration of the 
                            # EAM1 potential (EAM1, EAM2, etc are diff potentials fit to same DFT)
                        if os.path.exists(var.potpath + "/" + xc + "/" + pot) == False:
                            filesys.createWorkdir(var.potdir + "/" + xc + "/" + pot)
                        initialize.initDirsPaths( xc, "", pot )  # initialize directory and path names for this choice 
                            # of xc. DFT stage names aren't used here so just send mat=''
                        initialize.determineStage( 2 )  # Examine presence of APDworkdir and subdirectories to determine
                            # what stage in the APD process we are at (2 = potopt)
                        if var.verbosity == 2:
                            outf.underline(xc + "/" + pot + ":")

                        if var.stage > 7:
                            outf.printt(2, "Nothing to be done here...")
                        else:
                            if var.verbosity == 1:
                                outf.underline(xc + "/" + pot + ":")
                            var.stopAPD = False
                            while var.stage <= 7:
                                if var.verbosity > 0:
                                    print("Stage " + str(var.stage) + ": ", end="")
                                var.norun = False

                                if var.stage == 6:
                                    outf.printt(1, "Optimize potential")
                                    potFitOrTest(xc, pot, potItn, "fit")
                                elif var.stage == 7:
                                    outf.printt(1, "Testing potential")
                                    potFitOrTest(xc, pot, potItn, "test")

                                if var.stopAPD == True:
                                    outf.printt(
                                        1, "  Stage " + str(var.stage) + ": HALTED, requires user-intervention"
                                    )
                                    break
                                else:
                                    outf.printt(1, "  Stage " + str(var.stage) + ": COMPLETED")
                                    processJobs.endStage()  # This is called after each stage, and will stop the code
                                    if var.stopAPD == True:
                                        break
                                    outf.printt(1, "\n")
                                    var.stage += 1

        # --- LAMMPS simulation level ---
        outf.printt(1, "")
        outf.title("Potential driven MD level")
        outf.printt(2, "\nInitializing:")

        # Check if any higher order properties have been specified in settings.APD
        if var.properties_predict == None:
            outf.printt(
                2,
                "  No higher order observables set to be calculated in settings.APD (properties_predict tag): Not proceeding with LAMMPS MD",
            )
            if var.verbosity == 1:
                outf.printt(1, "\nInitializing:")
                outf.printt(
                    1,
                    "  No higher order observables in settings.APD (e.g. thermal_expansion): Not proceeding with LAMMPS MD",
                )
        else:
            # Check from the potOpt stage which potentials are being optimized
            outf.printt(
                2,
                "  Higher order observables set to be calculated in settings.APD. Checking if potentials have been optimized...",
            )
            xcFunc_potSim = initialize.xcFuncToSimul()
            if len(xcFunc_potSim) > 0:
                outf.printt(2, "  ...At least some potentials have been optimized: Proceeding with LAMMPS MD")

                # Set up directories if necessary
                if os.path.exists(var.potMDpath) == False:
                    filesys.createWorkdir(var.potMDdir)
                print("xcFunc_potSim=", xcFunc_potSim)
                for xc, pot in xcFunc_potSim:
                    if os.path.exists(var.potMDpath + "/" + xc) == False:
                        filesys.createWorkdir(var.potMDdir + "/" + xc)
                    for mat in var.materials:
                        if os.path.exists(var.potMDpath + "/" + xc + "/" + mat) == False:
                            filesys.createWorkdir(var.potMDdir + "/" + xc + "/" + mat)
                        if os.path.exists(var.potMDpath + "/" + xc + "/" + mat + "/" + pot) == False:
                            filesys.createWorkdir(var.potMDdir + "/" + xc + "/" + mat + "/" + pot)

                for xc, pot in xcFunc_potSim:
                    for mat in var.materials:
                        if var.verbosity == 2:
                            outf.underline(xc + "/" + mat + "/" + pot + ":")
                        initialize.initDirsPaths(xc, mat, pot)
                        initialize.determineStage(
                            3
                        )  # Examine presence of APDworkdir and subdirectories to determine what stage in the APD process we are at
                        if var.stage > 9:
                            outf.printt(2, "Nothing to be done here...")
                        else:
                            if var.verbosity == 1:
                                outf.underline(xc + "/" + mat + "/" + pot + ":")
                        var.stopAPD = False

                        DFTsetup.setupDFTfromPrevStage()  # stage 8 requires DFT structure data to set up the initial cell...

                        while var.stage <= 9:
                            if var.verbosity > 0:
                                print("Stage " + str(var.stage) + ": ", end="")
                            var.norun = False

                            if var.stage == 8:
                                outf.printt(1, "Simulation using potential")
                                simulationUsingPotential(xc, mat, pot, xcFunc_potSim)

                            if var.stage == 9:
                                outf.printt(1, "Statics using potential")
                                staticsUsingPotential(xc, mat, pot, xcFunc_potSim)

                            if var.stopAPD == True:
                                outf.printt(1, "  Stage " + str(var.stage) + ": HALTED, requires user-intervention")
                                break
                            else:
                                outf.printt(1, "  Stage " + str(var.stage) + ": COMPLETED")
                                processJobs.endStage()  # This is called after each stage and will stop the code
                                var.stage += 1
                                if var.stage == 9 and (var.config_gen == "DFT_NPT_EXISTING" or 
                                        var.config_gen == "DFT_NVT_EXISTING"):
                                    var.stage = 10

                outf.printt(1, "")
                outf.title("Analysis of MD simulations using potentials")
                outf.printt(1, "")
                potMDanalysis.postAnalyzePotMD(xcFunc_potSim)

    closeDown()
