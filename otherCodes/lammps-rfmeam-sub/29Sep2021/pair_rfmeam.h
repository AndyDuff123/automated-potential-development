/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------
   Contributing authors:
        Prashanth Srinivasan
        p.srinivasan@tudelft.nl
        Andrew Ian Duff
        andrew.duff@stfc.ac.uk
        Daniele Scopece
        daniele.scopece@empa.ch
------------------------------------------------------------------------- */


#ifdef PAIR_CLASS

PairStyle(rfmeam,PairRFMEAM)

#else

#ifndef LMP_PAIR_RFMEAMANDY_H
#define LMP_PAIR_RFMEAMANDY_H

#include "pair.h"

namespace LAMMPS_NS {

class PairRFMEAM : public Pair {
 public:
  PairRFMEAM(class LAMMPS *);
  ~PairRFMEAM();
  void compute(int, int);
  void settings(int, char **);
  void coeff(int, char **);
  void init_style();
  double init_one(int, int);

 protected:
     double *z;
     struct EmbFunc { //Embedding Function term parameters
     double *E0;
     double *E1;
     double *E2;
     EmbFunc() {
       E0 = NULL;
       E1 = NULL; 
       E2 = NULL;
     };
   };

   struct BackElDens { // background electron density term parameters
       double *alpha10;
       double *alpha11;
       double *alpha12;
       double *alpha13;
       double *alpha20;
       double *alpha21;
       double *alpha22;
       double *alpha23;
       double *alpha30;
       double *alpha31;
       double *alpha32;
       double *alpha33;
       double *alpha40;
       double *alpha41;
       double *alpha42;
       double *alpha43;
       double *t1;
       double *t2;
       double *t3;
       double *r10;
       double *r11;
       double *r12;
       double *r13;
       double *r20;
       double *r21;
       double *r22;
       double *r23;
       double *r30;
       double *r31;
       double *r32;
       double *r33;
       double *r40;
       double *r41;
       double *r42;
       double *r43;
     BackElDens() {
         alpha10 = NULL;
         alpha11 = NULL;
         alpha12 = NULL;
         alpha13 = NULL;
         alpha20 = NULL;
         alpha21 = NULL;
         alpha22 = NULL;
         alpha23 = NULL;
         alpha30 = NULL;
         alpha31 = NULL;
         alpha32 = NULL;
         alpha33 = NULL;
         alpha40 = NULL;
         alpha41 = NULL;
         alpha42 = NULL;
         alpha43 = NULL;
         t1 = NULL;
         t2 = NULL;
         t3 = NULL;
         r10 = NULL;
         r11 = NULL;
         r12 = NULL;
         r13 = NULL;
         r20 = NULL;
         r21 = NULL;
         r22 = NULL;
         r23 = NULL;
         r30 = NULL;
         r31 = NULL;
         r32 = NULL;
         r33 = NULL;
         r40 = NULL;
         r41 = NULL;
         r42 = NULL;
         r43 = NULL;
     };
   };

   struct PairPot { // Pair potential term parameters
       double **b1;
       double **b2;
       double **b3;
       double **b4;
       double **s1;
       double **s2;
       double **s3;
       double **s4;
     PairPot() {
         b1 = NULL;
         b2 = NULL;
         b3 = NULL;
         b4 = NULL;
         s1 = NULL;
         s2 = NULL;
         s3 = NULL;
         s4 = NULL;
         
     };      
   };

   struct rfmeamParam {
     EmbFunc ef;
     BackElDens bed;
     PairPot pp;
/*
     ef = embedding function
     bed = background electron density
     pp = pair potential
*/
   };

   rfmeamParam params;

 private:
  double cutoffMax;
  double cutmax;                // max cutoff for all elements
  int nelements;                // # of unique elements
  char **elements;              // names of unique elements
  double *mass;                 // mass of each element
  int *map;                     // mapping from atom types to elements
  int nmax;
  void allocate();
  void read_files(char *);
  int determine_species_order_1index(int, char **, char **, int *);
  int determine_species_order_2index(int, char **, char **, int **);
  //int determine_species_order_3index(int, char **, char **, int **);
  void check_read_parameters() ;
  void write_setflag(int) ;

  void compute_phi_dphi_pp(double &, double *, double *, int, int, double, double *, int, int); // phi and d_phi for i and j
  void compute_eff_d_eff_jlr(double &, double *, double *, int, int, double, double *); // ==> factor f and its derivatives wrt atom i and atom j
  void compute_rhosq_drhosq(int, double &, double **, int); // ==> rhosq_l and its gradient for ALL atoms
  void compute_P_dP_legendre_cos(double &, double *, double *, double *, int, double *, double, double*, double); // => P(cos_theta)^{(l)} and its gradient wrt j i k (the only non null)
};
}

#endif
#endif
