// clang-format off

/* ----------------------------------------------------------------------
 LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
 https://www.lammps.org/, Sandia National Laboratories
 Steve Plimpton, sjplimp@sandia.gov
 
 Copyright (2003) Sandia Corporation.  Under the terms of Contract
 DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
 certain rights in this software.  This software is distributed under
 the GNU General Public License.
 
 See the README file in the top-level LAMMPS directory.
 ------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------------------------------------------------
 Contributing authors : 
 Prashanth Srinivasan
 p.srinivasan@tudelft.nl
 Andrew Ian Duff
 andrew.duff@stfc.ac.uk
 Daniele Scopece
 daniele.scopece@empa.ch ------------------------------------------------------------------------------------------------------------------- */

#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "pair_rfmeam.h"
#include "atom.h"
#include "force.h"
#include "comm.h"
#include "memory.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "error.h"
#include "math_const.h"
#include "update.h"
#include "integrate.h"
#include "respa.h"
using namespace LAMMPS_NS;
using namespace MathConst;
#define MAXLINE 1024 //maximum number of characteres read in a line of the input parameters
#define MAXWORDINLINE 15 // Maximum number of words in the lines to read: increase if needed
#define ORDERREADUNDEFINED -1 // variable to initialize the order for the reading of input
#define INITPARAMETERS -9876543210.999 // initailization of the parameters in 2 indeces and 3 indeces
#define MAXLegenderePoly 3 // maximum order of legendre polynomial: increase if needed
#define TOLZERO 0.000000001 // zero tolerance
#define INFTYR 100000000.0 // infinity tolerance

// ---------------------------------------------------------------------------------- //

PairRFMEAM::PairRFMEAM(LAMMPS *lmp) : Pair(lmp) // constructor
{
    single_enable = 0;
    restartinfo = 0;
    one_coeff = 1;
    manybody_flag = 1;
    nmax = 0;
    nelements = 0;
    elements = nullptr;
    mass = nullptr;
    comm_forward = 38;
    comm_reverse = 30;
}

// ----------------------- //

PairRFMEAM::~PairRFMEAM() // Memory deallocation // Needs to be altered depending on the number of parameters
{
    if (copymode) return;

    bool DS_debug = false;
    memory->destroy(z);
    memory->destroy(params.ef.E0);
    memory->destroy(params.ef.E1);
    memory->destroy(params.ef.E2);
    memory->destroy(params.bed.alpha10);
    memory->destroy(params.bed.alpha11);
    memory->destroy(params.bed.alpha12);
    memory->destroy(params.bed.alpha13);
    memory->destroy(params.bed.alpha20);
    memory->destroy(params.bed.alpha21);
    memory->destroy(params.bed.alpha22);
    memory->destroy(params.bed.alpha23);
    memory->destroy(params.bed.alpha30);
    memory->destroy(params.bed.alpha31);
    memory->destroy(params.bed.alpha32);
    memory->destroy(params.bed.alpha33);
    memory->destroy(params.bed.alpha40);
    memory->destroy(params.bed.alpha41);
    memory->destroy(params.bed.alpha42);
    memory->destroy(params.bed.alpha43);
    memory->destroy(params.bed.t1);
    memory->destroy(params.bed.t2);
    memory->destroy(params.bed.t3);
    memory->destroy(params.bed.r10);
    memory->destroy(params.bed.r11);
    memory->destroy(params.bed.r12);
    memory->destroy(params.bed.r13);
    memory->destroy(params.bed.r20);
    memory->destroy(params.bed.r21);
    memory->destroy(params.bed.r22);
    memory->destroy(params.bed.r23);
    memory->destroy(params.bed.r30);
    memory->destroy(params.bed.r31);
    memory->destroy(params.bed.r32);
    memory->destroy(params.bed.r33);
    memory->destroy(params.bed.r40);
    memory->destroy(params.bed.r41);
    memory->destroy(params.bed.r42);
    memory->destroy(params.bed.r43);
    memory->destroy(params.pp.b1);
    memory->destroy(params.pp.b2);
    memory->destroy(params.pp.b3);
    memory->destroy(params.pp.b4);
    memory->destroy(params.pp.s1);
    memory->destroy(params.pp.s2);
    memory->destroy(params.pp.s3);
    memory->destroy(params.pp.s4);

    for (int i = 0; i < nelements; i++) delete [] elements[i];
    delete [] elements;
    delete [] mass;
    if (allocated) {
        memory->destroy(setflag);
        memory->destroy(cutsq);
        delete [] map;
    }
}

// ------------------------------ //

void PairRFMEAM::compute(int eflag, int vflag) // Computes total energy and force on atoms, calls functions to calculate embedding energies and pairwise energies // Needs to be altered whenever there is a change in the total energy formalism of RFMEAM
{
    int i,j,ii,n,inum_half,errorflag;
    double evdwl;
    int *ilist_half,*numneigh_half,**firstneigh_half;
    int *numneigh_full,**firstneigh_full;

    bool DS_print_rhosq = false ; // to print rho^2 to compare with Camelion
    FILE * file_rhosq ;
    bool DS_debug = false;
    bool DS_debug_final = false ; // write nothing
    bool DS_debug_memory = false ; // for the debugging of ram issue
    bool DS_debug_final2 = false ; // write the angular and embedding term
    bool avoid_nan = true ; // true = to avoid the NaN in embedding given from rhoisq_0
    evdwl = 0.0; // in this one must store the energy of the atom considered
    
    ev_init(eflag,vflag);
    
    tagint *tag = atom->tag;
    int nlocal = atom->nlocal;
    int nall = nlocal + atom->nghost;
    int newton_pair = force->newton_pair;
    double **x = atom->x; // atoms coordinates
    double **f = atom->f; // forces on the atoms
    int *type = atom->type; // type species of each atom (it's a number as in the input file)
    int ntypes = atom->ntypes; // number of types (=species) total
    int inum; // total number of INTERACTIONS treated by this core
    inum =  list->inum; // number of interactions in the cell considered by this processor (consider init_list)
    int *ilist;
    ilist = list->ilist; // ilist[i\in inum] = local index of the atom with this interaction
    int *numneigh ;
    numneigh = list->numneigh; // numneigh[i] = number of neighbours of the atom whose local index is i
    int **firstneigh ;
    firstneigh = list->firstneigh; // firstneigh[i][j] = (local?) index of the jth neighbor of atom i
    double sum_ef_i ; // Embedding function F_i(rho_i) == sum_i (F_i(rho_i))
    double sum_ang_i; // = sum_i sum_(j<>i) 0.5 * phi_ij(r_ij)
    double CapGamma_i;
    double rho_i;
    sum_ef_i = 0.0; //initialization of the embedding function term
    sum_ang_i = 0.0; // initialization of the angular term
    double energy_ef_i, energy_ang_i; // energy of the i_atom
    int i_atom, i_tag, i_type;
    int j_atom, j_tag, j_type;
    double x_i, y_i, z_i;
    double x_j, y_j, z_j;
    double g_yi;
    double delx_ij, dely_ij, delz_ij;
    double r_ij, S_ij, h_ij_rij, phi_ij_rij_here;
    int num_neigh_i;
    int *jlist;
    double coeff_i;
    double rhosq_0, rhosq_1, rhosq_2, rhosq_3; // for a fixed i
    double y_i_rho;
    double d_r[3] ; // dr_{ij}/dx_atom = vector [d/dx, d/dy, d/dz]
    double d_cos[3];
    double** d_r_diff_arr;
    memory->create(d_r_diff_arr,3,3,"pair:d_r_diff_arr");
    double d_phi[3], d_h[3];
    double d_S[3];
    double vec_ij[3];
    int atom_now;
    int mu_atom, mu_tag, mu_type;
    bool l_do_embedding = true; // to test forces just angular
    bool l_do_angular = true; //
    int n_max_neigh = 0;
    for (int findmax=0; findmax<inum; findmax++){
        atom_now = ilist[findmax];
        if (numneigh[atom_now]>n_max_neigh) n_max_neigh = numneigh[atom_now];
    }

    double **d_rhosqi_emb_All_0;
    memory->create(d_rhosqi_emb_All_0, n_max_neigh+1, 3, "pair:d_rhosqi_emb_All_0");
    double **d_rhosqi_emb_All_1 ;
    memory->create(d_rhosqi_emb_All_1, n_max_neigh+1, 3, "pair:d_rhosqi_emb_All_1");
    double **d_rhosqi_emb_All_2;
    memory->create(d_rhosqi_emb_All_2, n_max_neigh+1, 3, "pair:d_rhosqi_emb_All_2");
    double **d_rhosqi_emb_All_3;
    memory->create(d_rhosqi_emb_All_3, n_max_neigh+1, 3, "pair:d_rhosqi_emb_All_3");
    double **d_CapGamma_i ;
    memory->create(d_CapGamma_i, n_max_neigh+1, 3, "pair:d_CapGamma_i");
    double **d_yi_emb;
    memory->create(d_yi_emb, n_max_neigh+1, 3, "pair:d_yi_emb");
    double **d_Fi_emb;
    memory->create(d_Fi_emb, n_max_neigh+1, 3, "pair:d_Fi_emb");
    double d_g_yi;
    double factor_1, factor_2;
    for (int inow = 0; inow<inum ; inow++){ // for every atom...
        i_atom = ilist[inow]; // local index of the atom
        i_tag = tag[i_atom]; // global index of the atom
        i_type = map[type[i_atom]]; // type of the atom: 0 -> ntype-1 (as saved the parameters => use this)
        x_i = x[i_atom][0]; // x
        y_i = x[i_atom][1]; // y
        z_i = x[i_atom][2]; // z
        num_neigh_i = numneigh[i_atom]; // number of neighbors of atom i_atom (NOT inow)
        jlist = firstneigh[i_atom]; // list of neighbors

        double coeff_1[3], coeff_2[3], coeff_3[3];
        double d_h_ij[3], d_phi_ij[3];
        double d_h_ij_rij_di[3], d_h_ij_rij_dj[3];
        double phi_ij_rij_here, d_phi_ij_rij_di[3], d_phi_ij_rij_dj[3]; // for pair potential
        energy_ang_i = 0.0;

        if (l_do_angular) {
            if (DS_print_rhosq) file_rhosq = fopen ("Rhosq-Gamma...write.txt","a");
            for (int jnow=0; jnow<num_neigh_i ; jnow++){ // for every neighbour j of atom i.....
                j_atom = jlist[jnow]; // local index of the atom
                j_atom &= NEIGHMASK;
                j_tag = tag[j_atom]; // global index of the atom
                j_type = map[type[j_atom]]; // type of the atom
                x_j = x[j_atom][0]; // x
                y_j = x[j_atom][1]; // y
                z_j = x[j_atom][2]; // z
                delx_ij = x_i - x_j;
                dely_ij = y_i - y_j;
                delz_ij = z_i - z_j;
                r_ij = sqrt(delx_ij*delx_ij + dely_ij*dely_ij + delz_ij*delz_ij);
                vec_ij[0] = delx_ij;
                vec_ij[1] = dely_ij;
                vec_ij[2] = delz_ij;
                
               // compute_phi_dphi_pp(phi_ij_rij_here, d_phi_ij_rij_di, d_phi_ij_rij_dj, i_type, j_type, r_ij, vec_ij); // function call to calculate phi

                compute_phi_dphi_pp(phi_ij_rij_here, d_phi_ij_rij_di, d_phi_ij_rij_dj, i_type, j_type, r_ij, vec_ij, i_type, j_type); // function call to calculate phi
                
                energy_ang_i += 0.5 * phi_ij_rij_here; // added to pair potential

                sum_ang_i += 0.5 * phi_ij_rij_here; // added to pair potential

                for (int mu_now = 0; mu_now < num_neigh_i+1 ; mu_now++){ // calculating derivatives of pairwise term with respect to vector distances
                    if (mu_now == num_neigh_i) {
                        mu_atom = i_atom;
                    }
                    else if (mu_now < num_neigh_i) {
                        mu_atom = jlist[mu_now]; // local index of the atom
                        mu_atom &= NEIGHMASK;
                    }
                    mu_tag = tag[mu_atom]; // global index of the atom
                    mu_type = map[type[mu_atom]]; // type of the atom
                    if (mu_atom == i_atom) {
                        for (int i012=0; i012<3; i012++){
                            coeff_3[i012] = -d_phi_ij_rij_di[i012];
                            f[mu_atom][i012] += 0.5 * coeff_3[i012];
                        }
                        continue; // cycle to the next mu_now
                    }
                    if (mu_atom == j_atom) {
                        for (int i012=0; i012<3; i012++){
                            coeff_3[i012] = -d_phi_ij_rij_dj[i012];
                            f[mu_atom][i012] += 0.5 * coeff_3[i012];
                        }
                        continue; // cycle to the next mu_now
                    }
                } // mu_now = atom according to which the derivative is performed
            } // jnow cycle
        } //l_do_angular
        
        
        energy_ef_i = 0.0 ; //initialize to zero
        
        if (l_do_embedding) {
            bool l_rhosq_0_is_zero = false; // 2014-04-29
            compute_rhosq_drhosq(0, rhosq_0, d_rhosqi_emb_All_0, i_atom); // Function call to calculate rhosq_0 and derivative
            if (fabs(rhosq_0) <= TOLZERO) l_rhosq_0_is_zero = true;
            compute_rhosq_drhosq(1, rhosq_1, d_rhosqi_emb_All_1, i_atom);// Function call to calculate rhosq_1 and derivative
            compute_rhosq_drhosq(2, rhosq_2, d_rhosqi_emb_All_2, i_atom);// Function call to calculate rhosq_2 and derivative
            compute_rhosq_drhosq(3, rhosq_3, d_rhosqi_emb_All_3, i_atom);// Function call to calculate rhosq_3 and derivative
            if (avoid_nan && l_rhosq_0_is_zero) CapGamma_i = 0.0;
            else {
                CapGamma_i = params.bed.t1[i_type]*rhosq_1 + params.bed.t2[i_type]*rhosq_2 + params.bed.t3[i_type]*rhosq_3;
                CapGamma_i = CapGamma_i / rhosq_0;
            }
            rho_i = sqrt(rhosq_0) * ( 2.0/(1.0+exp(-CapGamma_i))); // rho_i
            
            if (DS_print_rhosq) fprintf (file_rhosq, "%d %d %f %f %f %f %f\n", i_atom, i_type, rhosq_0, rhosq_1, rhosq_2, rhosq_3, CapGamma_i);
            if (avoid_nan && l_rhosq_0_is_zero) coeff_i = 0.0; // tanto dopo energia diventa nulla
             else coeff_i = params.ef.E0[i_type]*sqrt(rho_i) + params.ef.E1[i_type]*rho_i*rho_i + params.ef.E2[i_type]*rho_i*rho_i*rho_i; // Embedding function with E0, E1 and E2 as the three parameters

            energy_ef_i = coeff_i; //Embedding energy of atom i
            sum_ef_i += coeff_i;
            
            // calculating derivatives of embedding function term
            for (int mu_now=0; mu_now<num_neigh_i+1; mu_now++){
                d_CapGamma_i[mu_now][0] = 0.0;
                d_CapGamma_i[mu_now][1] = 0.0;
                d_CapGamma_i[mu_now][2] = 0.0;
            }
            for (int mu_now=0; mu_now<num_neigh_i+1; mu_now++){
                for (int i012=0; i012<3; i012++){
                    if (avoid_nan && l_rhosq_0_is_zero) {
                        d_CapGamma_i[mu_now][i012] += 0.0;
                    }
                    else {
                        factor_1 = params.bed.t1[i_type] * d_rhosqi_emb_All_1[mu_now][i012] * rhosq_0;
                        factor_2 = params.bed.t1[i_type] * rhosq_1 * d_rhosqi_emb_All_0[mu_now][i012];
                        d_CapGamma_i[mu_now][i012] += (factor_1 - factor_2) / (rhosq_0*rhosq_0);
                        factor_1 = params.bed.t2[i_type] * d_rhosqi_emb_All_2[mu_now][i012] * rhosq_0;
                        factor_2 = params.bed.t2[i_type] * rhosq_2 * d_rhosqi_emb_All_0[mu_now][i012];
                        d_CapGamma_i[mu_now][i012] += (factor_1 - factor_2) / (rhosq_0*rhosq_0);
                        factor_1 = params.bed.t3[i_type] * d_rhosqi_emb_All_3[mu_now][i012] * rhosq_0;
                        factor_2 = params.bed.t3[i_type] * rhosq_3 * d_rhosqi_emb_All_0[mu_now][i012];
                        d_CapGamma_i[mu_now][i012] += (factor_1 - factor_2) / (rhosq_0*rhosq_0);
                    } // end if not den = 0
                }
            }
            for (int mu_now=0; mu_now<num_neigh_i+1; mu_now++){
                d_yi_emb[mu_now][0] = 0.0;
                d_yi_emb[mu_now][1] = 0.0;
                d_yi_emb[mu_now][2] = 0.0;
            }
            for (int mu_now=0; mu_now<num_neigh_i+1; mu_now++){ // choose atom wrt derivative
                for (int i012=0; i012<3; i012++){ // choose component of derivative
                    if (avoid_nan && l_rhosq_0_is_zero) {// if l_rhosq_0_is_zero =>
                        factor_1 = 0.0;
                        factor_2 = 0.0;
                    }
                    else { //here if not zero
                        factor_1 = (d_rhosqi_emb_All_0[mu_now][i012]/ (2.0*sqrt(rhosq_0))) * (2.0/(1.0+exp(-CapGamma_i)));
                        factor_2 = sqrt(rhosq_0) * 2.0 * d_CapGamma_i[mu_now][i012] * exp(-CapGamma_i) / ((1.0+exp(-CapGamma_i))*(1.0+exp(-CapGamma_i)));
                    }
                    d_yi_emb[mu_now][i012] = (factor_1 + factor_2); // same as derivative of rho_i
                }
            }
            for (int mu_now=0; mu_now<num_neigh_i+1; mu_now++){
                d_Fi_emb[mu_now][0] = 0.0;
                d_Fi_emb[mu_now][1] = 0.0;
                d_Fi_emb[mu_now][2] = 0.0;
            }
            for (int mu_now=0; mu_now<num_neigh_i+1; mu_now++){ // choose atom wrt derivative
                if (mu_now == num_neigh_i) {
		                  mu_atom = i_atom;
                }
                else if (mu_now < num_neigh_i) {
                    mu_atom = jlist[mu_now]; // local index of the atom
                    mu_atom &= NEIGHMASK;
                }
                mu_tag = tag[mu_atom]; // global index of the atom
                mu_type = map[type[mu_atom]]; // type of the atom
                for (int i012=0; i012<3; i012++){ // choose component of derivative
                    if (avoid_nan && l_rhosq_0_is_zero) {
                        factor_1 = 0.0;
                        factor_2 = 0.0;
                    }
                    else{
                        factor_1 = 0;
                        factor_2 = d_yi_emb[mu_now][i012]*((params.ef.E0[i_type]/(2*sqrt(rho_i)))+(2*params.ef.E1[i_type]*rho_i)+(3*params.ef.E2[i_type]*rho_i*rho_i));
                    }
                    d_Fi_emb[mu_now][i012] = factor_1 + factor_2;
                    f[mu_atom][i012] += -d_Fi_emb[mu_now][i012];
                }
            } // mu_now
            if (DS_print_rhosq) fclose(file_rhosq);
        } // l_do_embedding
        if (eflag) {
            evdwl = energy_ef_i + energy_ang_i; // energy of a SINGLE atom
        }
        ev_tally(i_atom, 0, nlocal, newton_pair, evdwl, 0.0, 0.0, 0.0, 0.0, 0.0);
    } // loop over atoms, 'i'
    
    memory->destroy(d_r_diff_arr);
    memory->destroy(d_rhosqi_emb_All_0);
    memory->destroy(d_rhosqi_emb_All_1);
    memory->destroy(d_rhosqi_emb_All_2);
    memory->destroy(d_rhosqi_emb_All_3);
    memory->destroy(d_CapGamma_i);
    memory->destroy(d_yi_emb);
    memory->destroy(d_Fi_emb);
    if (vflag_fdotr) virial_fdotr_compute();
}

// ---------------------------------------------------------------------- //

void PairRFMEAM::allocate() // Allocates memory for parameters // Needs to be altered depending on the number of parameters
{
    bool DS_debug = false;
    allocated = 1;
    int n = atom->ntypes;
    
    memory->create(setflag,n+1,n+1,"pair:setflag");
    memory->create(cutsq,n+1,n+1,"pair:cutsq");
    memory->create(z,n+1,"pair:z");

    map = new int[n+1];
    
    int nelements = atom->ntypes;
    memory->create(params.ef.E0, nelements, "pair:params.ef.E0");
    for (int iinit=0; iinit<nelements; iinit++) params.ef.E0[iinit] = INITPARAMETERS; //initialization
    memory->create(params.ef.E1, nelements, "pair:params.ef.E1");
    for (int iinit=0; iinit<nelements; iinit++) params.ef.E1[iinit] = INITPARAMETERS; //initialization
    memory->create(params.ef.E2, nelements, "pair:params.ef.E2");
    for (int iinit=0; iinit<nelements; iinit++) params.ef.E2[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha10, nelements, "pair:params.bed.alpha10");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha10[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha11, nelements, "pair:params.bed.alpha11");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha11[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha12, nelements, "pair:params.bed.alpha12");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha12[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha13, nelements, "pair:params.bed.alpha13");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha13[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha20, nelements, "pair:params.bed.alpha20");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha20[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha21, nelements, "pair:params.bed.alpha21");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha21[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha22, nelements, "pair:params.bed.alpha22");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha22[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha23, nelements, "pair:params.bed.alpha23");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha23[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha30, nelements, "pair:params.bed.alpha30");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha30[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha31, nelements, "pair:params.bed.alpha31");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha31[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha32, nelements, "pair:params.bed.alpha32");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha32[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha33, nelements, "pair:params.bed.alpha33");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha33[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha40, nelements, "pair:params.bed.alpha40");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha40[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha41, nelements, "pair:params.bed.alpha41");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha41[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha42, nelements, "pair:params.bed.alpha42");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha42[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.alpha43, nelements, "pair:params.bed.alpha43");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.alpha43[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.t1, nelements, "pair:params.bed.t1");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.t1[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.t2, nelements, "pair:params.bed.t2");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.t2[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.t3, nelements, "pair:params.bed.t3");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.t3[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r10, nelements, "pair:params.bed.r10");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r10[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r11, nelements, "pair:params.bed.r11");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r11[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r12, nelements, "pair:params.bed.r12");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r12[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r13, nelements, "pair:params.bed.r13");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r13[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r20, nelements, "pair:params.bed.r20");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r20[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r21, nelements, "pair:params.bed.r21");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r21[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r22, nelements, "pair:params.bed.r22");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r22[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r23, nelements, "pair:params.bed.r23");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r23[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r30, nelements, "pair:params.bed.r30");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r30[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r31, nelements, "pair:params.bed.r31");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r31[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r32, nelements, "pair:params.bed.r32");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r32[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r33, nelements, "pair:params.bed.r33");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r33[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r40, nelements, "pair:params.bed.r40");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r40[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r41, nelements, "pair:params.bed.r41");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r41[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r42, nelements, "pair:params.bed.r42");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r42[iinit] = INITPARAMETERS; //initialization
    memory->create(params.bed.r43, nelements, "pair:params.bed.r43");
    for (int iinit=0; iinit<nelements; iinit++) params.bed.r43[iinit] = INITPARAMETERS; //initialization
    
    int ncombinations2 = nelements*(nelements+1) / 2; // == number of columns to read
    
    memory->create(params.pp.b1, nelements, nelements, "pair:params.pp.b1");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.b1[iinit][jinit] = INITPARAMETERS;
        } }
    memory->create(params.pp.b2, nelements, nelements, "pair:params.pp.b2");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.b2[iinit][jinit] = INITPARAMETERS;
        } }
    memory->create(params.pp.b3, nelements, nelements, "pair:params.pp.b4");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.b3[iinit][jinit] = INITPARAMETERS;
        } }
    memory->create(params.pp.b4, nelements, nelements, "pair:params.pp.b4");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.b4[iinit][jinit] = INITPARAMETERS;
        } }
    memory->create(params.pp.s1, nelements, nelements, "pair:params.pp.s1");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.s1[iinit][jinit] = INITPARAMETERS;
        } }
    memory->create(params.pp.s2, nelements, nelements, "pair:params.pp.s2");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.s2[iinit][jinit] = INITPARAMETERS;
        } }
    memory->create(params.pp.s3, nelements, nelements, "pair:params.pp.s3");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.s3[iinit][jinit] = INITPARAMETERS;
        } }
    memory->create(params.pp.s4, nelements, nelements, "pair:params.pp.s4");
    for (int iinit=0; iinit<nelements; iinit++){
        for (int jinit=0; jinit<nelements; jinit++){
            params.pp.s4[iinit][jinit] = INITPARAMETERS;
        } }

    /*Useless*/
    /*int nangularscreening ;
    if (nelements == 1)
    nangularscreening = 1;
    else if (nelements == 2)
    nangularscreening = 6;
    else if (nelements == 3)
    nangularscreening = 18;
    else
    error->all(FLERR,"In pair_rfmeam.cpp : nelements > 3 => define in the file the values of nangularscreening ");
    /*Useless*/
    
}

// --------------------------------------------------------------------- //

void PairRFMEAM::settings(int narg, char **arg)
{
    if (narg != 0) error->all(FLERR,"Illegal pair_style command");
}

// --------------------------------------------------------------------- //

void PairRFMEAM::coeff(int narg, char **arg) // Making sure that the pair_style command is right in the input script
{
    bool DS_debug = false;
    int i,j,m,n;
    if (!allocated) allocate();
//    if (narg != 5) error->all(FLERR,"Incorrect args for pair coefficients"); // pair_coeff      * *
    
//    if (narg != 7) error->all(FLERR,"Incorrect args for pair coefficients"); // pair_coeff      * *
    
    nelements = atom->ntypes;
    if (narg != nelements*2+3) error->all(FLERR,"Incorrect args for pair coefficients"); // pair_coeff
    if (nelements < 1) error->all(FLERR,"Incorrect args for pair coefficients");
    
    elements = new char*[nelements]; // allocating the name of the elements
    mass = new double[nelements]; // allocating the mass
    for (i = 0; i < nelements; i++) {
        n = strlen(arg[i+3]) + 1;  // length of the name of the chemical species selected
        elements[i] = new char[n]; // name of the element allocated (pointer)
        strcpy(elements[i],arg[i+3]); // cp arg[i+3] ==> elements[i] where arg and elements are pointers
z[i]=utils::numeric(FLERR,arg[i+3+nelements],false,lmp);        
    }
    read_files(arg[2]);
    for (int ispecie=1; ispecie<=nelements; ispecie++)
    map[ispecie] = ispecie-1; // map[type=1] = 0 ; map[type=2] = 1 etc.
}

// --------------------------------------------------------------------- //

void PairRFMEAM::init_style() // Pair-style command features...
{
    bool DS_debug = false;

    if (atom->tag_enable == 0)
    error->all(FLERR,"Pair style rfmeam requires atom IDs");
    
    if (force->newton_pair == 0)
    error->all(FLERR,"Pair style RFMEAM requires newton pair on");

    neighbor->add_request(this,NeighConst::REQ_FULL);
}

// --------------------------------------------------------------------- //

double PairRFMEAM::init_one(int i, int j) // Function to return cut-off 
{
    if (setflag[i][j] == 0) error->all(FLERR,"All pair coeffs are not set");
    
    double cutmax;
    bool DS_debug = false;
    cutmax = cutoffMax; 
    return cutmax;
}

// --------------------------------------------------------------------- //

void PairRFMEAM::read_files(char *parametersfile) // Reads the input potential parameters file and assigns to parameter variables // Needs to be altered depending on the format of the potential file
{
    bool DS_debug = false;
    FILE *fp;
    if (comm->me == 0) {
        fp = utils::open_potential(parametersfile,lmp,nullptr); //guessed the last parameter
        if (fp == nullptr) {
            char str[128];
            sprintf(str,"Cannot open RFMEAM potential file %s",parametersfile);
            error->one(FLERR,str);
        }
    }
    
    int n,nwords;
    char **words = new char*[MAXWORDINLINE+1];
    char line[MAXLINE],*ptr;
    int eof = 0;
    char *acronym_now;
    int ncombinations2 = nelements*(nelements+1) / 2;
    /*int nangularscreening ;
    if (nelements == 1)
    nangularscreening = 1;
    else if (nelements == 2)
    nangularscreening = 6;
    else if (nelements == 3)
    nangularscreening = 18;
    else
    error->all(FLERR,"In pair_rfmeam.cpp : nelements > 3 => define in the file the values of nangularscreening ");
    */
    char **reading_input_n = new char*[nelements]; // to read the species name in the line of the blocks in the input file
    char **reading_input_2index = new char*[ncombinations2]; // to read the species name in the line of the blocks in the input file
    //char **reading_input_3index = new char*[nangularscreening]; // to read the species name in the line of the blocks in the input file
    int *species_order_1index; // allocate the vector to read the species in the lines of the declaration of the type in the input file (see below)
    memory->create(species_order_1index, nelements, "pair:species_order_1index");
    int **species_order_2index; // = new int*[2]; //,nelements; // first element = dimension ; second = index; 2 = number of indices
    memory->create(species_order_2index, 2, ncombinations2, "pair:species_order_2index");
    //int **species_order_3index ;
    //memory->create(species_order_3index, 3, nangularscreening, "pair:species_order_3index");
    
    int nlineparfile = 0; // number of line in the input file
    
    while (1) {
        nlineparfile ++; // line reading now
        if (comm->me == 0) {
            ptr = fgets(line,MAXLINE,fp); // get string from stream; read from fp => stored into line untile MAXLINE characters are read or a new line encountered
            if (ptr == nullptr) {
                eof = 1;
                fclose(fp);
            } else n = strlen(line) + 1;
        }
        
        /* MPI lines - dont edit */
        MPI_Bcast(&eof,1,MPI_INT,0,world);
        if (eof) break;
        MPI_Bcast(&n,1,MPI_INT,0,world);
        MPI_Bcast(line,n,MPI_CHAR,0,world);
        /* End of MPI lines */
        
        if (ptr = strchr(line,'#')) {
            *ptr = '\0'; //Ensure that the string is actually terminated
        }
        nwords = utils::count_words(line);
        if (nwords == 0) continue;
        nwords = 0;
        words[nwords++] = strtok(line,"' \t\n\r\f"); // divides into words
        while (words[nwords++] = strtok(nullptr,"' \t\n\r\f")) continue;
        
        int index_dim_1, index_dim_2, index_dim_3;

        acronym_now = words[0]; // save the acronym_now
       
        if (!strcmp(acronym_now,"cutoffMax")){
                cutoffMax = atof(words[1]);
                }

        if (!strcmp(acronym_now, "ef")) {
            for (int ii=1 ; ii <= nelements; ii++) {
                reading_input_n[ii-1] = words[ii]; // memo: reading_input_n starts from 0; words start from 1 to contain the species
            }
            determine_species_order_1index(nelements,elements,reading_input_n,species_order_1index);
        }

        else if (!strcmp(acronym_now, "ef.E0"))  {
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.ef.E0 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.ef.E0[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "ef.E1"))  {
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.ef.E1 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.ef.E1[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "ef.E2"))  {
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE this params.ef.E2 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.ef.E2[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed")) {
            for (int ii=1 ; ii <= nelements; ii++) {
                reading_input_n[ii-1] = words[ii]; // memo: reading_input_n starts from 0; words start from 1 to contain the species
            }
            determine_species_order_1index(nelements,elements,reading_input_n,species_order_1index);
        }

        else if (!strcmp(acronym_now, "bed.alpha10"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha10 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha10 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha10[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha11"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha11 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha11 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha11[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha12"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha12 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha12 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha12[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha13"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha13 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha13 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha13[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha20"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha20 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha20 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha20[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha21"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha21 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha21 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha21[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha22"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha22 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha22 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha22[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha23"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha23 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha23 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha23[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha30"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha30 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha30 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha30[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha31"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha31 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha31 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha31[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha32"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha32 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha32 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha32[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha33"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha33 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha33 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha33[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha40"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha40 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha40 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha40[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha41"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha41 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha41 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha41[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha42"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha42 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha42 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha42[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.alpha43"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.alpha43 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.alpha43 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.alpha43[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.t1"))  { // OK IT WORKS
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.t1 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.t1[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.t2"))  { // OK IT WORKS
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.t2 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.t2[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
                    }
        }
        
        else if (!strcmp(acronym_now, "bed.t3"))  { // OK IT WORKS
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.t3 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.t3[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r10"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r10 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r10 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r10[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r11"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r11 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r11 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r11[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r12"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r12 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r12 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r12[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r13"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r13 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r13 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r13[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r20"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r20 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r20 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r20[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r21"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r21 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r21 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r21[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r22"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r22 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r22 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r22[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r23"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r23 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r23 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r23[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r30"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r30 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r30 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r30[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r31"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r31 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r31 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r31[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r32"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r32 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r32 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r32[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r33"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r33 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r33 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r33[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r40"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r40 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r40 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r40[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r41"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r41 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r41 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r41[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r42"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r42 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r42 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r42[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
        else if (!strcmp(acronym_now, "bed.r43"))  { // OK IT WORKS
            if (DS_debug) printf("+++++++++++++++++++ bed.r43 found ! \n");
            for (int iihere=0; iihere<nelements; iihere++){
                if (species_order_1index[iihere] == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_1index[%d] NOT Defined!!: Put the intestation line ABOVE params.bed.r43 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.bed.r43[species_order_1index[iihere]] = atof(words[iihere+1]); // save in the correct position
            }
        }
        
    else if (!strcmp(acronym_now, "pp")) {
            for (int ii=1 ; ii <= ncombinations2; ii++) {
                reading_input_2index[ii-1] = words[ii]; // memo: reading_input_2index starts from 0; words start from 1 to contain the species
            }
            determine_species_order_2index(ncombinations2,elements,reading_input_2index,species_order_2index);
        }
        
        else if (!strcmp(acronym_now, "pp.b1"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.b1 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.b1[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.b1[index_dim_2][index_dim_1] = params.pp.b1[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else if (!strcmp(acronym_now, "pp.b2"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.b2 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.b2[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.b2[index_dim_2][index_dim_1] = params.pp.b2[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else if (!strcmp(acronym_now, "pp.b3"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.b3 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.b3[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.b3[index_dim_2][index_dim_1] = params.pp.b3[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else if (!strcmp(acronym_now, "pp.b4"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.b4 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.b4[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.b4[index_dim_2][index_dim_1] = params.pp.b4[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else if (!strcmp(acronym_now, "pp.s1"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.s1 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.s1[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.s1[index_dim_2][index_dim_1] = params.pp.s1[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else if (!strcmp(acronym_now, "pp.s2"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.s2 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.s2[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.s2[index_dim_2][index_dim_1] = params.pp.s2[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else if (!strcmp(acronym_now, "pp.s3"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.s3 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.s3[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.s3[index_dim_2][index_dim_1] = params.pp.s3[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else if (!strcmp(acronym_now, "pp.s4"))  { // OK IT WORKS
            for (int iihere=0; iihere<ncombinations2; iihere++){
                index_dim_1 = species_order_2index[0][iihere]; // index of the matrix line to be filled
                index_dim_2 = species_order_2index[1][iihere]; // index of the matrix column  to be filled
                if (index_dim_1 == ORDERREADUNDEFINED || index_dim_2 == ORDERREADUNDEFINED) {
                    printf("ERROR: species_order_2index[0/1][%d] NOT Defined!!: Put the intestation line ABOVE params.pp.s4 in the parameters file!\n",iihere);
                    exit(1);
                }
                params.pp.s4[index_dim_1][index_dim_2] = atof(words[iihere+1]); // save in the correct position in the matrix
                if (index_dim_1 != index_dim_2)
                params.pp.s4[index_dim_2][index_dim_1] = params.pp.s4[index_dim_1][index_dim_2]; //so symmetrized
            }
        }
        
        else {
            continue; // it is here if it is reading inside a block
        }
    } // whilewhile end
    check_read_parameters(); // Calls function to check if parameters have been assigned correctly
    delete [] words;
    delete [] reading_input_n;
    delete [] reading_input_2index;
    //delete [] reading_input_3index;
    memory->destroy(species_order_1index);
    memory->destroy(species_order_2index);
    //memory->destroy(species_order_3index);
    write_setflag(nelements);
} // end of read_files

// --------------------------------------------------------------------- //

int PairRFMEAM::determine_species_order_1index(int n_column_par, char **order_el_input, char **order_el_par, int *species_order_to_save) // Complements the read_files function
{
    bool DS_debug = false;
    for (int iinit=0; iinit<n_column_par; iinit++)
    species_order_to_save[iinit] = ORDERREADUNDEFINED;

    for (int icol=0; icol<n_column_par; icol++){ // select a column of the parameter file
        for (int iinput=0; iinput<n_column_par; iinput++) {  // run over the names of the input file
            if (strcmp(order_el_par[icol],order_el_input[iinput]) == 0) {
                species_order_to_save[icol] = iinput;
                break; // http://mathbits.com/MathBits/CompSci/looping/end.htm
            }
        }
        if (species_order_to_save[icol] == ORDERREADUNDEFINED) {
            printf("ERROR: not found species of column %d in the parameter file  = %s \n", icol,order_el_input[icol]);
            error->all(FLERR,"ERROR: not found species");
        }
    }
    return 0;
}

// --------------------------------------------------- //

int PairRFMEAM::determine_species_order_2index(int n_column_par, char **order_el_input, char **order_el_par, int **species_order_2) // Complements the read_files function
{
    bool DS_debug = false;
    bool DS_debug_final = false;
    
    int nindeces = 2;
    char **species_separated = new char*[n_column_par,nindeces]; // to separate
    char *single_element = new char;
    int ielement;
    for (int idim=0; idim <nindeces; idim++) {
        for (int iinit=0; iinit<n_column_par; iinit++) {
            species_order_2[idim][iinit] = ORDERREADUNDEFINED;
            //species_order_2_2[iinit] = ORDERREADUNDEFINED;
        }
    }
    for (int icol=0; icol<n_column_par; icol++){ // select a column of the parameter file
        single_element = strtok(order_el_par[icol],"-_");
        ielement = 0; // initiated position
        while (single_element != nullptr) {
            species_separated[icol,ielement] = single_element;
            ielement++; // incremement position
            single_element = strtok (nullptr, "-_"); // this is needed to keep on reading in the word
        }
        for (int idim=0; idim<nindeces; idim++) {
            for (int iinput=0; iinput<n_column_par; iinput++) {  // run over the names of the input file
                if (strcmp(species_separated[icol,idim],order_el_input[iinput]) == 0) { // tocheck indexes
                    species_order_2[idim][icol] = iinput;
                    break;
                }
            }
            if (species_order_2[idim][icol] == ORDERREADUNDEFINED) {
                printf("ERROR: not found species for dimension %d of column %d in the parameter file \n", idim, icol);
                exit(1);
            }
        } // end of dimension idim
    }
    delete [] single_element;
    delete [] species_separated;
    return 0;
}

// --------------------------------------------------- //

/*int PairRFMEAM::determine_species_order_3index(int n_column_par, char **order_el_input, char **order_el_par, int **species_order_3) // Complements the read_files function
{
    bool DS_debug = false;
    bool DS_debug_final = false;
    int nindeces = 3;
    char **species_separated = new char*[n_column_par,nindeces]; // to separate
    char *single_element = new char;
    int ielement;
    for (int idim=0; idim<nindeces; idim++) {
        for (int iinit=0; iinit<n_column_par; iinit++) {
            species_order_3[idim][iinit] = ORDERREADUNDEFINED;
        }
    }
    for (int icol=0; icol<n_column_par; icol++){ // select a column of the parameter file
        single_element = strtok(order_el_par[icol],"-_");
        ielement = 0; // initiated position
        while (single_element != nullptr) {
            species_separated[icol,ielement] = single_element;
            ielement++; // incremement position
            single_element = strtok (nullptr, "-_"); // this is needed to keep on reading in the word
        }
        for (int idim=0; idim<nindeces; idim++) {
            for (int iinput=0; iinput<n_column_par; iinput++) {  // run over the names of the input file YES
                if (strcmp(species_separated[icol,idim],order_el_input[iinput]) == 0) { // tocheck indexes
                    species_order_3[idim][icol] = iinput;
                    break;
                }
            }
            if (species_order_3[idim][icol] == ORDERREADUNDEFINED) {
                printf("ERROR: not found species for dimension %d of column %d in the parameter file \n", idim, icol);
                exit(1);
            }
    }
        }
    delete [] single_element;
    delete [] species_separated;
    return 0;
}

*/
// ------------------------------------------------------------------------------ //
        
void PairRFMEAM::check_read_parameters() // function to make sure parameters have been read from the potential file
{
            bool DS_debug = false;
            int icycle, jcycle, kcycle;
            if (DS_debug) printf("+check_read_parameters ++++++++++++++++ START check_read_parameters: nelements = %d ++++++++++ \n", nelements);
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.ef.E0[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.ef.E0[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.ef.E1[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.ef.E1[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.ef.E2[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.ef.E2[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha10[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha10[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha11[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha11[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha12[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha12[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha13[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha13[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha20[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha20[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha21[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha21[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha22[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha22[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha23[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha23[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha30[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha30[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha31[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha31[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha32[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha32[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha33[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha33[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha40[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha40[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha41[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha41[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha42[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha42[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.alpha43[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.alpha43[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.t1[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.t1[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.t2[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.t2[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.t3[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.t3[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r10[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r10[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r11[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r11[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r12[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r12[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r13[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r13[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r20[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r20[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r21[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r21[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r22[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r22[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r23[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r23[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r30[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r30[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r31[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r31[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r32[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r32[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r33[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r33[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r40[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r40[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r41[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r41[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r42[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r42[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                if (params.bed.r43[icycle]==INITPARAMETERS) {
                    printf("ERROR: params.bed.r43[%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle);
                    exit(1); }}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.b1[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.b1[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.b2[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.b2[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.b3[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.b3[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.b4[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.b4[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.s1[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.s1[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.s2[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.s2[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.s3[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.s3[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            for (icycle=0; icycle<nelements; icycle++) {
                for (jcycle=0; jcycle<nelements; jcycle++) {
                    if (params.pp.s4[icycle][jcycle]==INITPARAMETERS) {
                        printf("ERROR: params.pp.s4[%d][%d] is NOT read from the parameters file: INSERT IT !!!\n",icycle,jcycle);
                        exit(1); }}}
            if (DS_debug) printf("-check_read_parameters ----------------- END check_read_parameters = CHECK OK -----------\n");
}

// --------------------------------------------------- //

void PairRFMEAM::write_setflag(int nspeciestot)
{
            int icycle, jcycle;
            for (icycle=1; icycle<=nspeciestot; icycle++){
                for (jcycle=1; jcycle<=nspeciestot; jcycle++){
                    setflag[icycle][jcycle] = 1;  
                }}
}

// --------------------------------------------------- //
        

void PairRFMEAM::compute_phi_dphi_pp(double &phi, double *d_phi_di, double *d_phi_dj, int indsp_1, int indsp_2, double r_here, double *vec_dif, int i_atom, int j_atom) // function to calculate phi and dphi for an atom pair // Needs to be altered depending on the formalism of RFMEAM
{
            bool DS_debug = false;
    double z1=z[i_atom];
    double z2=z[j_atom];
    if (r_here<=0.9)
    {
        double rsbier,x,eps,depsdr,R_phi;
        rsbier=0.88534*0.5292/(pow(z1,0.23)+pow(z2,0.23));
        x=r_here/rsbier;
        eps=0.1818*exp(-3.2*x)+0.5099*exp(-0.9423*x)+0.2802*exp(-0.4029*x)+0.02817*exp(-0.2016*x);
        phi=14.3978*z1*z2*eps/r_here;
        depsdr=(1/rsbier)*(0.1818*(-3.2)*exp(-3.2*x)+0.5099*(-0.9423)*exp(-0.9423*x)+0.2802*(-0.4029)*exp(-0.4029*x)+0.02817*(-0.2016)*exp(-0.2018*x));
        R_phi=-14.3978*z1*z2*(eps/pow(r_here,2)-depsdr/r_here);
        for (int i012=0; i012 < 3 ; i012++){
            d_phi_di[i012] = R_phi * vec_dif[i012] / r_here;
            d_phi_dj[i012] = - R_phi * vec_dif[i012] / r_here;
        }
    }
    else if (r_here>0.9 && r_here<1.5)
    {
        double rsbier,x,eps,depsdr,k1,k2,k3,k4,R_phi;
        double b1,b2,b3,b4,s1,s2,s3,s4,phi1,phi2,phi3,phi4,dphi1,dphi2,dphi3,dphi4;
        double rdiffcb,rbiersq,rbiercb,cutoffminsq,cutoffmincb,B0,B1,B2,B3;
        
        rsbier=0.88534*0.5292/(pow(z1,0.23)+pow(z2,0.23));
        x=0.9/rsbier;
        eps=0.1818*exp(-3.2*x)+0.5099*exp(-0.9423*x)+0.2802*exp(-0.4029*x)+0.02817*exp(-0.2016*x);
        k1=14.3978*z1*z2*eps/0.9;
        depsdr=(1/rsbier)*(0.1818*(-3.2)*exp(-3.2*x)+0.5099*(-0.9423)*exp(-0.9423*x)+0.2802*(-0.4029)*exp(-0.4029*x)+0.02817*(-0.2016)*exp(-0.2018*x));
        k2=-14.3978*z1*z2*(eps/pow(0.9,2)-depsdr/0.9);
        
        b1 = params.pp.b1[indsp_1][indsp_2];
        b2 = params.pp.b2[indsp_1][indsp_2];
        b3 = params.pp.b3[indsp_1][indsp_2];
        b4 = params.pp.b4[indsp_1][indsp_2];
        s1 = params.pp.s1[indsp_1][indsp_2];
        s2 = params.pp.s2[indsp_1][indsp_2];
        s3 = params.pp.s3[indsp_1][indsp_2];
        s4 = params.pp.s4[indsp_1][indsp_2];
        
        if (1.5>=s1)
        {phi1=0;
            dphi1=0;}
        else
        {phi1=b1*(s1-1.5)*(s1-1.5)*(s1-1.5);
            dphi1=-3*b1*(s1-1.5)*(s1-1.5);}
        if (1.5>=s2)
        {phi2=0;
            dphi2=0;}
        else
        {phi2=b2*(s2-1.5)*(s2-1.5)*(s2-1.5);
            dphi2=-3*b2*(s2-1.5)*(s2-1.5);}
        if (1.5>=s3)
        {phi3=0;
            dphi3=0;}
        else
        {phi3=b3*(s3-1.5)*(s3-1.5)*(s3-1.5);
            dphi3=-3*b3*(s3-1.5)*(s3-1.5);}
        if (1.5>=s4)
        {phi4=0;
            dphi4=0;}
        else
        {phi4=b4*(s4-1.5)*(s4-1.5)*(s4-1.5);
            dphi4=-3*b4*(s4-1.5)*(s4-1.5);}
        k3=phi1+phi2+phi3+phi4;
        k4=dphi1+dphi2+dphi3+dphi4;
        
        rdiffcb=pow((0.9-1.5),3);
        rbiersq=0.9*0.9;
        rbiercb=0.9*0.9*0.9;
        cutoffminsq=1.5*1.5;
        cutoffmincb=1.5*1.5*1.5;
        
        B0=(-1/rdiffcb)*(-k3*rbiercb+3*k3*rbiersq*1.5+k4*rbiercb*1.5-3*k1*0.9*cutoffminsq+k2*rbiersq*cutoffminsq-k4*rbiersq*cutoffminsq+k1*cutoffmincb-k2*0.9*cutoffmincb);
        
        B1=(-1/rdiffcb)*(-k4*rbiercb+6*k1*0.9*1.5-6*k3*0.9*1.5-2*k2*rbiersq*1.5-k4*rbiersq*1.5+k2*0.9*cutoffminsq+2*k4*0.9*cutoffminsq+k2*cutoffmincb);
        
        B2=(-1/rdiffcb)*(-3*k1*0.9+3*k3*0.9+k2*rbiersq+2*k4*rbiersq-3*k1*1.5+3*k3*1.5+k2*0.9*1.5-k4*0.9*1.5-2*k2*cutoffminsq-k4*cutoffminsq);
        
        B3=-(2*k1-2*k3-k2*0.9-k4*0.9+k2*1.5+k4*1.5)/rdiffcb;
        
        phi=B0+B1*r_here+B2*r_here*r_here+B3*r_here*r_here*r_here;
        
        R_phi=B1+2*B2*r_here+3*B3*r_here*r_here;
        
        for (int i012=0; i012 < 3 ; i012++){
            d_phi_di[i012] = R_phi * vec_dif[i012] / r_here;
            d_phi_dj[i012] = - R_phi * vec_dif[i012] / r_here;
        }
        
    }
    else
    {   double b1,b2,b3,b4,s1,s2,s3,s4,phi1,phi2,phi3,phi4,dphi1,dphi2,dphi3,dphi4,R_phi;
        
            b1 = params.pp.b1[indsp_1][indsp_2];
            b2 = params.pp.b2[indsp_1][indsp_2];
            b3 = params.pp.b3[indsp_1][indsp_2];
            b4 = params.pp.b4[indsp_1][indsp_2];
            s1 = params.pp.s1[indsp_1][indsp_2];
            s2 = params.pp.s2[indsp_1][indsp_2];
            s3 = params.pp.s3[indsp_1][indsp_2];
            s4 = params.pp.s4[indsp_1][indsp_2];
            if (r_here>=s1)
            {phi1=0;
                dphi1=0;}
            else
                {phi1=b1*(s1-r_here)*(s1-r_here)*(s1-r_here);
                    dphi1=-3*b1*(s1-r_here)*(s1-r_here);}
            if (r_here>=s2)
            {phi2=0;
                dphi2=0;}
            else
                {phi2=b2*(s2-r_here)*(s2-r_here)*(s2-r_here);
                    dphi2=-3*b2*(s2-r_here)*(s2-r_here);}
            if (r_here>=s3)
            {phi3=0;
                dphi3=0;}
            else
                {phi3=b3*(s3-r_here)*(s3-r_here)*(s3-r_here);
                    dphi3=-3*b3*(s3-r_here)*(s3-r_here);}
            if (r_here>=s4)
            {phi4=0;
                dphi4=0;}
            else
                {phi4=b4*(s4-r_here)*(s4-r_here)*(s4-r_here);
                    dphi4=-3*b4*(s4-r_here)*(s4-r_here);}
            phi=phi1+phi2+phi3+phi4;
            R_phi=dphi1+dphi2+dphi3+dphi4;
            for (int i012=0; i012 < 3 ; i012++){
                d_phi_di[i012] = R_phi * vec_dif[i012] / r_here;
                d_phi_dj[i012] = - R_phi * vec_dif[i012] / r_here;
            }
    }
            return ; // returned the value
}

// --------------------------------- //

void PairRFMEAM::compute_rhosq_drhosq(int l_index, double &rhosq_here, double **d_rhosq_here, int i_atom) // function to calculate rhosq and drhosq // Needs to be altered depending on the formalism of RFMEAM
{
            bool DS_debug = false;
            int j_atom, j_type, j_tag;
            int         i_type, i_tag;
            int k_atom, k_type, k_tag;
            double x_i, y_i, z_i;
            double x_j, y_j, z_j;
            double x_k, y_k, z_k;
            int num_neigh;
            int *ilist; // list of neighbors
            double **x;
            int *type;
            tagint *tag;
            double delx_ij, dely_ij, delz_ij;
            double delx_ik, dely_ik, delz_ik;
            double r_ij, r_ik;
            double vec_ij[3], vec_ji[3];
            double vec_ik[3], vec_ki[3];
            double sum_over_k;

            double f_jl_rij, d_f_jl_rij_di[3], d_f_jl_rij_dj[3]; // df/dx (mu=i) ; (mu=j) :: the only ones not null
            double f_kl_rik, d_f_kl_rik_di[3], d_f_kl_rik_dk[3]; // df/dx (mu=i) ; (mu=k) :: the only ones not null
            double P_l_cosjik, dP_l_cosjik_dj[3], dP_l_cosjik_di[3], dP_l_cosjik_dk[3]; // Legendre polynomia and their derivatives wrt i,j,k
            double A_all;
            double B_f,B_P,B_all;
            int mu_atom, mu_tag, mu_type;
            double x_mu, y_mu, z_mu;
            if (l_index < 0) error->all(FLERR,"ERROR in compute_rhosq_drhosq: index l must be > 0 !!");
            if (l_index > MAXLegenderePoly) error->all(FLERR,"ERROR in compute_rhosq_drhosq: index l is > 3 ?!?!! This must be implemented !!");
            num_neigh = list->numneigh[i_atom]; // number of neighbors of i (without i itself)
            ilist = list->firstneigh[i_atom]; // list of neighbors of atom i_atom (here is not i_atom)
            x = atom->x ; // coordinates of all atoms (with local indeces)
            type = atom->type; // species 0->n_type-1 (with local indeces)
            tag = atom->tag; // tag for global index
            i_type = map[type[i_atom]]; // species 0-> n_types - 1
            i_tag = tag[i_atom]; // global index
            x_i = x[i_atom][0]; // coordinate x of i
            y_i = x[i_atom][1]; // coordinate y of i
            z_i = x[i_atom][2]; // coordinate z of i
            for (int init=0; init<num_neigh+1; init++){
                for (int i012=0; i012<3; i012++){
                    d_rhosq_here[init][i012] = 0.0;
                }
            }
            rhosq_here = 0.0;
            for (int j=0; j<num_neigh; j++){
                j_atom = ilist[j]; // local index
                j_tag  = tag[j_atom];    // global index
                j_type = map[type[j_atom]]; // species 0->ntype-1
                x_j = x[j_atom][0]; // coordinate x of j
                y_j = x[j_atom][1]; // coordinate y of j
                z_j = x[j_atom][2]; // coordinate z of j
                if (j_atom == i_atom) continue; // cycle for => only cases for j<>i : it will never happen! => anyway let's leave it
                delx_ij = x_i-x_j;
                dely_ij = y_i-y_j;
                delz_ij = z_i-z_j;
                r_ij = sqrt(delx_ij*delx_ij + dely_ij*dely_ij + delz_ij*delz_ij);
                vec_ij[0] = x_i-x_j;
                vec_ij[1] = y_i-y_j;
                vec_ij[2] = z_i-z_j;
                vec_ji[0] = - vec_ij[0];
                vec_ji[1] = - vec_ij[1];
                vec_ji[2] = - vec_ij[2];

                compute_eff_d_eff_jlr(f_jl_rij, d_f_jl_rij_di, d_f_jl_rij_dj, j_type, l_index, r_ij, vec_ij); // Function call to compute fij

                A_all = f_jl_rij;
                sum_over_k = 0.0;
                for (int k=0; k<num_neigh; k++) {
                    k_atom = ilist[k]; // local index
                    k_tag  =  tag[k_atom];    // global index
                    k_type = map[type[k_atom]]; // species 0->ntype-1
                    x_k = x[k_atom][0]; // coordinate x of j
                    y_k = x[k_atom][1]; // coordinate y of j
                    z_k = x[k_atom][2]; // coordinate z of j
                    if (k_atom == i_atom) continue; // cycle for => j<>i
                    delx_ik = x_i-x_k;
                    dely_ik = y_i-y_k;
                    delz_ik = z_i-z_k;
                    r_ik = sqrt(delx_ik*delx_ik + dely_ik*dely_ik + delz_ik*delz_ik);
                    vec_ik[0] = x_i-x_k;
                    vec_ik[1] = y_i-y_k;
                    vec_ik[2] = z_i-z_k;
                    vec_ki[0] = - vec_ik[0];
                    vec_ki[1] = - vec_ik[1];
                    vec_ki[2] = - vec_ik[2];

                    
                    compute_eff_d_eff_jlr(f_kl_rik, d_f_kl_rik_di, d_f_kl_rik_dk, k_type, l_index, r_ik, vec_ik); // function call to compute Fik
                    compute_P_dP_legendre_cos(P_l_cosjik, dP_l_cosjik_dj, dP_l_cosjik_di, dP_l_cosjik_dk, l_index, vec_ji, r_ij, vec_ki, r_ik); // Function call to calculate P and DP
                    sum_over_k += f_kl_rik * P_l_cosjik;
                    B_f = P_l_cosjik;
                    B_P = f_kl_rik;
                    B_all = f_kl_rik * P_l_cosjik;
                    
                    /*Calculating derivatives*/
                    for (int mu_now=0; mu_now < num_neigh + 1; mu_now++) {
                        if (mu_now == num_neigh)
                        mu_atom = i_atom;  //case to consider
                        else
              	         mu_atom = ilist[mu_now]; // local index
                        mu_tag  =  tag[mu_atom];    // global index
                        mu_type = map[type[mu_atom]]; // species 0->ntype-1
                        x_mu = x[mu_atom][0]; // coordinate x of j
                        y_mu = x[mu_atom][1]; // coordinate y of j
                        z_mu = x[mu_atom][2]; // coordinate z of j
                        if (mu_atom == i_atom) {
                            for (int i012=0; i012<3; i012++){
                                d_rhosq_here[mu_now][i012] += d_f_jl_rij_di[i012] * B_all;
                                d_rhosq_here[mu_now][i012] += A_all * d_f_kl_rik_di[i012] * B_f;
                                d_rhosq_here[mu_now][i012] += A_all * dP_l_cosjik_di[i012] * B_P;
                            }
                        }
                        if (mu_atom == j_atom) {
                            for (int i012=0; i012<3; i012++){
                                d_rhosq_here[mu_now][i012] += d_f_jl_rij_dj[i012] * B_all;
                                d_rhosq_here[mu_now][i012] += A_all * dP_l_cosjik_dj[i012] * B_P;
                            }
                        }
                        if (mu_atom == k_atom) {
                            for (int i012=0; i012<3; i012++){
                                d_rhosq_here[mu_now][i012] += A_all * d_f_kl_rik_dk[i012] * B_f;
                                d_rhosq_here[mu_now][i012] += A_all * dP_l_cosjik_dk[i012] * B_P;
                            }
                        }
                    } // mu_now
                } // sum over k
//                rhosq_here += sum_over_k * S_ij * h_jj_rij * f_jl_rij ;
                rhosq_here += sum_over_k * f_jl_rij ;
            } // sum over j
            return ; // returned the value
        }

        // ---------------------------------------- //
        
void PairRFMEAM::compute_eff_d_eff_jlr(double &f_here, double *d_f_di, double *d_f_dj, int j_species, int l_here, double r_here, double *vec_ij) // Function to calculate f and df // Needs to be altered based on the RFMEAM formalism
{
            bool DS_debug = false;
            double alpha1_here,alpha2_here,alpha3_here,alpha4_here,r1_here,r2_here,r3_here,r4_here,f1,f2,f3,f4,df1,df2,df3,df4;
            switch(l_here) {
                case (0):
                alpha1_here = params.bed.alpha10[j_species];
                alpha2_here = params.bed.alpha20[j_species];
                alpha3_here = params.bed.alpha30[j_species];
                alpha4_here = params.bed.alpha40[j_species];
                r1_here = params.bed.r10[j_species];
                r2_here = params.bed.r20[j_species];
                r3_here = params.bed.r30[j_species];
                r4_here = params.bed.r40[j_species];
                break;
                case (1):
                alpha1_here = params.bed.alpha11[j_species];
                alpha2_here = params.bed.alpha21[j_species];
                alpha3_here = params.bed.alpha31[j_species];
                alpha4_here = params.bed.alpha41[j_species];
                r1_here = params.bed.r11[j_species];
                r2_here = params.bed.r21[j_species];
                r3_here = params.bed.r31[j_species];
                r4_here = params.bed.r41[j_species];
                break;
                case (2):
                alpha1_here = params.bed.alpha12[j_species];
                alpha2_here = params.bed.alpha22[j_species];
                alpha3_here = params.bed.alpha32[j_species];
                alpha4_here = params.bed.alpha42[j_species];
                r1_here = params.bed.r12[j_species];
                r2_here = params.bed.r22[j_species];
                r3_here = params.bed.r32[j_species];
                r4_here = params.bed.r42[j_species];
                break;
                case (3):
                alpha1_here = params.bed.alpha13[j_species];
                alpha2_here = params.bed.alpha23[j_species];
                alpha3_here = params.bed.alpha33[j_species];
                alpha4_here = params.bed.alpha43[j_species];
                r1_here = params.bed.r13[j_species];
                r2_here = params.bed.r23[j_species];
                r3_here = params.bed.r33[j_species];
                r4_here = params.bed.r43[j_species];
                break;
                default:
                error->all(FLERR,"ERROR: What index of parameters alpha and r do you want ???");
            }
            if (r_here>=r1_here)
            {f1=0;
                df1=0;}
            else
            {f1=alpha1_here*(r1_here-r_here)*(r1_here-r_here)*(r1_here-r_here);
                df1=-3*alpha1_here*(r1_here-r_here)*(r1_here-r_here);}
            if (r_here>=r2_here)
            {f2=0;
                df2=0;}
            else
            {f2=alpha2_here*(r2_here-r_here)*(r2_here-r_here)*(r2_here-r_here);
                df2=-3*alpha2_here*(r2_here-r_here)*(r2_here-r_here);}
            if (r_here>=r3_here)
            {f3=0;
                df3=0;}
            else
            {f3=alpha3_here*(r3_here-r_here)*(r3_here-r_here)*(r3_here-r_here);
                df3=-3*alpha3_here*(r3_here-r_here)*(r3_here-r_here);}
            if (r_here>=r4_here)
            {f4=0;
                df4=0;}
            else
            {f4=alpha4_here*(r4_here-r_here)*(r4_here-r_here)*(r4_here-r_here);
                df4=-3*alpha4_here*(r4_here-r_here)*(r4_here-r_here);}
            f_here=f1+f2+f3+f4;
            double R_f_l;
            R_f_l=df1+df2+df3+df4;
            for (int i012=0; i012<3 ; i012++){
                d_f_di[i012] = R_f_l * vec_ij[i012] / r_here;
                d_f_dj[i012] = - R_f_l * vec_ij[i012] / r_here;
            }
            return ;
}

// ---------------------------------------------------------- //
        
void PairRFMEAM::compute_P_dP_legendre_cos(double &Pcosjik, double *d_Pcosjik_dj, double *d_Pcosjik_di, double *d_Pcosjik_dk, int l_index_leg, double *vec_ji, double mod_vec_ji, double* vec_ki, double mod_vec_ki) // Function to calculate P and DP
{
            bool DS_debug = false;
            double cosjik, d_cosjik_dj[3], d_cosjik_di[3], d_cosjik_dk[3];
            if (l_index_leg < 0) error->all(FLERR,"ERROR: Index for Legendre polynomia in compute_P_dP_legendre_cos < 0 !!!");
            if (l_index_leg > MAXLegenderePoly) 
            error->all(FLERR,"ERROR: Index for Legendre polynomia in compute_P_dP_legendre_cos beyond limit !! To implement if necessary!!!");
            if (l_index_leg == 0) {
                Pcosjik = 1.0; 
                for (int i012=0; i012<3; i012++){
                    d_Pcosjik_dj[i012] = 0.0;
                    d_Pcosjik_di[i012] = 0.0;
                    d_Pcosjik_dk[i012] = 0.0;
                }
                return;
            }
            double AAA, BBB;
            AAA = vec_ji[0]*vec_ki[0] + vec_ji[1]*vec_ki[1] + vec_ji[2]*vec_ki[2]; // dot product of vector ji and ki
            BBB = mod_vec_ji * mod_vec_ki; // product of the moduli of these vectors
            cosjik = AAA / BBB;
            for (int i012=0; i012<3; i012++){
                d_cosjik_dj[i012] = (vec_ki[i012] / BBB) - (AAA*vec_ji[i012]/(mod_vec_ji*mod_vec_ji*mod_vec_ji*mod_vec_ki));
                d_cosjik_di[i012] = ((-vec_ki[i012]-vec_ji[i012])/BBB) + (AAA* (vec_ji[i012]/(mod_vec_ji*mod_vec_ji*mod_vec_ji*mod_vec_ki) + vec_ki[i012]/(mod_vec_ki*mod_vec_ki*mod_vec_ki*mod_vec_ji)));
                d_cosjik_dk[i012] = (vec_ji[i012] / BBB) - (AAA*vec_ki[i012]/(mod_vec_ki*mod_vec_ki*mod_vec_ki*mod_vec_ji));
            }
            switch(l_index_leg) {
                case (1):
                Pcosjik = cosjik;
                for (int i012=0; i012<3; i012++){
                    d_Pcosjik_dj[i012] = d_cosjik_dj[i012];
                    d_Pcosjik_di[i012] = d_cosjik_di[i012];
                    d_Pcosjik_dk[i012] = d_cosjik_dk[i012];
                };
                break;
                case (2):
                Pcosjik = 0.5 * (3.0*cosjik*cosjik - 1.0); 
                for (int i012=0; i012<3; i012++){
                    d_Pcosjik_dj[i012] = d_cosjik_dj[i012] * 3.0 * cosjik;
                    d_Pcosjik_di[i012] = d_cosjik_di[i012] * 3.0 * cosjik;
                    d_Pcosjik_dk[i012] = d_cosjik_dk[i012] * 3.0 * cosjik;
                };
                break;
                case (3):
                Pcosjik = 0.5 * (5.0*cosjik*cosjik*cosjik - 3.0*cosjik);
                for (int i012=0; i012<3; i012++){
                    d_Pcosjik_dj[i012] = d_cosjik_dj[i012] * (15.0*cosjik*cosjik*0.5 - 3.0*0.5);
                    d_Pcosjik_di[i012] = d_cosjik_di[i012] * (15.0*cosjik*cosjik*0.5 - 3.0*0.5);
                    d_Pcosjik_dk[i012] = d_cosjik_dk[i012] * (15.0*cosjik*cosjik*0.5 - 3.0*0.5);
                };
                break; 
                default:
                error->all(FLERR,"ERROR: What index of Legendre ???");
            }
            return ; // returned the value
}
        
// End //


        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
