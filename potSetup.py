#
# Copyright (C) 2020-2023, Dr Andrew Ian Duff,
# Science and Technologies Facilities Research Council (STFC). All rights reserved.

import glob
import os
import re
import shutil, sys
from string import ascii_letters

import ase
import pymatgen.io.ase as aio
from pymatgen.core import Structure
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer

# Modules:
import DFTsetup,DFTanalysis
import filesys
import initialize
import outf
import processJobs
import var  # variable container


def formatNumber(n):
    # This ensures weights use the convention of MEAMfit. e.g. 10, 1, 0.1, 0.01 (I.e. no zeros after decimal unless necessary)
    return n if n % 1 else int(n)


def generateFitdbse(path, weight):

    # weight: Prefactor to multiply the standard energy, force and stress weights. Usually 1.0

    if var.meamfit_so: import meamfit_py

    os.chdir(path)
    # Clean directory of MEAMfit fitdbse file
    if os.path.isfile("fitdbse"):
        os.remove("fitdbse")
    if os.path.isfile("settings"):
        os.remove("settings")
    # Edit settings file
    if var.verbosity <= 1:
        settings_file = open("settings", "w")
        settings_file.write(" VERBOSITY=0\n")
        settings_file.close()
    outf.printt(2, "  Running MEAMfit to generate fitdbse file...")
    if var.meamfit_so and not var.startedMPIforMEAMfit:
        meamfit_py.start_mpi()
        var.startedMPIforMEAMfit = True
    processJobs.runMEAMfit(path, False)

    if weight != 1.0:
        outf.printt(2, "  Multiplying all weights by " + str(weight))
        fitdbseFile = open("fitdbse", "r")
        lines = fitdbseFile.readlines()
        fitdbseFile.close()
        fitdbseFile = open("fitdbse", "w")
        for line in lines:
            lineEdit = line.replace(
                "1 0.1 0.001",
                str(formatNumber(weight))
                + " "
                + str(formatNumber(0.1 * weight))
                + " "
                + str(formatNumber(0.001 * weight)),
            )
            fitdbseFile.write(lineEdit)
        fitdbseFile.close()

    # Files of the form: castep_cij__1__1.castep, need a further alteration. These are run with fixed cell paras, and
    # CASTEP only outputs stress at end (while forces at each step). Therefore MEAMfit generates zero stress weight.
    # Alter so that only last ionic step included, but with fitting to EFS. (This is necessary as my code for
    # generating .cfg files for MLIP runs into difficulities if not every config has associated EFSs)
    fitdbseFile = open("fitdbse", "r")
    lines = fitdbseFile.readlines()
    fitdbseFile.close()
    fitdbseFile = open("fitdbse", "w")
    for line in lines:
        # if line contains cij__ then just set to read in last config, and adjust to read also stress tensor
        # e.g: castep_cij__1__3.castep                            1-13   Fr 1 0.1 0
        # ->
        # castep_cij__1__3.castep                            13   Fr 1 0.1 0.001
        if "cij__" in line:
            # Split the line into parts
            parts = line.split()
            # Extract and modify the specific part of the line
            filename = parts[0]
            config_range = parts[1]
     
            # Get the last number in the range
            last_config = config_range.split('-')[-1]
            
            # Reconstruct the line with new parameters, preserving original spacing
            new_line = line.replace(config_range, last_config, 1)
            new_line_last_word_removed = re.sub(r'\s+\S+\s*$', '', new_line)
            new_line = new_line_last_word_removed + ' 0.001\n'
        else:
            new_line = line  # If no "cij__" in line, keep it unchanged
        fitdbseFile.write(new_line)
    fitdbseFile.close()


    os.chdir(var.dirpath)

def checkCoulSep():
    # There are a small category of molten salts for which a recent study has demonstrated fixed charges are a good approximation
    # (J. Chem. Phys. 153, 214502 (2020), the following molten salts can be modelled using fixed Coulomb charges: LiCl, NaCl, KCl, and RbCl)
    # Here we check if the materials being considered belong to this set, and if so prompt the user whether to separate the Coulomb part from the fitted potential.
    # This would require addition of settings tag to settings.APD by user.
    # Also check necessary files are present in the work directory if user specifies this option.

    if var.auto_select_temp == False and var.sep_coulomb_pot_specified == False:
        # Check if elements of material may suggest using a separated Coulomb + EAM form of potential

        # Check species of materials, looping over all material for one xc-func (all should contain the same materials)
        allMaterialsFixedCharge = True
        xc = var.xcFunc[0]
        for mat in var.materials:
            initialize.initDirsPaths(xc, mat, "")
            if var.dft_engine == "VASP":
                var.structure = Structure.from_file(var.DFTinputpath + "/POSCAR")
            elif var.dft_engine == "CASTEP":
                print(f'about to open {var.DFTinputpath}/castep.cell')
                castepCellFile = open(var.DFTinputpath + "/castep.cell", "r")
                print('castepCellFile=',castepCellFile)
                var.atoms = ase.io.castep.read_castep_cell(castepCellFile)
                var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
            elif var.dft_engine == "PWSCF":
                # following doesn't read everything in from input but should be enough for this subroutine
                # Note we do a whole 'setupDFTinput' because we also need to read in var.DFTprefix so we known
                # the name of the file. The way we get that is by looking at 1_.../ . I.e. just the following two
                # lines wouldn't work...
                # var.atoms = ase.io.read(var.DFTinputpath + '/' + var.DFTprefix + '.in', format='espresso-in')
                # var.structure = aio.AseAtomsAdaptor.get_structure(var.atoms)
                DFTsetup.setupDFTinput(var.DFTinputdir, var.DFTinputpath)
            outf.printt(2, "  Species in material " + str(mat) + ": " + str(var.structure.symbol_set))
            # Check if all the materials being modelled are in the category listed above:
            if len(var.structure.symbol_set) == 2:
                if not (
                    (var.structure.symbol_set[0] == "Cl" and var.structure.symbol_set[1] == "Li")
                    or (var.structure.symbol_set[0] == "Cl" and var.structure.symbol_set[1] == "Na")
                    or (var.structure.symbol_set[0] == "Cl" and var.structure.symbol_set[1] == "K")
                    or (var.structure.symbol_set[0] == "Cl" and var.structure.symbol_set[1] == "Rb")
                ):
                    allMaterialsFixedCharge = False
            else:
                allMaterialsFixedCharge = False
        if allMaterialsFixedCharge == True:
            var.stopAPD = True
            if var.verbosity == 1:
                print("  Species in material:" + str(var.structure.symbol_set))
            outf.printt(
                1,
                "  ...species belonging to list: LiCl, NaCl, KCl, RbCl which can be well approximated as having fixed charges [J. Chem. Phys. 153, 214502 (2020)]. If you are fitting to only the liquid phase (molten salt) you are advised to set sep_coulomb_pot=True to optimize a hybrid Coulomb + EAM potential, STOPPING",
            )
            outf.toAction(
                "If you are modelling only the liquid phase, it is recommended to add sep_coulomb_pot=True to your settings file. Please consult standard output for more information, add the tag (set it either True or False), and re-run APD"
            )
            quit()

    if var.sep_coulomb_pot_specified == True and (
        os.path.isfile(var.dirpath + "/getCoulombObservables.sh") == False
        or os.path.isfile(var.dirpath + "/lammpsIn_energyForce") == False
        or os.path.isfile(var.dirpath + "/lammpsIn_stress") == False
    ):
        outf.printt(
            0,
            "  ERROR: sep_coulomb_pot=True and getCoulombObservables.sh and/or lammpsIn_energyForce and/or lammpsIn_stress (from MEAMfit src/) not found in run directory, STOPPING",
        )
        quit()


def reorder_atom_data(atom_data, speciesCell, speciesCastep):

    """
    Reorders atom data from .cfg file based on species mapping from speciesCastep to speciesCell.

    Args:
    - atom_data: List of atom data lines from .cfg file.
    - speciesCell: Target species order from .cell file.
    - speciesCastep: Original species order in .cfg file.

    Returns:
    - Reordered list of atom data lines.
    """

    # Map speciesCastep to speciesCell
    species_map = {speciesCastep.index(spec): speciesCell.index(spec) for spec in speciesCastep}

    # Reorder atom data
    reordered_data = []
    for line in atom_data:
        parts = re.split(r'\s+', line.strip())
        if len(parts) > 2:
            species_idx = int(parts[1])
            new_species_idx = species_map.get(species_idx, species_idx)
            parts[1] = str(new_species_idx)
            reordered_line = ' '.join(parts)
            reordered_data.append(reordered_line + '\n')
        else:
            reordered_data.append(line)

    # Sort the data by new species index
    reordered_data.sort(key=lambda x: int(re.split(r'\s+', x.strip())[1]))

    return reordered_data


#def reorderSpeciesInCfg(speciesCell, speciesCastep, optpath):
#
#    """
#    DEPRECATED: CASTEP reorders species. The plan before was to correct for this be ordering them back to the original
#    .cell ordering in the .cfg file (that used to train MTP potentials). But instead now we retain this ordering and
#    instead change the ordering of species in the POSCAR.lmp files for LAMMPS simulations.
#
#    Reorder species in var.potOptpath/config.cfg. Species indices in current file (second column in AtomData blocks in 
#    cfg file) map to the species in the list 'speciesCastep'. We instead want to alter the .cfg file so that the
#    indices instead map to the species in 'speciesCell'. We then want to reorder these indices so that, as with the
#    original .cfg file, they are in increasing order. The purpose of this is to provide potentials which can be used
#    in conjunction with LAMMPS structure files that have species ordering the same as the .cell files used to run DFT
#    jobs. This seems preferable to reordering the initial structure file, which would then not be consistent with the
#    initial DFT structure.
#
#    Example: (very unphysical atom configuration, just for illustrative purposes. Only show one CFG block.
# 
#    Initial config.cfg:
# 
#    BEGIN_CFG
#     Size
#       190
#     Supercell
#            14.121821      0.000000      0.000000
#             0.000000     14.121821      0.000000
#             0.000000      0.000000     13.551698
#     AtomData:  id type       cartes_x      cartes_y      cartes_z           fx          fy          fz
#                 1    0       4.501881      9.619940     10.163773     0.298290   -0.575430    0.436370
#                 2    0       9.355226     12.984803     11.100779     0.298150   -0.476460   -0.019520
#                 3    1       0.000000      0.000000      6.775849     0.073190    0.033400    0.064110
#                 4    1       0.000000      0.000000      0.000000     0.049980    0.029690   -0.089390
#                 5    2       7.060911      0.000000      3.387925    -0.136080    0.170510   -0.154240
#                 6    2       0.000000      7.060911      3.387925     0.056240   -0.046680   -0.043150
#     Energy
#         -98168.634564079999
#     ...
#    END_CFG
# 
#    speciesCastep = ["Zr", "Nb", "C"]
#    speciesCell = ["C", "Nb", "Zr"]
# 
#    New config.cfg:
# 
#    BEGIN_CFG
#     Size
#       190
#     Supercell
#            14.121821      0.000000      0.000000
#             0.000000     14.121821      0.000000
#             0.000000      0.000000     13.551698
#     AtomData:  id type       cartes_x      cartes_y      cartes_z           fx          fy          fz
#                 1    0       7.060911      0.000000      3.387925    -0.136080    0.170510   -0.154240
#                 2    0       0.000000      7.060911      3.387925     0.056240   -0.046680   -0.043150
#                 3    1       0.000000      0.000000      6.775849     0.073190    0.033400    0.064110
#                 4    1       0.000000      0.000000      0.000000     0.049980    0.029690   -0.089390
#                 5    2       4.501881      9.619940     10.163773     0.298290   -0.575430    0.436370
#                 6    2       9.355226     12.984803     11.100779     0.298150   -0.476460   -0.019520
#     Energy
#         -98168.634564079999
#     ...
#    END_CFG
#    """
# 
#    cfg_path = os.path.join(optpath, 'config.cfg')
#    with open(cfg_path, 'r') as file:
#        lines = file.readlines()
# 
#    # Step 2: Process each configuration block
#    new_lines = []
#    inside_atom_data = False
#    for line in lines:
#        # Check for start of atom data block
#        if 'AtomData:' in line:
#            inside_atom_data = True
#            atom_data = []
#            new_lines.append(line)
#            continue
# 
#        # Check for end of atom data block
#        if inside_atom_data and 'Energy' in line:
#            inside_atom_data = False
#            # Process atom data
#            reordered_data = reorder_atom_data(atom_data, speciesCell, speciesCastep)
#            new_lines.extend(reordered_data)
#            new_lines.append(line)
#            continue
# 
#        # Collect atom data
#        if inside_atom_data:
#            atom_data.append(line)
#        else:
#            new_lines.append(line)
# 
#    # Step 3: Write the modified data back to the file
#    with open(cfg_path, 'w') as file:
#        for line in new_lines:
#            file.write(line)

def backup_file(original, backup):
    """
    Creates a backup of the original file.
    """
    try:
        shutil.copyfile(original, backup)
        print(f"Backup created: {backup}")
    except Exception as e:
        print(f"Error creating backup: {e}")
        sys.exit(1)

def split_configurations_cfg(cfg_lines):
    """
    Splits config.cfg into individual configurations.
    Each configuration starts with 'BEGIN_CFG' and ends with 'END_CFG'.
    Returns a list of configurations, each as a list of lines.
    """
    configurations = []
    current_cfg = []
    inside_cfg = False
    for line in cfg_lines:
        if line.strip() == 'BEGIN_CFG':
            inside_cfg = True
            current_cfg = [line]
        elif line.strip() == 'END_CFG':
            current_cfg.append(line)
            configurations.append(current_cfg)
            inside_cfg = False
        elif inside_cfg:
            current_cfg.append(line)
    return configurations

def split_observables(observables_lines, num_atoms_per_cfg_list):
    """
    Splits coulombObservablesFull into individual configurations.
    Each configuration consists of:
        - 1 line energy
        - N lines of forces (where N is the number of atoms for that config)
        - 4 lines of stress tensor (only last two lines are used)
    Returns a list of configurations, each as a list of lines.
    """
    configurations = []
    idx = 0
    total_configs = len(num_atoms_per_cfg_list)
    total_lines = len(observables_lines)

    for cfg_idx, num_atoms in enumerate(num_atoms_per_cfg_list):
        lines_per_cfg = 1 + num_atoms + 4  # energy + forces + stress
        if idx + lines_per_cfg > total_lines:
            print(f"Error: Not enough lines in coulombObservablesFull for configuration {cfg_idx + 1}.")
            sys.exit(1)
        cfg = observables_lines[idx:idx+lines_per_cfg]
        configurations.append(cfg)
        idx += lines_per_cfg
    if idx != total_lines:
        print("Warning: Extra lines detected in coulombObservablesFull that do not correspond to any configuration.")
    return configurations

def parse_config_cfg(config_lines):
    """
    Parses a single configuration from config.cfg.
    Returns a dictionary with keys: 'num_atoms', 'energy', 'forces', 'stress'.
    'forces' is a list of [fx, fy, fz] for each atom.
    'stress' is a list [xx, yy, zz, yz, xz, xy],
    """
    data = {
        'num_atoms': 0,
        'energy': 0.0,
        'forces': [],
        'stress': [],
    }
    section = None
    for i, line in enumerate(config_lines):
        stripped = line.strip()
        if stripped.startswith('Size'):
            # The next line contains the number of atoms
            if i + 1 < len(config_lines):
                size_line = config_lines[i+1].strip()
                try:
                    data['num_atoms'] = int(size_line)
                except ValueError:
                    print(f"Error: Invalid Size value '{size_line}' in config.cfg.")
                    sys.exit(1)
        elif stripped.startswith('Energy'):
            # The next line contains the energy
            if i + 1 < len(config_lines):
                energy_line = config_lines[i+1].strip()
                try:
                    data['energy'] = float(energy_line)
                except ValueError:
                    print(f"Error: Invalid Energy value '{energy_line}' in config.cfg.")
                    sys.exit(1)
        elif stripped.startswith('AtomData'):
            # The following lines contain atom data until a blank line or next section
            j = i + 1
            while j < len(config_lines):
                atom_line = config_lines[j].strip()
                if not atom_line or ':' in atom_line:
                    break
                parts = atom_line.split()
                if len(parts) >= 8:
                    try:
                        fx = float(parts[5])
                        fy = float(parts[6])
                        fz = float(parts[7])
                        data['forces'].append([fx, fy, fz])
                    except ValueError:
                        print(f"Error: Invalid force values in AtomData at line {j+1}.")
                        sys.exit(1)
                j += 1
        elif stripped.startswith('PlusStress'):
            # The next lines contain the stress tensor components
            j = i + 1
            stress_lines = []
            while j < len(config_lines):
                stress_line = config_lines[j].strip()
                if not stress_line or ':' in stress_line:
                    break
                stress_lines.extend(stress_line.split())
                j += 1
            if len(stress_lines) >=6:
                try:
                    data['stress'] = [float(x) for x in stress_lines[:6]]
                except ValueError:
                    print(f"Error: Invalid stress tensor values in config.cfg.")
                    sys.exit(1)
    # Validate parsed data
    if data['num_atoms'] == 0:
        print("Error: Number of atoms not found in a configuration.")
        sys.exit(1)
    if len(data['forces']) != data['num_atoms']:
        print(f"Error: Number of forces ({len(data['forces'])}) does not match number of atoms ({data['num_atoms']}).")
        sys.exit(1)
    if len(data['stress']) !=6:
        print("Error: Incomplete stress tensor data in config.cfg.")
        sys.exit(1)
    return data

def parse_observables(cfg_observables, num_atoms):
    """
    Parses the observables from coulombObservablesFull for a single configuration.
    Returns a dictionary with keys: 'energy', 'forces', 'stress'.
    'forces' is a list of [fx, fy, fz] for each atom.
    'stress' is a list [xx, yy, zz, yz, xz, xy].
    """
    data = {
        'energy': 0.0,
        'forces': [],
        'stress': []
    }
    if len(cfg_observables) != 1 + num_atoms + 4:
        print("Error: Invalid number of lines in a Coulomb observables configuration.")
        sys.exit(1)
    # Parse energy
    try:
        data['energy'] = float(cfg_observables[0].strip())
    except ValueError:
        print(f"Error: Invalid energy value '{cfg_observables[0].strip()}' in Coulomb observables.")
        sys.exit(1)
    # Parse forces
    for line in cfg_observables[1:1+num_atoms]:
        parts = line.strip().split()
        if len(parts) != 3:
            print(f"Error: Invalid force line in Coulomb observables: '{line.strip()}'.")
            sys.exit(1)
        try:
            fx, fy, fz = map(float, parts)
            data['forces'].append([fx, fy, fz])
        except ValueError:
            print(f"Error: Non-numeric force values in Coulomb observables: '{line.strip()}'.")
            sys.exit(1)
    # Parse stress (only last two lines)
    stress_lines = cfg_observables[1+num_atoms:1+num_atoms+4]
    # Only the last two lines contain stress data
    stress_values = []
    for line in stress_lines[2:]:
        stress_values.extend(line.strip().split())
    if len(stress_values) !=6:
        print("Error: Invalid stress tensor in Coulomb observables.")
        sys.exit(1)
    try:
        data['stress'] = [float(x) for x in stress_values]
    except ValueError:
        print("Error: Non-numeric stress tensor values in Coulomb observables.")
        sys.exit(1)

    return data

def modify_config_cfg(config_lines, modified_data):
    """
    Modifies the configuration lines with the modified observables.
    Returns the modified list of lines.
    """
    new_lines = config_lines.copy()
    # Modify Energy
    for i, line in enumerate(new_lines):
        if line.strip().startswith('Energy'):
            # The next line is the energy value
            energy_index = i + 1
            new_energy = f"  {modified_data['energy']:>22.12f}\n"
            new_lines[energy_index] = new_energy
            break
    # Modify Forces
    # Find 'AtomData' section
    for i, line in enumerate(new_lines):
        if line.strip().startswith('AtomData'):
            # Start modifying from the next line
            j = i + 1
            atom_count = 0
            while j < len(new_lines):
                atom_line = new_lines[j].strip()
                if not atom_line or ':' in atom_line:
                    break
                parts = new_lines[j].split()
                if len(parts) >=8:
                    # Replace fx, fy, fz with modified values
                    id_str = parts[0]
                    type_str = parts[1]
                    cartes_x = parts[2]
                    cartes_y = parts[3]
                    cartes_z = parts[4]
                    # Get modified forces
                    fx, fy, fz = modified_data['forces'][atom_count]
                    # Format the new force values with the same precision
                    new_force_str = f"{fx:>11.6f} {fy:>11.6f} {fz:>11.6f}"
                    # Reconstruct the line
                    new_atom_line = f"         {id_str:>5} {type_str:>4} {cartes_x:>14} {cartes_y:>13} {cartes_z:>13}  {new_force_str}\n"
                    new_lines[j] = new_atom_line
                    atom_count +=1
                j +=1
            break
    # Modify PlusStress
    for i, line in enumerate(new_lines):
        if line.strip().startswith('PlusStress'):
            # The next lines contain the stress tensor components
            j = i + 1
            stress_values = modified_data['stress']
            # stress is written over one line: (xx, yy, zz), last three (yz, xz, xy)
            stress_line = f"     {stress_values[0]:>11.5f} {stress_values[1]:>11.5f} {stress_values[2]:>11.5f} {stress_values[3]:>11.5f} {stress_values[4]:>11.5f} {stress_values[5]:>11.5f}\n"
            new_lines[j] = stress_line
            break
    return new_lines


def subtract_coul_from_cfg():

    # Subtracts Coulomb EFS in coulombObservablesFull (over many configs) from config.cfg (the MLIP file containing EFS)
    #
    # (Ensure you are first in the relevant directory before calling this)

    config='config.cfg'
    coulomb='coulombObservablesFull'
    backup='config_orig.cfg'

    # Step 1: Backup config.cfg
    backup_file(config, backup)

    # Step 2: Read config.cfg
    try:
        with open(config, 'r') as f:
            cfg_lines = f.readlines()
    except Exception as e:
        print(f"Error reading {config}: {e}")
        sys.exit(1)

    configurations_cfg = split_configurations_cfg(cfg_lines)
    total_configurations = len(configurations_cfg)
    print(f"Total configurations in {config}: {total_configurations}")

    # Step 3: Parse config.cfg to get number of atoms per configuration
    num_atoms_per_cfg = []
    for idx, cfg in enumerate(configurations_cfg):
        config_data = parse_config_cfg(cfg)
        num_atoms_per_cfg.append(config_data['num_atoms'])
    print("Parsed number of atoms for each configuration from config.cfg.")

    # Step 4: Read coulombObservablesFull
    try:
        with open(coulomb, 'r') as f:
            observables_lines = [line for line in f if line.strip()]  # Remove empty lines
    except Exception as e:
        print(f"Error reading {coulomb}: {e}")
        sys.exit(1)

    configurations_coulomb = split_observables(observables_lines, num_atoms_per_cfg)
    print(f"Total configurations in {coulomb}: {len(configurations_coulomb)}")

    # Check if number of configurations match
    if len(configurations_cfg) != len(configurations_coulomb):
        print("Error: Number of configurations in config.cfg and coulombObservablesFull do not match.")
        sys.exit(1)

    # Step 5: Iterate and modify
    modified_configurations = []
    total = len(configurations_cfg)
    for idx in range(total):
        cfg = configurations_cfg[idx]
        coulomb_cfg = configurations_coulomb[idx]
        num_atoms = num_atoms_per_cfg[idx]

        # Parse config.cfg
        config_data = parse_config_cfg(cfg)

        # Parse Coulomb observables
        coulomb_data = parse_observables(coulomb_cfg, num_atoms)

        # Subtract Coulomb observables
        modified_energy = config_data['energy'] - coulomb_data['energy']
        modified_forces = []
        for atom_idx in range(len(config_data['forces'])):
            cfg_force = config_data['forces'][atom_idx]
            coul_force = coulomb_data['forces'][atom_idx]
            new_force = [cfg_force[0] - coul_force[0],
                         cfg_force[1] - coul_force[1],
                         cfg_force[2] - coul_force[2]]
            modified_forces.append(new_force)
        # WARNING: MLIP outputs the virials in the order: xx, yy, zz, yz, xz, xy, whereas LAMMPS (used to calculate the
        # coulomb part) writes them in the order: pxx, pyy, pzz, pxy, pxz, pyz. I.e. elements 4 and 6 are switched
        modified_stress = [config_data['stress'][i] - coulomb_data['stress'][j] for i, j in zip(range(6), [0, 1, 2, 5, 4, 3])]

        # Prepare modified data
        modified_data = {
            'energy': modified_energy,
            'forces': modified_forces,
            'stress': modified_stress
        }

        # Modify the config.cfg lines
        new_cfg = modify_config_cfg(cfg, modified_data)
        new_cfg.append("\n")
        modified_configurations.append(new_cfg)

        # Progress update
        if (idx+1) % 100 ==0 or (idx+1) == total:
            print(f"Processed {idx+1}/{total} configurations.")

    # Step 6: Write back to config.cfg
    try:
        with open(config, 'w') as f:
            for cfg in modified_configurations:
                f.writelines(cfg)
        print(f"Modified configurations written to {config}")
    except Exception as e:
        print(f"Error writing to {config}: {e}")
        sys.exit(1)


def setupFittingJob(xc, pot, potItn):

    """
    Sets up fitting job for a particular pot, e.g. MEAM2B, copying DFT data for exchange-correlation 'xc' from all 
    materials sub-directories. DFT output files are relabelled according to their material, temperature and if 
    applicable defect specification. MEAMfit input files, 'fitdbse' and 'settings' even if not optimizing an EAM/MEAM 
    potential, as MEAMfit will be used to generate the .cfg file for MLIP in case of MTP potentials. The fitdbse file 
    is concatenated from fitdbse files in the DFT directories and amended to reflect the newly relabeled DFT oupput
    files. Additionally if potItn /= "", (e.g = "B") then DFT data from DFT/xc/{materials}/pot$potItn (e.g pot$potItn = 
    EAM1B) will also be included.

    If pot='EAM/MEAM' a few other preliminaries are taken care of to set up the MEAMfit job. If pot='MTP' similarly
    """

    if var.meamfit_so: import meamfit_py

    # Clean directory of MEAMfit inputs
    if os.path.isfile("settings"):
        os.remove("settings")
    if os.path.isfile("fitdbse"):
        os.remove("fitdbse")

    # Training data is taken from the DFT dirs, UNLESS we are doing a M/EAM optimization using the training set for
    # an MTP optimization. (This is the default behaviour when doing a tandem MTP-M/EAM optimization - MTPs do all
    # AL cycles then once finished we do a single shot optimization for each MEAM# potential on the corresponding MTP#z
    # training set (#=number, i.e. 1-5; z=AL cycles, i.e. A-Z)

    if var.MEAM_last and var.all_MTP_complete and 'EAM' in pot:

        # Copy training set from completed MTP# optimization (e.g. MTP3D, where D is the last AL cycle)
        # (We are already located in the relevant directory, e.g. ... /MEAM1/6_potOpt/)
        current_dir = os.getcwd()
        mtp_dirs = [d for d in os.listdir("../../") if f"MTP{potItn}" in d]
        mtp_dirs.sort()
        highest_mtp_dir = mtp_dirs[-1]
        pathToCopy = os.path.join(f"../../{highest_mtp_dir}", "6_potOpt")
        outf.printt(2,f'  Copying training set from {pathToCopy}')

        var_dft_files = glob.glob(f"../../{highest_mtp_dir}/6_potOpt/*.{var.DFTsuffix}")
        fitdbse_file = os.path.join(f"../../{highest_mtp_dir}/6_potOpt", "fitdbse")
        for file in var_dft_files:
            shutil.copy(file, "./")  # Copy to the current directory
        if os.path.exists(fitdbse_file):
            shutil.copy(fitdbse_file, "./")
        else:
            outf.printt(0, f"fitdbse file not found in {highest_mtp_dir}/6_potOpt, STOPPING")
            quit()

    else:

        # Copy fitdbse and vasprun files from previous stage. Need to do for all materials of a given xc.
        if var.skip_0K_props == False:
            outf.printt( 1, f"  Concatenating fitdbse files from: {var.DFTdir}/{xc}/'mat'/2_nonMD/'EvsV,elastic' and "
                f"{var.DFTdir}/{xc}/'mat'/5_trainingData/" )
        else:
            outf.printt( 1, f"  Concatenating fitdbse files from: {var.DFTdir}/{xc}/'mat'/5_trainingData/" )
        if potItn != "":
            outf.printt( 1, f"  Additionally fitdbse files from: {var.DFTdir}/{xc}/'mat'/5_trainingData/bad/{pot}/" )
        else:
            outf.printt(1, "  (no single shot DFT data detected)")
        nConf = 0
        linesFromFitdbseFiles = []
        for mat in var.materials:
            # We gather all the MD and (unless skip_0K_props = True) single shot DFT output from each material. In addition
            # if we are reoptimizing a potential using new DFT data, we need to also need to include this data
            direcs = ["5_trainingData/initial/"]
            potOfDirec = [""]
            # In the following we can't use DFTtrainingDataBaddir here unless we wish to recall initDirsPaths (and i'm not 
            # sure we wish to overwrite these...)
            if os.path.isdir( os.path.join( var.DFTpath, xc, mat ) + "/5_trainingData/bad/" + pot ):
                direcs.append("5_trainingData/bad/" + pot)
                potOfDirec.append(pot)
                # If we have eg EAM1C, then we also need EAM1B in the fitdbse as well
                newletter = pot[-1]
                while True:
                    newletter = ascii_letters[(ascii_letters.index(newletter) - 1)]  # move back in the alphabet
                    print("newletter=", newletter)
                    if newletter == "A":
                        break
                    potnew = pot[:-1] + newletter
                    direcs.append("5_trainingData/bad/" + potnew)
                    potOfDirec.append(potnew)
            outf.printt(2, "  potOfDirec=" + str(potOfDirec))
            if var.skip_0K_props == False:
                direcs.append("2_nonMD/EvsV/")
                direcs.append("2_nonMD/elastic/")
            if var.verbosity >= 1:
                print("  Directories to consider: ", end="")
                for direc in direcs:
                    print(var.DFTdir + "/" + xc + "/" + mat + "/" + direc + " ")
                print("")

            for iDirec, direc in enumerate(direcs):
                outf.printt(2, "  iDirec= " + str(iDirec))
                outf.printt(2, "  Checking for file " + var.DFTdir + "/" + xc + "/" + mat + "/" + direc + "/fitdbse")
                if os.path.exists(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/fitdbse") == False:
                    outf.printt(
                        0, "  ERROR: " + var.DFTdir + "/" + xc + "/" + mat + "/" + direc + "/fitdbse does not exist"
                    )
                with open(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/fitdbse", "r") as sources:
                    lines = sources.readlines()
                nConfList = re.findall(r"\d+", lines[0])
                nConf = nConf + int(nConfList[0])
                # Rename vasprun files. For 4_configs and 5_...: vasprun_1000.xml -> vasprun_mat_1000.xml etc. For EAM1B etc: vasprun_1627.xml (here 1627 is the config nmr
                # rather than temperature) -> vasprun_mat_EAM2_1627.xml
                if (
                    direc == "2_nonMD/EvsV/" or direc == "2_nonMD/elastic/" or direc == "5_trainingData/initial/"
                ):  # When we have multiple materials we are going to need to add in an ;rf after the Fr of the first config, to reset the reference structure (after changing it for the displaced structures and E-V structures)
                    # Here we add in the mp-id to the filename
                    for i, line in enumerate(lines):
                        # print('lines[i] before= ',lines[i])
                        if var.dft_engine == "VASP":
                            lines[i] = lines[i].replace("vasprun", "vasprun_" + mat)
                        elif var.dft_engine == "CASTEP":
                            lines[i] = lines[i].replace("castep_", "castep_" + mat + "_")
                            lines[i] = lines[i].replace(
                                "castep.castep", "castep_" + mat + ".castep"
                            )  # this file doesn't have a _ so must be treated separately
                        elif var.dft_engine == "PWSCF":
                            lines[i] = lines[i].replace(var.prefix_no_apost, var.prefix_no_apost + f"_{mat}_", 1)
                        if i == 1 and (direc == "2_nonMD/EvsV/" or direc == "2_nonMD/elastic/"):
                            lines[i] = re.sub(r" Fr ", " Fr;rf ", lines[i])  # define new reference energy in these cases
                    # need to rename the vasprun: vasprun_mat_cont etc
                    if var.dft_engine == "VASP":
                        for file in glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/vasprun*"):
                            vasprunIndex = file.index("vasprun")
                            oldFilename = file[vasprunIndex:]
                            newFilename = "vasprun_" + mat + oldFilename[7 : len(oldFilename)]
                            shutil.copy2(file, newFilename)
                    elif var.dft_engine == "CASTEP":
                        for file in glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/*.castep"):
                            castepIndex = file.index("castep")
                            oldFilename = file[castepIndex:]
                            newFilename = "castep_" + mat + oldFilename[6 : len(oldFilename)]
                            shutil.copy2(file, newFilename)
                    elif var.dft_engine == "PWSCF":
                        for file in glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/*.in") + \
                                    glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/*.out"):
                            filename = os.path.basename(file)
                            newFilename = filename.replace(var.prefix_no_apost, var.prefix_no_apost + f"_{mat}_", 1)
                            shutil.copy2(file, newFilename)
                else:
                    for i, line in enumerate(lines):
                        if var.dft_engine == "VASP":
                            lines[i] = lines[i].replace("vasprun", "vasprun_" + mat + "_" + potOfDirec[iDirec])
                        elif var.dft_engine == "CASTEP":
                            lines[i] = lines[i].replace("castep_", "castep_" + mat + "_" + potOfDirec[iDirec] + "_")
                        elif var.dft_engine == "PWSCF":
                            lines[i] = lines[i].replace(var.prefix_no_apost, var.prefix_no_apost + f"_{mat}_{potOfDirec[iDirec]}_", 1)
                    # need to rename the vasprun: vasprun_mat_cont etc
                    if var.dft_engine == "VASP":
                        for file in glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/vasprun*"):
                            vasprunIndex = file.index("vasprun")
                            oldFilename = file[vasprunIndex:]
                            newFilename = (
                                "vasprun_" + mat + "_" + potOfDirec[iDirec] + oldFilename[7 : len(oldFilename)]
                            )  # see the castep bit below - i should change this to that approach below
                            shutil.copy2(file, newFilename)
                    elif var.dft_engine == "CASTEP":
                        for file in glob.glob(
                            var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/*.castep"
                        ):  # don't use castep* because this also includes castep_keywords.json file
                            castepIndex = file.index("castep")
                            oldFilename = file[castepIndex:]
                            newFilename = oldFilename.replace("castep_", "castep_" + mat + "_" + potOfDirec[iDirec] + "_")
                            shutil.copy2(file, newFilename)
                    elif var.dft_engine == "PWSCF":
                        #print('glob.glob(',var.DFTpath,"/",xc,"/",mat,"/",direc,"/*.in=", glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/*.in"))
                        for file in glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/*.in") + \
                                    glob.glob(var.DFTpath + "/" + xc + "/" + mat + "/" + direc + "/*.out"):
                            filename = os.path.basename(file)
                            newFilename = filename.replace(var.prefix_no_apost, var.prefix_no_apost + f"_{mat}_{potOfDirec[iDirec]}_", 1)
                            print(' copying ',file,' to ',newFilename) # debug
                            shutil.copy2(file, newFilename)
                linesFromFitdbseFiles.extend(
                    lines[1:]
                )  # ignore first line   --- need to do a sed on the vasprun file (see also below regarding renaming)

        # Write fitdbse file
        fitdbse_file = open("fitdbse", "w")
        fitdbse_file.write(str(nConf) + " # Files | Configs to fit | Quantity to fit | Weights" + "\n")
        for line in linesFromFitdbseFiles:
            lineEdit = line.replace("  ", " ")
            fitdbse_file.write(lineEdit)
            # lineEdit=line[7:].replace('  ',' ') # reduce space to allow for longer vasprun filenames
            # if '1-1' in lineEdit:
            #    # Single shot, so need 'pot' to label the vasprun (s well as mat and the config number)
            #    fitdbse_file.write('vasprun_'+mat+'_'+pot+lineEdit)
            # else:
            #    # MD runs of format vasprun_mat_temp.xml
            #    fitdbse_file.write('vasprun_'+mat+lineEdit)
        fitdbse_file.close()

    # Run MEAMfit to generate settings file
    outf.printt(1, "  Running MEAMfit to generate settings file...")

    if var.meamfit_so and not var.startedMPIforMEAMfit:
        meamfit_py.start_mpi()
        var.startedMPIforMEAMfit = True
    processJobs.runMEAMfit('./', True)

    # Edit settings file
    outf.printt(1, "  Editing settings file...")
    # Add in CUTOFF_MAX value
    with open("settings", "r") as sources:
        lines = sources.readlines()
    with open("settings", "w") as sources:
        for line in lines:
            sources.write(re.sub(r"^ # CUTOFF_MAX=", " CUTOFF_MAX=4.5", line))
    if "MEAM" in pot:
        # Change TYPE to MEAM
        with open("settings", "r") as sources:
            lines = sources.readlines()
        with open("settings", "w") as sources:
            for line in lines:
                sources.write(re.sub(r"^ TYPE=EAM", " TYPE=MEAM", line))
    settings_file = open("settings", "a")
    settings_file.write(" VERBOSITY=2\n")
    settings_file.close()

    if "MTP" in pot:

        # Generate cfg file and calc min dist
        settings_file = open("settings", "a")
        settings_file.write(" WRITECFG=True\n")
        settings_file.close()
        outf.printt(1, "  Running MEAMfit to generate config file for use by MLIP...")
        outf.printt(2, "  var.startedMPI...=" + str(var.startedMPIforMEAMfit))
        if var.meamfit_so and not var.startedMPIforMEAMfit:
            meamfit_py.start_mpi()
            var.startedMPIforMEAMfit = True
            print('...started mpi for meamfit...')
        processJobs.runMEAMfit('./', True)

        # Read all DFT outputs from fitdbse to determine minimum separation of atoms, min_sepn, for use later
        min_sepn = None
        with open("sepnHistogram.out", "r") as histogram_file:
            histogram_file.readline()
            line = histogram_file.readline()
            if line:
                min_sepn = float(line.split()[0])
            else:
                min_sepn = None
        if min_sepn == None:
            outf.printt(0,'  ERROR: cannot extract minimum separation from sepnHistogram.out file, STOPPING')
            quit()
        # Needs testing!

        # Now remove the WRITECFG flag in case we need to run MEAMfit again (e.g. for determining coulomb contribs)
        with open("settings", "r") as file:
            lines = file.readlines()
        lines = lines[:-1]
        with open("settings", "w") as file:
            file.writelines(lines)

        # Read the cfg file to determine the number of species. This will be needed later in this subroutine
        num_species = -1
        process_atom_data = False
        with open("config.cfg", "r") as file:
            for line in file:
                if line.startswith(" AtomData:"):
                    process_atom_data = True  # Start processing when AtomData section is found
                    continue
                if line.startswith(" Energy"):
                    process_atom_data = False  # Stop processing when Energy line is reached
                    continue
                if process_atom_data and line.strip():
                    # Process lines with atom data
                    parts = line.split()
                    if len(parts) > 1:
                        try:
                            atom_type = int(parts[1])  # Second column contains atom types
                            num_species = max(num_species, atom_type)
                        except ValueError:
                            print('ERROR: cannot convert number in cfg file to integer')
                            quit()
                    else:
                        print('ERROR: too few numbers when reading cfg file')
                        quit()
        num_species = num_species + 1
        outf.printt(2, f"Number of atom types: {num_species}")

    if var.sep_coulomb_pot == True:
        settings_file = open("settings", "a")
        settings_file.write(" EXCLUDECOULOMB=true\n")
        ntypesp = 2  # the materials for which fixed charges are appropriate currently all contain two species. If 3 species materials are added instead loop over
        # var.structure.ntypesp, and include a loop over materials and use the largest value of var.structure.ntypesp
        for isp in range(1, ntypesp + 1):
            for jsp in range(1, ntypesp + 1):
                # Removed the following bit. It might be true for the initial fit to MD, but we can use the DFT from atoms getting too close together to refine the
                # potential, and there we need V_XX and V_YY
                #                if isp==jsp:
                #                    # Where Coulomb term is being used explicitly better performance has been found for NaCl treating just the non-identical interactions with classical pp
                #                    settings_file.write(' NTERMS_PP('+str(isp)+'_'+str(jsp)+')=0\n')
                #                else:
                settings_file.write(" NTERMS_PP(" + str(isp) + "_" + str(jsp) + ")=" + str(var.NTERMS) + "\n")
        settings_file.close()

        # Where Coulomb observables (Energy, forces, stresses) are to be removed prior to fitting (these will be added 
        # back in using a hybrid Coulomb+EAM potential during LAMMPS), evaluated these observables here
        shutil.copy2(var.dirpath + "/getCoulombObservables.sh", "./")
        shutil.copy2(var.dirpath + "/lammpsIn_energyForce", "./")
        shutil.copy2(var.dirpath + "/lammpsIn_stress", "./")
        outf.printt(1, "  Running MEAMfit to generate coulomb observables file...")
        outf.printt(2, "  var.startedMPI...=" + str(var.startedMPIforMEAMfit))
        if var.meamfit_so and not var.startedMPIforMEAMfit:
            meamfit_py.start_mpi()
            var.startedMPIforMEAMfit = True
        processJobs.runMEAMfit('./', True)
        outf.printCitation("lammps")  # LAMMPS is called by MEAMfit here to compute Coulomb observables

        if 'MTP' in pot:
           # In this case we would need to subtract the observables direct from the quantities in the .cfg file
           outf.printt(1, '  Subtract coulomb contributions from observables in .cfg file for MTP fitting')
           subtract_coul_from_cfg()

    if 'EAM' in pot or 'MEAM' in pot:
        if os.path.isfile(var.dirpath + "/script_MEAMfit"):
            outf.printt( 2, "  script_MEAMfit job submission script present in run directory. Copying to stage "
                "sub-directories and renaming as script" )
            shutil.copy2(var.dirpath + "/script_MEAMfit", "./script")
        if os.path.isfile(var.dirpath + "/run_MEAMfit.sh"):
            outf.printt( 2, "  run_MEAMfit.sh job submission script present in run directory. Copying to stage "
                "sub-directories" )
            shutil.copy2(var.dirpath + "/run_MEAMfit.sh", "./")
    elif 'MTP' in pot:
        if os.path.isfile(var.dirpath + "/script_MLIP"):
            outf.printt( 2, "  script_MLIP job submission script present in run directory. Copying to stage "
                "sub-directories and renaming as script" )
            shutil.copy2(var.dirpath + "/script_MLIP", "./script")
        # Template for potential:
        os.system("cp $APDdir/inputFiles/16.mtp ./") # ...later we may generalize to other levels
        # Change number of species
        with open("16.mtp", "r") as sources:
            lines = sources.readlines()
        with open("16.mtp", "w") as sources:
            for line in lines:
                new_line = re.sub(r"^species_count = 1", f"species_count = {num_species}", line)
                new_line = re.sub(r"^\tmin_dist = 2", f"\tmin_dist = {min_sepn - 0.1}", new_line) # need to check this
                sources.write(new_line)


def amendMTPfilename(path):

    """
    Adjust name of the Trained.mtp_ to Trained.mtp_{comp} where comp is a string containing the species order from the
    castep files. This is necessary because CASTEP reorders the species. For other potential types this info can be
    stored in the potential file itself, but MTP does not store species names in-file.
    """

    # CASTEP ordering is ostensibly in order of increasing atomic number, but to be sure read it from the files.
    speciesCastep, compositionCastep = DFTanalysis.speciesFromTrainingData(var.potOptpath)
    old_filename = os.path.join(path, 'Trained.mtp_')
    new_filename = os.path.join(path, 'Trained.mtp_' + compositionCastep)
    shutil.copy(old_filename, new_filename)


def get_previous_pot_opt_path(potOptpath):

    """
    Calculate the previous optimization path based on the current optimization path.
    If the current path ends in a letter, decrement the letter (skipping 'A').
    If no letter is present, it is the first iteration, return None.
    
    Example:
    If potOptpath is '.../MTP2C/6_potOpt/', the function returns '.../MTP2B/6_potOpt/'.
    If potOptpath is '.../MTP2/6_potOpt/', the function returns None.
    """

    match = re.search(r'MTP(\d+)([B-Z]?)', potOptpath)
    if match:
        number = int(match.group(1))
        iteration = match.group(2)
        if iteration:
            previous_iteration = chr(ord(iteration) - 1) if iteration != 'B' else ''
            potOptPrevpath = potOptpath.replace(f'MTP{number}{iteration}', f'MTP{number}{previous_iteration}')
        else:
            potOptPrevpath = None
    else:
        raise ValueError('The path format is not recognized')

    return potOptPrevpath


def setupTestingJob( optimizer ):

    # Set up testing job

    if optimizer == "MEAMfit":

        # Find deepest cont/ dir
        contdir = ""
        while True:
            if os.path.isdir(var.potOptpath + contdir + "/cont"):
                contdir = contdir + "/cont"
            else:
                break
     
        os.system("cp " + var.potOptpath + contdir + "/{fitdbse,settings," + var.DFTprefix + "*,potparas_best1} ./")
        if var.sep_coulomb_pot == True:
            os.system("cp " + var.potOptpath + contdir + "/coulombObservablesFull ./")
     
        outf.printt(1, "  Editing settings file...")
     
        settings_file = open("settings", "a")
        settings_file.write(" NOOPT=True\n")
        settings_file.write(" POTFILEIN=potparas_best1\n")
        settings_file.close()
        # Also make silent:
        with open("settings", "r") as sources:
            lines = sources.readlines()
        with open("settings", "w") as sources:
            for line in lines:
                sources.write(re.sub(r"^ VERBOSITY=2", " VERBOSITY=0", line))
        # Remove 'CONT=True' if it is present
        settings_file = open("settings", "r")
        settings_file_new = open("settings_new", "w")
        for line in settings_file:
            if " CONT=True" not in line:
                settings_file_new.write(line)
        settings_file.close()
        settings_file_new.close()
        os.system("mv settings_new settings")

    else:

        # (MLIP doesn't support continuation jobs so no need to find the latest cont/ dir here)
        os.system("cp " + var.potOptpath + "/{Trained.mtp_,config.cfg} ./")
        # Copy Trained.mtp_ from the previous iteration to Trained_prev.mtp_
        potOptPrevpath = get_previous_pot_opt_path(var.potOptpath)
        print('potOptPrevpath=',potOptPrevpath)
        if potOptPrevpath:
            # I.e. if we are on the 1st or a later AL cycle (not the first training iteration)
            os.system("cp " + potOptPrevpath + "/Trained.mtp_ ./Trained_prev.mtp_")
        if os.path.isfile(var.dirpath + "/script_MLIP_EFS"):
            outf.printt( 2, "  script_MLIP_EFS job submission script present in run directory. Copying to stage "
                "sub-directories and renaming as script" )
            shutil.copy2(var.dirpath + "/script_MLIP_EFS", "./script")


def contJobs(direcs):
    # Set up continuation jobs for potOpt calculations in directories: direcs.
    outf.printt(1, "  Setting up MEAMfit cont/ directories for:")

    for direc in direcs:
        outf.printt(1, "    " + direc)
        path = var.dirpath + "/" + direc
        os.mkdir(path + "/cont")
        os.system(
            "cp " + path + "/{script,run_MEAMfit.sh,bestoptfuncs,potparas_best*,settings,fitdbse} " + path + "/cont/"
        )
        if var.dft_engine == "VASP":
            os.system("cp " + path + "/*vasprun* " + path + "/cont/")
        elif var.dft_engine == "CASTEP":
            os.system("cp " + path + "/*castep* " + path + "/cont/")
        contPresent = False
        with open(path + "/cont/settings") as settings_file:
            if " CONT=True" in settings_file.read():
                contPresent = True
        if contPresent == False:
            settings_file = open(path + "/cont/settings", "a")
            settings_file.write(" CONT=True\n")
            settings_file.close()


def reorderSpeciesInPot(potFile, potType, compositionFileString, compositionMDstrucString):

    """
    Check if order of species in the potential file (compositionFileString, e.g. = 'NaCl') is same of that in the 
    start structure generated for the pot MD simulations (compositionMDstrucString). We used to compare it to the
    composition of the initial DFT input file (var.initialSpeciesString) since the resized cell has the same
    composition. However since introducing defects this ordering can change, so it is safer to make it more genearl.
    If ordering of species differs, change the potential file so that the order matches that of the structure. This
    involves reordering all the functions in the file as well as changing the filename. This is only necessary for 
    CASTEP because CASTEP internally reorders the species specified in the .cell file, so that the .castep files that
    potentials are trained on have had the species reordered.
    """

    outf.printt(2, f"  Composition from initial LAMMPS structure: {compositionMDstrucString}; from potential file: "
        "{compositionFileString}")
    #compositionFile = re.sub(r"([A-Z][a-z]*)(?=[A-Z]|$)", r"\1 ", compositionFileString).strip()
    #speciesFile = compositionFile.split()
    speciesFile = re.findall('[A-Z][a-z]*', compositionFileString)
    outf.printt(2, "  (from potential file separated into list:" + str(speciesFile) + ")")

    if compositionMDstrucString == compositionFileString:
        outf.printt(1, f"  Composition ordering of potential file ({compositionFileString}) same ordering as that of "
            "original .cell file - no need to reorder potential file.")
        return compositionFileString

    #compositionMDstruc = re.sub(r"([A-Z][a-z]*)(?=[A-Z]|$)", r"\1 ", compositionMDstrucString).strip()
    #speciesMDstruc = compositionStruc.split()
    speciesMDstruc = re.findall('[A-Z][a-z]*', compositionMDstrucString)
    outf.printt(2, "  (from structure separated into list:" + str(speciesMDstruc) + ")") # we used to 
                # compare to var.speciesInitialStrings, but changed this post introducing defects

    # Note: need to update EAM and MEAM if condiitionals to reflect the above change

    if potType == "EAM":
        # Read in potential file to determine different parts (density and embedding function for each species, then pair-potentials order by 1-1, 2-1, 2-2, 3-1, etc). potential file starts with e.g.:
        # 3 O Zr Ba
        #      1000000         0.0005000005000005         100000     0.4401372662400594E-04         4.4013286486739700
        #  8    15.9990300000  2.85531246000000 bcc
        potFileObj = open(potFile)
        lines = []
        for lineNum in range(0, 5):
            lines.append(potFileObj.readline())  # starting 3 comment lines, and next 3 lines
        # extract Nrho and Nr (1st and 3rd numbers in the 5th line). Nrho is number of densities used in grid to define embedding function; Nr number of distances to define densities and pair-potenials
        numbersFromLine = re.findall(r"[-+]?\d*\.\d+|\d+", lines[4])
        Nrho = float(numbersFromLine[0])
        Nr = float(numbersFromLine[2])
        outf.printt(2, "  From potential file: Nrho=" + str(Nrho) + ", Nr=" + str(Nr))
        # for each species read in Nrho/5 lines of embedding function and Nr/5 lines of density. Read these into the embeddingFunction and density arrays,
        # with species indexed according to the desired ordering of the initial DFT input file.
        embeddingFunction = [[] for _ in range(len(var.speciesInitial))]
        density = [[] for _ in range(len(var.speciesInitial))]
        for species in speciesFile:
            outf.printt(2, "  looking for " + str(species) + " in " + str(var.speciesMDstruc))
            iSpecies = var.speciesMDstruc.index(
                species
            )  # find the index in speciesInitial where 'species' is present. Not speciesInitial has: 'Element Ba', etc
            for iLine in range(
                0, int(Nrho / 5) + 1
            ):  # extra line for the header, e.g.:   8    15.9990300000  2.85531246000000 bcc
                embeddingFunction[iSpecies].append(potFileObj.readline())
            for iLine in range(0, int(Nr / 5)):
                density[iSpecies].append(potFileObj.readline())
        # read pair potentials
        pairpotential = [[[] for __ in range(len(var.speciesInitial))] for _ in range(len(var.speciesInitial))]
        # LAMMPS reads in pairpotentials in the order:  (1,1), (2,1), (2,2), (3,1), (3,2), (3,3), (4,1), ...
        # Since CASTEP reorders species these might correspond to:
        #                                               (O,O), (Zr,O), ...
        # If the .cell order we wish to regain is: Ba Zr O then we need to map these to new numbers:
        #                                               (3,3), (3,2), etc
        for isp in range(0, len(speciesFile)):
            for jsp in range(0, isp + 1):
                iSpecies = var.speciesMDstruc.index(
                    speciesFile[isp]
                )  # isp corresponds to the species speciesFile[isp]. so we need to check which index this species corresponds to in our new species ordering, defined by var.speciesInitial
                jSpecies = var.speciesMDstruc.index(speciesFile[jsp])
                if jSpecies > iSpecies:  # in lammps as shown above, always the larger species is put first
                    tmpVal = iSpecies
                    iSpecies = jSpecies
                    jSpecies = tmpVal
                for iLine in range(0, int(Nr / 5)):
                    pairpotential[iSpecies][jSpecies].append(potFileObj.readline())

        outf.printt(2, "  potFile=" + potFile)
        shutil.move(potFile, "./" + potFile + "_bkp")
        potFile = compositionMDstrucString + potFile[len(compositionFileString) :]  # save file with new composition
        outf.printt(
            2, "  Writing reordered potential to: " + potFile
        )  # perhapsreturn potFile from subroutine@? is it mneeded in ADP?
        with open(potFile, "w") as filecontent:
            for lineNum in range(0, 3):  # header lines
                filecontent.write(lines[lineNum])
            filecontent.write(
                " " + str(len(var.speciesMDstruc))
            )  # ,end='') # this line needs changing. e.g. like:  3 O Zr Ba. write out: len(var.speciesInitialStrings), var.speciesInitialStrings[0], var.speciesInitialStrings[1],...
            for speciesString in var.speciesMDstruc:
                filecontent.write(" " + speciesString)  # ,end='')
            filecontent.write("\n" + lines[4])
            # Embedding function & densities
            for iSpecies, species in enumerate(var.speciesMDstruc):
                for embFunc in embeddingFunction[iSpecies]:
                    filecontent.write(f"{embFunc}")
                for dens in density[iSpecies]:
                    filecontent.write(f"{dens}")
            # Pair potentials
            for isp in range(0, len(var.speciesMDstruc)):
                for jsp in range(0, isp + 1):
                    for pp in pairpotential[isp][jsp]:
                        filecontent.write(f"{pp}")  # pairpotential[isp,jsp])

    elif potType == "MEAM":
        # Readin data of form:
        # # RFMEAM - Embedding function :O ZrBa
        # ef                  O                   Zr                  Ba
        # ef.E0      -1.2083280322372600      -0.0071960616003596      -0.0011501375732473
        # ef.E1      -0.0002750065153041       0.0006811059217683       0.0004902663797872
        # and:
        # # RFMEAM - Pair potential
        # pp                  O -O                   O -Zr                  O -Ba                  Zr-Zr                  Zr-Ba                  Ba-Ba
        # pp.b1       4.6267990015953897      -4.6823730341144003       0.9709447817881010      -7.8495994994757403      -1.8775397517296999      -5.9077640830841300

        # Set up array (e.g. index[0:2]) so that if we want to convert to BaZrO ordering then
        # index[0]=2, index[1]=1 and index[2]=0.
        indexInFile = (
            []
        )  # Maps index of column to which we are writing to index of the original column from which we read
        for species in var.speciesMDstruc:
            indexInFile.append(speciesFile.index(species))

        indexInFile2 = []  # Do same for the pairpotential
        # First enumerate the order of species-pairs that will be appearing in the file
        # ** Need to deal with i) orders of e.g Ba-O will need to reverse. ii) O needs to be 'O '
        speciesPairsInFile = []
        for ispecies, species in enumerate(speciesFile):
            if (
                len(species) == 1
            ):  # Adjust length so that e.g. 'O' becomes 'O '. Necessary for rfmeam library formatting
                speciesCorlength = species + " "
            else:
                speciesCorlength = species
            ispecies2 = ispecies
            for species2 in speciesFile[ispecies:]:  # this should start at same index as species
                if len(species2) == 1:
                    species2Corlength = species2 + " "
                else:
                    species2Corlength = species2
                # if we are reading e.g. O-Zr-Ba and trying to convert to Ba-Zr-O then store species pair in
                # the 'speciesPairsInFile' list according to the ordering in Ba-Zr-O
                if (ispecies - ispecies2) * (
                    var.speciesMDstruc.index(species) - var.speciesMDstruc.index(species2)
                ) < 0:
                    swap = True
                else:
                    swap = False
                if swap == False:
                    speciesPairsInFile.append(speciesCorlength + species2Corlength)
                else:
                    speciesPairsInFile.append(species2Corlength + speciesCorlength)
                ispecies2 = ispecies2 + 1

        for ispecies, species in enumerate(var.speciesMDstruc):
            if (
                len(species) == 1
            ):  # Adjust length so that e.g. 'O' becomes 'O '. Necessary for rfmeam library formatting
                speciesCorlength = species + " "
            else:
                speciesCorlength = species
            for species2 in var.speciesMDstruc[ispecies:]:  # this should start at same index as species
                if len(species2) == 1:
                    species2Corlength = species2 + " "
                else:
                    species2Corlength = species2
                indexInFile2.append(speciesPairsInFile.index(speciesCorlength + species2Corlength))

        # Now read from potential file and write out new one. First copy current one to backup
        shutil.move(potFile, "./" + potFile + "_bkp")
        potFileObjOld = open(potFile + "_bkp", "r")
        potFile = "library.rfmeam." + compositionMDstrucString + potFile[15 + len(compositionFileString) :]
        potFileObj = open(potFile, "w")

        # Embedding function
        lineReadinOld = potFileObjOld.readline()
        potFileObj.writelines(" # RFMEAM - Embedding function :" + compositionMDstrucString + "\n")
        # ef line, e.g. : ef                  Ba                   Zr                  O
        lineReadinOld = potFileObjOld.readline()
        potFileObj.writelines(" ef                 ")
        for species in var.speciesMDstruc:
            potFileObj.writelines(species + "                    ")
        potFileObj.writelines("\n")
        # now lines ef.E0 etc. search through until we hit a comment line
        while True:
            lineReadinOld = potFileObjOld.readline()
            if "#" in lineReadinOld:
                break
            words = lineReadinOld.split()
            potFileObj.writelines(" " + words[0])
            for index in range(0, len(var.speciesMDstruc)):
                potFileObj.writelines("      " + str(words[indexInFile[index] + 1]))
            potFileObj.writelines("\n")
        potFileObj.writelines(" #\n")

        # Electron density
        lineReadinOld = potFileObjOld.readline()
        potFileObj.writelines(" # RFMEAM - Background electron density :" + compositionMDstrucString + "\n")
        lineReadinOld = potFileObjOld.readline()
        potFileObj.writelines(" bed                 ")
        for species in var.speciesMDstruc:
            potFileObj.writelines(species + "                    ")
        potFileObj.writelines("\n")
        # now lines bed.t1, bed.alpha10, bed.r1, etc. search through until we hit a comment line
        while True:
            lineReadinOld = potFileObjOld.readline()
            if "#" in lineReadinOld:
                break
            words = lineReadinOld.split()
            potFileObj.writelines(" " + words[0])
            for index in range(0, len(var.speciesMDstruc)):
                potFileObj.writelines("      " + str(words[indexInFile[index] + 1]))
            potFileObj.writelines("\n")
        potFileObj.writelines(" #\n")

        # Pair potential
        lineReadinOld = potFileObjOld.readline()
        potFileObj.writelines(" # RFMEAM - Pair potential\n")
        lineReadinOld = potFileObjOld.readline()
        potFileObj.writelines(" pp     ")
        for index in indexInFile2:
            speciesPair = speciesPairsInFile[index]
            speciesPair = speciesPair[0:2].strip() + "-" + speciesPair[2:4].strip()
            potFileObj.writelines(speciesPair + "                    ")
        potFileObj.writelines("\n")
        # now lines pp.b1, pp.s1, etc. search through until we hit a comment line
        while True:
            lineReadinOld = potFileObjOld.readline()
            if "#" in lineReadinOld:
                break
            words = lineReadinOld.split()
            potFileObj.writelines(" " + words[0])
            for index in indexInFile2:
                potFileObj.writelines("      " + str(words[index + 1]))
            potFileObj.writelines("\n")
        potFileObj.writelines(" #\n")

        # copy final two lines (cutoffMax)
        potFileObj.writelines(potFileObjOld.readline())
        potFileObj.writelines(potFileObjOld.readline())
        potFileObj.close()
        potFileObjOld.close()

    elif potType == "MTP":

        # Back up the original file
        shutil.copy(potFile, potFile + "_bkp")
        shutil.copy(potFile, f"{potFile}_{compositionFileString}")

        with open(potFile, 'r') as file:
            lines = file.readlines()

        # Reordering species_coeffs
        # Find the line containing species_coeffs
        species_coeffs_line_idx = next(i for i, line in enumerate(lines) if "species_coeffs" in line)
        species_coeffs_line = lines[species_coeffs_line_idx]
     
        # Extract and reorder species_coeffs
        species_coeffs_str = species_coeffs_line.split('=')[1].strip()
        species_coeffs_list = species_coeffs_str.strip('{}').split(',')
        species_coeffs = [float(coeff.strip()) for coeff in species_coeffs_list]
        print('speciesFile=',speciesFile,', speciesMDstruc=',speciesMDstruc,', species_coeffs=',species_coeffs)
        reordered_species_coeffs = [species_coeffs[speciesFile.index(species)] for species in speciesMDstruc]
        formatted_species_coeffs = ['{:.15e}'.format(coeff) for coeff in reordered_species_coeffs]
    
        # Replace the original species_coeffs line with reordered coefficients
        lines[species_coeffs_line_idx] = "species_coeffs = {" + ', '.join(map(str, formatted_species_coeffs)) + "}\n"

        # Reordering radial_coeffs
        # Find the index where radial_coeffs starts
        coeff_blocks = {}
        start_idx = next(i for i, line in enumerate(lines) if "radial_coeffs" in line) + 1
        # Extract coefficient blocks
        while start_idx < len(lines) and lines[start_idx].strip():
            pair_line = lines[start_idx].strip()
            if 'alpha_moments_count' in pair_line:
                break
            i, j = map(int, pair_line.split('-'))
            start_idx += 1
            coeff_block = []
        
            while lines[start_idx].strip().startswith('{'):
                coeff_block.append(lines[start_idx])
                start_idx += 1
        
            coeff_blocks[f"{i}-{j}"] = coeff_block
       
        # Reorder coefficient blocks according to new species order
        reordered_blocks = {}
        for pair, block in coeff_blocks.items():
            i, j = map(int, pair.split('-'))
            new_i = speciesMDstruc.index(speciesFile[i])
            new_j = speciesMDstruc.index(speciesFile[j])
            reordered_blocks[f"{new_i}-{new_j}"] = block
        
        # Replace the old blocks with reordered blocks in the file
        start_idx = next(i for i, line in enumerate(lines) if "radial_coeffs" in line) + 1
        while start_idx < len(lines) and lines[start_idx].strip():
            pair_line = lines[start_idx].strip()
            if 'alpha_moments_count' in pair_line:
                break
            i, j = map(int, pair_line.split('-'))
            start_idx += 1
            new_pair = f"{i}-{j}"
            #new_pair = f"{speciesMDstruc.index(speciesFile[i])}-{speciesMDstruc.index(speciesFile[j])}"
            lines[start_idx] = '\t\t ' + new_pair + '\n'
        
            # Insert the reordered coefficient block
            block = reordered_blocks[new_pair]
            for coeff_line in block:
                lines[start_idx] = coeff_line
                start_idx += 1
        
        # Overwrite the original file with reordered data
        with open(potFile, 'w') as file:
            file.writelines(lines)    

    return compositionMDstrucString  # i.e. the new composition


def backupOutputs(direc):

    """
    Backs up MLIP2 files from directory, direc/. This is a precursor to rerunning these jobs (unlike MEAMfit we do not
    set up a continuation job for MLIP2 because it doesn't have that functionality, so it is useful to back up the old
    outputs)
    """

    path = var.dirpath + "/" + direc

    # List all files in the directory
    try:
        files_in_directory = os.listdir(path)
    except OSError as e:
        outf.printt(2, f"    Error accessing directory {path}: {e}")
        return

    # Delete files not in var.DFTinput
    for file in ["output_p.txt", "error_p.txt"]:
        if file in files_in_directory:
            new_name = filesys.create_backup_name(file, path)
            os.rename(os.path.join(path, file), os.path.join(path, new_name))
            outf.printt(2, f"    Renamed {file} to {new_name}")
